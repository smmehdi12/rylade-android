package com.senarios.customer.fragments

import android.content.Intent
import android.location.Location
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.google.gson.reflect.TypeToken
import com.senarios.customer.R
import com.senarios.customer.activities.VendorProductsActivity
import com.senarios.customer.adaptors.AdaptorVendors
import com.senarios.customer.adaptors.RecyclerViewCallback
import com.senarios.customer.costants.Constants
import com.senarios.customer.costants.SPConstants
import com.senarios.customer.databinding.FragmentVendorsBinding
import com.senarios.customer.models.ModelBaseResponse
import com.senarios.customer.models.ModelCommunication
import com.senarios.customer.models.Vendor
import com.senarios.customer.retrofit.APIConstants
import com.senarios.customer.retrofit.ApiResponse
import com.senarios.customer.retrofit.NetworkCall
import com.senarios.customer.utility.Utility
import retrofit2.Response
import java.lang.Exception
import java.lang.reflect.Type

class VendorFragment : BaseFragment() ,ApiResponse,RecyclerViewCallback,Observer<MutableList<Vendor>>{
    private lateinit var binding:FragmentVendorsBinding
    private var categoryID:Int=-1
    private lateinit var adaptor: AdaptorVendors





    override fun getFragmentView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view=inflater.inflate(R.layout.fragment_vendors,container,false)
        binding= DataBindingUtil.bind(view)!!
        init()
        return  view
    }


    private fun init(){
        if (arguments!=null){
            var lat :String? =null;
            var lng :String? =null;
            binding.shimmer.visibility=View.VISIBLE
            categoryID=arguments!!.getInt(Constants.CATEGORY_ID)
            val list= mutableListOf<ModelCommunication>()
            list.add(ModelCommunication(Constants.ID,categoryID.toString()))
            if (Utility.getPreference(requireContext()).contains(SPConstants.ADDRESS_DATA)){
                 lat=Utility.getSelectedAddress(requireContext())!!.lat
                 lng=Utility.getSelectedAddress(requireContext())!!.lon
            }
            else{
                 lat=Utility.getPreferences(requireContext(),Constants.LAT,"") as String
                 lng=Utility.getPreferences(requireContext(),Constants.LNG,"") as String
            }

            if (!lat.isNullOrEmpty() && !lng.isNullOrEmpty()){
                list.add(ModelCommunication("lat",lat))
                list.add(ModelCommunication("long",lng))
            }
            NetworkCall.callAPI(context!!,getBaseService().getVendors(Utility.getJson(list)),this,false,APIConstants.GET_VENDORS)
        }


    }


    override fun OnBackPressed() {

    }

    override fun OnSuccess(endpoint: String, t: Response<ModelBaseResponse>, response: String) {
        try{
            val listType: Type = object : TypeToken<List<Vendor?>?>() {}.getType()
            val list = Utility.getgson().fromJson<MutableList<Vendor>>(response, listType)
            Utility.getDB().deleteVendors(categoryID)
            if (list.isNotEmpty()){
                Utility.getDB().insertVendors(list)
            }
            observeDB()
        }
        catch (e:Exception) {
            OnException(endpoint,e)
        }
    }

    override fun OnStatusfalse(endpoint:String,t: Response<ModelBaseResponse>, response: String, message: String) {
        observeDB()

    }

    override fun OnError(endpoint:String,code: Int, message: String) {
        observeDB()

    }


    override fun OnException(endpoint:String,exception: Throwable) {
        observeDB()

     }

    override fun OnNetworkError(endpoint:String,message: String) {
        observeDB()
    }

    override fun OnClick(position: Int, any: Any) {
        startActivity(Intent(getReference(),VendorProductsActivity::class.java).putExtra(Constants.VENDOR_ID,(any as Vendor).id))
    }


    private fun observeDB(){
        Utility.getDB().getVendor(categoryID).observe(this,this)
    }

    override fun onChanged(t: MutableList<Vendor>?) {
        binding.shimmer.visibility=View.GONE
            if (t!!.isNotEmpty()) {
                adaptor = AdaptorVendors(getReference(), t, this)
                binding.recyclerview.adapter = adaptor
            }
        else{
                binding.lottie.visibility=View.GONE
            }
    }
}