package com.senarios.customer.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.google.gson.reflect.TypeToken
import com.senarios.customer.R
import com.senarios.customer.activities.ProductDetailsActivity
import com.senarios.customer.adaptors.AdaptorVendorProducts
import com.senarios.customer.adaptors.RecyclerViewCallback
import com.senarios.customer.costants.Constants
import com.senarios.customer.databinding.FragmentVendorProductsBinding
import com.senarios.customer.models.*
import com.senarios.customer.retrofit.APIConstants
import com.senarios.customer.retrofit.ApiResponse
import com.senarios.customer.retrofit.NetworkCall
import com.senarios.customer.utility.Utility
import retrofit2.Response
import java.lang.Exception
import java.lang.reflect.Type
import kotlin.math.roundToInt

class VendorProductFragment : BaseFragment(),RecyclerViewCallback,ApiResponse,Observer<MutableList<ProductModel>> {
    private lateinit var binding:FragmentVendorProductsBinding
    private lateinit var categoryModel:VendorCategoryModel
    private lateinit var adaptor:AdaptorVendorProducts


    private fun init(){
        if (arguments!=null){
            binding.shimmer.visibility=View.VISIBLE
            categoryModel=arguments!!.getParcelable(Constants.CATEGORY_DATA)!!
            val list= mutableListOf<ModelCommunication>()
            list.add(ModelCommunication(Constants.CATEGORY_ID,categoryModel.id.toString()))
            NetworkCall.callAPI(context!!,getBaseService().getVendorProducts(Utility.getJson(list)),this,false, APIConstants.GET_VENDOR_PRODUCTS)
        }


    }
    override fun getFragmentView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view=inflater.inflate(R.layout.fragment_vendor_products,container,false)
        binding= DataBindingUtil.bind(view)!!
        init()
        return  view
    }

    override fun OnBackPressed() {

    }
    override fun OnSuccess(endpoint: String, t: Response<ModelBaseResponse>, response: String) {
        try{
            binding.shimmer.visibility=View.GONE
            val listType: Type = object : TypeToken<List<ProductModel?>?>() {}.getType()
            val list = Utility.getgson().fromJson<MutableList<ProductModel>>(response, listType)
            Utility.getDB().deleteProducts(categoryModel.id!!,categoryModel.vendorId!!)
            if (list.isNotEmpty()){
                Utility.getDB().insertVendorProducts(list)
            }
            observeDB()

        }
        catch (e: Exception) {
            OnException(endpoint,e)
        }
    }

    override fun OnStatusfalse(endpoint:String, t: Response<ModelBaseResponse>, response: String, message: String) {
        observeDB()
    }

    override fun OnError(endpoint:String,code: Int, message: String) {
        observeDB()
    }


    override fun OnException(endpoint:String,exception: Throwable) {
        observeDB()
    }

    override fun OnNetworkError(endpoint:String,message: String) {
        observeDB()
     }

    override fun OnClick(position: Int, any: Any) {
        startActivity(Intent(getReference(),ProductDetailsActivity::class.java).putExtra(Constants.PRODUCT_DATA,any as ProductModel))
    }

    private fun observeDB(){
        Utility.getDB().getProducts(categoryModel.id!!,categoryModel.vendorId!!).observe(this,this)
    }

    override fun onChanged(t: MutableList<ProductModel>?) {
        binding.shimmer.visibility=View.GONE
        if (t!=null && t.size>0){
            getDAO().updateCart(t)
            adaptor= AdaptorVendorProducts(getReference(),t,this)
            binding.recyclerview.adapter=adaptor
        }
        else{
            binding.lottie.visibility=View.GONE
        }
    }

    private fun getDiscountAmount(price : String, percent : String):Int{
        return price.toInt()-percent.toDouble().roundToInt()
    }
}