package com.senarios.customer.fragments


import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelStoreOwner
import com.senarios.customer.Rylade

import com.senarios.customer.callbacks.ActivityFragment
import com.senarios.customer.callbacks.ActivityStates
import com.senarios.customer.callbacks.FragmentChanger
import com.senarios.customer.db.DAO
import com.senarios.customer.di.Component
import com.senarios.customer.retrofit.APIConstants
import com.senarios.customer.retrofit.DataService
import com.senarios.customer.retrofit.Retrofit
import com.senarios.customer.utility.Utility
import com.senarios.customer.viewmodel.SharedVM

/**
 * A simple [Fragment] subclass.
 */
abstract class BaseFragment : Fragment(),ActivityStates,Utility {
    private lateinit var activityFragment : ActivityFragment
    private lateinit var fragmentChanger : FragmentChanger
    private lateinit var sharedVM: SharedVM;
    private var reference : Context? = null
    private lateinit var component: Component




    override fun onAttach(context: Context) {
        super.onAttach(context)
        activityFragment = context as ActivityFragment
        fragmentChanger=context as FragmentChanger
        activityFragment.get(this)
        reference=context
        sharedVM= ViewModelProvider(activity as ViewModelStoreOwner).get(SharedVM::class.java)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
       /* val activity:MainActivity=getActivity() as MainActivity
        activity.getbinding().drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);*/
        component=(activity!!.application as Rylade).component
        return getFragmentView(inflater, container, savedInstanceState)
    }


    public fun getDAO(): DAO {
        return component.getDAO()
    }




    abstract fun getFragmentView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?

    fun showToast(context: Context?,message:String){
        Toast.makeText(context,message, Toast.LENGTH_LONG).show()
    }

    fun getFragmentChanger():FragmentChanger{
        return fragmentChanger
    }

    fun getViewModel():SharedVM{
        return sharedVM
    }

    fun getBaseService(): DataService {
        return  Retrofit.getinstance(APIConstants.BASE_URL).getService()
    }

    open fun hideSoftKeyboard() {
        if (activity?.getCurrentFocus() != null) {
            val inputMethodManager = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
            inputMethodManager?.hideSoftInputFromWindow(activity!!.getCurrentFocus()!!.getWindowToken(), 0
            )
        }
    }

    open fun showSoftKeyboard() {
        val imm =
            context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
        imm?.toggleSoftInput(
            InputMethodManager.SHOW_FORCED,
            0
        )
    }

    fun getReference():Context{
        return reference!!
    }

    fun showDialog(dialogFragment: DialogFragment){
        dialogFragment.show(fragmentManager!!,"")
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            0 -> {
                if (ContextCompat.checkSelfPermission(
                        this.context!!,
                        Manifest.permission.RECEIVE_SMS
                    ) != PackageManager.PERMISSION_GRANTED
                ) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this.activity!!,Manifest.permission.RECEIVE_SMS)){
                        showPermissionDialog(
                            getString(com.senarios.customer.R.string.permission_title),
                            getString(com.senarios.customer.R.string.sms_permission),
                            arrayOf(Manifest.permission.RECEIVE_SMS),
                            false
                        )
                    }
                    else{
                        showPermissionDialog(
                            getString(com.senarios.customer.R.string.permission_title),
                            getString(com.senarios.customer.R.string.sms_permission),
                            arrayOf(Manifest.permission.RECEIVE_SMS),
                            true
                        )
                    }

                } else if (ContextCompat.checkSelfPermission(
                        this.context!!,
                        Manifest.permission.ACCESS_FINE_LOCATION
                    ) != PackageManager.PERMISSION_GRANTED
                ) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this.activity!!,Manifest.permission.ACCESS_FINE_LOCATION)) {
                        showPermissionDialog(
                            getString(com.senarios.customer.R.string.permission_title),
                            getString(com.senarios.customer.R.string.location_permission),
                            arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                            false
                        )
                    }
                    else{
                        showPermissionDialog(
                            getString(com.senarios.customer.R.string.permission_title),
                            getString(com.senarios.customer.R.string.location_permission),
                            arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                            true
                        )
                    }
                } else if (ContextCompat.checkSelfPermission(
                        this.context!!,
                        Manifest.permission.READ_EXTERNAL_STORAGE
                    ) != PackageManager.PERMISSION_GRANTED
                ) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this.activity!!,Manifest.permission.READ_EXTERNAL_STORAGE)) {
                        showPermissionDialog(
                            getString(com.senarios.customer.R.string.permission_title),
                            getString(com.senarios.customer.R.string.media_permission),
                            arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                            false
                        )
                    }
                    else{
                        showPermissionDialog(
                            getString(com.senarios.customer.R.string.permission_title),
                            getString(com.senarios.customer.R.string.media_permission),
                            arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                            true
                        )
                    }
                } else if (ContextCompat.checkSelfPermission(
                        this.context!!,
                        Manifest.permission.CAMERA
                    ) != PackageManager.PERMISSION_GRANTED
                ) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this.activity!!,Manifest.permission.CAMERA)) {
                        showPermissionDialog(
                            getString(com.senarios.customer.R.string.permission_title),
                            getString(com.senarios.customer.R.string.camera_permission),
                            arrayOf(Manifest.permission.CAMERA),
                            false
                        )
                    }
                    else{
                        showPermissionDialog(
                            getString(com.senarios.customer.R.string.permission_title),
                            getString(com.senarios.customer.R.string.camera_permission),
                            arrayOf(Manifest.permission.CAMERA),
                            true
                        )
                    }
                }
            }
            1 -> {
                if (ActivityCompat.shouldShowRequestPermissionRationale(this.activity!!,Manifest.permission.RECEIVE_SMS)){
                    showPermissionDialog(
                        getString(com.senarios.customer.R.string.permission_title),
                        getString(com.senarios.customer.R.string.sms_permission),
                        arrayOf(Manifest.permission.RECEIVE_SMS),
                        false
                    )
                }
                else{
                    showPermissionDialog(
                        getString(com.senarios.customer.R.string.permission_title),
                        getString(com.senarios.customer.R.string.sms_permission),
                        arrayOf(Manifest.permission.RECEIVE_SMS),
                        true
                    )
                }
            }

            2 -> {
                if (ContextCompat.checkSelfPermission(
                        this.context!!,
                        Manifest.permission.ACCESS_FINE_LOCATION
                    ) != PackageManager.PERMISSION_GRANTED
                )  if (ActivityCompat.shouldShowRequestPermissionRationale(this.activity!!,Manifest.permission.ACCESS_FINE_LOCATION)) {
                    showPermissionDialog(
                        getString(com.senarios.customer.R.string.permission_title),
                        getString(com.senarios.customer.R.string.location_permission),
                        arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                        false
                    )
                }
                else{
                    showPermissionDialog(
                        getString(com.senarios.customer.R.string.permission_title),
                        getString(com.senarios.customer.R.string.location_permission),
                        arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                        true
                    )
                }
            }

            3-> {
                if (ContextCompat.checkSelfPermission(
                        this.context!!,
                        Manifest.permission.READ_EXTERNAL_STORAGE
                    ) != PackageManager.PERMISSION_GRANTED
                ) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this.activity!!,Manifest.permission.READ_EXTERNAL_STORAGE)) {
                        showPermissionDialog(
                            getString(com.senarios.customer.R.string.permission_title),
                            getString(com.senarios.customer.R.string.media_permission),
                            arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                            false
                        )
                    }
                    else{
                        showPermissionDialog(
                            getString(com.senarios.customer.R.string.permission_title),
                            getString(com.senarios.customer.R.string.media_permission),
                            arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                            true
                        )
                    }
                }
            }

            4-> {
                if (ContextCompat.checkSelfPermission(
                        this.context!!,
                        Manifest.permission.CAMERA
                    ) != PackageManager.PERMISSION_GRANTED
                ) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this.activity!!,Manifest.permission.CAMERA)) {
                        showPermissionDialog(
                            getString(com.senarios.customer.R.string.permission_title),
                            getString(com.senarios.customer.R.string.camera_permission),
                            arrayOf(Manifest.permission.CAMERA),
                            false
                        )
                    }
                    else{
                        showPermissionDialog(
                            getString(com.senarios.customer.R.string.permission_title),
                            getString(com.senarios.customer.R.string.camera_permission),
                            arrayOf(Manifest.permission.CAMERA),
                            true
                        )
                    }
                }
            }
            5-> {
                if (ContextCompat.checkSelfPermission(this.context!!, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this.activity!!,Manifest.permission.READ_EXTERNAL_STORAGE)|| ActivityCompat.shouldShowRequestPermissionRationale(this.activity!!,Manifest.permission.CAMERA)) {
                        showPermissionDialog(
                            getString(com.senarios.customer.R.string.permission_title),
                            getString(com.senarios.customer.R.string.camera_permission),
                            arrayOf(
                                Manifest.permission.CAMERA,
                                Manifest.permission.READ_EXTERNAL_STORAGE
                            ),
                            false
                        )
                    }
                    else{
                        showPermissionDialog(
                            getString(com.senarios.customer.R.string.permission_title),
                            getString(com.senarios.customer.R.string.camera_permission),
                            arrayOf(
                                Manifest.permission.CAMERA,
                                Manifest.permission.READ_EXTERNAL_STORAGE
                            ),
                            true
                        )
                    }
                }
            }
        }

    }


    fun showPermissionDialog(title:String,message:String,permission:Array<String>,isSetting:Boolean){
        val builder= Utility.getAlertDialoge(this.context!!,title,message)
        if (isSetting){
            builder.setPositiveButton("Go to Setting") { p0, p1 ->
                Utility.goToSettings(this.context!!)
            }
        }
        else {
            builder.setPositiveButton("Alright") { p0, p1 ->
                ActivityCompat.requestPermissions(
                    this.activity!!,
                    permission,
                    1
                );
            }
        }
        builder  .setNegativeButton("Nah, I'm good"
        ) { p0, p1 -> p0?.dismiss() }
        builder  .show()
    }

    open fun hasPermissions(context: Context?, permissions: Array<String>
    ): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (permission in permissions) {
                if (context.checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                    return false

                }

            }
        }
        return true
    }

    fun checkMYPermission() {
        if(!hasPermissions(this.context!!, Utility.getPermissions())){
            ActivityCompat.requestPermissions(this.activity!!, Utility.getPermissions(),0)
        }
    }
    fun checkSMSPermission() {
        if(!hasPermissions(this.context!!, arrayOf(Manifest.permission.RECEIVE_SMS))){
            ActivityCompat.requestPermissions(this.activity!!,arrayOf(Manifest.permission.RECEIVE_SMS) ,1)
        }
    }
    fun checkLocationPermission() {
        if(!hasPermissions(this.context!!, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION))){
            ActivityCompat.requestPermissions(this.activity!!,arrayOf(Manifest.permission.ACCESS_FINE_LOCATION) ,2)
        }
    }
    fun checkStoragePermission() {
        if(!hasPermissions(this.context!!, arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE))){
            ActivityCompat.requestPermissions(this.activity!!,arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE) ,3)
        }
    }
    fun checkCameraPermission() {
        if(!hasPermissions(this.context!!, arrayOf(Manifest.permission.CAMERA))){
            ActivityCompat.requestPermissions(this.activity!!,arrayOf(Manifest.permission.CAMERA) ,4)
        }
    }
    fun checkMediaPermission() {
        if(!hasPermissions(this.context!!, arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.CAMERA))){
            ActivityCompat.requestPermissions(this.activity!!,arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.CAMERA) ,5)
        }
    }
}
