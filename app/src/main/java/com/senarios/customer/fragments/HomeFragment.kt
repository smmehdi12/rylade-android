package com.senarios.customer.fragments


import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import com.google.gson.reflect.TypeToken
import com.senarios.customer.R
import com.senarios.customer.activities.*
import com.senarios.customer.adaptors.AdaptorModules
import com.senarios.customer.adaptors.RecyclerViewCallback
import com.senarios.customer.costants.Constants
import com.senarios.customer.databinding.FragmentHomeBinding
import com.senarios.customer.models.INIModel
import com.senarios.customer.models.MainCategoryModel
import com.senarios.customer.models.ModelBaseResponse
import com.senarios.customer.models.Modules
import com.senarios.customer.retrofit.ApiResponse
import com.senarios.customer.utility.Utility
import retrofit2.Response

/**
 * A simple [Fragment] subclass.
 */
class HomeFragment : BaseFragment(),View.OnClickListener ,ApiResponse , RecyclerViewCallback, Observer<MutableList<MainCategoryModel>> {
    private lateinit var binding:FragmentHomeBinding



    override fun getFragmentView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
      val view=inflater.inflate(R.layout.fragment_home,container,false)
       binding=DataBindingUtil.bind(view)!!
        init()
        return  view
    }

     fun init() {

        binding.medicineLayout.setOnClickListener(this)
        binding.rideLayout.setOnClickListener(this)
        binding.ecommerceLayout.setOnClickListener(this)
        binding.deliveryLayout.setOnClickListener(this)
         observeDB()

    }

    private fun observeDB(){
        Utility.getDB().getMainCategories().observe(this,this)
    }

    override fun OnBackPressed() {
        if (activity != null) {
            activity!!.finish()
        }
    }

    /*
    * OnClick for previous views which are disabled
    * */
    override fun onClick(v: View?) {
        when(v?.id){
            binding.deliveryLayout.id->{
                startActivity(Intent(activity, OrderParcelActivity::class.java))
            }
            binding.ecommerceLayout.id->{
                startActivity(Intent(context!!,EcommerceActivity::class.java))
            }
            binding.rideLayout.id->{
                startActivity(Intent(context!!,RequestRideActivity::class.java))
            }
            binding.medicineLayout.id->{
                startActivity(Intent(context!!,OrderMedicineActivity::class.java))
            }

        }

    }

    /*
    * api call
    * link #ApiResponse
    *  */
    override fun OnSuccess(endpoint: String, t: Response<ModelBaseResponse>, response: String) {
        try{

            val itemType = object : TypeToken<MutableList<INIModel>>() {}.type
            val list=Utility.getgson().fromJson<MutableList<INIModel>>(response,itemType)
            Utility.setPreferences(getReference(),Constants.INSTANT,Utility.getgson().toJson(list[0]))
        }
        catch (e:Exception){

        }
    }

    override fun OnStatusfalse(endpoint: String, t: Response<ModelBaseResponse>, response: String, message: String) {

    }

    override fun OnError(endpoint: String, code: Int, message: String) {

    }

    override fun OnException(endpoint: String, exception: Throwable) {

    }

    override fun OnNetworkError(endpoint: String, message: String) {

    }

    private fun getModulesList(t: MutableList<MainCategoryModel>):MutableList<Modules>{
        val list= mutableListOf<Modules>()
        val model=INIModel.getModelFromSP(requireContext())
        if (model!!.medicine_module==1) {
            list.add(Modules(Constants.MODULE_MED, R.drawable.icons, true, null))
        }
        if (model.rider_module==1) {
            list.add(Modules(Constants.MODULE_RIDE, R.drawable.icons3, true, null))
        }
        for (item in t){
            list.add(Modules(item.name!!,R.drawable.icons1,false,item))
        }
        if (model.parcel_module==1) {
            list.add(Modules(Constants.MODULE_DEL, R.drawable.icons2, true, null))
        }
//        list.add(Modules(Constants.MODULE_FAV_PRODUCT,R.drawable.ic_heart_filled, true, null))
        return list
    }

    override fun onChanged(t: MutableList<MainCategoryModel>?) {
        binding.recyclerView.layoutManager=GridLayoutManager(requireContext(),2)
        binding.recyclerView.adapter=AdaptorModules(requireContext(),getModulesList(t!!),this)

    }

    override fun OnClick(position: Int, any: Any) {
        if ((any as Modules).isOffline) {
            when (any.title) {
                Constants.MODULE_MED -> {
                    startActivity(Intent(requireContext(), OrderMedicineActivity::class.java))
                }
                Constants.MODULE_RIDE -> {
                    startActivity(Intent(requireContext(), RequestRideActivity::class.java))
                }
                Constants.MODULE_DEL -> {
                    startActivity(Intent(requireContext(), OrderParcelActivity::class.java))
                }
                Constants.MODULE_FAV_PRODUCT -> {
                    startActivity(Intent(requireContext(),FavouriteProductsActivity::class.java))
                }

            }
        }
        else{
            startActivity(Intent(requireContext(),EcommerceActivity::class.java).putExtra(Constants.MAIN_CATEGORY, any.mainCategoryModel!!.id!!))
        }

    }


}
