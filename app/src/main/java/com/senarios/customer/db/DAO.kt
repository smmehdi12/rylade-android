package com.senarios.customer.db

import androidx.lifecycle.LiveData
import androidx.room.*
import com.senarios.customer.models.*

@Dao
abstract class DAO {
    //insert functions
    @Insert(onConflict = OnConflictStrategy.REPLACE)
   abstract fun insertMainCategories(list:MutableList<MainCategoryModel>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertVendors(list:MutableList<Vendor>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertVendor(model:Vendor)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertVendorCategories(list:MutableList<VendorCategoryModel>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertVendorProducts(list:MutableList<ProductModel>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract  fun inserProduct(product:ProductModel)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertCart(model:CartModel):Long

    @Query("select * from MainCategory")
    abstract fun getMainCategories():LiveData<MutableList<MainCategoryModel>>

    @Query("select * from Vendor where category_id=:categoryid AND user_status==1")
    abstract fun getVendor(categoryid:Int):LiveData<MutableList<Vendor>>

    @Query("select is_delivery from Vendor where id=:vendor_id")
    abstract  fun getVendorDeliveryStatus(vendor_id:Int):LiveData<Int>

    @Query("select * from VendorCategory where vendor_id=:vendor_id")
    abstract fun getVendorCategory(vendor_id:Int):LiveData<MutableList<VendorCategoryModel>>

    @Query("select * from Products where category_id=:category_id AND vendor_id=:vendor_id AND status==1")
    abstract  fun getProducts(category_id:Int,vendor_id:Int):LiveData<MutableList<ProductModel>>

    @Query("select * from Cart")
    abstract fun getCart():LiveData<MutableList<CartModel>>



 @Query("select * from Cart")
 abstract fun getCartItems():MutableList<CartModel>

    @Query("select * from products where lower(product_name) LIKE lower(:product_name)")
    abstract  fun searchProducts(product_name:String):MutableList<ProductModel>

    @Query("select * from products")
    abstract fun getAllProducts():MutableList<ProductModel>

    @Query("select * from products where id =:id")
    abstract  fun checkProductExist(id:Int):MutableList<ProductModel>

    @Query("delete from MainCategory")
    abstract fun clearMainCategoryTable()

    @Query("delete from Vendor")
    abstract fun clearVendorTable()

    @Query("delete from VendorCategory")
    abstract fun clearVendorCategoryTable()

    @Query("delete from Products")
    abstract  fun clearProductsTable()

    @Query("delete from Cart")
    abstract fun clearCartTable()

    @Query("update cart set quantity=:quantity where id=:id")
    abstract fun updateQuantity(id:Long,quantity:Int)

    @Query("delete from Cart where id=:id")
    abstract  fun deleteCartItem(id:Long)



    @Query("SELECT vendor_id FROM Cart ORDER BY id ASC LIMIT 1")
    abstract fun getVendorIDFromCart():Long

    @Query("delete from VendorCategory where vendor_id=:vendor_id")
    abstract  fun deleteVendorCategory(vendor_id:Int)

    @Query("delete from Products where category_id=:category_id AND vendor_id=:vendor_id")
    abstract fun deleteProducts(category_id:Int,vendor_id:Int)

    @Query("delete from Vendor where category_id=:category_id")
    abstract  fun deleteVendors(category_id:Int)

    @Query("select * from vendor where id=:vendor_id")
    abstract fun getVendorByID(vendor_id:Int):MutableList<Vendor>

    @Query("Update Cart set isOutStock=:isOutStock where id=:id")
    abstract fun updateOutOfOrder(id:Long,isOutStock:Boolean)



    fun updateCart(products:MutableList<ProductModel>){
  val cartItems=getCartItems()
  for (productModel in products){
   for (cart in cartItems){
    if (cart.product_id==productModel.id){
     if (productModel.iz_discount==1){
     val cartModel=CartModel(null,productModel.categoryId,productModel.categoryName,productModel.createdAt,productModel.description,
       productModel.id,productModel.image,productModel.fixed_discount,productModel.productName,productModel.promoCodeId,productModel.quantityAvailable,
       productModel.status,productModel.subCategoryName,productModel.updatedAt,productModel.weight,
       productModel.vendorId,null,cart.quantity,0,productModel.sales_tax,productModel.fixed_discount,productModel.iz_discount,productModel.price,-1,false)
         deleteCartItem(cart.id!!)
         insertCart(cartModel)

     }
     else{
     val cartModel=CartModel(null,productModel.categoryId,productModel.categoryName,productModel.createdAt,productModel.description,
       productModel.id,productModel.image,productModel.price,productModel.productName,productModel.promoCodeId,productModel.quantityAvailable,
       productModel.status,productModel.subCategoryName,productModel.updatedAt,productModel.weight,
       productModel.vendorId,null,1,0,productModel.sales_tax,productModel.fixed_discount,productModel.iz_discount,productModel.price,-1,false)
         deleteCartItem(cart.id!!)
         insertCart(cartModel)
     }
    }
   }

  }

 }



}