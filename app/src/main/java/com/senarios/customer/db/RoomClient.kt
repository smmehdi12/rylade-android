package com.senarios.customer.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.senarios.customer.costants.SPConstants
import com.senarios.customer.models.*

@Database(entities = arrayOf(MainCategoryModel::class,Vendor::class,VendorCategoryModel::class,ProductModel::class,CartModel::class),version = RoomClient.DATABASE_VERSION,exportSchema = false)
abstract class RoomClient ():RoomDatabase() {

    companion object {
        private var mInstance : RoomClient ? = null
        const val DATABASE_VERSION = 22
        @Synchronized
        fun getInstance(context:Context): RoomClient {
            if (mInstance == null) {
                mInstance = Room.databaseBuilder(context, RoomClient::class.java, SPConstants.SP).allowMainThreadQueries().fallbackToDestructiveMigration().build()
            }
            return mInstance!!
        }
    }
    abstract fun dao(): DAO



}