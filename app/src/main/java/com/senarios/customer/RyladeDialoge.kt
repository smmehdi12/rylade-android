package com.senarios.customer

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.rylade_loading_dialoge.*

class RyladeDialoge(context: Context) : Dialog(context) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.rylade_loading_dialoge)
        Glide.with(context).load(R.drawable.loading).into(rylade_logo)
        window?.setBackgroundDrawableResource(android.R.color.transparent)
        setCancelable(false)


    }
}