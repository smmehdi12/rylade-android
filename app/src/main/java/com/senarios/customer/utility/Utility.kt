package com.senarios.customer.utility

import android.Manifest
import android.app.*
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Canvas
import android.location.Address
import android.location.Geocoder
import android.location.LocationManager
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.net.Uri
import android.net.wifi.WifiManager
import android.os.Build.VERSION
import android.os.Build.VERSION_CODES
import android.provider.Settings
import android.util.Log
import android.widget.EditText
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.github.tntkhang.fullscreenimageview.library.FullScreenImageViewActivity
import com.google.android.gms.maps.model.BitmapDescriptor
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.AutocompleteSessionToken
import com.google.android.libraries.places.api.net.PlacesClient
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.senarios.customer.R
import com.senarios.customer.Rylade
import com.senarios.customer.costants.Constants
import com.senarios.customer.costants.Messages
import com.senarios.customer.costants.SPConstants
import com.senarios.customer.db.DAO
import com.senarios.customer.models.CartModel
import com.senarios.customer.models.ModelCommunication
import com.senarios.customer.models.ProductModel
import com.senarios.customer.models.User
import com.senarios.customer.retrofit.APIConstants
import com.senarios.customer.retrofit.DataService
import com.senarios.customer.retrofit.Retrofit
import com.senarios.customer.viewmodel.SharedVM
import com.stepstone.apprating.AppRatingDialog
import com.tapadoo.alerter.Alerter
import io.karn.notify.Notify
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import net.alhazmy13.mediapicker.Image.ImagePicker
import org.greenrobot.eventbus.EventBus
import java.io.File
import java.util.*


/*custom util class which has static functions for helping in certain tasks */
interface Utility {

    @Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
    companion object {
        private lateinit var sessionInstance: AutocompleteSessionToken
        private const val TAG = "UTL"
        fun showToast(context: Context?, message: String) {
            Toast.makeText(context, message, Toast.LENGTH_LONG).show()
        }

        fun getAlertDialoge(context: Context?, title: String?, message: String?): MaterialAlertDialogBuilder {
            val alertDialog = MaterialAlertDialogBuilder(context!!, R.style.Theme_MaterialComponents_Light_Dialog_Alert)
            alertDialog.setTitle(title)
            alertDialog.setMessage(message)
            alertDialog.setCancelable(false)
            return alertDialog
        }

        fun validateET(editText: EditText): Boolean {
            return editText.text.toString().trim().isEmpty()
        }

        fun setErrorET(editText: EditText) {
            editText.error = Messages.EMPTY_FIELD
            editText.requestFocus()
        }

        fun setRErrorET(editText: EditText, message: String) {
            editText.error = message
            editText.requestFocus()
        }

        fun getTextET(editText: EditText?): String {
            return editText?.text.toString().trim()
        }


        fun isNetworkConnected(context: Context): Boolean {
            var result = false
            val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            if (VERSION.SDK_INT >= VERSION_CODES.M) {
                if (cm != null) {
                    val capabilities = cm.getNetworkCapabilities(cm.activeNetwork)
                    if (capabilities != null) {
                        if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                            result = true
                        } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
                            result = true
                        }
                    }
                }
            } else {
                if (cm != null) {
                    val activeNetwork = cm.activeNetworkInfo
                    if (activeNetwork != null) { // connected to the internet
                        if (activeNetwork.type == ConnectivityManager.TYPE_WIFI) {
                            result = true
                        } else if (activeNetwork.type == ConnectivityManager.TYPE_MOBILE) {
                            result = true
                        }
                    }
                }
            }
            return result
        }

        fun setErrorOnET(editText: EditText, message: String) {
            editText.error = message
            editText.requestFocus()
        }

        fun getJson(list: List<ModelCommunication>): JsonObject {
            val json = JsonObject()
            for (value in list) {
               if (value.isJsonArray){
                   json.add(value.key, value.array)
                   continue
               }
                json.addProperty(value.key, value.value)

            }
            Log.v("json", json.toString())
            return json
        }

        fun getJsonWithInt(list: List<ModelCommunication>): JsonObject {
            val json = JsonObject()
            for (value in list) {
                json.addProperty(value.key, value.value)
            }
            Log.v("json", json.toString())
            return json
        }

        fun showErrorDialog(context: Context?, layour_ID: Int, title: String?, message: String): androidx.appcompat.app.AlertDialog {
            val errordialoge = getAlertDialoge(context!!, title, message)
            errordialoge.setView(layour_ID)
            errordialoge.setPositiveButton("Ok") { p0, p1 -> p0?.dismiss() }
            return errordialoge.show()

        }

        fun showNErrorDialog(context: Context?): androidx.appcompat.app.AlertDialog {
            val networkDialoge = getAlertDialoge(context!!, Messages.NETWORK_ERROR_TITLE, Messages.NETWORK_ERROR)
            networkDialoge.setView(R.layout.dialog_network_error)
            networkDialoge.setPositiveButton("Ok") { p0, p1 -> p0?.dismiss() }.setNegativeButton("GO to settings") { p0, p1 -> context.startActivity(Intent(WifiManager.ACTION_PICK_WIFI_NETWORK)) }
            return networkDialoge.show()
        }

        fun showErrorDialogBuilder(context: Context?, layour_ID: Int, title: String?, message: String): androidx.appcompat.app.AlertDialog.Builder {
            val errordialoge = getAlertDialoge(context!!, title, message)
            errordialoge.setView(layour_ID)
            errordialoge.setPositiveButton("Ok") { p0, p1 -> p0?.dismiss() }
            return errordialoge

        }

        fun showNErrorDialogwithFinish(context: Context?): androidx.appcompat.app.AlertDialog {
            val networkDialoge = getAlertDialoge(context!!, Messages.NETWORK_ERROR_TITLE, Messages.NETWORK_ERROR)
            networkDialoge.setView(R.layout.dialog_network_error)
            networkDialoge.setPositiveButton("Ok") { p0, p1 ->
                p0?.dismiss()
                (context as Activity).finish()
            }.setNegativeButton("GO to settings") { p0, p1 -> context.startActivity(Intent(WifiManager.ACTION_PICK_WIFI_NETWORK)) }
            return networkDialoge.show()
        }

        fun setPreferences(context: Context?, key: String?, `object`: Any?) {
            val preference = context?.getSharedPreferences(SPConstants.SP, SPConstants.MODE)
            if (preference != null) {
                if (`object` is String) {
                    preference.edit().putString(key, `object` as String?).apply()
                } else if (`object` is Int) {
                    preference.edit().putInt(key, `object`).apply()
                } else if (`object` is Boolean) {
                    preference.edit().putBoolean(key, `object`).apply()
                }
            }
        }

        fun getPreferences(context: Context?, key: String?, `object`: Any?): Any? {
            val preference = context?.getSharedPreferences(SPConstants.SP, SPConstants.MODE)
            if (preference != null) {
                if (`object` is String) {
                    return preference.getString(key, "")
                } else if (`object` is Int) {
                    return preference.getInt(key, -1)
                } else if (`object` is Boolean) {
                    return preference.getBoolean(key, false)
                }
            }
            return ""
        }

        fun getgson(): Gson {
            return Gson().newBuilder().setLenient().setPrettyPrinting().serializeNulls().create()
        }

        fun getUserToken(sharedVM: SharedVM): String {
            return APIConstants.BEARER + sharedVM.getLoggedUser().value!!.token
        }

        fun getNotloggedToken(sharedVM: SharedVM): String {
            return APIConstants.BEARER + sharedVM.getUser().value!!.token
        }

        fun removeEvents() {
            val event = EventBus.getDefault().getStickyEvent(ModelCommunication::class.java)
            if (event != null) {
                EventBus.getDefault().removeStickyEvent(event)
            }
        }

        fun removeStringEvents() {
            val event = EventBus.getDefault().getStickyEvent(String::class.java)
            if (event != null) {
                EventBus.getDefault().removeStickyEvent(event)
            }
        }

        fun getMediaBuilder(activity: Activity): ImagePicker.Builder {
            return ImagePicker.Builder(activity).mode(ImagePicker.Mode.CAMERA_AND_GALLERY).compressLevel(ImagePicker.ComperesLevel.HARD).scale(800, 800).directory(ImagePicker.Directory.DEFAULT).allowMultipleImages(false).enableDebuggingMode(true)
        }

        fun getPermissions(): Array<String> {
            return arrayOf(Manifest.permission.RECEIVE_SMS, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA, Manifest.permission.ACCESS_FINE_LOCATION)
        }


        fun hasPermissions(context: Context?, permissions: Array<String>): Boolean {
            if (VERSION.SDK_INT >= VERSION_CODES.M && context != null && permissions != null) {
                for (permission in permissions) {
                    if (context.checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                        return false

                    }

                }
            }
            return true
        }

        fun goToSettings(context: Context?) {
            val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            val uri: Uri = Uri.fromParts("package", context!!.getPackageName(), null)
            intent.data = uri
            context.startActivity(intent)
        }

        fun getPreference(context: Context?): SharedPreferences {
            return context!!.getSharedPreferences(SPConstants.SP, SPConstants.MODE)
        }

        fun checkIfUserLogged(context: Context?): Boolean {
            val user = getgson().fromJson<User>(getPreferences(context, SPConstants.USER, "") as String, User::class.java)
            return user != null
        }

        fun getService(): DataService {
            return Retrofit.getinstance(APIConstants.BASE_URL).getService()
        }

        fun hasLocationPermission(context: Context?): Boolean {
            return hasPermissions(context, arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION))
        }

        fun hasMediaPermission(context: Context?): Boolean {
            return hasPermissions(context, arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA))
        }

        fun hasSmSPermission(context: Context?): Boolean {
            return hasPermissions(context, arrayOf(Manifest.permission.RECEIVE_SMS))
        }

        fun hasContactPermission(context: Context?): Boolean {
            return hasPermissions(context, arrayOf(Manifest.permission.READ_CONTACTS))
        }

        fun checkLocationServices(context: Context?): Boolean {
            val locationManager = context!!.getSystemService(Context.LOCATION_SERVICE) as LocationManager
            return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
        }

        fun getUser(context: Context?): User {
            return getgson().fromJson(getPreference(context).getString(SPConstants.USER, ""), User::class.java)
        }

         fun showFirebaseNotification(context: Context?,tl: String, tex: String,intent:Intent?) {
            try {
                val manager = context!!.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                if (VERSION.SDK_INT >= VERSION_CODES.O) {
                    val channel = NotificationChannel("0", "Rylade", NotificationManager.IMPORTANCE_HIGH)
                    manager.createNotificationChannel(channel)
                }
                val builder = Notify.with(context).asBuilder().setContentTitle(tl).setContentText(tex)
                if (intent!=null){
                    val pending= PendingIntent.getActivity(context,0,intent, PendingIntent.FLAG_ONE_SHOT)
                    builder.setContentIntent(pending)
                }
                manager.notify(0, builder.build())
            }
            catch (e: java.lang.Exception){
                Log.v(TAG,""+e.message)
            }


        }

        fun showNotification(context: Context?, tl: String, tex: String) {
            Notify.with(context!!).content {
                title = tl
                text = tex
            }
                .actions {  }
                .show()
        }

        fun getPlacesSession(): AutocompleteSessionToken {
            if (!::sessionInstance.isInitialized) {
                sessionInstance = AutocompleteSessionToken.newInstance()
            }
            return sessionInstance
        }

        fun getPlacesClient(context: Context?): PlacesClient {
            return Places.createClient(context!!)
        }

        fun getLatLng(context: Context?, text: String): LatLng? {
            val coder = Geocoder(context)
            var latLng: LatLng? = null
            latLng = try {
                val addresses = coder.getFromLocationName(text, 5)
                LatLng(addresses[0].latitude, addresses[0].longitude)
            } catch (e: Exception) {
                return null
            }
            return latLng
        }

        fun getAddress(context: Context?, latLng: LatLng): String? {
            val coder = Geocoder(context)
            try {
                val addresses = coder.getFromLocation(latLng.latitude, latLng.longitude, 5)
                return addresses[0].getAddressLine(0)
            } catch (e: Exception) {
                return null
            }
        }

        fun showDatePickerDialog(context: Context, datepickercallback: DatePickerDialog.OnDateSetListener) {
            val c = Calendar.getInstance()
            val year = c.get(Calendar.YEAR)
            val month = c.get(Calendar.MONTH)
            val datOFMonth = c.get(Calendar.DAY_OF_MONTH)
            DatePickerDialog(context, android.R.style.Widget_Material_Light_DatePicker, datepickercallback, year, month, datOFMonth).show()
        }

        fun Time_Picker_Dialog(context: Context, timePickerDialog: TimePickerDialog.OnTimeSetListener) {
            val cal = Calendar.getInstance()
            TimePickerDialog(context, timePickerDialog, cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), true).show()

        }

        fun showOrderNotification(context: Context) {
            showNotification(context, context.getString(R.string.order_notification_title), context.getString(R.string.order_notification_message))
        }

        ///////
        fun showParcelOrder(context: Context) {
            showNotification(context, context.getString(R.string.parcel_notification_title), context.getString(R.string.order_notification_message))
        }

        fun getGoogleService(): DataService {
            return Retrofit.getinstance(APIConstants.BASE_GOOGLE).getService()
        }


        fun bitmapDescriptorFromVector(context: Context, vectorResId: Int): BitmapDescriptor {
            val vectorDrawable = ContextCompat.getDrawable(context, vectorResId);
            vectorDrawable!!.setBounds(0, 0, vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight());
            val bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
            val canvas = Canvas(bitmap);
            vectorDrawable.draw(canvas);
            return BitmapDescriptorFactory.fromBitmap(bitmap);
        }

        fun loadFullScreenImageView(context: Context, uri: String?) {
            val arrayList = ArrayList<String>()
            arrayList.add(uri!!)
            val fullImageIntent = Intent(context, FullScreenImageViewActivity::class.java)
            fullImageIntent.putStringArrayListExtra(FullScreenImageViewActivity.URI_LIST_DATA, arrayList)
            fullImageIntent.putExtra(FullScreenImageViewActivity.IMAGE_FULL_SCREEN_CURRENT_POS, 0)
            context.startActivity(fullImageIntent)
        }

        fun showRating(): AppRatingDialog.Builder {
            return AppRatingDialog.Builder().setPositiveButtonText("Submit").setNegativeButtonText("Cancel").setNeutralButtonText("Later").setNoteDescriptions(Arrays.asList("Very Bad", "Not good", "Quite ok", "Very Good", "Excellent !!!")).setDefaultRating(2).setTitle("Rate the Delivery Boy").setDescription("Please select some stars and give your feedback").setCommentInputEnabled(true).setDefaultComment("Please enter comment").setStarColor(R.color.colorPrimary).setNoteDescriptionTextColor(R.color.colorPrimary).setTitleTextColor(R.color.colorPrimary).setDescriptionTextColor(R.color.colorPrimary).setHint("Please write your comment here ...").setHintTextColor(R.color.gray).setCommentTextColor(R.color.colorPrimary).setCommentBackgroundColor(R.color.whitegrey).setWindowAnimation(R.style.MyDialogFadeAnimation).setCancelable(false).setCanceledOnTouchOutside(false)

        }

        fun getFCM(context: Context?): String {
            return getPreferences(context, SPConstants.FCM, "") as String
        }

        fun showLog(text: String) {
            Log.v(TAG, text)
        }

        fun getDB(): DAO {
            return Rylade.getInstance().getDB()
        }



        fun getObservable(completeable: Completable):Completable{
            return completeable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
        }

        fun getJsonArrayFromList(list: MutableList<CartModel>): JsonArray? {
            try {
                val array = JsonArray()
                for (item in list) {
                    val obj = JsonObject()
                    obj.addProperty(Constants.PRODUCT_NAME, item.productName)
                    obj.addProperty(Constants.PRODUCT_ID, item.product_id)
                    obj.addProperty(Constants.IS_DISCOUNT,item.iss_discount)
                    if (item.iss_discount==1){
                        obj.addProperty(Constants.DISCOUNT_PRICE, item.price)
                        obj.addProperty(Constants.PRODUCT_PRICE, item.originalPrice)
                    }
                    else{
                        obj.addProperty(Constants.PRODUCT_PRICE, item.price)
                    }
                    obj.addProperty(Constants.PRODUCT_QUANTITY, item.quantity)
                    obj.addProperty(Constants.PRODUCT_IMAGE, item.image)
                    obj.addProperty(Constants.PRODUCT_CATEGORY_ID, item.categoryId)
                    array.add(obj)
                }
                return array

            } catch (e: Exception) {
                showLog("Order Item Error " + e.message)
                return null
            }
        }

        fun showAlerter(context: Context?, title: String, message: String) {
            Alerter.create(context as Activity).setIcon(R.drawable.alerter_ic_notifications).setBackgroundColorInt(R.color.colorPrimary).setTitle(title).setText(message).show()
        }

        fun removeSpValue(context: Context?, key: String) {
            getPreference(context).edit().remove(key).apply()
        }

        fun callIntent(context: Context, number: String) {
            val intent = Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", number, null));
            context.startActivity(intent);
        }


        fun deleteAllFiles(path: String?) {
            val directory = File(path!!)
            if (directory.exists()) {
                if (!directory.listFiles().isNullOrEmpty() && directory.listFiles().isNotEmpty()) {
                    for (file in directory.listFiles()) {
                        showLog("file deleted" + file.path)
                        file.delete()
                    }
                }
            }
        }

        fun showELog(e: Throwable) {
            Log.v(TAG,""+e.localizedMessage)
        }

        fun getReviews(context: Context?,order_id:Int,list: MutableList<ProductModel>): JsonArray? {
            try {
                val array = JsonArray()
                for (item in list) {
                    val obj = JsonObject()
                    if (item.comment.isNullOrEmpty() || item.rating.isNullOrEmpty()){
                        continue
                    }
                    obj.addProperty(Constants.PRODUCT_NAME, item.productName)
                    obj.addProperty(Constants.PRODUCT_ID, item.id)
                    obj.addProperty(Constants.PRODUCT_COMMENT, item.comment)
                    obj.addProperty(Constants.PRODUCT_RATING, item.rating)
                    obj.addProperty(Constants.ORDER_ID,order_id)
                    obj.addProperty(Constants.VENDOR_ID,item.vendorId)
                    obj.addProperty(Constants.USER_ID, getUser(context).id.toString())
                    obj.addProperty(Constants.USER_NAME, getUser(context!!).f_name + " " + Utility.getUser(context).l_name)
                    array.add(obj)
                }
                return array

            } catch (e: Exception) {
                showELog(e)
                return null
            }
        }


        fun getSelectedAddress(context: Context?): com.senarios.customer.models.Address? {
            return getgson().fromJson(getPreferences(context,SPConstants.ADDRESS_DATA,"") as String,com.senarios.customer.models.Address::class.java)


        }

        fun setDefaultAddress(mContext: Context, mAddress: com.senarios.customer.models.Address) {
            setPreferences(mContext, SPConstants.ADDRESS_DATA,getgson().toJson(mAddress))
        }


    }
}