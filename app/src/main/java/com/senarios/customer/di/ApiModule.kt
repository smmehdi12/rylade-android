package com.senarios.customer.di

import com.senarios.customer.retrofit.APIConstants
import com.senarios.customer.retrofit.DataService
import com.senarios.customer.retrofit.Retrofit
import dagger.Module
import dagger.Provides
import javax.inject.Named
import javax.inject.Singleton

@Module
class ApiModule {

    @Singleton
    @Provides
    fun getService():DataService{
       return Retrofit.getinstance(APIConstants.BASE_URL).getService()
    }





}