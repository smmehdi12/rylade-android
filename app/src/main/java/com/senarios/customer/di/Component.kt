package com.senarios.customer.di

import android.app.Application
import android.content.Context
import com.senarios.customer.DbModule
import com.senarios.customer.MyModule
import com.senarios.customer.activities.MainActivity
import com.senarios.customer.activities.MapActivity
import com.senarios.customer.db.DAO
import com.senarios.customer.fragments.HomeFragment
import com.senarios.customer.models.Directory
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton


@Singleton
@Component(modules = arrayOf(MyModule::class, DbModule::class, ApiModule::class,UtilityModule::class))
interface Component {
    fun inject(mainActivity: MainActivity)
    fun inject(mapActivity: MapActivity)
    fun inject(homeFragment: HomeFragment)

    @dagger.Component.Builder
    interface Builder {
        @BindsInstance
        fun setApplication(application: Application):Builder
        fun build():com.senarios.customer.di.Component

    }

    fun getComponent():com.senarios.customer.di.Component
    fun getContext():Context
    fun getDAO():DAO
    fun getDirectory():Directory
}