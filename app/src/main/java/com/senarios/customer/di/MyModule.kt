package com.senarios.customer

import android.app.Application
import android.content.Context
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
  class MyModule() {

    @Singleton
    @Provides
        fun getContext(application: Application): Context {
            return  application.applicationContext
        }
}