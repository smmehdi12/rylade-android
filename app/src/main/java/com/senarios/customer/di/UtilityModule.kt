package com.senarios.customer.di

import android.os.Environment
import com.senarios.customer.models.Directory
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class UtilityModule {


    @Singleton
    @Provides
    fun getDirectory():Directory {
        return Directory(Environment.getExternalStorageDirectory().toString() + "/mediapicker","/videos","/images")
    }


}