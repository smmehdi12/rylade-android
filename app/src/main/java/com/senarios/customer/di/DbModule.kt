package com.senarios.customer

import android.content.Context
import com.senarios.customer.db.DAO
import com.senarios.customer.db.RoomClient
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module
class DbModule {

    @Singleton
    @Provides
     fun getDbInstance(context:Context):RoomClient{
        return RoomClient.getInstance(context)
    }

    @Singleton
    @Provides
    fun getDAO(context:Context):DAO{
        return getDbInstance(context).dao()
    }





}