package com.senarios.customer

import android.app.Application
import com.google.android.gms.tasks.OnSuccessListener
import com.google.android.libraries.places.api.Places
import com.google.firebase.FirebaseApp
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.InstanceIdResult
import com.google.firebase.messaging.FirebaseMessaging
import com.senarios.customer.costants.SPConstants
import com.senarios.customer.db.DAO
import com.senarios.customer.di.Component
import com.senarios.customer.di.DaggerComponent
import com.senarios.customer.utility.Utility
import com.squareup.picasso.Picasso

/*
* custom app class for this app
* Author - Mr Hashim
* December 2019
* {@link }
* */

class Rylade : Application(),OnSuccessListener<InstanceIdResult>{
    lateinit var component: Component

    override fun onCreate() {
        super.onCreate()
        mInstance=this
        Places.initialize(this,resources.getString(R.string.Maps_Places_Key))
        Picasso.with(this).isLoggingEnabled=true
        FirebaseApp.initializeApp(this)
        FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener(this)
        FirebaseMessaging.getInstance().subscribeToTopic("customer")

        Thread.setDefaultUncaughtExceptionHandler { t, e ->
            Utility.showELog(e)

        }

        // RxJavaPlugins.setErrorHandler { throwable: Throwable? -> } // nothing or some logging

        setUpComponent()

    }

    /*
    * set ups component interface for single access throughout the app
    * this component is used by Base Classes like @Link com.senarios.rylade.activities.BaseAcivity
    * this can be used in non-lifecycle components as well.
    * */
    private fun setUpComponent() {
        component = DaggerComponent.builder().setApplication(this).build()
    }

    /*
    * manages fcm token creation
    *
    *
    * */

    override fun onSuccess(p0: InstanceIdResult?) {
        if (p0?.id!=null){
            Utility.setPreferences(this, SPConstants.FCM,p0.token)
        }
    }

    fun getDB() :DAO{
        return component.getDAO()
    }


    /*
    * equivalent to static in java
    * created a statis instance of app.
    * */
    companion object{
        private lateinit var mInstance: Rylade
        fun getInstance():Rylade{
            return mInstance
        }
    }




}