package com.senarios.customer.callbacks


/*this interface connects the fragments with base class*/
interface ActivityFragment {
    fun get(activityStates: ActivityStates)

}