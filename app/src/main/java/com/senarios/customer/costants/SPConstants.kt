package com.senarios.customer.costants

import android.content.Context

/*constant strings related to shared preference*/

interface SPConstants {
    companion object{
        const val SP="Rylade"
        const val MODE= Context.MODE_PRIVATE
        const val USER="user_data"
        const val OTP="otpsms"
        const val IS_RIDE="is_ride"
        const val FCM="fcm"
        const val IS_FORGET_PASSWORD="is forget password"
        const val IS_FACEBOOK_VERIFY="is facebook verify"
        const val ADDRESS_DATA="sp_address"
    }
}