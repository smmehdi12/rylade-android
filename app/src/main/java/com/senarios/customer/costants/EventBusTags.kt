package com.senarios.customer.costants

interface EventBusTags {
    companion object{
        const val OTP_RECEIVER_SMS="otp sms"
        const val LOGIN_SIGN_UP_EMAIL="user email"
    }
}