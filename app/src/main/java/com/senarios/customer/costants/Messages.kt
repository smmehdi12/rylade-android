package com.senarios.customer.costants
/*text line shown in application on alert dialoges, toasts etc will be added here*/
interface Messages {
    companion object {
        const val NETWORK_ERROR="Please enable Wifi | Mobile data"
        const val EMPTY_EMAIL = "Enter email"
        const val EMPTY_FIELD = "This Field Is Required"
        const val INVALID_EMAIL = "Enter valid email"
        const val NETWORK_ERROR_TITLE = "Network Error"
        const val PASSWORD_ERROR="Password Must be 8 Characters"
        const val NULL_BODY_ERROR="No Response from Server"

        const val ERROR_TITLE = "Error"
        const val INPUT_MISMATCH="Both fields should be same"

        const val UNVERIFIED_TITLE="Unverified User"
        const val UNVERIFIED_MESSAGE="Your account is not verified, Do you want to verify it now?"

        const val VERIFIED_TITLE="Verification Successful"
        const val VERIFIED_MESSAGE="Your account has been verified! Please Login and enjoy Our Services"

        const val EMAIL_NO_EXIST="Email Not Exists"
    }
}