package com.senarios.customer.costants

interface Constants {
    companion object{


        val LAT="lat"
        val LNG="long"
        val LOCATION = "locationobj"
        val INSTANT="instant model"
        //defaults
        const val PASSWORD: String="password"
        const val EMAIL: String="email"
        const val USER_ID="user_id"
        const val USER_NAME = "user_name"
        const val ADDRESS_ID="address_id"
        const val ID="id"
        const val NAME="name"
        const val F_NAME="f_name"
        const val L_NAME="l_name"
        const val PHONE="phone_number"
        const val TYPE="user_type"
        const val UPDATED_AT="updated_at"
        const val CREATED_AT="created_at"
        const val MANUAL_SENDER_ADDRESS="manual_sender_address"
        const val MANUAL_RECEIVER_ADDRESS="manual_receiever_address"
        const val FB_ID="fb_id"

        //Modules
        const val MODULE_MED="Medicine"
        const val MODULE_RIDE="Ride"
        const val MODULE_DEL="Delivery"
        const val MODULE_FAV_PRODUCT="Favourite Products"






        //general
       const val AMOUNT="amount"
       const val STATUS="status"
        const val RIDER_ID="rider_id"


        //order medicine
        const val MEDICINE_ORDER_DATA="medicine order data"
        const val ADDRESS_DATA="address"
        const val LATLNG_DATA="latlng"
        const val MEDICINE_QUANTITY="quantity"
        const val MEDICINE_DOSE="dose"
        const val MEDICINE_TYPE="type"
        const val MEDICINE_INSTRUCTION="instructions"
        const val MEDICINE_ADDRESS_ID="address_id"


        //parcel delivery
        const val PARCEL_SENDER_NAME="senders_name"
        const val PARCEL_SENDER_PHONE="senders_contact"
        const val PARCEL_SENDER_ADDRESS="senders_address"
        const val SENDER_LAT="senders_lat"
        const val SENDER_LNG="senders_long"

        const val PARCEL_RECEIVER_NAME="receivers_name"
        const val PARCEL_RECEIVER_PHONE="receivers_contact"
        const val PARCEL_RECEIVER_ADDRESS="receivers_address"
        const val PARCEL_RECEIVER_LAT="receivers_lat"
        const val PARCEL_RECEIVER_LNG="receivers_long"

        const val PARCEL_ITEM_NAME="item_name"
        const val PARCEL_ITEM_WEIGHT="weight"
        const val PARCEL_ITEM_PHOTO="photo"

        //request ride
        const val ORIGIN="origins"
        const val DESTINATION="destinations"
        const val KEY="key"
        const val UNIT="units"
        const val IMPERIAL="imperial"
        const val RIDE_PICKUP_ADDRESS= "pickup_address"
        const val RIDE_DROP0FF_ADDRESS=  "dropoff_address"
        const val RIDE_DROP0FF_LAT="dropoff_lat"
        const val RIDE_DROP0FF_LNG="dropoff_lon"
        const val RIDE_DISTANCE="distance_km"
        const val RIDE_PICKUP_LAT="pickup_lat"
        const val RIDE_PICKUP_LNG="pickup_lon"



        //order
        const val ORDER_ID="order_id"
        const val ORDER_DATA="order_data"
        const val ORDER_TYPE="order_type"
        const val ORDER_STATUS_PENDING="pending"
        const val ORDER_STATUS_APPROVED="approved"
        const val ORDER_STATUS_PFA="pending_for_approval"
        const val ORDER_STATUS_PV="pending_vendor"
        const val ORDER_STATUS_RBV="rejected_by_vendor"
        const val ORDER_STATUS_CANCELLED="cancelled"
        const val ORDER_STATUS_ASSIGNED="assigned"
        const val ORDER_STATUS_COMPLETED="completed"
        const val ORDER_STATUS_WAITING="waiting"
        const val RATING="rating"
        const val IS_RATED="is_rated"
        const val ORDER_DATE="order_date"
        const val ORDER_TIME="order_time"
        const val IS_VENDOR="is_vendor"


        //
        //ride related data
        const val ESTIMATE_FARE="estimated_fare"
        const val INITIAL_FARE="initial_fare"
        const val CALCULATED_FARE="calculated_fare"
        const val WAITING_TIME="waiting_time"
        const val WAITING_COST="waiting_cost"
        const val LAST_TIME="last_time"
        const val TOTAL_FARE="total_fare"
        const val LAST_LAT="last_lat"
        const val LAST_LNG="last_long"
        const val TOTAL_TIME="total_time"
        const val ESTIMATE_TIME="estimated_time"
        const val ESTIMATE_DISTANCE="estimated_distance"


        //fcm tags
        const val USER="rider"
        const val RIDE_ORDER="ride_order"
        const val ORDER="order"
        const val ON_SITE="Arrived"
        const val START_RIDE="Start Ride"
        const val END_RIDE="End Ride"


        //ecommerce
        const val MAIN_CATEGORY="main_category_id"
        const val VENDOR_ID="vendor_id"
        const val CATEGORY_ID="category_id"
        const val CATEGORY_DATA="category_Data"
        const val PRODUCT_DATA="product data"
        const val SALES_TAX="sales_tax"


        //cart Order item

        const val PRODUCT_IMAGE="image"
        const val PRODUCT_ID="product_id"
        const val PRODUCT_QUANTITY="quantity"
        const val PRODUCT_PRICE="price"
        const val DISCOUNT_PRICE="discounted_price"
        const val IS_DISCOUNT="iss_discount"
        const val PRODUCT_NAME="product_name"
     const val PRODUCT_CATEGORY_ID="category_id"
        const val PRODUCT_COMMENT="comment"
        const val PRODUCT_RATING="review"

     //cart order main

     //promo code
     const val PROMO="promo_code_name"
        const val PROMO_DATA="promo code model"
        const val SUB_TOTAL="sub_total"
        const val TOTAL_AMOUNT="total_amount"
        const val DELIVERY_FEES="delivery_fees"
        const val DELIVERY_FEE="delivery_fee"
        const val IS_PROMO="is_promo"
        const val PROMO_AMOUNT="promo_amount"
        const val PROMO_ID="promo_id"



    }
}