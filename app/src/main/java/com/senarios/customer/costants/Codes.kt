package com.senarios.customer.costants

interface Codes {
    companion object{
        //permissionresultcodes
        const val ALL_PERMISSION_REQUEST=0
        const val SMS_PERMISSION_REQUEST=10
        const val LOCATION_PERMISSION_REQUEST=20
        const val STORAGE_PERMISSION_REQUEST=30
        const val CAMERA_PERMISSION_REQUEST=40
        const val MEDIA_PERMISSION_REQUEST=50


        //address result code
         const val ADDRESS_REQUEST=60
         const val IMAGE_REQUEST=40

        //latlng result code
        const val LATLNG_REQUEST_CODE=70


        //request contact code
        const val CONTACTS_PERMISSION_REQUEST=80
        const val SENDER_CONTACTS_REQUEST=90
        const val RECEIVER_CONTACTS_REQUEST=100
        const val SENDER_ADDRESS_REQUEST=110
        const val RECEIVER_ADDRESS_REQUEST=120

        //request ride codes
        const val RIDE_PICKUP_REQUEST=130
        const val RIDE_DROPOFF_REQUEST=140

    }
}