package com.senarios.customer.costants

/*tags related to fragments*/

interface Tags {
    companion object{
        const val ADDRESS="Address"
        const val HOME: String="Home Screen"
        const val LOGIN_SIGN_UP="Login Signup Dialog Fragment"
        const val LOGIN="Login Dialog Fragment"
        const val REGISTER="Register Dialog Fragment"
        const val VERIFY_OTP="Verify OTP Dialog Fragment"
    }
}