package com.senarios.customer.adaptors

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.palette.graphics.Palette
import androidx.recyclerview.widget.RecyclerView
import com.senarios.customer.R
import com.senarios.customer.databinding.ProductcategoriesbyidBinding

class VendorProductAdapter(val context: Context,val productslist:MutableList<com.senarios.customer.models.ProductCategoryModel>,val  callback:RecyclerViewCallback):
    RecyclerView.Adapter<VendorProductAdapter.VendorProductViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VendorProductViewHolder {
       val view=LayoutInflater.from(parent.context).inflate(R.layout.productcategoriesbyid,parent,false)
        val  bindings: ProductcategoriesbyidBinding =DataBindingUtil.bind(view)!!
        return VendorProductViewHolder(bindings)

    }

    override fun getItemCount(): Int {
       return productslist.size
    }

    override fun onBindViewHolder(holder: VendorProductViewHolder, position: Int) {
       val adapter=holder.bindings
        adapter.data=productslist.get(position)
    }


    inner class VendorProductViewHolder(val bindings: ProductcategoriesbyidBinding) :RecyclerView.ViewHolder(bindings.root){

        init {

        }



    }
}