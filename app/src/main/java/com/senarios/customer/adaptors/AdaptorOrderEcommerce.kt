package com.senarios.customer.adaptors

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.senarios.customer.R
import com.senarios.customer.databinding.ItemOrderEcommerceBinding
import com.senarios.customer.databinding.ItemOrderMedicineBinding
import com.senarios.customer.models.CartModel

/*listview inside myaddress screen, this class will show all the address we fetch from server*/

class AdaptorOrderEcommerce (val context: Context, val list:MutableList<CartModel>, val callback: RecyclerViewCallback): RecyclerView.Adapter<AdaptorOrderEcommerce.holder>() {



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): holder {
        val view=LayoutInflater.from(parent.context).inflate(R.layout.item_order_ecommerce,parent,false)
        val binding:ItemOrderEcommerceBinding=DataBindingUtil.bind(view)!!
        return holder(binding)
    }

    override fun getItemCount(): Int {
        return list.size
     }

    override fun onBindViewHolder(holder: holder, position: Int) {
     holder.binding.data=list[position]

    }



   inner class holder(val binding: ItemOrderEcommerceBinding) : RecyclerView.ViewHolder(binding.root) ,View.OnClickListener {
       init {
           binding.imageview.setOnClickListener(this)
       }

       override fun onClick(v: View?) {
           callback.OnClick(adapterPosition,list[adapterPosition])
       }

    }
}