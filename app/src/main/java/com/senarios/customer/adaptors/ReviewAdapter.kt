package com.senarios.customer.adaptors

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.senarios.customer.R
import com.senarios.customer.databinding.ItemsReviewBinding
import com.senarios.customer.models.Comment
import com.senarios.customer.models.CommentsItem

class ReviewAdapter(val context: Context,val list: ArrayList<CommentsItem>,val recyclerViewCallback: RecyclerViewCallback):
    RecyclerView.Adapter<ReviewAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view=LayoutInflater.from(parent.context).inflate(R.layout.items_review,parent,false)
        val binding:ItemsReviewBinding=DataBindingUtil.bind(view)!!
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
       holder.binding.data=list[position]

    }


    inner class ViewHolder(val binding:ItemsReviewBinding): RecyclerView.ViewHolder(binding.root) {

    }


}