package com.senarios.customer.adaptors

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.senarios.customer.R
import com.senarios.customer.databinding.ItemCartItemBinding
import com.senarios.customer.models.*

/*listview inside myaddress screen, this class will show all the address we fetch from server*/

class AdaptorCartItems (val context: Context,  val callback: RecyclerViewCallback): RecyclerView.Adapter<AdaptorCartItems.holder>() {
    private val list:ArrayList<CartModel> = ArrayList()


    fun setData(list: List<CartModel>){
        this.list.clear()
        this.list.addAll(list)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): holder {
        val view=LayoutInflater.from(parent.context).inflate(R.layout.item_cart_item,parent,false)
        val binding: ItemCartItemBinding =DataBindingUtil.bind(view)!!
        return holder(binding)
    }

    override fun getItemCount(): Int {
        return list.size
     }

    override fun onBindViewHolder(holder: holder, position: Int) {
     holder.binding.data=list[position]
    }





   inner class holder(val binding: ItemCartItemBinding) : RecyclerView.ViewHolder(binding.root) ,View.OnClickListener {
       init {
           binding.minusIcon.setOnClickListener(this)
           binding.plusIcon.setOnClickListener(this)
       }

       override fun onClick(v: View?) {
           when (v?.id){
               binding.minusIcon.id->{
                   callback.updateCartItem(adapterPosition,list[adapterPosition],binding.tvCounter.text.toString().toInt(),false)
               }
               binding.plusIcon.id->{
                   callback.updateCartItem(adapterPosition,list[adapterPosition],binding.tvCounter.text.toString().toInt(),true)
               }
           }

       }

    }
    fun getData():MutableList<CartModel>{
        return list
    }
}