package com.senarios.customer.adaptors

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Filter
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.senarios.customer.R
import com.senarios.customer.databinding.ItemSearchProductBinding
import com.senarios.customer.models.ProductModel
import org.greenrobot.eventbus.EventBus

class SearchAdaptor(val ref: Context,val list:MutableList<ProductModel>) : RecyclerView.Adapter<SearchAdaptor.Holder>() {
    inner class Holder (val binding:ItemSearchProductBinding) : RecyclerView.ViewHolder(binding.root){
        init {
            binding.root.setOnClickListener {
                EventBus.getDefault().post(list[adapterPosition])
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        val view:View= LayoutInflater.from(ref).inflate(R.layout.item_search_product,parent,false)
        val binding:ItemSearchProductBinding=DataBindingUtil.bind(view)!!
        return Holder(binding)
    }

    override fun getItemCount(): Int {
     return list.size
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.binding.product=list[position]
    }





}