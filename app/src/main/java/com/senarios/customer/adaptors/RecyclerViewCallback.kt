package com.senarios.customer.adaptors

import com.senarios.customer.models.Address
import com.senarios.customer.models.CartModel

/*interface responsible for handling callbacks on recyclerview item click inside fragment or activity its attached*/
interface RecyclerViewCallback {
    fun OnEdit(position:Int, any:Any){

    }

    fun OnDelete(position:Int,any:Any){

    }

    fun OnClick(position:Int,any:Any){

    }
    fun  OnScroll(){

    }
    fun openGallery(position: Int){

    }

    fun getPosition(position: Int){

    }
    fun updateCartItem(position: Int,  cartModel: CartModel,quantity:Int ,flag:Boolean){

    }

    fun setDefaultAddress(position: Int,mAddress: Address){

    }
}