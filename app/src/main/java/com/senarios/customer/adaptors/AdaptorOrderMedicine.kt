package com.senarios.customer.adaptors

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.senarios.customer.R
import com.senarios.customer.databinding.ItemMedicineViewBinding
import com.senarios.customer.databinding.ItemOrderMedicineBinding
import com.senarios.customer.databinding.ItemOrdersBinding
import com.senarios.customer.models.MedicineModel

/*listview inside myaddress screen, this class will show all the address we fetch from server*/

class AdaptorOrderMedicine (val context: Context, val list:MutableList<MedicineModel>, val callback: RecyclerViewCallback): RecyclerView.Adapter<AdaptorOrderMedicine.holder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): holder {
        val view=LayoutInflater.from(parent.context).inflate(R.layout.item_order_medicine,parent,false)
        val binding:ItemOrderMedicineBinding=DataBindingUtil.bind(view)!!
        return holder(binding)
    }

    override fun getItemCount(): Int {
        return list.size
     }

    override fun onBindViewHolder(holder: holder, position: Int) {
     holder.binding.data=list[position]

        if (list[position].dose.isNullOrEmpty()){
            holder.binding.viewFirst.visibility=View.GONE
            holder.binding.dose.visibility=View.GONE
        }
        if (list[position].instruction.isNullOrEmpty()){
            holder.binding.viewSecond.visibility=View.GONE
            holder.binding.type.visibility=View.GONE
        }
        holder.binding.titleMain.text="Medicine"+" "+(position+1)

    }



   inner class holder(val binding: ItemOrderMedicineBinding) : RecyclerView.ViewHolder(binding.root) ,View.OnClickListener {
       init {
           binding.imageView.setOnClickListener(this)
       }

       override fun onClick(v: View?) {
           callback.OnClick(adapterPosition,list[adapterPosition])
       }

    }
}