package com.senarios.customer.adaptors

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.senarios.customer.R
import com.senarios.customer.databinding.ItemVendorProductsBinding
import com.senarios.customer.models.*

/*listview inside myaddress screen, this class will show all the address we fetch from server*/

class AdaptorVendorProducts (val context: Context, val list:MutableList<ProductModel>, val callback: RecyclerViewCallback): RecyclerView.Adapter<AdaptorVendorProducts.holder>() {



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): holder {
        val view=LayoutInflater.from(context).inflate(R.layout.item_vendor_products,parent,false)
        val binding: ItemVendorProductsBinding =DataBindingUtil.bind(view)!!
        return holder(binding)
    }

    override fun getItemCount(): Int {
        return list.size
     }

    override fun onBindViewHolder(holder: holder, position: Int) {
        holder.binding.data=list[position]

    }



   inner class holder(val binding: ItemVendorProductsBinding) : RecyclerView.ViewHolder(binding.root) ,View.OnClickListener {
       init {
           binding.layout.setOnClickListener(this@holder)

       }

       override fun onClick(v: View?) {
           callback.OnClick(adapterPosition,list[adapterPosition])
       }

    }

}