package com.senarios.customer.adaptors

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.BitmapDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.palette.graphics.Palette
import androidx.recyclerview.widget.RecyclerView
import com.senarios.customer.R
import com.senarios.customer.databinding.ItemMedicineViewBinding
import com.senarios.customer.databinding.ItemOrderMedicineBinding
import com.senarios.customer.databinding.ItemOrdersBinding
import com.senarios.customer.databinding.ItemVendorBinding
import com.senarios.customer.models.Vendor

/*listview inside myaddress screen, this class will show all the address we fetch from server*/

class AdaptorVendors (val context: Context, val list:MutableList<Vendor>, val callback: RecyclerViewCallback): RecyclerView.Adapter<AdaptorVendors.holder>() {



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): holder {
        val view=LayoutInflater.from(parent.context).inflate(R.layout.item_vendor,parent,false)
        val binding:ItemVendorBinding=DataBindingUtil.bind(view)!!
        return holder(binding)
    }

    override fun getItemCount(): Int {
        return list.size
     }

    override fun onBindViewHolder(holder: holder, position: Int) {
     holder.binding.data=list[position]


    }



   inner class holder(val binding: ItemVendorBinding) : RecyclerView.ViewHolder(binding.root) ,View.OnClickListener {
       init {
           binding.layoutCategory.setOnClickListener(this)
       }

       override fun onClick(v: View?) {
           callback.OnClick(adapterPosition,list[adapterPosition])
       }

    }
}