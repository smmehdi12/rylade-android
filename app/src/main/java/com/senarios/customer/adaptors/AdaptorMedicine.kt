package com.senarios.customer.adaptors

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.AdapterView
import android.widget.EditText
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.senarios.customer.R
import com.senarios.customer.databinding.ItemMedicineViewBinding
import com.senarios.customer.models.MedicineModel

/*listview inside myaddress screen, this class will show all the address we fetch from server*/

class AdaptorMedicine (val context: Context,val recyclerViewCallback: RecyclerViewCallback): RecyclerView.Adapter<AdaptorMedicine.holder>() {
    private val list= mutableListOf<MedicineModel>()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): holder {
        val view=LayoutInflater.from(parent.context).inflate(R.layout.item_medicine_view,parent,false)
        val binding:ItemMedicineViewBinding=DataBindingUtil.bind(view)!!
        return holder(binding)
    }

    override fun getItemCount(): Int {
        return list.size
     }

    override fun onBindViewHolder(holder: holder, position: Int) {
        val medicine= list[position]
        if (position!=0){
            holder.binding.ivRemoveView.visibility=View.VISIBLE
        }
        else{
            holder.binding.ivRemoveView.visibility=View.GONE
        }

        if (medicine.photo!=null){
            holder.binding.pictureImg.setImageBitmap(Bitmap.createScaledBitmap(BitmapFactory.decodeFile(medicine.photo),50,50,false))
        }
        else{
            holder.binding.pictureImg.setImageResource(R.drawable.picture)
        }
        if (medicine.name!=null){
            holder.binding.nameEdt.setText(medicine.name)
        }
        else{
            holder.binding.nameEdt.setText("")
        }
        if (medicine.quantity!=null){
            holder.binding.quantityEdt.setText(medicine.quantity)
        }
        else{
            holder.binding.quantityEdt.setText("")
        }
        if (medicine.dose!=null){
            holder.binding.doseEdt.setText(medicine.dose)
        }
        else{
            holder.binding.doseEdt.setText("")
        }
        if (medicine.instruction!=null){
            holder.binding.instructionsTxt.setText(medicine.instruction)
        }
        else{
            holder.binding.instructionsTxt.setText("")
        }
        if (medicine.type!=null){
            if (medicine.type.equals(context.resources.getStringArray(R.array.type)[0],true)){
                holder.binding.type.setSelection(0)
            }
            else{
                holder.binding.type.setSelection(1)
            }
        }


    }

    fun removeItem(position:Int){
        list.removeAt(position)
        notifyDataSetChanged()
    }

    fun getItems():List<MedicineModel>{
        return list
    }
    fun addItem(){
        list.add(MedicineModel(-1,-1,null,null,null,null,null,null,null,null,false,null
        ,null))
        notifyItemInserted(list.size-1)
        recyclerViewCallback.OnScroll()
    }

   inner class holder(val binding: ItemMedicineViewBinding) : RecyclerView.ViewHolder(binding.root),TextWatcher,View.OnFocusChangeListener,AdapterView.OnItemSelectedListener ,View.OnClickListener {
       init {

           binding.ivRemoveView.setOnClickListener(this)
           /////////
           // binding.takeSnapPrescriptionBtn.visibility=View.GONE

          // binding.takeSnapPrescriptionBtn.setOnClickListener(this)
           binding.type.onItemSelectedListener=this
           binding.instructionsTxt.onFocusChangeListener = this


           //edittextcallbacks
           binding.nameEdt.addTextChangedListener(this)
           binding.quantityEdt.addTextChangedListener(this)
           binding.doseEdt.addTextChangedListener(this)
           binding.instructionsTxt.addTextChangedListener(this)
           binding.instructionsTxt.imeOptions = EditorInfo.IME_ACTION_DONE;
           binding.instructionsTxt.setRawInputType(InputType.TYPE_CLASS_TEXT);

       }

       override fun onClick(v: View?) {
           recyclerViewCallback.OnEdit(adapterPosition, list[adapterPosition])
           when (v?.id) {
               binding.ivRemoveView.id->{
                   removeItem(adapterPosition)
                   recyclerViewCallback.OnScroll()
               }
               binding.takeSnapPrescriptionBtn.id->{
                   recyclerViewCallback.openGallery(adapterPosition)
               }
           }
       }



       override fun onNothingSelected(parent: AdapterView<*>?) {

       }

       override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
           if(adapterPosition!=-1) {
               list[adapterPosition].type = binding.type.selectedItem.toString()
           }
        }

       override fun onFocusChange(v: View?, hasFocus: Boolean) {
           if (binding.instructionsTxt.hasFocus()){
               setGreenDrawable(binding.instructionsTxt)
           }
           else{
               setGrayDrawable(binding.instructionsTxt)
           }
       }

       override fun afterTextChanged(s: Editable?) {

       }

       override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

       }

       override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
           when {
               binding.nameEdt.hasFocus() -> {
                   list[adapterPosition].name=binding.nameEdt.text.toString()
               }
               binding.quantityEdt.hasFocus() -> {
                   list[adapterPosition].quantity=binding.quantityEdt.text.toString()
               }
               binding.doseEdt.hasFocus() -> {
                   list[adapterPosition].dose=binding.doseEdt.text.toString()
               }
               binding.instructionsTxt.hasFocus() -> {
                   list[adapterPosition].instruction=binding.instructionsTxt.text.toString()
               }
           }
           recyclerViewCallback.OnEdit(adapterPosition, list[adapterPosition])

       }
   }



    fun setGrayDrawable(editText: EditText){
        editText.background=context.resources.getDrawable(R.drawable.email_grey,context.theme)

    }
    fun setGreenDrawable(editText: EditText){
        editText.background=context.resources.getDrawable(R.drawable.email_green,context.theme)
    }


}