package com.senarios.customer.adaptors

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.senarios.customer.R
import com.senarios.customer.databinding.ItemMainActivityBinding
import com.senarios.customer.models.Modules
import com.senarios.customer.utility.Utility
import com.squareup.picasso.Picasso
import kotlin.math.roundToInt

class AdaptorModules(val context: Context,val list: MutableList<Modules>,val listener:RecyclerViewCallback) : RecyclerView.Adapter<AdaptorModules.holder>(){


    inner class holder(val binding:ItemMainActivityBinding) : RecyclerView.ViewHolder(binding.root){
        init {
            binding.root.setOnClickListener {
                listener.OnClick(adapterPosition,list[adapterPosition])
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): holder {
        val view=LayoutInflater.from(context).inflate(R.layout.item_main_activity,parent,false)
        val binding:ItemMainActivityBinding=DataBindingUtil.bind(view)!!
        return holder(binding)

    }

    override fun getItemCount(): Int {
       return list.size
    }

    override fun onBindViewHolder(holder: holder, position: Int) {
        holder.binding.model = list[position]
        if (list[position].isOffline) {
            holder.binding.imageView.setImageResource(list[position].icon)
        }
        else{
            val width = (context.resources.getDimension(R.dimen.image_size_width)/context.resources.displayMetrics.density).roundToInt()
            val height = (context.resources.getDimension(R.dimen.image_size_width)/context.resources.displayMetrics.density).roundToInt()
            Utility.showLog("width = "+ width + "height = "+ height)
            Picasso.with(context)
                .load(list[position].mainCategoryModel!!.image_url)
                .error(R.drawable.icons1)
                .into(holder.binding.imageView)
        }
    }

}