package com.senarios.customer.adaptors

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.senarios.customer.R
import com.senarios.customer.databinding.ItemAddressesBinding
import com.senarios.customer.databinding.ItemFaqBinding
import com.senarios.customer.models.FaqModel

/*listview inside myaddress screen, this class will show all the address we fetch from server*/

class AdaptorFAQ (val context: Context, val list:MutableList<FaqModel>, val callback: RecyclerViewCallback): RecyclerView.Adapter<AdaptorFAQ.holder>() {



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): holder {
        val view=LayoutInflater.from(parent.context).inflate(R.layout.item_faq,parent,false)
        val binding:ItemFaqBinding=DataBindingUtil.bind(view)!!
        return holder(binding)
    }

    override fun getItemCount(): Int {
        return list.size
     }

    override fun onBindViewHolder(holder: holder, position: Int) {
        holder.binding.data=list[position]

    }



   inner class holder(val binding: ItemFaqBinding) : RecyclerView.ViewHolder(binding.root)  {



    }
}