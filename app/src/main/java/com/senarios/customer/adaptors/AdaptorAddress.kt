package com.senarios.customer.adaptors

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.senarios.customer.R
import com.senarios.customer.databinding.ItemAddressesBinding

/*listview inside myaddress screen, this class will show all the address we fetch from server*/

class AdaptorAddress (val context: Context,val addresses:MutableList<com.senarios.customer.models.Address>,val callback: RecyclerViewCallback): RecyclerView.Adapter<AdaptorAddress.holder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): holder {
        val view=LayoutInflater.from(parent.context).inflate(R.layout.item_addresses,parent,false)
        val binding:ItemAddressesBinding=DataBindingUtil.bind(view)!!
        return holder(binding)
    }

    override fun getItemCount(): Int {
        return addresses.size
     }

    override fun onBindViewHolder(holder: holder, position: Int) {
        val b=holder.binding
        b.data=addresses.get(position)


    }

    fun removeItem(position:Int){
        addresses.removeAt(position)
        notifyDataSetChanged()
    }

   inner class holder(val binding: ItemAddressesBinding) : RecyclerView.ViewHolder(binding.root) ,View.OnClickListener {
       init {
           binding.editImg.setOnClickListener(this)
           binding.deleteImg.setOnClickListener(this)
           binding.cardview.setOnClickListener(this)
           binding.itemAddressTvSetDefault.setOnClickListener(this)
       }



       override fun onClick(v: View?) {
           when (v?.id) {
               binding.editImg.id -> {
                   callback.OnEdit(adapterPosition, addresses[adapterPosition])
               }
               binding.deleteImg.id -> {
                   callback.OnDelete(adapterPosition, addresses[adapterPosition])
               }
               binding.cardview.id->{
                   callback.OnClick(adapterPosition,addresses[adapterPosition])
               }
               binding.itemAddressTvSetDefault.id->{
                   callback.setDefaultAddress(adapterPosition,addresses[adapterPosition])
               }
           }
       }
    }
}