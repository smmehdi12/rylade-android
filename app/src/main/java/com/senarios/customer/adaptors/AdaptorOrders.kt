package com.senarios.customer.adaptors

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.senarios.rylad.BaseDialogeFragment
import com.senarios.customer.R
import com.senarios.customer.activities.MyOrdersActivity
import com.senarios.customer.costants.Constants
import com.senarios.customer.databinding.ItemOrdersBinding
import com.senarios.customer.dialogfragments.DialogeCallback
import com.senarios.customer.dialogfragments.RatingFragmentDialog
import com.senarios.customer.models.OrderModel
import com.senarios.customer.utility.Utility

/*listview inside myaddress screen, this class will show all the address we fetch from server*/



class AdaptorOrders (val context: Context, val list:MutableList<OrderModel>, val callback: RecyclerViewCallback,val isActive:Boolean): RecyclerView.Adapter<AdaptorOrders.holder>(),DialogeCallback {



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): holder {
        val view=LayoutInflater.from(parent.context).inflate(R.layout.item_orders,parent,false)
        val binding:ItemOrdersBinding=DataBindingUtil.bind(view)!!
        return holder(binding)
    }

    override fun getItemCount(): Int {
        return list.size
     }

    override fun onBindViewHolder(holder: holder, position: Int) {
        val b=holder.binding
        b.data= list[position]
        if (list[position].order_type==1){
            holder.binding.name.text = "Medicine"
    }
        else if (list[position].order_type==2){
            holder.binding.name.text = "Parcel Delivery"
        }
        else if (list[position].order_type==3){
            holder.binding.name.text = "Ride"
        }
        else if (list[position].order_type==4){
            holder.binding.name.text = "E-Commerce"
        }



    }



   inner class holder(val binding: ItemOrdersBinding) : RecyclerView.ViewHolder(binding.root) ,View.OnClickListener {
       init {
           binding.cardview.setOnClickListener(this)
       }

       override fun onClick(v: View?) {
           when (v?.id) {
               binding.cardview.id -> {
                       if (list.size > 0) {
                           if (!isActive) {
                               if (list[adapterPosition].order_type == 4) {
                                   if (list[adapterPosition].is_rated == 0) {
//                                       Utility.showToast(context, "message")
                                       val ratingDialog = RatingFragmentDialog()
                                       val bundle = Bundle()
                                       bundle.putParcelable(
                                           Constants.ORDER_DATA,
                                           list[adapterPosition]
                                       )
                                       ratingDialog.arguments = bundle
                                       OnChange(ratingDialog)

                                   } else {
                                       callback.OnClick(adapterPosition, list[adapterPosition])
                                   }

                               } else {
                                   if (list[adapterPosition].is_rated == 0) {
                                       callback.getPosition(adapterPosition)
                                       Utility.showRating().create(context as MyOrdersActivity)
                                           .show()
                                   } else {
                                       callback.OnClick(adapterPosition, list[adapterPosition])
                                   }
                               }
                           }
                           else{
                               callback.OnClick(adapterPosition, list[adapterPosition])
                           }
                       }


               }
           }
       }
    }

    fun getData():MutableList<OrderModel>{
        return list
    }

    override fun OnChange(fragment: BaseDialogeFragment) {
        context as MyOrdersActivity
       fragment.show(context.supportFragmentManager,"dialog")
    }

    override fun OnTrigger() {

    }


}


