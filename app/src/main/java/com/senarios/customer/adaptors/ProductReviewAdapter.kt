package com.senarios.customer.adaptors

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.RatingBar
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.senarios.customer.R
import com.senarios.customer.databinding.ItemsProductReviewBinding
import com.senarios.customer.databinding.ItemsReviewBinding
import com.senarios.customer.models.CartModel
import com.senarios.customer.models.Comment
import com.senarios.customer.models.ProductModel
import kotlin.math.roundToInt

class ProductReviewAdapter(val context: Context,val list: ArrayList<ProductModel>,val recyclerViewCallback: RecyclerViewCallback):
    RecyclerView.Adapter<ProductReviewAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view=LayoutInflater.from(parent.context).inflate(R.layout.items_product_review,parent,false)
        val binding:ItemsProductReviewBinding=DataBindingUtil.bind(view)!!
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.data=list[position]
    }


    inner class ViewHolder(val binding:ItemsProductReviewBinding): RecyclerView.ViewHolder(binding.root),TextWatcher,RatingBar.OnRatingBarChangeListener {
        init {
            binding.ratingComment.addTextChangedListener(this)
            binding.rating.onRatingBarChangeListener = this
        }

        override fun afterTextChanged(s: Editable?) {

        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            if (adapterPosition!=-1){
                list[adapterPosition].comment=binding.ratingComment.text.toString()
            }

        }

        override fun onRatingChanged(ratingBar: RatingBar?, rating: Float, fromUser: Boolean) {
            if (adapterPosition!=-1){
                list[adapterPosition].rating=rating.roundToInt().toString()
            }
        }
    }

    fun getData():ArrayList<ProductModel>{
        return list
    }

}