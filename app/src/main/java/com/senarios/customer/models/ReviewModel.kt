package com.senarios.customer.models

import android.view.View
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.room.PrimaryKey
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.google.gson.annotations.SerializedName
import com.senarios.customer.retrofit.APIConstants

class ReviewModel(


    @SerializedName("comments")
    val list_comments:ArrayList<Comment>,

    @SerializedName("average")
    var average_score:String,
   @SerializedName("vendor")
   var object_val:VendorModel



    ) {


}
class Comment(@SerializedName("user_name")
var name_user:String?,
    @SerializedName("created_at")
    var date:String?,
    @SerializedName("comment")
    var review:String?,
    @SerializedName("review")
    var rating_points:Int)
{

}

class VendorModel(

    @SerializedName("category_id")
    var categoryId: Int?,
    @SerializedName("created_at")
    var createdAt: String?,
    @SerializedName("email")
    var email: String?,
    @SerializedName("email_verified_at")
    var emailVerifiedAt: String?,
    @SerializedName("f_name")
    var fName: String?,
    @SerializedName("fcm")
    var fcm: String?,
    @SerializedName("id")
    @PrimaryKey(autoGenerate = false)
    var id: Int?,
    @SerializedName("image")
    var image: String?,
    @SerializedName("is_verified")
    var isVerified: Boolean?,
    @SerializedName("l_name")
    var lName: String?,
    @SerializedName("login_facebook")
    var loginFacebook: Boolean?,
    @SerializedName("model")
    var model: String?,
    @SerializedName("no_plate")
    var noPlate: String?,
    @SerializedName("otp")
    var otp: String?,
    @SerializedName("phone_number")
    var phoneNumber: String?,
    @SerializedName("rider_availability")
    var riderAvailability: String?,
    @SerializedName("rider_work_status")
    var riderWorkStatus: String?,
    @SerializedName("service_area_id")
    var serviceAreaId: String?,
    @SerializedName("token")
    var token: String?,
    @SerializedName("updated_at")
    var updatedAt: String?,
    @SerializedName("user_status")
    var userStatus: Int?,
    @SerializedName("user_type")
    var userType: String?
){


}
@BindingAdapter("loadingimageview")
fun loadimageview(view: View,image:String){
    Glide.with(view.context)
        .load(APIConstants.BASE_VENDOR_PHOTO +image)
        .apply(RequestOptions().timeout(20000))
        .diskCacheStrategy(DiskCacheStrategy.DATA)
        .apply(RequestOptions.overrideOf(100,100))
        .into(view as ImageView)
}

