package com.senarios.customer.models


import com.google.gson.annotations.SerializedName

 class PromoModel(
    @SerializedName("created_at")
    var createdAt: String?,
    @SerializedName("discount_percentage")
    var discountPercentage: Int?,
    @SerializedName("discount_pkr")
    var discountPkr: Int?,
    @SerializedName("expiry_date")
    var expiryDate: String?,
    @SerializedName("id")
    var id: Int?,
    @SerializedName("promo_code_name")
    var promoCodeName: String?,
    @SerializedName("updated_at")
    var updatedAt: String?,
    @SerializedName("vendor_id")
    var vendorId: Int?
)