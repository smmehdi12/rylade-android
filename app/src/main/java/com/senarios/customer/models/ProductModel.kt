package com.senarios.customer.models


import android.graphics.Paint
import android.os.Parcelable
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.Nullable
import androidx.databinding.BindingAdapter
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.google.gson.annotations.SerializedName
import com.senarios.customer.retrofit.APIConstants
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.parcel.Parcelize
import java.lang.Exception

@Parcelize
@Entity(tableName = "Products")
data class ProductModel(
    @SerializedName("category_id")
    @ColumnInfo(name = "category_id")
    var categoryId: Int?,
    @SerializedName("category_name")
    @ColumnInfo(name = "category_name")
    var categoryName: String?,
    @SerializedName("created_at")
    @ColumnInfo(name = "created_at")
    var createdAt: String?,
    @SerializedName("description")
    @ColumnInfo(name = "description")
    var description: String?,
    @SerializedName("id")
    @ColumnInfo(name = "id")
    @PrimaryKey(autoGenerate = false)
    var id: Int?,
    @SerializedName("image")
    @ColumnInfo(name = "image")
    var image: String?,
    @SerializedName("price")
    @ColumnInfo(name = "price")
    var price: Int?,
    @SerializedName("product_name")
    @ColumnInfo(collate = ColumnInfo.NOCASE,name = "product_name")
    var productName: String?,
    @SerializedName("promo_code_id")
    @ColumnInfo(name = "promo_code_id")
    var promoCodeId: Int?,
    @SerializedName("quantity_available")
    @ColumnInfo(name = "quantity_available")
    var quantityAvailable: Int?,
    @SerializedName("status")
    @ColumnInfo(name = "status")
    var status: Int?,
    @SerializedName("sub_category_name")
    @ColumnInfo(name = "sub_category_name")
    var subCategoryName: String?,
    @SerializedName("updated_at")
    @ColumnInfo(name = "updated_at")
    var updatedAt: String?,
    @SerializedName("weight")
    @ColumnInfo(name = "weight")
    var weight: Int?,
    @SerializedName("vendor_id")
    @ColumnInfo(name = "vendor_id")
    var vendorId: Int?,
    @SerializedName("sales_tax")
    @ColumnInfo(name = "sales_tax")
    var sales_tax: Double,
    @SerializedName("fixed_discount")
    @ColumnInfo(name = "fixed_discount")
    var fixed_discount: Int?,
    @ColumnInfo(name = "iz_discount")
    var iz_discount: Int?,
    @ColumnInfo(name = "comment",defaultValue = "")
    var comment:String?,
    @ColumnInfo(name = "rating",defaultValue = "")
    var rating:String?
):Parcelable {
}

@BindingAdapter("loadProductImage")
fun loadProductImage(view: View, image:String){
    Glide.with(view.context)
        .load(APIConstants.BASE_PRODUCT_PHOTO + image)
        .apply(RequestOptions().timeout(20000))
        .diskCacheStrategy(DiskCacheStrategy.DATA)
        .apply(RequestOptions.overrideOf(100,100))
        .into(view as CircleImageView)
}
@BindingAdapter("loadProductImageView")
fun loadProductImageView(view: View, image:String){
    Glide.with(view.context)
        .load(APIConstants.BASE_PRODUCT_PHOTO + image)
        .apply(RequestOptions().timeout(20000))
        .diskCacheStrategy(DiskCacheStrategy.DATA)
        .into(view as ImageView)
}

@BindingAdapter(value = arrayOf("setprice","pricetype","isDiscount"),requireAll = false)
fun setPrice(view:View?,price: Int?,type:String?,isDiscount : Int?){
    com.senarios.customer.utility.Utility.showLog("Price $price")
    com.senarios.customer.utility.Utility.showLog("type $type")
    com.senarios.customer.utility.Utility.showLog("isDiscount $isDiscount")
    try{
    if (view is TextView) {
        if (type!!.toInt() == 0) {
            view.text = price!!.toString()+" PKR"
            if (isDiscount == 1) {
                view.paintFlags = Paint.STRIKE_THRU_TEXT_FLAG
            }

        } else {
            view.text = price!!.toString()+" PKR"
            if (isDiscount == 1) {
                view.visibility=View.VISIBLE
            }
        }
    }
    }
    catch (e:Exception){
        e.printStackTrace()
    }
}

