package com.senarios.customer.models


import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(tableName = "VendorCategory")
data class VendorCategoryModel(
    @SerializedName("category_name")
    @ColumnInfo(name = "category_name")
    var categoryName: String?,
    @SerializedName("created_at")
    @ColumnInfo(name = "created_at")
    var createdAt: String?,
    @SerializedName("id")
    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = "id")
    var id: Int?,
    @SerializedName("updated_at")
    @ColumnInfo(name = "updated_at")
    var updatedAt: String?,
    @SerializedName("vendor_id")
    @ColumnInfo(name = "vendor_id")
    var vendorId: Int?
):Parcelable