package com.senarios.customer.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ResponseProducts(
	val vendor: Vendor? = null,
	@SerializedName("products")
	val products: List<ProductModel?>? = null
) : Parcelable
