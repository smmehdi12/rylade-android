package com.senarios.customer.models

import android.content.Context
import com.senarios.customer.costants.Constants
import com.senarios.customer.utility.Utility

class INIModel(
    val created_at: String,
    val id: Int,
    val medicine_instance: Int,
    val medicine_non_instance: Int,
    val parcel_instance: Int,
    val parcel_non_instance: Int,
    val updated_at: String,
    val delivery_fee:String?,
    val rider_module:Int,
    val medicine_module:Int,
    val parcel_module:Int
) {
    companion object {
        fun getModelFromSP(context: Context): INIModel? {
            try {
                return Utility.getgson().fromJson(Utility.getPreferences(context, Constants.INSTANT, "") as String, INIModel::class.java)
            }
            catch (e : Exception){
                return null
             }
        }

    }


 }