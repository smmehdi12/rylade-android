package com.senarios.customer.models

import com.google.gson.JsonArray

/*model class for key pair values*/
class ModelCommunication() {
    var array:JsonArray = JsonArray()
    var key : String = ""
    var value: String= ""
    var isJsonArray=false
    constructor(key: String, value: String) : this(){
    this.key=key
        this.value=value
    }
    constructor(key: String, value: JsonArray,isJsonArray:Boolean) : this(){
        this.key=key
        this.array=value
        this.isJsonArray=isJsonArray
    }
}