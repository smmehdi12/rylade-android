package com.senarios.customer.models

import android.os.Parcelable
import android.view.View
import androidx.databinding.BindingAdapter
import com.google.gson.annotations.SerializedName
import com.senarios.customer.costants.Constants
import com.senarios.customer.retrofit.APIConstants
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.parcel.Parcelize

@Parcelize
class MedicineModel (
    @SerializedName(Constants.ID ) var id:Int?,
    @SerializedName(Constants.ORDER_ID) var orderId: Int,
    @SerializedName(Constants.PARCEL_ITEM_PHOTO ) var photo:String?,
    @SerializedName(Constants.NAME ) var name:String?,
    @SerializedName(Constants.MEDICINE_QUANTITY ) var quantity:String?,
    @SerializedName(Constants.MEDICINE_DOSE ) var dose:String?,
    @SerializedName(Constants.MEDICINE_TYPE ) var type:String?,
    @SerializedName(Constants.MEDICINE_INSTRUCTION )  var instruction:String?,
    @SerializedName(Constants.UPDATED_AT) var updatedAt: String?,
    @SerializedName(Constants.CREATED_AT) var createdAt: String?,
    @SerializedName("instance") var instance: Boolean,
    @SerializedName(Constants.ORDER_TIME) var orderTime: String?,
    @SerializedName(Constants.ORDER_DATE) var orderDate: String?


    ): Parcelable{



}
@BindingAdapter("loadImageMedicine")
fun loadImageMedicine(imageview: View, photo: String?) {
    if (photo.isNullOrEmpty()) {
        imageview.visibility = View.GONE
    } else {
        Picasso.with(imageview.context).load(APIConstants.BASE_MEDICINE_IMAGE + photo).into(imageview as CircleImageView)
    }
}