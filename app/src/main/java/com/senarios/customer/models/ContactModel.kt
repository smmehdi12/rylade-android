package com.senarios.customer.models


import com.google.gson.annotations.SerializedName

 class ContactModel(
    @SerializedName("address")
    var address: String?,
    @SerializedName("created_at")
    var createdAt: String?,
    @SerializedName("email")
    var email: String?,
    @SerializedName("facebook_link")
    var facebookLink: String?,
    @SerializedName("id")
    var id: Int?,
    @SerializedName("instagram_link")
    var instagramLink: String?,
    @SerializedName("phone_number")
    var phoneNumber: String?,
    @SerializedName("updated_at")
    var updatedAt: String?,
    @SerializedName("website_link")
    var websiteLink: String?
)