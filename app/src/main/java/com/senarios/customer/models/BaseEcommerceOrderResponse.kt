package com.senarios.customer.models

import com.google.gson.annotations.SerializedName

class BaseEcommerceOrderResponse(@SerializedName("items" )var list:MutableList<CartModel>,
                                 @SerializedName("address") var address: Address)