package com.senarios.customer.models

import android.view.View
import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.facebook.internal.Utility

class BindingAdaptors()

@BindingAdapter(value = arrayOf("setTitle","setText","setType"),requireAll = false)
fun setTitleText(view: View,title:String?,text:String?,type:String?){
    if (text.isNullOrEmpty()){
        view.visibility=View.GONE
    }
    else{
        view.visibility=View.VISIBLE
    }
    if (view is TextView){
        (view as TextView).text=title+ text +  type  }
}


@BindingAdapter("setdefault")
fun setdefault(mTextView: View,mID : Int){
    if (mTextView == null || com.senarios.customer.utility.Utility.getSelectedAddress(mTextView.context)==null){
        return;
    }
    if (com.senarios.customer.utility.Utility.getSelectedAddress(mTextView.context)!!.id.equals(mID)){
        mTextView.visibility=View.GONE
    }
    else{
        mTextView.visibility=View.VISIBLE
    }


}