package com.senarios.customer.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class Address (@SerializedName("id") var id : Int,
               @SerializedName("user_id") var user_id : Int,
               @SerializedName("address_type") var address_type : String,
               @SerializedName("building") var building : String,
               @SerializedName("street") var street : String,
               @SerializedName("area") var area : String,
               @SerializedName("floor") var floor : String,
               @SerializedName("rider_note") var rider_note : String,
               @SerializedName("town") var town : String,
               @SerializedName("other_title") var other_title : String,
               @SerializedName("created_at") var created_at : String,
               @SerializedName("updated_at") var updated_at : String,
               @SerializedName("address") var address : String,
               @SerializedName("lat") var lat : String,
               @SerializedName("lon") var lon : String,
    @SerializedName("manual_address") var manual_address : String
) : Parcelable {

}