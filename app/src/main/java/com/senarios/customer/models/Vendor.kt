package com.senarios.customer.models


import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.media.Image
import android.os.Parcelable
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import androidx.palette.graphics.Palette
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.target.Target
import com.bumptech.glide.request.transition.Transition
import com.google.gson.annotations.SerializedName
import com.senarios.customer.R
import com.senarios.customer.retrofit.APIConstants.Companion.BASE_VENDOR_PHOTO
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(tableName = "Vendor")
class Vendor(
    @SerializedName("category_id")
    @ColumnInfo(name = "category_id")
    var categoryId: Int?,
    @SerializedName("created_at")
    @ColumnInfo(name = "created_at")
    var createdAt: String?,
    @SerializedName("email")
    @ColumnInfo(name = "email")
    var email: String?,
    @SerializedName("email_verified_at")
    @ColumnInfo(name = "email_verified_at")
    var emailVerifiedAt: String?,
    @SerializedName("f_name")
    @ColumnInfo(name = "f_name")
    var fName: String?,
    @SerializedName("fcm")
    @ColumnInfo(name = "fcm")
    var fcm: String?,
    @SerializedName("id")
    @ColumnInfo(name = "id")
    @PrimaryKey(autoGenerate = false)
    var id: Int?,
    @SerializedName("image")
    @ColumnInfo(name = "image")
    var image: String?,
    @SerializedName("is_verified")
    @ColumnInfo(name = "is_verified")
    var isVerified: Boolean?,
    @SerializedName("l_name")
    @ColumnInfo(name = "l_name")
    var lName: String?,
    @SerializedName("login_facebook")
    @ColumnInfo(name = "login_facebook")
    var loginFacebook: Boolean?,
    @SerializedName("model")
    @ColumnInfo(name = "model")
    var model: String?,
    @SerializedName("no_plate")
    @ColumnInfo(name = "no_plate")
    var noPlate: String?,
    @SerializedName("otp")
    @ColumnInfo(name = "otp")
    var otp: String?,
    @SerializedName("phone_number")
    @ColumnInfo(name = "phone_number")
    var phoneNumber: String?,
    @SerializedName("rider_availability")
    @ColumnInfo(name = "rider_availability")
    var riderAvailability: String?,
    @SerializedName("rider_work_status")
    @ColumnInfo(name = "rider_work_status")
    var riderWorkStatus: String?,
    @SerializedName("service_area_id")
    @ColumnInfo(name = "service_area_id")
    var serviceAreaId: String?,
    @SerializedName("token")
    @ColumnInfo(name = "token")
    var token: String?,
    @SerializedName("updated_at")
    @ColumnInfo(name = "updated_at")
    var updatedAt: String?,
    @SerializedName("user_status")
    @ColumnInfo(name = "user_status")
    var userStatus: Int?,
    @SerializedName("user_type")
    @ColumnInfo(name = "user_type")
    var userType: String?,
    @ColumnInfo(name = "is_delivery")
    @SerializedName("is_delivery")
    var is_delivery : Int

):Parcelable

@BindingAdapter("loadimage")
fun loadImage(view: View,image:String?){
   Glide.with(view.context)
      .load(BASE_VENDOR_PHOTO+image)
       .apply(RequestOptions().timeout(20000))
       .diskCacheStrategy(DiskCacheStrategy.DATA)
       //apply(RequestOptions.overrideOf(100,100))
      .into(view as ImageView)

}
@BindingAdapter(value = ["loadimage", "loadname"],requireAll = true)
fun loadVendorImageAndName(view: View,image:String?,textView: TextView){
    Glide.with(view.context)
        .asBitmap()
        .load(BASE_VENDOR_PHOTO+image)
        .apply(RequestOptions().timeout(20000))
        .diskCacheStrategy(DiskCacheStrategy.DATA)
        //apply(RequestOptions.overrideOf(100,100))
        .into(object : CustomTarget<Bitmap>() {
            override fun onLoadCleared(placeholder: Drawable?) {

            }

            override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                (view as ImageView).setImageBitmap(resource)
                Palette.from(resource).generate {
                    if (it != null) {
                        textView.setTextColor(it.getDarkVibrantColor(ContextCompat.getColor(view.context, R.color.colorPrimary)))
                    }
                }
            }

        })
}