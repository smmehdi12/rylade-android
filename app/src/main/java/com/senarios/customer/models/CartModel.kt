package com.senarios.customer.models

import android.os.Parcelable
import android.view.View
import androidx.databinding.BindingAdapter
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(tableName = "Cart")
class CartModel(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    var id:Long?,
    @SerializedName("category_id")
    @ColumnInfo(name = "category_id")
    var categoryId: Int?,
    @SerializedName("category_name")
    @ColumnInfo(name = "category_name")
    var categoryName: String?,
    @SerializedName("created_at")
    @ColumnInfo(name = "created_at")
    var createdAt: String?,
    @SerializedName("description")
    @ColumnInfo(name = "description")
    var description: String?,
    @SerializedName("product_id")
    @ColumnInfo(name = "product_id")
    var product_id: Int?,
    @SerializedName("image")
    @ColumnInfo(name = "image")
    var image: String?,
    @SerializedName("price")
    @ColumnInfo(name = "price")
    var price: Int?,
    @SerializedName("product_name")
    @ColumnInfo(name = "product_name")
    var productName: String?,
    @SerializedName("promo_code_id")
    @ColumnInfo(name = "promo_code_id")
    var promoCodeId: Int?,
    @SerializedName("quantity_available")
    @ColumnInfo(name = "quantity_available")
    var quantityAvailable: Int?,
    @SerializedName("status")
    @ColumnInfo(name = "status")
    var status: Int?,
    @SerializedName("sub_category_name")
    @ColumnInfo(name = "sub_category_name")
    var subCategoryName: String?,
    @SerializedName("updated_at")
    @ColumnInfo(name = "updated_at")
    var updatedAt: String?,
    @SerializedName("weight")
    @ColumnInfo(name = "weight")
    var weight: Int?,
    @SerializedName("vendor_id")
    @ColumnInfo(name = "vendor_id")
    var vendorId: Int?,
    @SerializedName("vendor_name")
    @ColumnInfo(name = "vendor_name")
    var vendor_name: String?,
    @SerializedName("quantity")
    @ColumnInfo(name = "quantity")
    var quantity: Int?,
    @SerializedName("order_id")
    @ColumnInfo(name = "order_id")
    var order_id: Int?,
    @SerializedName("sales_tax")
    @ColumnInfo(name = "sales_tax")
    var sales_tax: Double,
    @SerializedName("fixed_discount")
    @ColumnInfo(name = "fixed_discount")
    var fixed_discount: Int?,
    @SerializedName("iss_discount")
    @ColumnInfo(name = "iss_discount")
    var iss_discount: Int?,
    @ColumnInfo(name = "originalPrice")
    var originalPrice: Int?,
    @SerializedName("discounted_price")
    @ColumnInfo(name = "discounted_price")
    var discounted_price: Int?,
    @ColumnInfo(name = "isOutStock")
    var isOutStock:Boolean


) :Parcelable

@BindingAdapter("setoutstock")
fun setoutstock(view: View,isOutStock: Boolean){
    if (isOutStock){
        view.visibility=View.VISIBLE
    }

}