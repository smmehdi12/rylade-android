package com.senarios.customer.models

import com.google.gson.annotations.SerializedName

class OrderListsModel(@SerializedName("pending") val pending : MutableList<OrderModel>?,
                      @SerializedName("completed") val completed : MutableList<OrderModel>?) {
}