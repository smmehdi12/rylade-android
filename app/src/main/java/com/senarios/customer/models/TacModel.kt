package com.senarios.customer.models


import com.google.gson.annotations.SerializedName

data class TacModel(
    @SerializedName("created_at")
    var createdAt: Any?,
    @SerializedName("id")
    var id: Int?,
    @SerializedName("term_and_conditions")
    var termAndConditions: String?,
    @SerializedName("updated_at")
    var updatedAt: Any?
)