package com.senarios.customer.models

import android.os.Parcelable
import android.view.View
import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.google.gson.annotations.SerializedName
import com.senarios.customer.R
import com.senarios.customer.costants.Constants
import com.senarios.customer.utility.Utility
import kotlinx.android.parcel.Parcelize

@Parcelize
class OrderModel (@SerializedName(Constants.ID) val id : Int,
                  @SerializedName(Constants.ORDER_TYPE) val order_type : Int?,
                  @SerializedName(Constants.USER_ID) val user_id : Int?,
                  @SerializedName(Constants.CREATED_AT) val created_at : String?,
                  @SerializedName(Constants.UPDATED_AT) val updated_at : String?,
                  @SerializedName(Constants.ADDRESS_ID) val address_id : Int?,
                  @SerializedName(Constants.AMOUNT) val amount : String?,
                  @SerializedName(Constants.STATUS) val status : String?,
                  @SerializedName(Constants.RIDER_ID) val rider_id : Int?,
                  @SerializedName(Constants.DELIVERY_FEES)var deliveryFees: String?,
                  @SerializedName(Constants.IS_PROMO) var isPromo: Int?,
                  @SerializedName(Constants.PROMO_AMOUNT) var promoAmount: String?,
                  @SerializedName(Constants.PROMO_ID) var promoId: Int?,
                  @SerializedName(Constants.SUB_TOTAL) var subTotal: String?,
                  @SerializedName(Constants.ORDER_TIME) var orderTime: String?,
                  @SerializedName(Constants.ORDER_DATE) var orderDate: String?,
    @SerializedName(Constants.IS_VENDOR) var is_vendor: String?,


    @SerializedName(Constants.IS_RATED) var is_rated:Int,

    @SerializedName(Constants.VENDOR_ID) var vendor_id:Int
    ):Parcelable

@BindingAdapter("setOrderBackground")
fun setOrderBackground(view: View, status: String?) {
    if (status!=null) {
        Utility.showLog("Order Status "+status)
        if (status.equals(Constants.ORDER_STATUS_PENDING, true) || status.equals(Constants.ORDER_STATUS_APPROVED, true) || status.equals(Constants.ORDER_STATUS_WAITING, true) || status.equals(Constants.ORDER_STATUS_PFA, true) || status.equals(Constants.ORDER_STATUS_PV, true) ) {
            view.setBackgroundResource(R.drawable.status_completed)
            (view as TextView).text="Processing"
        } else if (status.equals(Constants.ORDER_STATUS_CANCELLED, true)) {
            view.setBackgroundResource(R.drawable.status)
            (view as TextView).text="Cancelled"
        }
        else if (status.equals(Constants.ORDER_STATUS_ASSIGNED, true)) {
            view.setBackgroundResource(R.drawable.status_in_progress)
            (view as TextView).text="In Progress"
        }
        else if (status.equals(Constants.ORDER_STATUS_COMPLETED, true)) {
            view.setBackgroundResource(R.drawable.status_completed)
            (view as TextView).text="Completed"
        }
        else if (status.equals(Constants.ORDER_STATUS_RBV, true)) {
            view.setBackgroundResource(R.drawable.status)
            (view as TextView).text="Rejected"
        }
    }
}

@BindingAdapter("setOrderStatus")
fun setOrderStatus(view: View, status: String?) {
    if (status!=null) {
        if (status.equals(Constants.ORDER_STATUS_PENDING, true) || status.equals(Constants.ORDER_STATUS_APPROVED, true) || status.equals(Constants.ORDER_STATUS_WAITING, true) || status.equals(Constants.ORDER_STATUS_PFA, true) ) {
            (view as TextView).text="Order Status : Processing"
        } else if (status.equals(Constants.ORDER_STATUS_CANCELLED, true)) {
            (view as TextView).text="Order Status : Cancelled"
        }
        else if (status.equals(Constants.ORDER_STATUS_ASSIGNED, true)) {
            (view as TextView).text="Order Status : In Progress"
        }
        else if (status.equals(Constants.ORDER_STATUS_COMPLETED, true)) {
            (view as TextView).text="Order Status : Completed"
        }
    }
}


@BindingAdapter("setDeliveryFee")
fun setDeliveryFee(view: View,text: String?){
    if (text.isNullOrEmpty()){
        view.visibility=View.GONE
    }
    else{
        (view as TextView).text="Delivery Fee : Rs. "+text.toDouble()
    }
}
@BindingAdapter("setPromoAmount")
fun setPromoAmount(view: View,text: String?){
    if (text.isNullOrEmpty()){
        view.visibility=View.GONE
    }
    else{
        (view as TextView).text="Promo Amount : Rs. "+text.toDouble()
    }
}
@BindingAdapter("setView")
fun setView(view: View,text: String?){
    if (text.isNullOrEmpty()){
        view.visibility=View.GONE
    }
    else{
        view.visibility=View.VISIBLE
    }
}

