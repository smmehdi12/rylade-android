package com.senarios.customer.models
import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(tableName = "MainCategory")
 class MainCategoryModel(
    @SerializedName("created_at")
    @ColumnInfo(name = "created_at",defaultValue = "")
    var createdAt: String?,
    @SerializedName("id")
    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name ="id" )
    var id: Int?,
    @SerializedName("name")
    @ColumnInfo(name = "name")
    var name: String?,
    @SerializedName("updated_at")
    @ColumnInfo(name = "updated_at")
    var updatedAt: String?,
   @SerializedName("image_url")
   @ColumnInfo(name = "image_url")
   var image_url: String?


):Parcelable