package com.senarios.customer.models

import com.google.gson.annotations.SerializedName
import com.senarios.customer.costants.Constants

class RideModel (
    var pickup_address:String?,
    var pickup_lat:String?,
    var pickup_lon:String?,
    var dropoff_address:String?,
    var dropoff_lat:String?,
    var dropoff_lon:String?,
    @SerializedName("distance_km") var distance:String?,
    var amount:String?,
    var duration:String?,
    @SerializedName(Constants.ESTIMATE_FARE) var estimate_fare : String?,
    @SerializedName(Constants.INITIAL_FARE) var initial_fare : String?,
    @SerializedName(Constants.TOTAL_FARE) var total_Fare : String?,
    @SerializedName(Constants.ESTIMATE_TIME) var estimate_time : String?,
    @SerializedName(Constants.CALCULATED_FARE) var calculated_fare : String?,
    @SerializedName(Constants.WAITING_TIME) var waiting_time : String?,
    @SerializedName(Constants.WAITING_COST) var waiting_cost : String?,
    @SerializedName(Constants.LAST_TIME) var last_time : String?,
    @SerializedName(Constants.TOTAL_TIME) var total_time : String?,
    @SerializedName(Constants.LAST_LAT) var last_lat : String?,
    @SerializedName(Constants.LAST_LNG) var last_lng : String?,
    @SerializedName(Constants.STATUS) var status : String?,
    @SerializedName(Constants.ESTIMATE_DISTANCE) var estimated_distance : String?
)