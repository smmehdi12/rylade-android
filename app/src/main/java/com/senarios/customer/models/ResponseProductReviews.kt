package com.senarios.customer.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ResponseProductReviews(

	@field:SerializedName("average")
	val average: String? = null,

	@field:SerializedName("comments")
	val comments: List<CommentsItem?>? = null,

	@field:SerializedName("vendor")
	val vendor: Vendor? = null
) : Parcelable



@Parcelize
data class CommentsItem(

	@field:SerializedName("updated_at")
	val updatedAt: String? = null,

	@field:SerializedName("user_id")
	val userId: Int? = null,

	@field:SerializedName("biker_id")
	val bikerId: String? = null,

	@field:SerializedName("review")
	val review: Int? = null,

	@field:SerializedName("user_name")
	val userName: String? = null,

	@field:SerializedName("product_id")
	val productId: Int? = null,

	@field:SerializedName("vendor_id")
	val vendorId: Int? = null,

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("comment")
	val comment: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("order_id")
	val orderId: Int? = null
) : Parcelable
