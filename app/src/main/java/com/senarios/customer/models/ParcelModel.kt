package com.senarios.customer.models

import android.view.View
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.gson.annotations.SerializedName
import com.senarios.customer.costants.Constants
import com.senarios.customer.retrofit.APIConstants

class ParcelModel (@SerializedName(Constants.ID) var id: Int,
                   @SerializedName(Constants.ORDER_ID) var orderId: Int,
                   @SerializedName(Constants.PARCEL_ITEM_PHOTO)var photo:String?,
                   @SerializedName(Constants.PARCEL_ITEM_NAME)var item_name:String?,
                   @SerializedName(Constants.PARCEL_ITEM_WEIGHT) var item_weight:String?,
                   @SerializedName(Constants.PARCEL_SENDER_NAME)var sender_name:String?,
                   @SerializedName(Constants.PARCEL_SENDER_PHONE)var sender_phone:String?,
                   @SerializedName(Constants.PARCEL_SENDER_ADDRESS)var sender_address:String?,
                   @SerializedName(Constants.SENDER_LAT)var sender_lat:String?,
                   @SerializedName(Constants.SENDER_LNG)var sender_lon:String?,
                   @SerializedName(Constants.PARCEL_RECEIVER_NAME)var receiver_name:String?,
                   @SerializedName(Constants.PARCEL_RECEIVER_PHONE)var receiver_phone:String?,
                   @SerializedName(Constants.PARCEL_RECEIVER_ADDRESS)var receiver_address:String?,
                   @SerializedName(Constants.PARCEL_RECEIVER_LAT)var receiver_lat:String?,
                   @SerializedName(Constants.PARCEL_RECEIVER_LNG)var receiver_lon:String?,
                   @SerializedName(Constants.UPDATED_AT) var updatedAt: String?,
                   @SerializedName(Constants.CREATED_AT) var createdAt: String?,
                   @SerializedName(Constants.ORDER_TIME) var orderTime: String?,
                   @SerializedName(Constants.ORDER_DATE) var orderDate: String?,
                   @SerializedName(Constants.MANUAL_SENDER_ADDRESS) var manualSenderAddress: String?,
                   @SerializedName(Constants.MANUAL_RECEIVER_ADDRESS) var manualRecieverAddress: String?,

    var amount: String?){

}
@BindingAdapter("photo")
fun photo(imageview: View, photo: String?) {
    Glide.with(imageview.context)
        .load(APIConstants.BASE_PARCEL_IMAGE + photo)
        .apply(RequestOptions().timeout(20000))
        .into(imageview as ImageView)
}