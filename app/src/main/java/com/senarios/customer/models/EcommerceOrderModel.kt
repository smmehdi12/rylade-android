package com.senarios.customer.models

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import com.senarios.customer.BR

class EcommerceOrderModel() : BaseObservable() {


    @get:Bindable
    var totalAmount:Double=0.0
    set(value) {
        field=value
        notifyPropertyChanged(BR.totalAmount)
    }
    @get:Bindable
    var subTotal:Double=0.0
        set(value) {
            field=value
            notifyPropertyChanged(BR.subTotal)
        }

    @get:Bindable
    var isPromo:Boolean=false


    @get:Bindable
    var promoID:Int=0
        set(value) {
            field=value
            notifyPropertyChanged(BR.promoID)
        }

    @get:Bindable
    var promoAmount:Double=0.0
        set(value) {
            field=value
            notifyPropertyChanged(BR.promoAmount)
        }

    @get:Bindable
    var addressID:Int=0
        set(value) {
            field=value
            notifyPropertyChanged(BR.addressID)
        }

    @get:Bindable
    var vendorID:Int=0
        set(value) {
            field=value
            notifyPropertyChanged(BR.vendorID)
        }

    @get:Bindable
    var address:String=""
        set(value) {
            field=value
            notifyPropertyChanged(BR.address)
        }

    @get:Bindable
    var deliveryFee:Double=0.0
        set(value) {
            field=value
            notifyPropertyChanged(BR.deliveryFee)
        }

    @get:Bindable
    var promoTitle:String=""
        set(value) {
            field=value
            notifyPropertyChanged(BR.promoTitle)
        }

    @get:Bindable
    var taxAmount:Double=0.0
        set(value) {
            field=value
            notifyPropertyChanged(BR.taxAmount)
        }

    @get:Bindable
    var promoPercent:Double=0.0
        set(value) {
            field=value
            notifyPropertyChanged(BR.promoPercent)
        }

    fun reset() {
         promoPercent=0.0
         taxAmount=0.0
        promoAmount=0.0
        subTotal=0.0
        totalAmount=0.0
        promoTitle=""
        promoID=0
    }

}