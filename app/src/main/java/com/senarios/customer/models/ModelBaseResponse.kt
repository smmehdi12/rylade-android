package com.senarios.customer.models

/*base response class wrapped in retrofit response class which returns the response in deserialized kotlin pojo*/
data class ModelBaseResponse(
    val status : Boolean,
    val error : Boolean,
    val message : String,
    val data : Any
)