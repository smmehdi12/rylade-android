package com.senarios.customer.models

import com.google.gson.annotations.SerializedName
import com.senarios.customer.costants.SPConstants

/*model class for users*/
class User (
    @SerializedName("id") var id : Int,
    @SerializedName("f_name") var f_name : String,
    @SerializedName("l_name") var l_name : String,
    @SerializedName("phone_number") var phone_number : String,
    @SerializedName("user_type") var user_type : String,
    @SerializedName("status") var status : Int,
    @SerializedName("email") var email : String,
    @SerializedName("email_verified_at") var email_verified_at : String,
    @SerializedName("token") var token : String,
    @SerializedName("otp") var otp : String,
    @SerializedName("is_verified") var is_verified : Boolean,
    @SerializedName("created_at") var created_at : String,
    @SerializedName("updated_at") var updated_at : String,
    @SerializedName("login_facebook") var login_facebook : Boolean,
    @SerializedName("is_delivery") var is_delivery : Boolean,
    @SerializedName(SPConstants.FCM) var fcm_ID:String?,
    @SerializedName("no_plate") var no_plate:String?,
    @SerializedName("model") var model:String?

){
}