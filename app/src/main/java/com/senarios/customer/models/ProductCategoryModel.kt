package com.senarios.customer.models

import com.google.gson.annotations.SerializedName

class ProductCategoryModel (
    @SerializedName("id")
    val id:Int,
    @SerializedName("description")
    val descrip:String,
    @SerializedName("price")
    val price:Int,
    @SerializedName("quantity_available")
    val quantity_value:Int,
    @SerializedName("category_name")
    val category_name:String,
    @SerializedName("sub_category_name")
    val sub_cat_name:String,
    @SerializedName("created_at")
    val created_date:String,
    @SerializedName("updated_at")
    val updated_at:String,
    @SerializedName("product_name")
    val product_name:String






){
}