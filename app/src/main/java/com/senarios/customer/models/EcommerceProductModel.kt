package com.senarios.customer.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class EcommerceProductModel (
    var id:Long?,
    @SerializedName("category_id")
    var categoryId: Int?,
    @SerializedName("category_name")
    var categoryName: String?,
    @SerializedName("created_at")
    var createdAt: String?,
    @SerializedName("description")
    var description: String?,
    @SerializedName("product_id")
    var product_id: Int?,
    @SerializedName("image")
    var image: String?,
    @SerializedName("price")
    var price: Int?,
    @SerializedName("product_name")
    var productName: String?,
    @SerializedName("promo_code_id")
    var promoCodeId: Int?,
    @SerializedName("quantity_available")
    var quantityAvailable: Int?,
    @SerializedName("status")
    var status: Int?,
    @SerializedName("sub_category_name")
    var subCategoryName: String?,
    @SerializedName("updated_at")
    var updatedAt: String?,
    @SerializedName("weight")
    var weight: Int?,
    @SerializedName("vendor_id")
    var vendorId: Int?,
    @SerializedName("vendor_name")
    var vendor_name: String?,
    @SerializedName("quantity")
    var quantity: Int?,
    @SerializedName("order_id")
    var order_id: Int?



) : Parcelable