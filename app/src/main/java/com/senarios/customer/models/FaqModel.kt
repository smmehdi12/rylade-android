package com.senarios.customer.models


import com.google.gson.annotations.SerializedName

data class FaqModel(
    @SerializedName("answer")
    var answer: String?,
    @SerializedName("created_at")
    var createdAt: String?,
    @SerializedName("id")
    var id: Int?,
    @SerializedName("question")
    var question: String?,
    @SerializedName("updated_at")
    var updatedAt: String?
)