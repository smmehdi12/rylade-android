package com.senarios.customer.activities

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.google.gson.reflect.TypeToken
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItemAdapter
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems
import com.senarios.customer.R
import com.senarios.customer.callbacks.FragmentChanger
import com.senarios.customer.costants.Constants
import com.senarios.customer.databinding.ActivityEcommerceBinding
import com.senarios.customer.databinding.FragmentVendorsBinding
import com.senarios.customer.fragments.VendorFragment
import com.senarios.customer.models.*
import com.senarios.customer.retrofit.ApiResponse
import com.senarios.customer.utility.Utility
import kotlinx.android.synthetic.main.toolbar.view.*
import retrofit2.Response
import java.lang.reflect.Type

class EcommerceActivity : BaseAcivity(),View.OnClickListener,ApiResponse ,FragmentChanger,Observer<MutableList<MainCategoryModel>>{
    private lateinit var binding: ActivityEcommerceBinding
    private lateinit var adapter: FragmentPagerItemAdapter
    private lateinit var creator:FragmentPagerItems.Creator
    private lateinit var menuItem: MenuItem
    private var main_cateogry:Int=-1;


    override fun setBinding() {
        binding=DataBindingUtil.setContentView(this,R.layout.activity_ecommerce)
    }

    override fun init() {
        binding.toolbar.toolbar.title="Vendors"
        binding.toolbar.toolbar.inflateMenu(R.menu.cart_menu)
        setSupportActionBar(binding.toolbar.toolbar)
        binding.toolbar.toolbar.setNavigationOnClickListener(this)
//        NetworkCall.callAPI(this,getService().getMainCategories(),this,true,APIConstants.GET_MAIN_CATEGORIES)

        if (intent!=null){
            val vendorFragment=VendorFragment()
            vendorFragment.arguments=getBundle(intent.getIntExtra(Constants.MAIN_CATEGORY,0))
            supportFragmentManager.beginTransaction().replace(binding.container.id,vendorFragment).commitAllowingStateLoss()
        }
        else{
            finish()
        }

    }

    override fun OnCancelPermissioDialog() {

    }

    override fun OnPermissionApproved() {

    }

    override fun OnTrigger() {

    }

    override fun OnSuccess(endpoint: String, t: Response<ModelBaseResponse>, response: String) {
        try{
            val listType: Type = object : TypeToken<List<MainCategoryModel?>?>() {}.getType()
            val list = Utility.getgson().fromJson<MutableList<MainCategoryModel>>(response, listType)
            if (list.isNotEmpty()){
                Utility.getDB().clearMainCategoryTable()
                Utility.getDB().insertMainCategories(list)
            }
            observeDB()
        }
        catch (e:Exception){
            Utility.showLog("Ecommerce Activity"+e.message)
        }

    }

    override fun OnStatusfalse(endpoint:String,t: Response<ModelBaseResponse>, response: String, message: String) {
        observeDB()
    }

    override fun OnError(endpoint:String,code: Int, message: String) {
        observeDB()
    }


    override fun OnException(endpoint:String,exception: Throwable) {
        observeDB()
    }

    override fun OnNetworkError(endpoint:String,message: String) {
        observeDB()
    }

    private fun getBundle(item: Int): Bundle? {
        val b = Bundle()
        b.putInt(Constants.CATEGORY_ID, item)
        return b
    }

    override fun OnChange(fragment: Fragment, tag: String) {

    }

    override fun onChanged(t: MutableList<MainCategoryModel>?) {
            if (t!!.isNotEmpty()) {
                creator = FragmentPagerItems.with(this)
                for (item in t) {
                    creator.add(item.name, VendorFragment::class.java, getBundle(item.id!!))
                }
                adapter = FragmentPagerItemAdapter(supportFragmentManager, creator.create())
                binding.viewpager.adapter = adapter
                binding.viewpagertab.setViewPager(binding.viewpager)
                binding.group.visibility = View.VISIBLE
            }
        observeCart()
    }

    private fun observeCart() {
        Utility.getDB().getCart().observe(this, Observer<MutableList<CartModel>> { t ->
            if (isMenu()) {
                if (!t.isNullOrEmpty()) {
                    menuItem.isVisible = true
                    updateMenuItem(t.size)

                } else {
                    menuItem.isVisible = false
                }
            }
        })
    }

   private fun observeDB(){
        Utility.getDB().getMainCategories().observe(this,this)
    }



    @SuppressLint("SetTextI18n")
    private fun updateMenuItem(count:Int){
        val view=menuItem.actionView
        val tvCounter=view.findViewById<TextView>(R.id.tv_counter)
        tvCounter.text=""+count

    }
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.cart_menu,menu)
        menuItem=menu!!.findItem(R.id.cart)
        (menuItem.actionView as ConstraintLayout).setOnClickListener(this)
        observeCart()
        return true
    }

    private fun isMenu():Boolean{
        return ::menuItem.isInitialized
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId==R.id.search){
            startActivity(Intent(this@EcommerceActivity,SearchActivity::class.java))
        }

        return true
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.cartView -> {
                startActivity(Intent(this@EcommerceActivity, CartActivity::class.java))
            }
            else -> {
                finish()
            }
        }
    }


}