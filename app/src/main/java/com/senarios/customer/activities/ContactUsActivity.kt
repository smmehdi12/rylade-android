package com.senarios.customer.activities

import android.util.Log
import android.view.View
import com.google.gson.reflect.TypeToken
import com.senarios.customer.R
import com.senarios.customer.costants.Messages
import com.senarios.customer.models.ContactModel
import com.senarios.customer.models.ModelBaseResponse
import com.senarios.customer.retrofit.APIConstants
import com.senarios.customer.retrofit.ApiResponse
import com.senarios.customer.retrofit.NetworkCall
import com.senarios.customer.utility.Utility
import mehdi.sakout.aboutpage.AboutPage
import retrofit2.Response
import java.lang.reflect.Type


class ContactUsActivity : BaseAcivity() , ApiResponse {
    private val TAG=javaClass.name


    override fun setBinding() {
//        binding=DataBindingUtil.setContentView(this, R.layout.activity_faq)
    }

    override fun init() {
        NetworkCall.callAPI(this,getService().getContactUS(),this,true,APIConstants.GET_CONTACT_US)
    }

    override fun OnCancelPermissioDialog() {

    }

    override fun OnPermissionApproved() {

    }

    override fun OnTrigger() {

    }

    override fun OnSuccess(endpoint: String, t: Response<ModelBaseResponse>, response: String) {
        val listType: Type = object : TypeToken<List<ContactModel>>() {}.getType()
        val list= Utility.getgson().fromJson<List<ContactModel>>(response, listType)
        if(list.isNotEmpty()) {
            val aboutPage: View = AboutPage(this).setDescription("").isRTL(false).setImage(R.drawable.ic_android_black).addGroup("Connect with us").addEmail(list[0].email).addWebsite(list[0].websiteLink).addFacebook(list[0].facebookLink).addInstagram(list[0].instagramLink).create()
            setContentView(aboutPage)
        }
        Log.v(TAG,""+list.size)
    }

    override fun OnStatusfalse(endpoint:String,t: Response<ModelBaseResponse>, response: String, message: String) {
        Utility.showErrorDialog(this@ContactUsActivity,R.layout.dialog_error_505, Messages.ERROR_TITLE,message)
    }

    override fun OnError(endpoint:String,code: Int, message: String) {
        Utility.showErrorDialog(this@ContactUsActivity,R.layout.dialog_error_505, Messages.ERROR_TITLE,message)
    }


    override fun OnException(endpoint:String,exception: Throwable) {
        Utility.showErrorDialog(this@ContactUsActivity,R.layout.dialog_error, Messages.ERROR_TITLE,getString(R.string.something_went_wrong))
    }

    override fun OnNetworkError(endpoint:String,message: String) {
        Utility.showNErrorDialog(this@ContactUsActivity)
    }
}