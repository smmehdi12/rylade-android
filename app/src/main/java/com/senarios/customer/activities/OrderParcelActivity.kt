package com.senarios.customer.activities

import android.app.Activity
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Intent
import android.database.Cursor
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.provider.ContactsContract
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.CompoundButton
import android.widget.DatePicker
import android.widget.TimePicker
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.gms.maps.model.LatLng
import com.senarios.customer.R
import com.senarios.customer.costants.Codes
import com.senarios.customer.costants.Constants
import com.senarios.customer.costants.Messages
import com.senarios.customer.databinding.FragmentParcelPickupDetailsBinding
import com.senarios.customer.dialogfragments.LoginSignupDialog
import com.senarios.customer.models.INIModel
import com.senarios.customer.models.ModelBaseResponse
import com.senarios.customer.models.ParcelModel
import com.senarios.customer.models.User
import com.senarios.customer.retrofit.APIConstants
import com.senarios.customer.retrofit.ApiResponse
import com.senarios.customer.retrofit.NetworkCall
import com.senarios.customer.utility.Utility
import com.senarios.customer.viewmodel.SharedVM
import net.alhazmy13.mediapicker.Image.ImagePicker
import retrofit2.Response
import java.util.*


class OrderParcelActivity : BaseAcivity() , ApiResponse,TextWatcher,DatePickerDialog.OnDateSetListener,TimePickerDialog.OnTimeSetListener,CompoundButton.OnCheckedChangeListener,View.OnClickListener,Observer<User>{
    private lateinit var binding:FragmentParcelPickupDetailsBinding
    private lateinit var dialog: AlertDialog
    private lateinit var sharedVM: SharedVM
    var parcelDate:String=""
    private var parcel:ParcelModel? = ParcelModel(-1,-1,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null)



    override fun setBinding() {
        binding=DataBindingUtil.setContentView(this, R.layout.fragment_parcel_pickup_details)
    }

    override fun init() {
        binding.toolbar.setNavigationOnClickListener(this)
        binding.sendersPhonebook.setOnClickListener(this)
        binding.receiversPhonebook.setOnClickListener(this)
        binding.senderAddress.setOnClickListener(this)
        binding.receiversAddress.setOnClickListener(this)
        binding.takeSnapPrescriptionBtn.setOnClickListener(this)
        binding.confirmBtn.setOnClickListener(this)
        binding.nameEdt.addTextChangedListener(this)
        binding.weightEdt.addTextChangedListener(this)
        binding.mapSenderAddress.addTextChangedListener(this)
        binding.mapReceiversAddress.addTextChangedListener(this)
        binding.isInstant.setOnCheckedChangeListener(this)
        binding.isInstant.isChecked=true
        ////
        sharedVM=ViewModelProviders.of(this).get(SharedVM::class.java)
        sharedVM.getLoggedUser().observe(this,this)


     }


    private fun contactIntent(code:Int){
        val intent = Intent(Intent.ACTION_PICK, ContactsContract.CommonDataKinds.Phone.CONTENT_URI)
        startActivityForResult(intent, code)
    }

    override fun OnCancelPermissioDialog() {
        super.OnCancelPermissioDialog()
    }

    override fun OnPermissionApproved() {

    }

    override fun OnTrigger() {

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode== Codes.SENDER_CONTACTS_REQUEST &&resultCode == Activity.RESULT_OK){
            val uri: Uri? = data!!.data
            val cursor: Cursor =getContentResolver().query(uri!!, null, null, null, null)!!

            if (cursor.moveToFirst()) {
                val phoneIndex: Int = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)
                val nameIndex: Int = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME)
                parcel!!.sender_name = cursor.getString(nameIndex)
                parcel!!.sender_phone = cursor.getString(phoneIndex)
                binding.sendersName.setText(parcel!!.sender_name)
                binding.sendersContact.setText(parcel!!.sender_phone)
            }
            cursor.close()
        }
        else if (requestCode== Codes.RECEIVER_CONTACTS_REQUEST &&resultCode == Activity.RESULT_OK){
            val uri: Uri? = data!!.data
            val cursor: Cursor =getContentResolver().query(uri!!, null, null, null, null)!!

            if (cursor.moveToFirst()) {
                val phoneIndex: Int = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)
                val nameIndex: Int = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME)
                parcel!!.receiver_name = cursor.getString(nameIndex)
                parcel!!.receiver_phone = cursor.getString(phoneIndex)
                binding.receiversName.setText(parcel!!.receiver_name)
                binding.receiversContact.setText(parcel!!.receiver_phone)
            }
            cursor.close()
        }
        else if (requestCode== Codes.SENDER_ADDRESS_REQUEST && data!= null){
           val sender_latlng=data!!.getParcelableExtra<LatLng>(Constants.LATLNG_DATA)!!
            parcel!!.sender_lat=sender_latlng.latitude.toString()
            parcel!!.sender_lon=sender_latlng.longitude.toString()
            binding.senderAddress.setText(Utility.getAddress(this,latLng = sender_latlng))
        }
        else if (requestCode== Codes.RECEIVER_ADDRESS_REQUEST && data!= null){
           val receiver_latlng=data!!.getParcelableExtra<LatLng>(Constants.LATLNG_DATA)!!
            parcel!!.receiver_lat=receiver_latlng.latitude.toString()
            parcel!!.receiver_lon=receiver_latlng.longitude.toString()
            binding.receiversAddress.setText(Utility.getAddress(this,latLng = receiver_latlng))
        }
        else if (requestCode == ImagePicker.IMAGE_PICKER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            if (data!=null) {
                val mPaths: List<String> = data.getStringArrayListExtra(ImagePicker.EXTRA_IMAGE_PATH)
                val bitmap= BitmapFactory.decodeFile(mPaths[0])
                if (bitmap!=null){
                    parcel!!.photo=mPaths[0]
                    binding.pictureImg.setImageBitmap(Bitmap.createScaledBitmap(BitmapFactory.decodeFile(mPaths[0]),50,50,false))
                }

            }
        }
    }

    override fun onClick(v: View?) {
        hideSoftKeyboard()
        when(v?.id){
            binding.sendersPhonebook.id->{
                if (!Utility.hasContactPermission(this)){
                    checkContactPermission()
                }
                else{
                    contactIntent(Codes.SENDER_CONTACTS_REQUEST)

                }
            }
            binding.senderAddress.id->{
                startActivityForResult(Intent(this@OrderParcelActivity,MapActivity::class.java),Codes.SENDER_ADDRESS_REQUEST)

            }
            binding.receiversPhonebook.id->{
                if (!Utility.hasContactPermission(this)){
                    checkContactPermission()
                }
                else{
                    contactIntent(Codes.RECEIVER_CONTACTS_REQUEST)

                }
            }
            binding.receiversAddress.id->{
                startActivityForResult(Intent(this@OrderParcelActivity,MapActivity::class.java),Codes.RECEIVER_ADDRESS_REQUEST)
            }
            binding.takeSnapPrescriptionBtn.id->{
                if (!hasPermissions(this, arrayOf(android.Manifest.permission.READ_EXTERNAL_STORAGE,android.Manifest.permission.READ_EXTERNAL_STORAGE))){
                    checkMediaPermission()
                }
                else{
                    Utility.getMediaBuilder(this).build()
                }
            }
            binding.confirmBtn.id->{
                if (Utility.checkIfUserLogged(this)) {
                    ShowInstantDialog()
                    //validateInputs()
                }
                else{
                    showDialog(LoginSignupDialog())
                }
            }

            else->{
                finish()
            }
        }
    }
    /////
    private fun ShowInstantDialog() {
        Utility.getAlertDialoge(this,getString(R.string.InstantTitle),getString(R.string.InstantMessage))
            .setNegativeButton("Not Instant")
            {
                    p0, p1 ->
                Utility.showDatePickerDialog(this,this)
            }
            .setPositiveButton("Instant"
            ) { p0, p1 ->
                parcel!!.orderDate=""
                parcel!!.orderTime=""
                binding!!.isInstant.isChecked=false
                validateInputs()
                 }
            .show()

    }

    private fun validateInputs() {
        hideSoftKeyboard()
        parcel!!.sender_address=Utility.getTextET(binding.senderAddress)
        parcel!!.receiver_address=Utility.getTextET(binding.receiversAddress)
        parcel!!.manualSenderAddress=Utility.getTextET(binding.mapSenderAddress)
        parcel!!.manualRecieverAddress=Utility.getTextET(binding.mapReceiversAddress)
        if (parcel!!.photo ==null || parcel!!.photo!!.isEmpty()){
            getMissingFieldDialog()
        }
        else if (parcel!!.item_name ==null || parcel!!.item_name!!.isEmpty()){
            Utility.setErrorET(binding.nameEdt)
        }
        else if (parcel!!.item_weight ==null || parcel!!.item_weight!!.isEmpty()){
            Utility.setErrorET(binding.weightEdt)
        }
        else if (parcel!!.sender_name ==null || parcel!!.sender_name!!.isEmpty()){
            Utility.setErrorET(binding.sendersName)
        }
        else if (parcel!!.sender_phone ==null || parcel!!.sender_phone!!.isEmpty()){
            Utility.setErrorET(binding.sendersContact)
        }
        else if (parcel!!.receiver_name ==null || parcel!!.receiver_name!!.isEmpty()){
            Utility.setErrorET(binding.receiversName)
        }
        else if (parcel!!.receiver_phone ==null || parcel!!.receiver_phone!!.isEmpty()){
            Utility.setErrorET(binding.receiversContact)
        }
        else if (parcel!!.sender_address ==null || parcel!!.sender_address!!.isEmpty()){
            Utility.setErrorET(binding.senderAddress)
        }
        else if (parcel!!.receiver_address ==null || parcel!!.receiver_address!!.isEmpty()){
            Utility.setErrorET(binding.receiversAddress)
        }
        else if(parcel!!.manualSenderAddress==null||parcel!!.manualSenderAddress!!.isEmpty())
        {
            Utility.setErrorET(binding.mapSenderAddress)
        }
        else if(parcel!!.manualRecieverAddress==null||parcel!!.manualRecieverAddress!!.isEmpty())
        {
            Utility.setErrorET(binding.mapReceiversAddress)

        }
        else{
            NetworkCall.callAPI(this,getService().order_Parcel(
                APIConstants.BEARER+Utility.getUser(this).token
                ,getPart(parcel!!.photo!!,Constants.PARCEL_ITEM_PHOTO)
                ,Utility.getUser(this).id,
                0,
                parcel!!.item_name!!,
                parcel!!.item_weight!!,
                parcel!!.sender_name!!,
                parcel!!.sender_phone!!,
                parcel!!.sender_address!!,
                parcel!!.sender_lat!!,
                parcel!!.sender_lon!!,
                parcel!!.receiver_name!!,
                parcel!!.receiver_phone!!,
                parcel!!.receiver_address!!,
                parcel!!.receiver_lat!!,
                parcel!!.receiver_lon!!,
                parcel!!.orderDate!!,
                parcel!!.orderTime!!,
                parcel!!.manualSenderAddress!!,
                parcel!!.manualRecieverAddress!!,
                parcel!!.amount!!,
                binding.isInstant.isChecked
            ),this,true,APIConstants.ORDER_PARCEL)
        }

    }

    override fun afterTextChanged(s: Editable?) {


    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        parcel!!.item_name=Utility.getTextET(binding.nameEdt)
        parcel!!.item_weight=Utility.getTextET(binding.weightEdt)
        parcel!!.sender_name=Utility.getTextET(binding.sendersName)
        parcel!!.sender_phone=Utility.getTextET(binding.sendersContact)
        parcel!!.receiver_name=Utility.getTextET(binding.receiversName)
        parcel!!.receiver_phone=Utility.getTextET(binding.receiversContact)

    }

    override fun OnSuccess(endpoint: String, t: Response<ModelBaseResponse>, response: String) {
        try{
            ////////
//            Utility.showParcelOrder(this)
            Utility.showFirebaseNotification(this, this.getString(R.string.parcel_notification_title), this.getString(R.string.order_notification_message),Intent(this,MyOrdersActivity::class.java))
            finish()
        }
        catch (e:Exception){
            OnException(endpoint,e)
        }

    }

    override fun OnStatusfalse(endpoint:String,t: Response<ModelBaseResponse>, response: String, message: String) {
        dialog=Utility.showErrorDialog(this,R.layout.dialog_error_505, Messages.ERROR_TITLE,message)
    }

    override fun OnError(endpoint:String,code: Int, message: String) {
        dialog=Utility.showErrorDialog(this,R.layout.dialog_error_505, Messages.ERROR_TITLE,message)
    }


    override fun OnException(endpoint:String,exception: Throwable) {
        dialog=Utility.showErrorDialog(this,R.layout.dialog_error, Messages.ERROR_TITLE,getString(R.string.something_went_wrong))
    }

    override fun OnNetworkError(endpoint:String,message: String) {
        dialog=Utility.showNErrorDialog(this)
    }

    fun isDialog():Boolean{
        return ::dialog.isInitialized
    }

    override fun onResume() {
        super.onResume()
        if (isDialog()&&dialog.isShowing){
            dialog.dismiss()
        }
    }

    override fun onChanged(t: User?) {
        if (t!=null){
            binding.sendersName.setText(""+t.f_name+t.l_name)
            binding.sendersContact.setText(""+t.phone_number)
        }

    }

    override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
        try {
            val instant = Utility.getgson().fromJson<INIModel>(Utility.getPreferences(this, Constants.INSTANT, "").toString(), INIModel::class.java)
            if (isChecked) {
                parcel!!.amount = ""+instant.parcel_instance
            } else {
                parcel!!.amount = ""+instant.parcel_non_instance
            }
        }
        catch (e:Exception){

        }
    }

    override fun onDateSet(p0: DatePicker?, p1: Int, p2: Int, p3: Int) {
        val month = p2 + 1
        val c = Calendar.getInstance()
        c.set(Calendar.YEAR,p1)
        c.set(Calendar.MONTH,p2)
        c.set(Calendar.DAY_OF_MONTH,p3)
        val cDate=Calendar.getInstance().time
        val sDate=c.time
        if (sDate.after(cDate)) {
            parcelDate = "" + p1 + ":" + month + ":" + p3
            Utility.Time_Picker_Dialog(this, this)

        } else {
            Utility.showToast(this, "Please Select Future Date")
            Utility.showDatePickerDialog(this, this)
        }


    }

    override fun onTimeSet(p0: TimePicker?, p1: Int, p2: Int) {
        var time_value:String=p1.toString()+":"+p2.toString()
        parcel!!.orderDate=parcelDate
        parcel!!.orderTime=time_value

        validateInputs()
        //Utility.showToast(this,"message"+time_value)
    }
}