package com.senarios.customer.activities

import android.content.Intent
import android.location.Location
import android.text.Editable
import android.text.TextWatcher
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import androidx.databinding.DataBindingUtil
import com.google.gson.reflect.TypeToken
import com.senarios.customer.R
import com.senarios.customer.adaptors.SearchAdaptor
import com.senarios.customer.costants.Constants
import com.senarios.customer.costants.SPConstants
import com.senarios.customer.databinding.ActivitySearchBinding
import com.senarios.customer.models.*
import com.senarios.customer.retrofit.APIConstants
import com.senarios.customer.retrofit.ApiResponse
import com.senarios.customer.retrofit.NetworkCall
import com.senarios.customer.utility.Utility
import kotlinx.android.synthetic.main.toolbar_search.view.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import retrofit2.Response
import java.lang.reflect.Type

class SearchActivity : BaseAcivity() , View.OnClickListener,ApiResponse,TextWatcher ,View.OnFocusChangeListener {
    private lateinit var binding:ActivitySearchBinding
    private lateinit var menuItem: MenuItem
    private lateinit var adaptor: SearchAdaptor
    private lateinit var product: ProductModel


    override fun setBinding() {
        binding=DataBindingUtil.setContentView(this,R.layout.activity_search)
    }

    override fun init() {
        binding.toolbar.search.inflateMenu(R.menu.map_search_menu)
        binding.toolbar.search.autocomplete.addTextChangedListener(this)
        setSupportActionBar(binding.toolbar.search)
        binding.toolbar.search.setNavigationOnClickListener(this)
        binding.toolbar.search.autocomplete.onFocusChangeListener=this
        binding.toolbar.search.autocomplete.threshold=0

    }

    override fun OnCancelPermissioDialog() {

    }

    override fun OnPermissionApproved() {

    }

    override fun OnTrigger() {

    }

    override fun onClick(v: View?) {
        finish()
    }

    override fun afterTextChanged(s: Editable?) {

    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        if (::menuItem.isInitialized){
            menuItem.isVisible = !s.isNullOrEmpty() && s.isNotEmpty()
        }
        if (!s.toString().isEmpty()){
            var lat :String? =null;
            var lng :String? =null;
            binding.progressBar.visibility-View.VISIBLE
            binding.recyclerview.visibility=View.GONE
            val list= mutableListOf<ModelCommunication>()
            list.add(ModelCommunication("name",s.toString().toLowerCase()))
            if (Utility.getPreference(this).contains(SPConstants.ADDRESS_DATA)){
                lat=Utility.getSelectedAddress(this)!!.lat
                lng=Utility.getSelectedAddress(this)!!.lon
            }
            else{
                lat=Utility.getPreferences(this,Constants.LAT,"") as String
                lng=Utility.getPreferences(this,Constants.LNG,"") as String
            }
            if (!lat.isEmpty() && !lng.isEmpty() ){
                list.add(ModelCommunication("lat",lat))
                list.add(ModelCommunication("long",lng))
            }
            NetworkCall.callAPI(this,getService().search(Utility.getJson(list)),this,false,APIConstants.SEARCH)
        }


    }

    override fun OnSuccess(endpoint: String, t: Response<ModelBaseResponse>, response: String) {
        if (APIConstants.SEARCH.equals(endpoint)) {
            binding.progressBar.visibility-View.GONE
            binding.recyclerview.visibility=View.VISIBLE
            val listType: Type = object : TypeToken<List<ProductModel?>?>() {}.getType()
            val list = Utility.getgson().fromJson<MutableList<ProductModel>>(response, listType)
            if (list.size > 0) {
                getDAO().updateCart(list)
                adaptor = SearchAdaptor(this, list)
                binding.recyclerview.adapter=adaptor
            }
        }
        else{
            getDAO().insertVendor(Utility.getgson().fromJson(response,Vendor::class.java))
            startActivity(Intent(this@SearchActivity, ProductDetailsActivity::class.java).putExtra(Constants.PRODUCT_DATA, product))
        }
    }

    override fun OnStatusfalse(endpoint: String, t: Response<ModelBaseResponse>, response: String, message: String) {
        binding.progressBar.visibility-View.GONE
    }

    override fun OnError(endpoint: String, code: Int, message: String) {
        binding.progressBar.visibility-View.GONE
    }

    override fun OnException(endpoint: String, exception: Throwable) {
        binding.progressBar.visibility-View.GONE
    }

    override fun OnNetworkError(endpoint: String, message: String) {
        binding.progressBar.visibility-View.GONE
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.map_search_menu,menu)
        menuItem=menu!!.findItem(R.id.clear_search)
        menuItem.isVisible = false
        return true
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public fun getProduct( product:ProductModel) {
        if (product.vendorId!=null) {
            this.product=product
            getDAO().inserProduct(product)
            if (getDAO().getVendorByID(product.vendorId!!).size > 0) {
                startActivity(Intent(this@SearchActivity, ProductDetailsActivity::class.java).putExtra(Constants.PRODUCT_DATA, product))
            } else {
                val list = mutableListOf<ModelCommunication>()
                list.add(ModelCommunication("id ", "" + product.vendorId))
                NetworkCall.callAPI(this, getService().getVendor(Utility.getJson(list)), this, false, APIConstants.GET_VENDOR)
            }
        }
    }

    override fun onResume() {
        super.onResume()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }

    override fun onFocusChange(v: View?, hasFocus: Boolean) {
//        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
//        if (::adaptor.isInitialized){
//            adaptor.filter.filter(binding.toolbar.search.autocomplete.text.toString())
//        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.clear_search->{
                binding.toolbar.autocomplete.setText("")
            }
        }
        return true
    }


}
