package com.senarios.customer.activities

import android.animation.Animator
import android.app.Activity
import android.content.Intent
import android.content.IntentSender
import android.location.Location
import android.os.Looper
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.databinding.DataBindingUtil
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.tasks.OnSuccessListener
import com.google.android.gms.tasks.Task
import com.muddzdev.styleabletoast.StyleableToast
import com.senarios.customer.R
import com.senarios.customer.costants.Codes
import com.senarios.customer.costants.Constants
import com.senarios.customer.costants.Messages
import com.senarios.customer.costants.SPConstants
import com.senarios.customer.databinding.FragmentRequestRideBinding
import com.senarios.customer.dialogfragments.LoginSignupDialog
import com.senarios.customer.models.*
import com.senarios.customer.retrofit.*
import com.senarios.customer.utility.Utility
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import org.json.JSONObject
import retrofit2.Response
import kotlin.math.roundToInt


class RequestRideActivity : BaseAcivity(), OnMapReadyCallback, ApiResponse, OnSuccessListener<Location> ,View.OnClickListener, ApiGoogleResponse {
    private lateinit var binding: FragmentRequestRideBinding
    private var ride: RideModel = RideModel(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null)
    private val REQUEST_CHECK_SETTINGS: Int = 1000
    private lateinit var map: GoogleMap
    private lateinit var locationRequest: LocationRequest
    private lateinit var locationCallback: LocationCallback
    private lateinit var location: Location
    private lateinit var rideModel: RideModel
    private lateinit var orderModel: OrderModel
    private lateinit var riderModel: User
    private lateinit var builder: LocationSettingsRequest.Builder
    private val currentZoom: Float = 16f
    private var markers: MutableList<MarkerOptions> = mutableListOf()
    private val TAG: String = javaClass.name


    override fun setBinding() {
        binding = DataBindingUtil.setContentView(this, com.senarios.customer.R.layout.fragment_request_ride)
    }


    override fun init() {
        binding.actionbar.title = "Request Ride"
        binding.actionbar.setNavigationOnClickListener(this)
        binding.dropoffLL.setOnClickListener(this)
        binding.dropoffEdt.setOnClickListener(this)
        binding.pickupLL.setOnClickListener(this)
        binding.pickupEdt.setOnClickListener(this)
        binding.confirmbtnride.setOnClickListener(this)
        binding.cancelbtnride.setOnClickListener(this)
        binding.contactView.setOnClickListener(this)
        binding.cancelView.setOnClickListener(this)
        binding.confirmbtnlocation.setOnClickListener(this)

        if (!Utility.hasLocationPermission(this)) {
            checkLocationPermission()
        } else {
            createLocationRequest()
        }

    }

    override fun OnCancelPermissioDialog() {
        super.OnCancelPermissioDialog()
    }

    override fun OnPermissionApproved() {
       val fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        fusedLocationClient.lastLocation.addOnSuccessListener(this)

        if (Utility.checkIfUserLogged(this)) {
            val list = mutableListOf<ModelCommunication>()
            list.add(ModelCommunication(Constants.USER_ID, Utility.getUser(this).id.toString()))
            NetworkCall.callAPI(this, Utility.getService().checkRideStatus(APIConstants.BEARER + Utility.getUser(this).token, Utility.getJson(list)), this, true, APIConstants.CHECK_RIDE_STATUS)
        }
        else{
            binding.locationsCv.visibility=View.VISIBLE
        }
    }

    override fun OnTrigger() {
        callRideAPI()
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            binding.dropoffLL.id -> {
                startActivityForResult(Intent(this@RequestRideActivity, MapActivity::class.java), Codes.RIDE_DROPOFF_REQUEST)
            }
            binding.pickupLL.id -> {
                startActivityForResult(Intent(this@RequestRideActivity, MapActivity::class.java), Codes.RIDE_PICKUP_REQUEST)
            }
            binding.dropoffEdt.id -> {
                startActivityForResult(Intent(this@RequestRideActivity, MapActivity::class.java), Codes.RIDE_DROPOFF_REQUEST)
            }
            binding.pickupEdt.id -> {
                startActivityForResult(Intent(this@RequestRideActivity, MapActivity::class.java), Codes.RIDE_PICKUP_REQUEST)
            }
            binding.confirmbtnride.id -> {
                if (Utility.checkIfUserLogged(this)) {
                    callRideAPI()
                } else {
                    showDialog(LoginSignupDialog())
                }

            }
            binding.cancelbtnride.id -> {
                hideShowConfirmationCV()
                hideShowLocationCV()
            }
            binding.contactView.id->{
                if (::riderModel.isInitialized){
                    Utility.callIntent(this,riderModel.phone_number)
                }
            }
            binding.cancelView.id->{
                callCancelAPI()
            }
            binding.confirmbtnlocation.id -> {
                try {
                    NetworkGoogleCall.callAPI(this, Utility.getGoogleService().getDirectionAPI(Constants.IMPERIAL, ride.pickup_lat!! + "," + ride.pickup_lon, ride.dropoff_lat!! + "," + ride.dropoff_lon, resources.getString(R.string.Maps_Places_Key)), this, true, APIConstants.DIRECTION)

                } catch (e: Exception) {
                    Log.v(TAG, ""+e.message!!)
                }
            }

            else -> {
                finish()
            }
        }
    }

    private fun callCancelAPI() {
        val list:MutableList<ModelCommunication> = mutableListOf()
        list.add(ModelCommunication(Constants.USER_ID, Utility.getUser(this).id.toString()))
        list.add(ModelCommunication(Constants.ORDER_ID, orderModel.id.toString()))
        NetworkCall.callAPI(this,
            Utility.getService().cancelOrder(APIConstants.BEARER+ Utility.getUser(this).token,Utility.getJson(list)),this, true, APIConstants.CANCEL_ORDER)


    }

    private fun callRideAPI() {
        val list: MutableList<ModelCommunication> = mutableListOf()
        list.add(ModelCommunication(Constants.USER_ID, Utility.getUser(this).id.toString()))
        list.add(ModelCommunication(Constants.RIDE_DROP0FF_ADDRESS, ride.dropoff_address!!))
        list.add(ModelCommunication(Constants.RIDE_DROP0FF_LAT, ride.dropoff_lat!!))
        list.add(ModelCommunication(Constants.RIDE_DROP0FF_LNG, ride.dropoff_lon!!))
        list.add(ModelCommunication(Constants.RIDE_PICKUP_ADDRESS, ride.pickup_address!!))
        list.add(ModelCommunication(Constants.RIDE_PICKUP_LAT, ride.pickup_lat!!))
        list.add(ModelCommunication(Constants.RIDE_PICKUP_LNG, ride.pickup_lon!!))
        list.add(ModelCommunication(Constants.RIDE_DISTANCE, ride.distance!!))
        list.add(ModelCommunication(Constants.ESTIMATE_TIME, ride.estimate_time!!))
        list.add(ModelCommunication(Constants.ESTIMATE_FARE, ride.estimate_fare!!))
        list.add(ModelCommunication(Constants.ESTIMATE_DISTANCE, ride.distance!!))
        NetworkCall.callAPI(this, getService().postRideOrder(APIConstants.BEARER + Utility.getUser(this).token, Utility.getJson(list)), this, true, APIConstants.REQUEST_RIDE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode== Activity.RESULT_OK) {
            var latLng: LatLng? = null

            if (requestCode == Codes.RIDE_DROPOFF_REQUEST && data != null) {
                latLng = data.getParcelableExtra(Constants.LATLNG_DATA)!!
                ride.dropoff_lat = latLng.latitude.toString()
                ride.dropoff_lon = latLng.longitude.toString()
                ride.dropoff_address = Utility.getAddress(this@RequestRideActivity, latLng = latLng)
                binding.dropoffEdt.setText(ride.dropoff_address)
                binding.pickupLL.visibility = View.VISIBLE
                createMarker(latLng, "DropOff", 0)
            } else if (requestCode == Codes.RIDE_PICKUP_REQUEST && data != null) {
                latLng = data.getParcelableExtra(Constants.LATLNG_DATA)!!
                ride.pickup_lat = latLng.latitude.toString()
                ride.pickup_lon = latLng.longitude.toString()
                ride.pickup_address = Utility.getAddress(this@RequestRideActivity, latLng = latLng)
                binding.pickupEdt.setText(ride.pickup_address)
                createMarker(latLng, "PickUp", 1)
            } else if (requestCode == REQUEST_CHECK_SETTINGS) {
                if (Utility.checkLocationServices(this)) {
                    requestLocation()
                } else {
                    locationRequestTask()
                }
            }
            if (!Utility.getTextET(binding.pickupEdt).isEmpty()) {
                if (!binding.confirmbtnlocation.isVisible) {
                    binding.confirmbtnlocation.visibility = View.VISIBLE
                    createMarker(LatLng(ride.pickup_lat!!.toDouble(), ride.pickup_lon!!.toDouble()), "PickUp", 1)
                }
            }
        }
    }

    private fun createMarker(latLng: LatLng, type: String, index: Int) {
        if (isMap()) {
            map.clear()
            val marker = MarkerOptions().position(latLng).title(type).icon(BitmapDescriptorFactory.fromResource(R.drawable.map_pin))
            if (index == 0) {
                if (markers.size > 0) {
                    markers.removeAt(0)
                    markers.add(0, marker)
                } else {
                    markers.add(0, marker)
                }

            } else if (index == 1) {
                if (markers.size == 2) {
                    markers.removeAt(1)
                    markers.add(1, marker)
                } else {
                    markers.add(1, marker)
                }
            }
            addMarkers()
            if (markers.size == 1) {
                moveCamera(latLng)
            } else {
                moveCamera(getBounds())
            }
        }
    }

    private fun addMarkers() {
        map.clear()
        for (item in markers) {
            map.addMarker(item)
        }
    }

    private fun initMap() {
        val mapFragment = supportFragmentManager.findFragmentById(R.id.map_fragment) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    override fun onMapReady(p0: GoogleMap?) {
        if (p0 != null) {
            map = p0;
            map.uiSettings.isMyLocationButtonEnabled = true
            if (isLocation()) {
                moveCamera(LatLng(location.latitude, location.longitude))
            }
        }
    }


    private fun createLocationRequest() {
        locationRequest = LocationRequest.create()
        locationRequest.interval = 500
        locationRequest.fastestInterval = 500
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        builder = LocationSettingsRequest.Builder().addLocationRequest(locationRequest)
        locationCallback = object : LocationCallback() {
            override fun onLocationResult(p0: LocationResult?) {
                if (p0 != null) {
                    location = p0.lastLocation
                    if (!isMap()) {
                        initMap()
                    }
                    if (isMap()) {
                        if (map.cameraPosition.zoom == 0f) {
                            if (isLocation()) {
                                moveCamera(getCurrentLatLng())
                            }
                        }
                    }
                    removeRequestLocation()
                    OnPermissionApproved()
                }
            }

        }
        locationRequestTask()
    }

    private fun locationRequestTask() {
        val client: SettingsClient = LocationServices.getSettingsClient(this)
        val task: Task<LocationSettingsResponse> = client.checkLocationSettings(builder.build())
        task.addOnSuccessListener {
            requestLocation()
        }

        task.addOnFailureListener { exception ->
            if (exception is ResolvableApiException) {
                try {
                    exception.startResolutionForResult(this@RequestRideActivity, REQUEST_CHECK_SETTINGS)
                } catch (sendEx: IntentSender.SendIntentException) {

                }
            }
        }
    }

    private fun isLocation(): Boolean {
        return ::location.isInitialized
    }

    override fun onResume() {
        super.onResume()
        //        if (isLocationCallback()){
        //            requestLocation()
        //        }
        //        if(isLocation() && isMap()){
        //            moveCamera(LatLng(location.latitude, location.longitude))
        //        }


    }

    override fun onStop() {
        super.onStop()
        if (::locationCallback.isInitialized) {
            removeRequestLocation()
        }
        EventBus.getDefault().unregister(this)
    }

    private fun requestLocation() {
        LocationServices.getFusedLocationProviderClient(this).requestLocationUpdates(locationRequest, locationCallback, Looper.getMainLooper())

    }

    private fun removeRequestLocation() {
        LocationServices.getFusedLocationProviderClient(this).removeLocationUpdates(locationCallback)

    }


    private fun moveCamera(latlng: LatLng) {
        if (isMap()) {
            map.animateCamera(CameraUpdateFactory.newLatLngZoom(latlng, currentZoom))
            if (!binding.locationsCv.isVisible) {
                //                binding.locationsCv.visibility = View.VISIBLE
            }
        }
    }

    private fun moveCamera(bounds: LatLngBounds) {
        if (isMap()) {
            map.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 100))
            if (!binding.confirmbtnlocation.isVisible) {
                binding.confirmbtnlocation.visibility = View.VISIBLE
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: kotlin.IntArray) {
        if (Utility.hasLocationPermission(this)) {
            createLocationRequest()
        } else {
            handleLocationPermissionResult()
        }

    }

    private fun getCurrentLatLng(): LatLng {
        return LatLng(location.latitude, location.longitude)
    }

    private fun isMap(): Boolean {
        return ::map.isInitialized
    }

    private fun isLocationCallback(): Boolean {
        return ::locationCallback.isInitialized
    }

    private fun getBounds(): LatLngBounds {
        val builder = LatLngBounds.Builder()
        for (item in markers) {
            builder.include(item.getPosition())
        }
        return builder.build()
    }

    private fun hideShowConfirmationCV() {
        if (!binding.confirmationCv.isVisible) {
            binding.confirmationCv.animate().alpha(1f).setListener(object : Animator.AnimatorListener {
                override fun onAnimationRepeat(animation: Animator?) {

                }

                override fun onAnimationEnd(animation: Animator?) {
                    binding.confirmationCv.visibility = View.VISIBLE
                }

                override fun onAnimationCancel(animation: Animator?) {

                }

                override fun onAnimationStart(animation: Animator?) {

                }

            }).duration = 1000
        } else {
            binding.confirmationCv.animate().alpha(0f).setListener(object : Animator.AnimatorListener {
                override fun onAnimationRepeat(animation: Animator?) {

                }

                override fun onAnimationEnd(animation: Animator?) {
                    binding.confirmationCv.visibility = View.GONE
                }

                override fun onAnimationCancel(animation: Animator?) {

                }

                override fun onAnimationStart(animation: Animator?) {

                }

            }).duration = 1000
        }
    }

    private fun hideShowLocationCV() {
        if (!binding.locationsCv.isVisible) {
            binding.locationsCv.animate().alpha(1f).setListener(object : Animator.AnimatorListener {
                override fun onAnimationRepeat(animation: Animator?) {

                }

                override fun onAnimationEnd(animation: Animator?) {
                    binding.locationsCv.visibility = View.VISIBLE
                }

                override fun onAnimationCancel(animation: Animator?) {

                }

                override fun onAnimationStart(animation: Animator?) {

                }

            }).duration = 1000
        } else {
            binding.locationsCv.animate().alpha(0f).setListener(object : Animator.AnimatorListener {
                override fun onAnimationRepeat(animation: Animator?) {

                }

                override fun onAnimationEnd(animation: Animator?) {
                    binding.locationsCv.visibility = View.GONE
                }

                override fun onAnimationCancel(animation: Animator?) {

                }

                override fun onAnimationStart(animation: Animator?) {

                }

            }).duration = 1000
        }
    }

    override fun OnGoogleSuccess(endpoint: String, t: Response<GoogleDistanceApiResponse>) {
        Log.v(TAG, "Google api called")
        ride.distance = t.body()!!.rows[0].elements[0].distance.text
        ride.duration = t.body()!!.rows[0].elements[0].duration.text
        ride.estimate_time = t.body()!!.rows[0].elements[0].duration.text
        ride.distance = "" + (ride.distance!!.split(" ")[0].toDouble() * 1.609).toString().toDouble().roundToInt()
        ride.estimated_distance = ride.distance
        ride.amount = "" + (ride.distance!!.toDouble() * 4 + 25).toString().toDouble().roundToInt()
        binding.estimateFare.text = "Estimate Fare : RS " + ride.amount
        ride.estimate_fare = ride.amount
        hideShowConfirmationCV()
        hideShowLocationCV()
        Log.v(TAG, Utility.getgson().toJson(ride))

    }

    override fun OnGoogleError(endpoint: String, code: Int, message: String) {
        Log.v(TAG, "Google api error")
        OnError(endpoint, code, message)
    }

    override fun OnGoogleException(endpoint: String, exception: Throwable) {
        Log.v(TAG, "Google api exception")
        OnException(endpoint, exception)
    }

    override fun OnSuccess(endpoint: String, t: Response<ModelBaseResponse>, response: String) {
        try {
            if (APIConstants.REQUEST_RIDE.equals(endpoint)) {
                Utility.getPreference(this@RequestRideActivity).edit().putBoolean(SPConstants.IS_RIDE, true).apply()
                Utility.getAlertDialoge(this, getString(R.string.ride_success_title), getString(R.string.ride_success_message)).setPositiveButton("Ok") { dialog, which -> finish() }.setCancelable(false).show()
            }

            else if(endpoint.equals(APIConstants.CANCEL_ORDER)){
                finish()
                StyleableToast.makeText(this, "Order Cancelled Successfully!", Toast.LENGTH_LONG, R.style.mytoast).show();

            }
            else {
                val json = JSONObject(response)
                if (json.has(Constants.USER) && json.has(Constants.RIDE_ORDER) && json.has(Constants.ORDER)) {
                    orderModel = Utility.getgson().fromJson(json.getJSONObject(Constants.ORDER).toString(), OrderModel::class.java)
                    rideModel = Utility.getgson().fromJson(json.getJSONObject(Constants.RIDE_ORDER).toString(), RideModel::class.java)
                    if (json.isNull(Constants.USER)) {
                        binding.waitingCV.visibility = View.VISIBLE
                        binding.rideView.visibility = View.GONE
                    } else {
                        riderModel = Utility.getgson().fromJson(json.getJSONObject(Constants.USER).toString(), User::class.java)
                        binding.rider = riderModel
                        binding.orderModel = orderModel
                        binding.rideModel = rideModel
                        restoreRideState()
                    }
                } else if (json.has(Constants.RIDE_ORDER) && json.has(Constants.ORDER)) {
                    binding.waitingCV.visibility = View.VISIBLE
                    binding.rideView.visibility = View.GONE
                }
            }

        } catch (e: Exception) {
            Utility.showLog("request ride" + e.message)
        }
    }



    override fun OnStatusfalse(endpoint: String, t: Response<ModelBaseResponse>, response: String, message: String) {
        if (APIConstants.REQUEST_RIDE.equals(endpoint)) {
            Utility.showErrorDialog(this@RequestRideActivity, R.layout.dialog_error_505, Messages.ERROR_TITLE, message)
        } else {
            binding.locationsCv.visibility = View.VISIBLE
        }
    }

    override fun OnError(endpoint: String, code: Int, message: String) {
        if (APIConstants.REQUEST_RIDE.equals(endpoint)) {
            Utility.showErrorDialog(this@RequestRideActivity, R.layout.dialog_error_505, Messages.ERROR_TITLE, message)
        } else {
            binding.locationsCv.visibility = View.VISIBLE
        }
    }


    override fun OnException(endpoint: String, exception: Throwable) {
        Utility.showErrorDialog(this@RequestRideActivity, R.layout.dialog_error, Messages.ERROR_TITLE, getString(R.string.something_went_wrong))
    }

    override fun OnNetworkError(endpoint: String, message: String) {
        Utility.showNErrorDialog(this@RequestRideActivity)
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun getValues(list: MutableList<ModelCommunication>) {
        if (list != null && list.size > 0) {
            try {
                riderModel = Utility.getgson().fromJson(list.get(0).value, User::class.java)
                rideModel = Utility.getgson().fromJson(list.get(1).value, RideModel::class.java)
                orderModel = Utility.getgson().fromJson(list.get(2).value, OrderModel::class.java)
                binding.rider = riderModel
                binding.rideModel = rideModel
                binding.orderModel = orderModel
                restoreRideState()
            } catch (e: Exception) {
                Utility.showLog("" + e.message)
            }
        }
    }
    private fun restoreRideState() {
        if (orderModel.status.equals(Constants.ORDER_STATUS_PENDING) || orderModel.status.equals(Constants.ORDER_STATUS_APPROVED)) {
            binding.waitingCV.visibility = View.VISIBLE
            binding.rideView.visibility = View.GONE
        } else if (orderModel.status.equals(Constants.ORDER_STATUS_ASSIGNED)) {
            binding.waitingCV.visibility = View.GONE
            binding.rideView.visibility = View.VISIBLE
            markers.clear()
            markers.add(MarkerOptions().position(LatLng(rideModel.dropoff_lat!!.toDouble(), rideModel.dropoff_lon!!.toDouble())).icon(BitmapDescriptorFactory.fromResource(R.drawable.map_pin)).title("Drop Off"))
            markers.add(MarkerOptions().position(LatLng(rideModel.pickup_lat!!.toDouble(), rideModel.pickup_lon!!.toDouble())).icon(BitmapDescriptorFactory.fromResource(R.drawable.map_pin)).title("Pick Up"))
            addMarkers()
            moveCamera(getBounds())
            if (rideModel.status.equals(Constants.START_RIDE)) {
                binding.cancelView.visibility = View.GONE
                binding.contactView.visibility = View.GONE
            } else if (rideModel.status.equals(Constants.ON_SITE)) {
                binding.cancelView.visibility = View.GONE
            } else if (rideModel.status.equals(Constants.END_RIDE)) {
                binding.cancelView.visibility = View.GONE
            } else if (rideModel.status == null) {
                binding.cancelView.visibility = View.VISIBLE
            }
        } else if (orderModel.status.equals(Constants.ORDER_STATUS_COMPLETED)) {
            Utility.getAlertDialoge(this, "Ride Complete", "Your fare was " + rideModel.total_Fare+" PKR").setPositiveButton("Ok") { dialog, which -> finish() }.show()
            Utility.showNotification(this,"Ride Complete","Your fare was " + rideModel.total_Fare+" PKR")
        } else if (orderModel.status.equals(Constants.ORDER_STATUS_PFA)) {
            binding.waitingCV.visibility = View.VISIBLE
            binding.rideView.visibility = View.GONE
        }
    }

    override fun onSuccess(p0: Location?) {
        if (p0 != null) {
           val address:String? = Utility.getAddress(this, LatLng(location.latitude, location.longitude))
            if(!address.isNullOrEmpty()){
                binding.pickupEdt.setText(address)
                ride.pickup_lat = p0.latitude.toString()
                ride.pickup_lon = p0.longitude.toString()
                ride.pickup_address=address
            }

        }
    }
}
