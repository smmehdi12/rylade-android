package com.senarios.customer.activities

import android.content.DialogInterface
import android.content.Intent
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.senarios.rylad.BaseDialogeFragment
import com.senarios.customer.R
import com.senarios.customer.adaptors.AdaptorAddress
import com.senarios.customer.adaptors.RecyclerViewCallback
import com.senarios.customer.costants.Codes
import com.senarios.customer.costants.Constants
import com.senarios.customer.costants.Messages
import com.senarios.customer.costants.SPConstants
import com.senarios.customer.databinding.FragmentAddressesBinding
import com.senarios.customer.dialogfragments.AddAddressDialog
import com.senarios.customer.dialogfragments.DialogeCallback
import com.senarios.customer.dialogfragments.LoginSignupDialog
import com.senarios.customer.models.Address
import com.senarios.customer.models.ModelBaseResponse
import com.senarios.customer.models.ModelCommunication
import com.senarios.customer.retrofit.APIConstants
import com.senarios.customer.retrofit.ApiResponse
import com.senarios.customer.retrofit.NetworkCall
import com.senarios.customer.utility.Utility
import com.senarios.customer.viewmodel.SharedVM
import retrofit2.Response

/*this activity will handle everything related to my addresses*/

class AddressActivity : BaseAcivity(), View.OnClickListener,DialogeCallback,ApiResponse,RecyclerViewCallback{
    private lateinit var binding: FragmentAddressesBinding
    private lateinit var sharedVM: SharedVM
    private lateinit var adaptor:AdaptorAddress
    private  var addresses= ArrayList<Address>()
    private lateinit var dialog: AlertDialog
    private lateinit var dialogFragment:BaseDialogeFragment
    private var position:Int=-1
    private val list= mutableListOf<ModelCommunication>()


    override fun setBinding() {
        binding=DataBindingUtil.setContentView(this, R.layout.fragment_addresses)

    }

    override fun onResume() {
        super.onResume()

        if (::dialog.isInitialized){
            dialog.dismiss()
        }
        if (::dialogFragment.isInitialized ) {
            if(!dialogFragment.isVisible) {
            getAddresses()
        } else{
                getAddresses()
            }
        }
        else{
            getAddresses()
        }
    }

    private fun getAddresses() {
        list.clear()
        list.add(ModelCommunication(Constants.USER_ID,sharedVM.getLoggedUser().value!!.id.toString()))
        NetworkCall.callAPI(this,getService().getAddress(Utility.getUserToken(sharedVM),Utility.getJson(list)),this,true,APIConstants.GET_ADDRESS)
    }

    override fun init(){
        sharedVM= ViewModelProvider(this).get(SharedVM::class.java)
        binding.toolbar.setNavigationOnClickListener(this)
        binding.button3.setOnClickListener(this)
        binding.recyclerView.layoutManager=LinearLayoutManager(this)

    }

    override fun onClick(v: View?) {
        hideSoftKeyboard()
        when (v?.id) {
            binding.button3.id->{
                dialogFragment=AddAddressDialog()
                showDialog(dialogFragment)
            }
            else->{
                finish()
            }


        }

    }

    override fun OnChange(fragment: BaseDialogeFragment) {

    }

    override fun OnCancelPermissioDialog() {
        super.OnCancelPermissioDialog()
    }

    override fun OnPermissionApproved() {

    }

    override fun OnTrigger() {
        getAddresses()
    }

    override fun OnSuccess(endpoint:String,t: Response<ModelBaseResponse>, response: String) {
        try{
            if (endpoint.equals(APIConstants.DELETE_ADDRESS,true)){
                if(::adaptor.isInitialized) {
                    adaptor.removeItem(position)
                }
            }
            else {
                val itemType = object : TypeToken<List<Address>>() {}.type
                addresses = Gson().fromJson<ArrayList<Address>>(response, itemType)
                adaptor = AdaptorAddress(this, addresses, this)
                binding.recyclerView.adapter = adaptor
            }
        }
        catch (e:Exception){
            OnException(endpoint,e)
        }

    }

    override fun OnStatusfalse(endpoint:String,t: Response<ModelBaseResponse>, response: String, message: String) {
        dialog=Utility.showErrorDialog(this@AddressActivity,R.layout.dialog_error_505, Messages.ERROR_TITLE,message)
    }

    override fun OnError(endpoint:String,code: Int, message: String) {
        dialog=Utility.showErrorDialog(this@AddressActivity,R.layout.dialog_error_505, Messages.ERROR_TITLE,message)
    }


    override fun OnException(endpoint:String,exception: Throwable) {
        dialog=Utility.showErrorDialog(this@AddressActivity,R.layout.dialog_error, Messages.ERROR_TITLE,getString(R.string.something_went_wrong))
    }

    override fun OnNetworkError(endpoint:String,message: String) {
        dialog=Utility.showNErrorDialog(this@AddressActivity)
    }

    override fun OnEdit(position: Int, any:Any) {
        if (addresses.size>0){
            this.position=position
        sharedVM.setAddress(addresses[position])
            dialogFragment=AddAddressDialog()
        showDialog(dialogFragment)
        }
    }

    override fun OnClick(position: Int, any: Any) {
        if (callingActivity!=null) {
            val intent = Intent()
            this.position = position
            intent.putExtra(Constants.ADDRESS_DATA, Utility.getgson().toJson(addresses[position]))
            setResult(Codes.ADDRESS_REQUEST, intent)
            finish()
        }
    }

    override fun OnDelete(position: Int, any:Any) {
        val address=any as Address
        this.position=position
       val builder= Utility.getAlertDialoge(this,getString(R.string.delete_address_title),getString(R.string.delete_address_message))
            .setPositiveButton("Yes") { p0, p1 ->
                list.clear()
                list.add(ModelCommunication(Constants.USER_ID,sharedVM.getLoggedUser().value!!.id.toString()))
                list.add(ModelCommunication(Constants.ADDRESS_ID,address.id.toString()))
                NetworkCall.callAPI(this@AddressActivity,getService().deleteAddress(Utility.getUserToken(sharedVM),Utility.getJson(list)),this@AddressActivity,true,APIConstants.DELETE_ADDRESS)
            }
           .setNegativeButton(getString(R.string.imgood)
           ) { p0, p1 -> p0!!.dismiss() }
        dialog=builder.show()
      }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode==Codes.LATLNG_REQUEST_CODE&& data!=null){
            if (::dialogFragment.isInitialized){
                dialogFragment.onActivityResult(requestCode, resultCode, data)
            }
        }
    }

    override fun setDefaultAddress(position: Int, mAddress: Address) {
        if (Utility.getDB().getCartItems().size>0){
                Utility.getAlertDialoge(this, "Cart will be cleared", "By changing the location any item in your cart will be removed!")
                    .setPositiveButton("Ok") { dialog, which ->
                        Utility.getDB().clearCartTable()
                        Utility.setDefaultAddress(this@AddressActivity,mAddress)
                        adaptor.notifyDataSetChanged();
                    }.setNegativeButton("no") { dialog, which ->
                        dialog!!.dismiss()
                    }.show()
            return
        }
        Utility.setDefaultAddress(this@AddressActivity,mAddress)
        adaptor.notifyDataSetChanged();


    }
}
