package com.senarios.customer.activities

import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.senarios.customer.R
import com.senarios.customer.costants.Messages
import com.senarios.customer.costants.SPConstants
import com.senarios.customer.databinding.FragmentProfileBinding
import com.senarios.customer.dialogfragments.EditPasswordDialog
import com.senarios.customer.models.ModelBaseResponse
import com.senarios.customer.models.ModelCommunication
import com.senarios.customer.models.User
import com.senarios.customer.retrofit.APIConstants
import com.senarios.customer.retrofit.ApiResponse
import com.senarios.customer.retrofit.NetworkCall
import com.senarios.customer.utility.Utility
import com.senarios.customer.viewmodel.SharedVM
import retrofit2.Response

class EditProfileActivity : BaseAcivity() ,TextView.OnEditorActionListener,ApiResponse,View.OnClickListener{
    private lateinit var binding:FragmentProfileBinding
    private lateinit var sharedVM: SharedVM
    private lateinit var dialog: AlertDialog




    override fun setBinding() {
        binding=DataBindingUtil.setContentView(this, R.layout.fragment_profile)
        sharedVM= ViewModelProvider(this).get(SharedVM::class.java)
    }

    override fun init() {

        binding.data=sharedVM.getLoggedUser().value
        if (sharedVM.getLoggedUser().value!!.login_facebook){
            binding.group.visibility=View.GONE
        }
        binding.lastNameEdt.setOnEditorActionListener(this)
        binding.changePasswordEdt.setOnClickListener(this)
        binding.saveButton.setOnClickListener(this)
        binding.toolbar.setNavigationOnClickListener(this)



     }

    override fun OnCancelPermissioDialog() {

    }

    override fun OnPermissionApproved() {

    }

    override fun OnTrigger() {

    }

    override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean {
       if (actionId==EditorInfo.IME_ACTION_DONE){
           validateInput()
           return true
       }
        return true
    }

    private fun validateInput() {
        val fname=Utility.getTextET(binding.firstNameEdt)
        val lname=Utility.getTextET(binding.lastNameEdt)
        if (fname.isEmpty()){
            Utility.setErrorET(binding.firstNameEdt)
        }
        else if (lname.isEmpty()){
            Utility.setErrorET(binding.lastNameEdt)
        }
        else if (!fname.equals(sharedVM.getLoggedUser().value!!.f_name,true)||!lname.equals(sharedVM.getLoggedUser().value!!.l_name,true)){
            val list= mutableListOf<ModelCommunication>()
            list.add(ModelCommunication("f_name",fname))
            list.add(ModelCommunication("l_name",lname))
            list.add(ModelCommunication("user_id",sharedVM.getLoggedUser().value!!.id.toString()))


            NetworkCall.callAPI(this,getService().editprofile(Utility.getUserToken(sharedVM),Utility.getJson(list)),this,true,APIConstants.EDITPROFILE)
        }

    }

    override fun onClick(v: View?) {
        when(v?.id){
            binding.changePasswordEdt.id->{
                showDialog(EditPasswordDialog())
            }
            binding.saveButton.id->{
                validateInput()
            }
            else->{
                finish()
            }
        }

    }

    override fun OnSuccess(endpoint: String, t: Response<ModelBaseResponse>, response: String) {
        try{
            val data=Utility.getgson().fromJson(response,User::class.java)
            Utility.setPreferences(this,SPConstants.USER,response)
            sharedVM.setLoggedUser(data)
            showToast(this,"Profile Updated Successfully")
            finish()
        }
        catch (e:Exception){
            OnException(endpoint,e)
        }
    }

    override fun OnStatusfalse(endpoint:String,t: Response<ModelBaseResponse>, response: String, message: String) {
        dialog= Utility.showErrorDialog(this,R.layout.dialog_error_505,
            Messages.ERROR_TITLE,message)
    }

    override fun OnError(endpoint:String,code: Int, message: String) {
        dialog= Utility.showErrorDialog(this,R.layout.dialog_error_505,
            Messages.ERROR_TITLE,message)
    }


    override fun OnException(endpoint:String,exception: Throwable) {
        dialog= Utility.showErrorDialog(this,R.layout.dialog_error, Messages.ERROR_TITLE,exception.localizedMessage!!)
    }

    override fun OnNetworkError(endpoint:String,message: String) {
        dialog= Utility.showNErrorDialog(this)
    }

    override fun onResume() {
        super.onResume()
        if (::dialog.isInitialized){
            dialog.dismiss()
        }
    }
}