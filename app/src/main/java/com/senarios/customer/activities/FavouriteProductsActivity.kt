package com.senarios.customer.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.senarios.customer.R

class FavouriteProductsActivity : BaseAcivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_favourite_products)
    }

    override fun setBinding() {

    }

    override fun init() {

    }

    override fun OnPermissionApproved() {

    }

    override fun OnTrigger() {

    }
}