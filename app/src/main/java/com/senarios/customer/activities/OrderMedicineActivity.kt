package com.senarios.customer.activities

import android.app.Activity
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.view.View
import android.widget.CompoundButton
import android.widget.DatePicker
import android.widget.TimePicker
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.senarios.rylad.BaseDialogeFragment
import com.senarios.customer.R
import com.senarios.customer.adaptors.AdaptorMedicine
import com.senarios.customer.adaptors.RecyclerViewCallback
import com.senarios.customer.costants.Codes
import com.senarios.customer.costants.Constants
import com.senarios.customer.databinding.FragmentMedicineBinding
import com.senarios.customer.dialogfragments.LoginSignupDialog
import com.senarios.customer.models.Address
import com.senarios.customer.models.MedicineModel
import com.senarios.customer.services.MedicineOrderService
import com.senarios.customer.utility.Utility
import com.senarios.customer.viewmodel.SharedVM
import net.alhazmy13.mediapicker.Image.ImagePicker
import java.util.*


/*this activity will be responsible for handling everything regarding the medicine view*/
class OrderMedicineActivity : BaseAcivity(), View.OnClickListener, CompoundButton.OnCheckedChangeListener, DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener, RecyclerViewCallback {
    private val TAG: String = javaClass.name
    private lateinit var binding: FragmentMedicineBinding
    private lateinit var sharedVM: SharedVM
    private var clickedLayoutID = -1
    private lateinit var adaptor: AdaptorMedicine
    var proper_date = ""



    override fun setBinding() {
        binding = DataBindingUtil.setContentView(this, R.layout.fragment_medicine)
        sharedVM= ViewModelProvider(this).get(SharedVM::class.java)
    }

    override fun OnChange(fragment: BaseDialogeFragment) {
        fragment.show(supportFragmentManager, "dialog")
    }

    override fun OnCancelPermissioDialog() {
        super.OnCancelPermissioDialog()
    }

    override fun OnPermissionApproved() {

    }

    override fun OnTrigger() {

    }

    override fun init() {
        binding.takeSnapPrescriptionBtn.setOnClickListener(this)
        adaptor = AdaptorMedicine(this, this)
        binding.recyclerView.layoutManager = LinearLayoutManager(this)
        binding.recyclerView.adapter = adaptor
        adaptor.addItem()


        binding.toolbar.setNavigationOnClickListener(this)
        binding.confirmBtn.setOnClickListener(this)
        binding.addBtn.setOnClickListener(this)
        binding.isInstant.setOnCheckedChangeListener(this)


    }

    override fun onClick(v: View?) {
        when (v?.id) {
            binding.confirmBtn.id -> {
                if (Utility.checkIfUserLogged(this)) {
                    if (!checkList(adaptor.getItems())) {
                        Utility.showErrorDialog(this@OrderMedicineActivity, R.layout.dialog_error_fields, getString(R.string.missing_field_title), getString(R.string.missing_field_message))
                    } else {
//                        if (adaptor.getItems()[0].photo.equals(null)) {
//                            showImageNotSelectedDialog()
//                        } else {
                            ShowInstantDialog()
//                        }

                    }
                } else {
                    showDialog(LoginSignupDialog())
                }
            }
            binding.addBtn.id -> {
                adaptor.addItem()
                binding.recyclerView.scrollToPosition(adaptor.itemCount - 1)
            }
            binding.takeSnapPrescriptionBtn.id -> {
                openGallery(0)

            }
            binding.toolbar.id -> {
                showAgreedDialog()
            }
            else -> {

                finish()
            }

        }

    }

    override fun onBackPressed() {
        showAgreedDialog()
    }

    private fun showAgreedDialog() {
        Utility.getAlertDialoge(this, getString(R.string.AgrredTitle), getString(R.string.AgreedMessage)).setNegativeButton("Cancel") { p0, p1 ->
            Utility.showToast(this, "Cancelled")
        }.setPositiveButton("Ok") { p0, p1 -> finish() }.show()

    }

    private fun ShowInstantDialog() {
        Utility.getAlertDialoge(this, getString(R.string.InstantTitle), getString(R.string.InstantMessage)).setNegativeButton("Not Instant") { p0, p1 ->
            Show_Date_Picker()
        }.setPositiveButton("Instant") { p0, p1 ->
                adaptor.getItems()[0].instance = true
                showAddressDialog()
            }.show()

    }

    private fun Show_Date_Picker() {
        Utility.showDatePickerDialog(this, this)


    }

    private fun showImageNotSelectedDialog() {

        Utility.getAlertDialoge(this,getString(R.string.ImageNotSelected),getString(R.string.ImageMessage)).setPositiveButton("Ok"
        ) { p0, p1 -> p0.dismiss()}
            .show()
    }

    private fun showAddressDialog() {
        Utility.getAlertDialoge(this, getString(R.string.select_address_title), getString(R.string.select_address_message)).setPositiveButton("Ok") { p0, p1 -> startActivityForResult(Intent(this@OrderMedicineActivity, AddressActivity::class.java), Codes.ADDRESS_REQUEST) }.show()
    }

    private fun checkList(items: List<MedicineModel>): Boolean {
        for (medicine in items) {
            when {
                medicine.name == null -> {
                    return false
                }
                medicine.quantity == null -> {
                    return false
                }
                medicine.dose == null -> {
                    return true
                }
                medicine.instruction == null -> {
                    return true
                }
                medicine.photo == null -> {
                    return true
                }
            }


        }
        return true
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == ImagePicker.IMAGE_PICKER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            if (data != null) {
                val mPaths: List<String> = data.getStringArrayListExtra(ImagePicker.EXTRA_IMAGE_PATH)
                val bitmap = BitmapFactory.decodeFile(mPaths[0])
                if (bitmap != null) {
                    binding.pictureImg1.setImageBitmap(Bitmap.createScaledBitmap(bitmap, 50, 50, false))
                    adaptor.getItems()[clickedLayoutID].photo = mPaths.get(0)
                    adaptor.notifyDataSetChanged()
                }

            }
        } else if (requestCode == Codes.ADDRESS_REQUEST && data != null) {
            val address = Utility.getgson().fromJson<Address>(data.getStringExtra(Constants.ADDRESS_DATA), Address::class.java)
            val intent = Intent(this@OrderMedicineActivity, MedicineOrderService::class.java)
            val arrayMedicine = adaptor.getItems() as ArrayList<MedicineModel>
            intent.putParcelableArrayListExtra(Constants.MEDICINE_ORDER_DATA, arrayMedicine)
            intent.putExtra(Constants.ADDRESS_DATA, Utility.getgson().toJson(address))
            showOrderDialog(intent)

        }
    }

    private fun showOrderDialog(intent: Intent) {
        Utility.getAlertDialoge(this, getString(R.string.processing_order), getString(R.string.processing_order_message)).setPositiveButton("Ok") { p0, p1 ->
            startService(intent)
            finish()
        }.show()
    }


    override fun OnEdit(position: Int, any: Any) {
        clickedLayoutID = position
    }

    override fun OnScroll() {
        binding.recyclerView.scrollToPosition(adaptor.itemCount - 1)
    }

    override fun openGallery(position: Int) {
        clickedLayoutID = position
        if (!hasPermissions(this, arrayOf(android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.READ_EXTERNAL_STORAGE))) {
            checkMediaPermission()
        } else {
            Utility.getMediaBuilder(this).build()
        }
    }

    override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {

    }

    override fun onDateSet(p0: DatePicker?, p1: Int, p2: Int, p3: Int) {
        val month = p2 + 1
        val c = Calendar.getInstance()
        c.set(Calendar.YEAR,p1)
        c.set(Calendar.MONTH,p2)
        c.set(Calendar.DAY_OF_MONTH,p3)
        val cDate=Calendar.getInstance().time
        val sDate=c.time
        if (sDate.after(cDate)) {
                proper_date = "" + p1 + ":" + month + ":" + p3
                Utility.Time_Picker_Dialog(this, this)

        } else {
            Utility.showToast(this, "Please Select Future Date")
            Utility.showDatePickerDialog(this, this)
        }


    }

    override fun onTimeSet(p0: TimePicker?, p1: Int, p2: Int) {
        val time_value: String = p1.toString() + ":" + p2.toString()
        adaptor.getItems()[0].orderDate = proper_date
        adaptor.getItems()[0].orderTime = time_value
        adaptor.getItems()[0].instance = false
        showAddressDialog()


    }


}
