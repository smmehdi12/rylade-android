package com.senarios.customer.activities

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapsInitializer
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.*
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.appbar.AppBarLayout.OnOffsetChangedListener
import com.google.gson.reflect.TypeToken
import com.muddzdev.styleabletoast.StyleableToast
import com.senarios.customer.R
import com.senarios.customer.adaptors.AdaptorOrderEcommerce
import com.senarios.customer.adaptors.AdaptorOrderMedicine
import com.senarios.customer.adaptors.RecyclerViewCallback
import com.senarios.customer.costants.Constants
import com.senarios.customer.costants.Messages
import com.senarios.customer.databinding.FragmentOrderEcommerceDetailsBinding
import com.senarios.customer.databinding.FragmentOrderMedicineDetailsBinding
import com.senarios.customer.databinding.FragmentOrderParcelDetailsBinding
import com.senarios.customer.databinding.FragmentOrderRideDetailsBinding
import com.senarios.customer.models.*
import com.senarios.customer.retrofit.APIConstants
import com.senarios.customer.retrofit.ApiResponse
import com.senarios.customer.retrofit.NetworkCall
import com.senarios.customer.utility.Utility
import com.senarios.customer.utility.Utility.Companion.bitmapDescriptorFromVector
import com.senarios.customer.utility.Utility.Companion.loadFullScreenImageView
import kotlinx.android.synthetic.main.toolbar.view.*
import retrofit2.Response
import java.lang.reflect.Type
import kotlin.math.roundToInt


class OrderDetailsActivity : BaseAcivity(),OnOffsetChangedListener,ApiResponse, OnMapReadyCallback,View.OnClickListener ,RecyclerViewCallback{
    private lateinit var vParcelbinding : FragmentOrderParcelDetailsBinding
    private lateinit var vMedicineBinding : FragmentOrderMedicineDetailsBinding
    private lateinit var vRideBinding : FragmentOrderRideDetailsBinding
    private lateinit var vEcommerceBinding: FragmentOrderEcommerceDetailsBinding
    private lateinit var ecommerceOrderResponse:BaseEcommerceOrderResponse
    private lateinit var order:OrderModel
    private lateinit var parcelOrderModel: ParcelModel
    private lateinit var rideModel: RideModel
    private lateinit var medicineModel:MedicineModel
    private lateinit var medicineList: MutableList<MedicineModel>
    private var scrollRange = -1

    private  var bundle: Bundle? = null



    override fun setBinding() {

        order = intent.getParcelableExtra(Constants.ORDER_DATA)!!
        if (::order.isInitialized && order != null) {
            callOrderDetailAPI()
        }
    }

    private fun initMedicineDetails() {
        vMedicineBinding.order=order
        vMedicineBinding.toolbar.setNavigationOnClickListener(this)
        vMedicineBinding.cancel.setOnClickListener(this)
        vMedicineBinding.imageviewMedicineOrder.setOnClickListener(this)
        vMedicineBinding.recyclerView.adapter=AdaptorOrderMedicine(this,medicineList,this)
        vMedicineBinding.data=medicineList[0]
        if (order.status!!.equals(Constants.ORDER_STATUS_PENDING,true)){
            vMedicineBinding.cancel.visibility=View.VISIBLE
        }

    }

    private fun callOrderDetailAPI() {
        val list:MutableList<ModelCommunication> = mutableListOf()
        list.add(ModelCommunication(Constants.ORDER_ID, order.id.toString()))
        NetworkCall.callAPI(this,getService().getOrderDetails(
            APIConstants.BEARER+ Utility.getUser(this).token,
            Utility.getJson(list))
            ,this,
            true
            , APIConstants.GET_ORDER_DETAIl)
    }

    private fun initParcelDetails() {
        vParcelbinding.order=order
        vParcelbinding.data=parcelOrderModel
        vParcelbinding.collapseActionView.title="Parcel Delivery"
        vParcelbinding.toolbar.setNavigationOnClickListener(this)
        vParcelbinding.appbar.addOnOffsetChangedListener(this)
        vParcelbinding.imageview.setOnClickListener(this)
        vParcelbinding.cancel.setOnClickListener(this)
        if (order.status!!.equals(Constants.ORDER_STATUS_PENDING,true)){
            vParcelbinding.cancel.visibility=View.VISIBLE
        }

    }

    private fun initRideDetails() {
        vRideBinding.toolbar.setNavigationOnClickListener(this)
        vRideBinding.cancel.setOnClickListener(this)
        vRideBinding.mapView.onCreate(bundle)
        vRideBinding.mapView.getMapAsync(this)
        if (order.status!!.equals(Constants.ORDER_STATUS_PENDING,true)){
            vRideBinding.cancel.visibility=View.VISIBLE
        }

    }

    override fun init() {

    }

    override fun OnCancelPermissioDialog() {

    }

    override fun OnPermissionApproved() {

    }

    override fun OnTrigger() {

    }

    override fun onOffsetChanged(appBarLayout: AppBarLayout?, verticalOffset: Int) {
        if (scrollRange == -1) {
            scrollRange = appBarLayout!!.totalScrollRange
        }
        if (scrollRange + verticalOffset == 0) {
            vParcelbinding.toolbar.setBackgroundColor(ContextCompat.getColor(this@OrderDetailsActivity, R.color.white))
            vParcelbinding.toolbar.setNavigationIcon(R.drawable.close)
        } else {
            vParcelbinding.toolbar.title="Parcel Delivery"
            vParcelbinding.toolbar.setNavigationIcon(R.drawable.ic_close)
            vParcelbinding.toolbar.setBackgroundColor(ContextCompat.getColor(this@OrderDetailsActivity,
                R.color.transparent))
        }
    }

    override fun OnSuccess(endpoint: String, t: Response<ModelBaseResponse>, response: String) {
        try{
            if (APIConstants.GET_ORDER_DETAIl.equals(endpoint,true)){
                when (order.order_type) {
                    1 -> {
                        val listType: Type = object : TypeToken<List<MedicineModel?>?>() {}.getType()
                        medicineList = Utility.getgson().fromJson(response, listType)
                        vMedicineBinding = DataBindingUtil.setContentView(this, R.layout.fragment_order_medicine_details)
                        initMedicineDetails()
                    }
                    2 -> {
                        parcelOrderModel = Utility.getgson().fromJson(response, ParcelModel::class.java)
                        vParcelbinding = DataBindingUtil.setContentView(this, R.layout.fragment_order_parcel_details)
                        initParcelDetails()
                    }
                    3 -> {
                        rideModel = Utility.getgson().fromJson(response, RideModel::class.java)
                        vRideBinding = DataBindingUtil.setContentView(this, R.layout.fragment_order_ride_details)
                        initRideDetails()
                    }
                    4 -> {
                        ecommerceOrderResponse=Utility.getgson().fromJson(response, BaseEcommerceOrderResponse::class.java)
                        vEcommerceBinding=DataBindingUtil.setContentView(this,R.layout.fragment_order_ecommerce_details)
                        initEcommerceDetails()
                    }
                    else -> {

                    }
                }
            }
            else if (endpoint.equals(APIConstants.CANCEL_ORDER,true)){
                finish()
                StyleableToast.makeText(this, "Order Cancelled Successfully!", Toast.LENGTH_LONG, R.style.mytoast).show();

            }


        }
        catch (e:Exception){
            OnException(endpoint,e)
        }

    }

    private fun initEcommerceDetails() {
        vEcommerceBinding.order=order
        vEcommerceBinding.address=ecommerceOrderResponse.address
        vEcommerceBinding.product=ecommerceOrderResponse.list[0]
        vEcommerceBinding.toolbar.toolbar.setNavigationOnClickListener(this)
        vEcommerceBinding.toolbar.toolbar.title="Ecommerce Order Details"
        vEcommerceBinding.cancel.setOnClickListener(this)
        vEcommerceBinding.recyclerView.adapter=AdaptorOrderEcommerce(this,ecommerceOrderResponse.list,this)
        if (order.status!!.equals(Constants.ORDER_STATUS_PENDING,true)){
            vEcommerceBinding.cancel.visibility=View.VISIBLE
        }


    }

    override fun OnStatusfalse(endpoint:String,t: Response<ModelBaseResponse>, response: String, message: String) {
        Utility.showErrorDialogBuilder(this@OrderDetailsActivity,R.layout.dialog_error_505, Messages.ERROR_TITLE,message)
            .setPositiveButton("OK", { dialog, which -> finish() })
            .show()
    }

    override fun OnError(endpoint:String,code: Int, message: String) {
        Utility.showErrorDialogBuilder(this@OrderDetailsActivity,R.layout.dialog_error_505, Messages.ERROR_TITLE,message)
            .setPositiveButton("OK", { dialog, which -> finish() })
            .show()
    }


    override fun OnException(endpoint:String,exception: Throwable) {
        Utility.showErrorDialogBuilder(this@OrderDetailsActivity,R.layout.dialog_error, Messages.ERROR_TITLE,getString(R.string.something_went_wrong))
            .setPositiveButton("OK", { dialog, which -> finish() })
            .show()

    }

    override fun OnNetworkError(endpoint:String,message: String) {
        Utility.showNErrorDialogwithFinish(this@OrderDetailsActivity)
    }


    override fun OnClick(position: Int, any: Any) {
        if (any is MedicineModel) {
            loadFullScreenImageView(this, APIConstants.BASE_MEDICINE_IMAGE + (any as MedicineModel).photo)
        }
        else if (any is CartModel){
            loadFullScreenImageView(this, APIConstants.BASE_PRODUCT_PHOTO + (any as CartModel).image)
        }

    }

    override fun onClick(v: View?) {
        val id=v?.id
        if (isParcel() && id == vParcelbinding.imageview.id) {
            loadFullScreenImageView(this, APIConstants.BASE_PARCEL_IMAGE + parcelOrderModel.photo)
        }
        else if (isParcel() && id == vParcelbinding.imageview.id) {
            loadFullScreenImageView(this, APIConstants.BASE_PARCEL_IMAGE + parcelOrderModel.photo)
        }
        else if (isMedicine() && id == vMedicineBinding.cancel.id) {
            callCancelAPI()
        }
        else if (isParcel() && id == vParcelbinding.cancel.id) {
            callCancelAPI()
        }
        else if (isRide() && id == vRideBinding.cancel.id) {
            callCancelAPI()
        }
        else if(isMedicine()&&id==vMedicineBinding.imageviewMedicineOrder.id)
        {
            loadFullScreenImageView(this,APIConstants.BASE_MEDICINE_IMAGE+medicineList[0].photo)
        }
        else if (isEcom() && id==vEcommerceBinding.cancel.id){
            callCancelAPI()
        }
        else{
            finish()
        }
    }

    private fun callCancelAPI() {
        val list:MutableList<ModelCommunication> = mutableListOf()
        list.add(ModelCommunication(Constants.USER_ID, Utility.getUser(this).id.toString()))
        list.add(ModelCommunication(Constants.ORDER_ID, order.id.toString()))
        NetworkCall.callAPI(this,
            Utility.getService().cancelOrder(APIConstants.BEARER+ Utility.getUser(this).token,Utility.getJson(list)),this, true, APIConstants.CANCEL_ORDER)


    }

    private fun isParcel():Boolean{
        return ::vParcelbinding.isInitialized
    }
    private fun isMedicine():Boolean{
        return ::vMedicineBinding.isInitialized
    }
    private fun isRide():Boolean{
        return ::vRideBinding.isInitialized
    }

    private fun isEcom():Boolean{
        return ::vEcommerceBinding.isInitialized
    }

    override fun onMapReady(p0: GoogleMap?) {
        if (isRide()&&p0!=null){
            try {
                p0.uiSettings.isMyLocationButtonEnabled = true
                p0.setMapStyle(MapStyleOptions.loadRawResourceStyle(this,R.raw.map_style))
                MapsInitializer.initialize(this)
                p0.addMarker(
                    MarkerOptions().icon(bitmapDescriptorFromVector(this, R.drawable.ic_dropoff_location))
                        .title("Drop Off")
                        .position(
                            LatLng(rideModel.dropoff_lat!!.toDouble(), rideModel.dropoff_lon!!.toDouble())))
                p0.addMarker(MarkerOptions().icon(bitmapDescriptorFromVector(this, R.drawable.ic_pickup_location))
                        .title("Pick Up")
                        .position(LatLng(rideModel.pickup_lat!!.toDouble(), rideModel.pickup_lon!!.toDouble()))
                )
                moveCamera(getBounds(), p0)
                setRideData()
                vRideBinding.mapView.onResume()
            }
            catch (e:Exception){
                OnException("",e)
            }

        }

    }

    private fun setRideData() {
        if (rideModel.estimated_distance.isNullOrEmpty()){
            rideModel.estimated_distance="0"
        }

        if (!rideModel.estimate_time.isNullOrEmpty()){
            if (rideModel.estimate_time!!.toInt()<60) {
                rideModel.estimate_time = "0";
            }
            else{
                rideModel.estimate_time=""+(rideModel.estimate_time!!.toInt()/60.0).roundToInt()
            }
        }
        else{
            rideModel.estimate_time = "0";
        }
        if (!rideModel.total_time.isNullOrEmpty()){
            if (rideModel.total_time!!.split(":")[0].toInt()<0 && rideModel.total_time!!.split(":")[1].toInt()<60) {
                rideModel.total_time = "0";
            }
            else{
                rideModel.total_time=""+(rideModel.total_time!!.split(":")[0].toInt() +(rideModel.total_time!!.split(":")[1].toInt()/60.0)).roundToInt()
            }
        }
        else{
            rideModel.total_time = "0";
        }
        if (!rideModel.waiting_time.isNullOrEmpty()){
            rideModel.waiting_time=""+(rideModel.waiting_time!!.toInt()/60.0).roundToInt()
        }
        else{
            rideModel.waiting_time="0"
        }
        if (rideModel.waiting_cost.isNullOrEmpty()){
            rideModel.waiting_cost="0"
        }


        vRideBinding.data = rideModel
    }

    private fun moveCamera(bounds: LatLngBounds, map: GoogleMap){
        map.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds,100))
    }
    private fun getBounds(): LatLngBounds {
        val builder = LatLngBounds.Builder()
        builder.include(LatLng(rideModel.dropoff_lat!!.toDouble(),rideModel.dropoff_lon!!.toDouble()))
        builder.include(LatLng(rideModel.pickup_lat!!.toDouble(),rideModel.pickup_lon!!.toDouble()))
        return builder.build()
    }
    override fun onStart() {
        super.onStart()
        if (isRide()){
            vRideBinding.mapView.onStart()
        }
    }

    override fun onResume() {
        super.onResume()
        if (isRide()){
            vRideBinding.mapView.onResume()
        }
    }

    override fun onStop() {
        super.onStop()
        if (isRide()){
            vRideBinding.mapView.onStop()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (isRide()){
            vRideBinding.mapView.onDestroy()
        }
    }

    override fun onLowMemory() {
        super.onLowMemory()
        if (isRide()){
            vRideBinding.mapView.onLowMemory()
        }
    }

}