package com.senarios.customer.activities

import android.content.DialogInterface
import android.content.Intent
import android.view.View
import android.view.View.*
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.google.gson.JsonObject
import com.google.gson.reflect.TypeToken
import com.senarios.customer.R
import com.senarios.customer.adaptors.AdaptorCartItems
import com.senarios.customer.adaptors.RecyclerViewCallback
import com.senarios.customer.costants.Codes
import com.senarios.customer.costants.Constants
import com.senarios.customer.databinding.ActivityCartBinding
import com.senarios.customer.dialogfragments.LoginSignupDialog
import com.senarios.customer.dialogfragments.PromoCodeDialog
import com.senarios.customer.models.*
import com.senarios.customer.retrofit.APIConstants
import com.senarios.customer.retrofit.ApiResponse
import com.senarios.customer.retrofit.NetworkCall
import com.senarios.customer.utility.Utility
import kotlinx.android.synthetic.main.toolbar.view.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import retrofit2.Response
import java.lang.reflect.Type
import kotlin.math.roundToInt

class CartActivity : BaseAcivity(),ApiResponse ,RecyclerViewCallback,Observer<MutableList<CartModel>>, OnClickListener{
    private lateinit var binding:ActivityCartBinding
    private lateinit var adaptor:AdaptorCartItems
    private lateinit var address: Address
    private lateinit var promoModel: PromoModel
    private lateinit var cartList: MutableList<CartModel>
    private val model= EcommerceOrderModel()


    override fun onResume() {
        super.onResume()
            if (Utility.getPreference(this).contains(Constants.PROMO_DATA)){
                promoModel=Utility.getgson().fromJson(Utility.getPreferences(this,Constants.PROMO_DATA,"").toString(),PromoModel::class.java)
                Utility.showLog("Promo "+Utility.getgson().toJson(promoModel))
            }
            if (Utility.getPreference(this).contains(Constants.ADDRESS_DATA)){
                address=Utility.getgson().fromJson(Utility.getPreferences(this,Constants.ADDRESS_DATA,"").toString(),Address::class.java)
                Utility.showLog("Address "+Utility.getgson().toJson(address))
                setAddress()
        }
    }



    override fun setBinding() {
        binding=DataBindingUtil.setContentView(this, R.layout.activity_cart)

    }

    override fun init() {
        adaptor = AdaptorCartItems(this@CartActivity,this)
        binding.recyclerview.adapter=adaptor
        binding.model=model
        Utility.getDB().getCart().observe(this,this)
        binding.toolbar.toolbar.setNavigationOnClickListener(this)
        binding.toolbar.toolbar.title="Cart"
        binding.cvAddress.setOnClickListener(this)
        binding.btnPlaceOrder.setOnClickListener(this)
        binding.tvPromo.setOnClickListener(this)
        binding.tvRemovePromo.setOnClickListener(this)
    }

    override fun OnCancelPermissioDialog() {

    }

    override fun OnPermissionApproved() {

    }

    override fun OnTrigger() {

    }

    override fun updateCartItem(position: Int, cartModel: CartModel, quantity: Int,flag:Boolean) {
        updateQuantity(position, cartModel, quantity, flag)
    }

    private fun updateQuantity(position: Int, cartModel: CartModel, quantity: Int,flag:Boolean) {
        var count=quantity
        if (count>=1 ) {
            if (flag) {
                count++
            } else {
                count--
            }
            if (count < 1) {
                Utility.getDB().deleteCartItem(cartModel.id!!)
            } else if (count >= 1) {
                Utility.getDB().updateQuantity(cartModel.id!!, count)
            }
        }

    }

    override fun onChanged(t: MutableList<CartModel>?) {
        val instant = Utility.getgson().fromJson<INIModel>(Utility.getPreferences(this, Constants.INSTANT, "").toString(), INIModel::class.java)

        if (t!=null && t.size>0){
            cartList=t
            binding.tvCounter.text=""+t.size
            Utility.getDB().getVendorDeliveryStatus(t[0].vendorId!!).observe(this, Observer {
                if (it==0){
                    if (instant!=null) {
                        if (instant.delivery_fee != null) {
                            model.deliveryFee = instant.delivery_fee.toDouble()
                            Utility.showLog("Delivery Fee "+model.deliveryFee)
                        }
                    }
                    applyCalculations(false)

                }
                else{
                    applyCalculations(false)
                }
            })
        }
        else{
            clearCart()
        }
    }

    private fun applyCalculations(  removePromo : Boolean) {
        model.reset()
        if (::promoModel.isInitialized) {
            model.isPromo = !removePromo
            model.promoPercent = promoModel.discountPercentage!! / 100.0
            Utility.showLog("Promo Percent "+model.promoPercent)
        }
        if (::cartList.isInitialized) {
            for (item in cartList) {
                if (model.isPromo) {

                    val promoAmount=item.price!!*model.promoPercent;

                    val discountedPrice= (item.price!!-promoAmount)

                    model.promoAmount += promoAmount * item.quantity!!

                    Utility.showLog("Discount price $discountedPrice")

                    Utility.showLog("Promo Amount $discountedPrice")

                    model.taxAmount +=((item.sales_tax / 100.0) * discountedPrice) * item.quantity!!

                    Utility.showLog("Tax Percent "+item.sales_tax/100.0)

                    Utility.showLog("Tax Amount "+model.taxAmount)

                    model.subTotal += (discountedPrice * item.quantity!!)

                    Utility.showLog("Subtotal "+model.subTotal)

                    model.totalAmount += (discountedPrice* item.quantity!!)
                }
                else{
                    model.taxAmount +=((item.sales_tax / 100.0) * item.price!!) * item.quantity!!
                    Utility.showLog("Tax Percent "+item.sales_tax/100.0)
                    Utility.showLog("Tax Amount "+model.taxAmount)

                    model.subTotal += (item.price!! * item.quantity!!)

                    Utility.showLog("Subtotal "+model.subTotal)

                    model.totalAmount += (item.price!! * item.quantity!!)


                }
            }
        }

        model.totalAmount+=model.deliveryFee+model.taxAmount
        Utility.showLog("Total "+model.totalAmount)

        if (model.isPromo) {
            binding.groupCoupon.visibility = VISIBLE
            binding.tvPromo.visibility = GONE
            binding.tvRemovePromo.visibility = VISIBLE

            model.promoTitle=promoModel.promoCodeName!!
            model.promoID=promoModel.id!!

        }else{
            model.subTotal=model.promoAmount+model.subTotal
            model.totalAmount=model.promoAmount+model.totalAmount

            binding.groupCoupon.visibility= GONE
            binding.tvPromo.visibility= VISIBLE
            binding.tvRemovePromo.visibility= GONE

            model.isPromo=false
            model.promoAmount=0.0
            model.promoID=0
            model.promoPercent=0.0
            model.promoTitle=""
            Utility.getPreference(this).edit().remove(Constants.PROMO_DATA).apply()
        }

        adaptor.setData(cartList)

    }





    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
    if (requestCode== Codes.ADDRESS_REQUEST && data!=null){
        address=Utility.getgson().fromJson(data.getStringExtra(Constants.ADDRESS_DATA), Address::class.java)
        setAddress()
        Utility.setPreferences(this,Constants.ADDRESS_DATA,Utility.getgson().toJson(address))
        }
    }

    private fun setAddress(){
        binding.tvAddress.text=address.address
        binding.tvAddress.visibility= VISIBLE
        model.address=address.address
        model.addressID=address.id
    }


    override fun onClick(v: View?) {
        when(v?.id){
            binding.cvAddress.id-> {
                if (Utility.checkIfUserLogged(this)) {
                    startActivityForResult(Intent(this@CartActivity, AddressActivity::class.java), Codes.ADDRESS_REQUEST
                    )
                }
                else{
                    showDialog(LoginSignupDialog())
                }
            }
            binding.btnPlaceOrder.id->{
                if (binding.tvAddress.visibility== GONE){
                    showToast(this,"Please Select Delivery Address")
                }
                else{
                    if (Utility.checkIfUserLogged(this)) {
                        createOrderDialog()
                    }
                    else{
                        showDialog(LoginSignupDialog())
                    }

                }
            }
            binding.tvPromo.id->{
                showDialog(PromoCodeDialog())
            }
            binding.tvRemovePromo.id->{
                showClearPromoDialog()
            }

            else->{
                finish()
            }
        }
    }

    private fun showClearPromoDialog() {
        Utility.getAlertDialoge(this,"Remove Promo "+model.promoTitle,"Are you sure you want to remove this Promo Code?")
            .setPositiveButton("Yo", object : DialogInterface.OnClickListener {
                override fun onClick(dialog: DialogInterface?, which: Int) {
                    applyCalculations(true)
                }

            })
            .setNegativeButton("Na", object : DialogInterface.OnClickListener {
                override fun onClick(dialog: DialogInterface?, which: Int) {
                    dialog?.dismiss()
                }

            })
            .show()

    }


    private fun createOrderDialog() {
        Utility.getAlertDialoge(this,"Confirmation","Do you want to place this order?")
            .setPositiveButton("Ok"
            ) { dialog, _ -> createOrder()
            }
            .setNegativeButton("Cancel"
            ) { dialog, _ ->
                dialog.dismiss()
            }
            .show()
    }


    private fun createOrder() {
        val json=JsonObject()
        if (model.isPromo) {
            json.addProperty(Constants.IS_PROMO,1)
            json.addProperty(Constants.PROMO_AMOUNT,model.promoAmount)
            json.addProperty(Constants.PROMO_ID,model.promoID)
        }
        else{
            json.addProperty(Constants.IS_PROMO,0)
        }
        json.addProperty(Constants.SALES_TAX,model.taxAmount.roundToInt().toString())
        json.addProperty(Constants.VENDOR_ID,adaptor.getData()[0].vendorId)
        json.addProperty(Constants.USER_ID,Utility.getUser(this).id.toString())
        json.addProperty(Constants.SUB_TOTAL,""+model.subTotal.roundToInt())
        json.addProperty(Constants.TOTAL_AMOUNT,""+model.totalAmount.roundToInt())
        json.addProperty(Constants.ADDRESS_ID,model.addressID)
        json.addProperty(Constants.DELIVERY_FEES, ""+model.deliveryFee.roundToInt())
        json.add("items",Utility.getJsonArrayFromList(adaptor.getData()))
        val data=Utility.getgson().toJson(json)
        Utility.showLog("ECOM Data"+data)
        NetworkCall.callAPI(this,getService().order_Ecommerce(APIConstants.BEARER+Utility.getUser(this).token,json),this,true,APIConstants.ORDER_ECOMMERCE)

    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    fun getPromo(promoModel: PromoModel){
        this.promoModel=promoModel
        applyCalculations(false)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun showError(modelCommunication: ModelCommunication){
        Utility.showAlerter(this,"Error",modelCommunication.value)
    }


    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun OnSuccess(endpoint: String, t: Response<ModelBaseResponse>, response: String) {
        try{
            Utility.showFirebaseNotification(this, "Rylade Order", "Your Order has been placed Successfully",Intent(this, MyOrdersActivity::class.java))
            clearCart()

        }
        catch (e:Exception){
            OnException(endpoint,e)
        }
    }

    override fun OnStatusfalse(endpoint: String, t: Response<ModelBaseResponse>, response: String, message: String) {
//        showError(ModelCommunication("",message))
        if (t.body()!!.message.contains("Quantity",true)){
            val listType: Type = object : TypeToken<List<ProductModel?>?>() {}.getType()
            val list = Utility.getgson().fromJson<MutableList<ProductModel>>(response, listType)
            for (item in adaptor.getData()){
                for(revert_item in list){
                    if (item.product_id==revert_item.id){
                        getDAO().updateOutOfOrder(item.id!!,true)
                    }
                }
            }
            Utility.getAlertDialoge(this,"Out of Stock","You have some items in your cart which are out of stock, please remove them to proceed!")
                .setPositiveButton("Ok") { dialog, which -> adaptor.notifyDataSetChanged() }.show()
        }
     }

    override fun OnError(endpoint: String, code: Int, message: String) {
        showError(ModelCommunication("",message))
     }

    override fun OnException(endpoint: String, exception: Throwable) {
        showError(ModelCommunication("",""+exception.message))
    }

    override fun OnNetworkError(endpoint: String, message: String) {
        showError(ModelCommunication("",message))
    }

    private fun clearCart(){
        Utility.getDB().clearCartTable()
        Utility.getPreference(this).edit().remove(Constants.PROMO_DATA).apply()
//        Utility.getPreference(this).edit().remove(Constants.ADDRESS_DATA).apply()
        startActivity(Intent(this,EcommerceActivity::class.java).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK))
    }
}