package com.senarios.customer.activities

import android.view.View
import androidx.databinding.DataBindingUtil
import com.google.gson.reflect.TypeToken
import com.senarios.customer.R
import com.senarios.customer.adaptors.AdaptorFAQ
import com.senarios.customer.adaptors.RecyclerViewCallback
import com.senarios.customer.costants.Messages
import com.senarios.customer.databinding.ActivityFaqBinding
import com.senarios.customer.models.FaqModel
import com.senarios.customer.models.ModelBaseResponse
import com.senarios.customer.retrofit.APIConstants
import com.senarios.customer.retrofit.ApiResponse
import com.senarios.customer.retrofit.NetworkCall
import com.senarios.customer.utility.Utility
import kotlinx.android.synthetic.main.toolbar.view.*
import retrofit2.Response
import java.lang.reflect.Type

class FAQActivity : BaseAcivity() ,ApiResponse,RecyclerViewCallback,View.OnClickListener{
    private lateinit var binding:ActivityFaqBinding
    private lateinit var adaptor: AdaptorFAQ


    override fun setBinding() {
        binding=DataBindingUtil.setContentView(this, R.layout.activity_faq)
    }

    override fun init() {
        binding.toolbar.toolbar.title="FAQ's"
        binding.toolbar.toolbar.setNavigationOnClickListener(this)
        NetworkCall.callAPI(this,getService().getFAQ(),this,true, APIConstants.GET_FAQ)
    }

    override fun OnCancelPermissioDialog() {

     }

    override fun OnPermissionApproved() {

     }

    override fun OnTrigger() {

    }

    override fun OnSuccess(endpoint: String, t: Response<ModelBaseResponse>, response: String) {

        val listType: Type = object : TypeToken<List<FaqModel>>() {}.getType()
        val list= Utility.getgson().fromJson<MutableList<FaqModel>>(response, listType)
        adaptor= AdaptorFAQ(this@FAQActivity,list,this)
        binding.recyclerView.adapter=adaptor
    }

    override fun OnStatusfalse(endpoint:String,t: Response<ModelBaseResponse>, response: String, message: String) {
        Utility.showErrorDialog(this@FAQActivity,R.layout.dialog_error_505, Messages.ERROR_TITLE,message)
    }

    override fun OnError(endpoint:String,code: Int, message: String) {
        Utility.showErrorDialog(this@FAQActivity,R.layout.dialog_error_505, Messages.ERROR_TITLE,message)
    }


    override fun OnException(endpoint:String,exception: Throwable) {
        Utility.showErrorDialog(this@FAQActivity,R.layout.dialog_error, Messages.ERROR_TITLE,getString(R.string.something_went_wrong))
    }

    override fun OnNetworkError(endpoint:String,message: String) {
        Utility.showNErrorDialog(this@FAQActivity)
    }

    override fun onClick(v: View?) {
        finish()
    }
}