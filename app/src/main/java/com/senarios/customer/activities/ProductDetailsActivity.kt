package com.senarios.customer.activities

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer

import com.senarios.customer.R
import com.senarios.customer.costants.Constants
import com.senarios.customer.databinding.ActivityProductDetailsBinding
import com.senarios.customer.dialogfragments.ReviewFragmentDialog
import com.senarios.customer.models.*
import com.senarios.customer.retrofit.APIConstants
import com.senarios.customer.retrofit.ApiResponse
import com.senarios.customer.retrofit.NetworkCall
import com.senarios.customer.utility.Utility
import com.senarios.customer.utility.Utility.Companion.showELog
import kotlinx.android.synthetic.main.toolbar.view.*
import retrofit2.Response
import java.lang.Exception
import java.text.DecimalFormat

class ProductDetailsActivity : BaseAcivity(), View.OnClickListener ,ApiResponse,
    Observer<MutableList<CartModel>>{
    private lateinit var productModel: ProductModel
    private lateinit var binding:ActivityProductDetailsBinding
    private var cartModel:CartModel? = null
    private lateinit var menuItem: MenuItem



    override fun setBinding() {
        binding=DataBindingUtil.setContentView(this, R.layout.activity_product_details)
    }

    override fun init() {
        if (intent.hasExtra(Constants.PRODUCT_DATA)){
            setToolbar()
            productModel=intent.getParcelableExtra(Constants.PRODUCT_DATA)!!
            binding.data=productModel
            binding.btnAddToCart.setOnClickListener(this)
            binding.btnAdd.setOnClickListener(this)
            binding.btnMinus.setOnClickListener(this)
            binding.tvRating.setOnClickListener(this)
            val list= mutableListOf<ModelCommunication>()
            list.add(ModelCommunication(Constants.VENDOR_ID, productModel.vendorId.toString()))
            list.add(ModelCommunication(Constants.PRODUCT_ID, productModel.id.toString()))
            NetworkCall.callAPI(
                this,
                getService().getReviewsFromUser(Utility.getJson(list)),
                this,
                true,
                APIConstants.GET_REVIEWS
            )
        }

    }

    private fun setToolbar() {
        binding.toolbar.toolbar.title="Product Details"
        setSupportActionBar(binding.toolbar.toolbar)
        binding.toolbar.toolbar.inflateMenu(R.menu.profile_menu)
        binding.toolbar.toolbar.setNavigationOnClickListener(this)
    }

    override fun OnCancelPermissioDialog() {

    }

    override fun OnPermissionApproved() {

    }

    override fun OnTrigger() {

    }

    override fun onClick(v: View?) {
        when (v?.id){
            binding.btnAddToCart.id->{
                if (Utility.getDB().getVendorIDFromCart()!=0L && Utility.getDB().getVendorIDFromCart().toInt()!=productModel.vendorId) {
                    popError()
                }
                else{
                    addToCart()
                }
            }
            binding.btnAdd.id->{
                updateQuantity(true)
            }
            binding.btnMinus.id->{
                updateQuantity(false)
            }
            binding.rating.id->{
                OnChange(ReviewFragmentDialog())
            }
            R.id.cartView->{
                startActivity(Intent(this@ProductDetailsActivity,CartActivity::class.java))
                finish()
            }
//            R.id.peopele->{
//                OnChange(ReviewFragmentDialog())
//            }
            binding.tvRating.id->{
                val fragment=ReviewFragmentDialog()
                val bundle =Bundle()
                bundle.putParcelable(Constants.PRODUCT_DATA,productModel)
                fragment.arguments= bundle
                showDialog(fragment)
            }
            else->{
                finish()
            }
        }


    }

    private fun popError() {
        Utility.getAlertDialoge(this,"","Your will lose your previous cart items.")
            .setPositiveButton("Ok"
            ) { dialog, which ->
                Utility.getDB().clearCartTable()
                addToCart()
            }
            .setNegativeButton("Cancel"
            ) { dialog, which ->
                dialog.dismiss()
            }
            .show()
    }

    private fun updateQuantity(flag:Boolean) {
        var count=binding.tvCounter.text.toString().toInt()
        if (count>=1 ) {
            if (flag) {
                count++
            } else {
                count--
            }
            binding.tvCounter.text = "" + count
            if (count < 1) {
                Utility.getDB().deleteCartItem(cartModel!!.id!!)
                cartModel = null
                binding.btnAddToCart.visibility = View.VISIBLE
            } else if (count >= 1) {
                cartModel!!.quantity=count
                Utility.getDB().updateQuantity(cartModel!!.id!!, binding.tvCounter.text.toString().toInt())
            }
        }

    }

    private fun addToCart() {
        if (productModel.iz_discount==1){
            cartModel=CartModel(null,productModel.categoryId,productModel.categoryName,productModel.createdAt,productModel.description,
                productModel.id,productModel.image,productModel.fixed_discount,productModel.productName,productModel.promoCodeId,productModel.quantityAvailable,
                productModel.status,productModel.subCategoryName,productModel.updatedAt,productModel.weight,
                productModel.vendorId,null,1,0,productModel.sales_tax,productModel.fixed_discount,productModel.iz_discount,productModel.price,-1,false)
        }
        else{
            cartModel=CartModel(null,productModel.categoryId,productModel.categoryName,productModel.createdAt,productModel.description,
                productModel.id,productModel.image,productModel.price,productModel.productName,productModel.promoCodeId,productModel.quantityAvailable,
                productModel.status,productModel.subCategoryName,productModel.updatedAt,productModel.weight,
                productModel.vendorId,null,1,0,productModel.sales_tax,productModel.fixed_discount,productModel.iz_discount,productModel.price,-1,false)
        }
        cartModel!!.id=Utility.getDB().insertCart(cartModel!!)
        binding.btnAddToCart.visibility=View.GONE
        binding.tvCounter.text="1"
    }



    override fun onChanged(t: MutableList<CartModel>?) {
        if (isMenu()) {
        if (t!=null && t.size>0) {
            menuItem.isVisible = true
                updateMenuItem(t.size)
            } else {
                menuItem.isVisible = false
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.cart_menu,menu)
        menuItem=menu!!.findItem(R.id.cart)
        (menuItem.actionView as ConstraintLayout).setOnClickListener(this)
        Utility.getDB().getCart().observe(this,this)
        return true
    }

    private fun updateMenuItem(count:Int){
        val view=menuItem.actionView
        val tvCounter=view.findViewById<TextView>(R.id.tv_counter)
        tvCounter.text=""+count
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId==R.id.search){
            startActivity(Intent(this@ProductDetailsActivity,SearchActivity::class.java))
        }
        return true
    }

    private fun isMenu():Boolean{
        return ::menuItem.isInitialized
    }

    override fun OnSuccess(endpoint: String, t: Response<ModelBaseResponse>, response: String) {
        val data=Utility.getgson().fromJson(response,ResponseProductReviews::class.java)
        try {
            if (data!=null){
                if (!data.comments.isNullOrEmpty() && !data.average.isNullOrEmpty()){
                    binding.rating.text=DecimalFormat("#.##").format(data.average.toDouble()) + "("+ data.comments.size +"+)"
                    binding.rating.setOnClickListener(this)
                    getViewModel().setVendor(data.vendor!!)
                    getViewModel().setComments(data.comments as ArrayList<CommentsItem>)
                }
                else{
                    binding.rating.visibility=View.GONE
                }

            }
        }
        catch (e:Exception){
            showELog(e)
        }


    }

    override fun OnStatusfalse(endpoint: String, t: Response<ModelBaseResponse>, response: String, message: String) {

    }

    override fun OnError(endpoint: String, code: Int, message: String) {

    }

    override fun OnException(endpoint: String, exception: Throwable) {

    }

    override fun OnNetworkError(endpoint: String, message: String) {

    }

}