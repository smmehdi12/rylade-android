package com.senarios.customer.activities

import android.content.Intent
import android.view.View
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import com.muddzdev.styleabletoast.StyleableToast
import com.senarios.customer.R
import com.senarios.customer.adaptors.AdaptorOrders
import com.senarios.customer.adaptors.RecyclerViewCallback
import com.senarios.customer.costants.Constants
import com.senarios.customer.costants.Constants.Companion.ORDER_DATA
import com.senarios.customer.costants.Messages
import com.senarios.customer.databinding.FragmentOrdersBinding
import com.senarios.customer.models.ModelBaseResponse
import com.senarios.customer.models.ModelCommunication
import com.senarios.customer.models.OrderListsModel
import com.senarios.customer.models.OrderModel
import com.senarios.customer.retrofit.APIConstants
import com.senarios.customer.retrofit.ApiResponse
import com.senarios.customer.retrofit.NetworkCall
import com.senarios.customer.utility.Utility
import com.stepstone.apprating.listener.RatingDialogListener
import org.json.JSONObject
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*
import kotlin.Comparator


class MyOrdersActivity : BaseAcivity()  ,RecyclerViewCallback, RatingDialogListener,View.OnClickListener,ApiResponse{
     private lateinit var binding:FragmentOrdersBinding
    private lateinit var pAdaptor:AdaptorOrders
    private lateinit var cAdaptor:AdaptorOrders
    private var position:Int=-1


    override fun setBinding() {
        binding=DataBindingUtil.setContentView(this, R.layout.fragment_orders)
    }

    override fun onResume() {
        super.onResume()
       callOrderAPI()
    }

    private fun callOrderAPI() {
        val list:MutableList<ModelCommunication> = mutableListOf()
        list.add(ModelCommunication(Constants.USER_ID,Utility.getUser(this).id.toString()))
        NetworkCall.callAPI(this,getService().getOrders(APIConstants.BEARER+ Utility.getUser(this).token,Utility.getJson(list))
            ,this,
            true
            ,APIConstants.GET_ORDER)
    }

    override fun init() {
       binding.toolbar.setNavigationOnClickListener(this)
    }

    override fun OnCancelPermissioDialog() {

    }

    override fun OnPermissionApproved() {

    }

    override fun OnTrigger() {
        callOrderAPI()
    }

    override fun OnClick(position: Int, any: Any) {
        if (any is OrderModel) {
            if (any.order_type==3){
                if (any.status.equals(Constants.ORDER_STATUS_COMPLETED)){
                    startActivity(Intent(this@MyOrdersActivity, OrderDetailsActivity::class.java).putExtra(ORDER_DATA, any))
                }
                else{
                    startActivity(Intent(this@MyOrdersActivity, RequestRideActivity::class.java))
                }


            }
            else {
                startActivity(Intent(this@MyOrdersActivity, OrderDetailsActivity::class.java).putExtra(ORDER_DATA, any))
            }

        }
    }

    override fun onClick(v: View?) {
        when(v?.id){
            else->{
                finish()
            }
        }
    }

    override fun OnSuccess(endpoint: String, t: Response<ModelBaseResponse>, response: String) {
        try{

            if (APIConstants.GET_ORDER.equals(endpoint)) {

                val json = JSONObject(response)
                val listsModel = Utility.getgson().fromJson<OrderListsModel>(response, OrderListsModel::class.java)
                Collections.sort(listsModel.pending!!, object : Comparator<OrderModel> {
                    override fun compare(o1: OrderModel?, o2: OrderModel?): Int {
                        val format= SimpleDateFormat("yyyy-MM-dd")
                        val date1=format.parse(o1!!.created_at!!.split(" ")[0])
                        val date2=format.parse(o2!!.created_at!!.split(" ")[0])
                        return date1.compareTo(date2)
                    }
                })
                listsModel.pending.reverse()
                cAdaptor = AdaptorOrders(this, listsModel.pending, this, true)
                binding.currentOrdersRV.adapter = cAdaptor

                Collections.sort(listsModel.completed!!, object : Comparator<OrderModel> {
                    override fun compare(o1: OrderModel?, o2: OrderModel?): Int {
                        val format= SimpleDateFormat("yyyy-MM-dd")
                        val date1=format.parse(o1!!.created_at!!.split(" ")[0])
                        val date2=format.parse(o2!!.created_at!!.split(" ")[0])
                        return date1.compareTo(date2)
                    }
                })
                listsModel.completed.reverse()
                pAdaptor = AdaptorOrders(this, listsModel.completed, this, false)
                binding.pastOrdersRV.adapter = pAdaptor
            }
            else{
                callOrderAPI()
                StyleableToast.makeText(this, "Rider Rated Successfully!", Toast.LENGTH_LONG, R.style.mytoast).show();

            }

        }
        catch (e:Exception){
            OnException(endpoint,e)
        }

    }
    override fun OnStatusfalse(endpoint:String,t: Response<ModelBaseResponse>, response: String, message: String) {
        Utility.showErrorDialog(this@MyOrdersActivity,R.layout.dialog_error_505, Messages.ERROR_TITLE,message)
    }

    override fun OnError(endpoint:String,code: Int, message: String) {
        Utility.showErrorDialog(this@MyOrdersActivity,R.layout.dialog_error_505, Messages.ERROR_TITLE,message)
    }


    override fun OnException(endpoint:String,exception: Throwable) {
        Utility.showErrorDialog(this@MyOrdersActivity,R.layout.dialog_error, Messages.ERROR_TITLE,getString(R.string.something_went_wrong))
    }

    override fun OnNetworkError(endpoint:String,message: String) {
        Utility.showNErrorDialog(this@MyOrdersActivity)
    }

    override fun onNegativeButtonClicked() {


    }

    override fun onNeutralButtonClicked() {

    }

    override fun onPositiveButtonClicked(rate: Int, comment: String) {
        val list:MutableList<ModelCommunication> = mutableListOf()
        list.add(ModelCommunication(Constants.USER_ID,Utility.getUser(this).id.toString()))
        list.add(ModelCommunication(Constants.ORDER_ID,pAdaptor.getData()[position].id.toString()))
        list.add(ModelCommunication(Constants.RATING,rate.toString()))
        NetworkCall.callAPI(this,getService().rateOrder(APIConstants.BEARER+Utility.getUser(this).token, Utility.getJson(list)), this,true,APIConstants.RATING)
    }

    override fun getPosition(position: Int) {
        this.position=position
    }


}