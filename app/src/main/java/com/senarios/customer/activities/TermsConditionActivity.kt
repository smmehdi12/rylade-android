package com.senarios.customer.activities

import android.view.View
import androidx.databinding.DataBindingUtil
import com.google.gson.reflect.TypeToken
import com.senarios.customer.R
import com.senarios.customer.costants.Messages
import com.senarios.customer.databinding.ActivityTermsConditionsBinding
import com.senarios.customer.models.ModelBaseResponse
import com.senarios.customer.models.TacModel
import com.senarios.customer.retrofit.APIConstants
import com.senarios.customer.retrofit.ApiResponse
import com.senarios.customer.retrofit.NetworkCall
import com.senarios.customer.utility.Utility
import kotlinx.android.synthetic.main.toolbar.view.*
import retrofit2.Response
import java.lang.reflect.Type

class TermsConditionActivity: BaseAcivity(),ApiResponse, View.OnClickListener {
    private lateinit var binding:ActivityTermsConditionsBinding


    override fun setBinding() {
        binding=DataBindingUtil.setContentView(this, R.layout.activity_terms_conditions)
    }

    override fun init() {
        NetworkCall.callAPI(this,getService().getTAC(),this,true,APIConstants.GET_TAC)
        binding.toolbar.toolbar.title="Terms & Conditions"
        binding.toolbar.toolbar.setNavigationOnClickListener(this)
     }

    override fun OnCancelPermissioDialog() {

     }

    override fun OnPermissionApproved() {

    }

    override fun OnTrigger() {

    }

    override fun OnSuccess(endpoint: String, t: Response<ModelBaseResponse>, response: String) {
        val listType: Type = object : TypeToken<List<TacModel>>() {}.getType()
        val list= Utility.getgson().fromJson<MutableList<TacModel>>(response, listType)
        binding.data=list[0]
    }

    override fun OnStatusfalse(endpoint:String,t: Response<ModelBaseResponse>, response: String, message: String) {
        Utility.showErrorDialog(this@TermsConditionActivity,R.layout.dialog_error_505, Messages.ERROR_TITLE,message)
    }

    override fun OnError(endpoint:String,code: Int, message: String) {
        Utility.showErrorDialog(this@TermsConditionActivity,R.layout.dialog_error_505, Messages.ERROR_TITLE,message)
    }


    override fun OnException(endpoint:String,exception: Throwable) {
        Utility.showErrorDialog(this@TermsConditionActivity,R.layout.dialog_error, Messages.ERROR_TITLE,getString(R.string.something_went_wrong))
    }

    override fun OnNetworkError(endpoint:String,message: String) {
        Utility.showNErrorDialog(this@TermsConditionActivity)
    }

    override fun onClick(v: View?) {
        finish()
    }
}