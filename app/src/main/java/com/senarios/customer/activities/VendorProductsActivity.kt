package com.senarios.customer.activities

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.google.gson.reflect.TypeToken
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItemAdapter
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems
import com.senarios.customer.R
import com.senarios.customer.callbacks.FragmentChanger
import com.senarios.customer.costants.Constants
import com.senarios.customer.databinding.ActivityVendorProductsBinding
import com.senarios.customer.dialogfragments.ReviewFragmentDialog
import com.senarios.customer.fragments.VendorProductFragment
import com.senarios.customer.models.*
import com.senarios.customer.retrofit.APIConstants
import com.senarios.customer.retrofit.ApiResponse
import com.senarios.customer.retrofit.NetworkCall
import com.senarios.customer.utility.Utility
import kotlinx.android.synthetic.main.toolbar.view.*
import retrofit2.Response
import java.lang.reflect.Type

class VendorProductsActivity : BaseAcivity() , View.OnClickListener,FragmentChanger,ApiResponse,Observer<MutableList<VendorCategoryModel>>
{
    private lateinit var binding:ActivityVendorProductsBinding
    private lateinit var adapter: FragmentPagerItemAdapter
    private lateinit var creator:FragmentPagerItems.Creator
    private var vendorID:Int=-1
    private lateinit var menuItem: MenuItem


    override fun setBinding() {
        binding=DataBindingUtil.setContentView(this, R.layout.activity_vendor_products)
    }

    override fun init() {

        if (intent.hasExtra(Constants.VENDOR_ID)) {
            vendorID=intent.getIntExtra(Constants.VENDOR_ID,-1)

            setToolbar()

            callApi()


        }
    }

    private fun setToolbar() {
        binding.toolbar.toolbar.title = "Vendor Products"
        binding.toolbar.toolbar.inflateMenu(R.menu.profile_menu)
        setSupportActionBar(binding.toolbar.toolbar)
        binding.toolbar.toolbar.setNavigationOnClickListener(this)

    }

    private fun observeCart() {
        Utility.getDB().getCart().observe(this, Observer<MutableList<CartModel>> { t ->
            if (isMenu()) {
                if (!t.isNullOrEmpty()) {
                    menuItem.isVisible = true
                    updateMenuItem(t.size)

                } else {
                    menuItem.isVisible = false
                }
            }
        })
    }

    private fun callApi() {
        val list= mutableListOf<ModelCommunication>()
        list.add(ModelCommunication(Constants.VENDOR_ID,vendorID.toString()))
        NetworkCall.callAPI(
            this,
            getService().getVendorCategories(Utility.getJson(list)),
            this,
            true,
            APIConstants.GET_VENDOR_CATEGORIES
        )
    }

    override fun OnCancelPermissioDialog() {

    }

    override fun OnPermissionApproved() {

    }

    override fun OnTrigger() {

    }

    override fun OnSuccess(endpoint: String, t: Response<ModelBaseResponse>, response: String) {
        try {
            if (endpoint.equals(APIConstants.GET_VENDOR_CATEGORIES)) {
                val listType: Type = object : TypeToken<List<VendorCategoryModel?>?>() {}.getType()
                val list = Utility.getgson().fromJson<MutableList<VendorCategoryModel>>(response, listType)
                Utility.getDB().deleteVendorCategory(vendorID)
                if (list.isNotEmpty()) {
                    Utility.getDB().deleteVendorCategory(vendorID)
                    Utility.getDB().insertVendorCategories(list)
                }
                    observeDB()
            }
        }
        catch (e:Exception){
            OnException(endpoint,e)
        }

    }

    override fun OnStatusfalse(endpoint:String, t: Response<ModelBaseResponse>, response: String, message: String) {
        observeDB()
    }

    override fun OnError(endpoint:String,code: Int, message: String) {
        observeDB()
    }


    override fun OnException(endpoint:String,exception: Throwable) {
        observeDB()
    }

    override fun OnNetworkError(endpoint:String,message: String) {
        observeDB()
    }

    private fun getBundle(item: VendorCategoryModel): Bundle? {
        val b = Bundle()
        b.putParcelable(Constants.CATEGORY_DATA, item)
        return b
    }

    override fun OnChange(fragment: Fragment, tag: String) {

    }
    private fun observeDB(){
        Utility.getDB().getVendorCategory(vendor_id = vendorID).observe(this,this)
    }

    override fun onChanged(t: MutableList<VendorCategoryModel>) {
                if (!t.isNullOrEmpty()) {
                    creator = FragmentPagerItems.with(this)
                    for (item in t) {
                        creator.add(
                            item.categoryName,
                            VendorProductFragment::class.java,
                            getBundle(item)
                        )
                    }
                    adapter = FragmentPagerItemAdapter(supportFragmentManager, creator.create())
                    binding.viewpager.adapter = adapter
                    binding.viewpagertab.setViewPager(binding.viewpager)
                    binding.group.visibility = View.VISIBLE
                }
        observeCart()
    }


    private fun updateMenuItem(count:Int){
        val view=menuItem.actionView
        val tvCounter=view.findViewById<TextView>(R.id.tv_counter)
        tvCounter.text=""+count

    }
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.profile_menu,menu)
        menuItem=menu!!.findItem(R.id.cart)
        (menuItem.actionView as ConstraintLayout).setOnClickListener(this)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        when(item.itemId)
        {
//            R.id.peopele-> {
//                val ratingDialog = ReviewFragmentDialog()
//                val bundle = Bundle()
//                bundle.putString(Constants.VENDOR_ID,vendorID.toString())
//                ratingDialog.arguments = bundle
//                OnChange(ratingDialog)
//                return  true
//            }
            R.id.search->{
                startActivity(Intent(this@VendorProductsActivity,SearchActivity::class.java))
            }

        }
        return super.onOptionsItemSelected(item)
    }



private fun isMenu():Boolean{
    return ::menuItem.isInitialized
}
    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.cartView -> {
                startActivity(Intent(this@VendorProductsActivity, CartActivity::class.java))
                finish()
            }

            else -> {
                finish()
            }
        }
    }
}