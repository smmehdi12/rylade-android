package com.senarios.customer.activities

import android.app.Activity
import android.content.Intent
import android.content.IntentSender
import android.location.Location
import android.os.Looper
import android.text.Editable
import android.text.TextWatcher
import android.view.KeyEvent
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.core.view.isVisible
import androidx.databinding.DataBindingUtil
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.tasks.Task
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.api.model.RectangularBounds
import com.google.android.libraries.places.api.model.TypeFilter
import com.google.android.libraries.places.api.net.FindAutocompletePredictionsRequest
import com.google.android.libraries.places.api.net.FindAutocompletePredictionsResponse
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import com.senarios.customer.R
import com.senarios.customer.costants.Codes
import com.senarios.customer.costants.Constants
import com.senarios.customer.databinding.FragmentMapBinding
import com.senarios.customer.di.Component
import com.senarios.customer.utility.Utility
import kotlinx.android.synthetic.main.toolbar.view.*
import java.util.*


class MapActivity : BaseAcivity(),GoogleMap.OnCameraIdleListener,GoogleMap.OnCameraMoveListener,Toolbar.OnMenuItemClickListener,TextView.OnEditorActionListener ,View.OnClickListener,TextWatcher,OnMapReadyCallback {
    private val REQUEST_CHECK_SETTINGS: Int=1000
    private lateinit var binding:FragmentMapBinding
    private lateinit var map:GoogleMap
    private lateinit var locationRequest:LocationRequest
    private lateinit var locationCallback: LocationCallback
    private lateinit var location:Location
    private lateinit var builder:LocationSettingsRequest.Builder
    private val currentZoom:Float=18f
    private val AUTOCOMPLETE_REQUEST_CODE:Int = 1;


    override fun setComponent(component: Component) {
        component.inject(this)
    }

    override fun setBinding() {
        binding=DataBindingUtil.setContentView(this, R.layout.fragment_map)
    }



    override fun init() {
        binding.confirmBtn.setOnClickListener(this)

        binding.toolbar.toolbar.inflateMenu(R.menu.map_menu)
        binding.toolbar.toolbar.setOnMenuItemClickListener(this)

        getSearchToolbar().inflateMenu(R.menu.map_search_menu)
        getSearchToolbar().setOnMenuItemClickListener(this)
        getSearchToolbar().setNavigationOnClickListener {
            hideSearch()
        }

        binding.autocompleteTV.addTextChangedListener(this)
        binding.autocompleteTV.setOnEditorActionListener(this)
        binding.toolbar.toolbar.setNavigationOnClickListener(this)

        if (!Utility.hasLocationPermission(this)){
            checkLocationPermission()
        }
        else{
            createLocationRequest()

        }
    }



    private fun initMap() {
        val mapFragment=supportFragmentManager.findFragmentById(R.id.map_fragment) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }
    override fun OnTrigger() {

    }

    override fun onClick(v: View?) {
        when (v?.id){
            binding.confirmBtn.id->{
                setResult(Codes.LATLNG_REQUEST_CODE,Intent().putExtra(Constants.LATLNG_DATA,map.cameraPosition.target))
                setResult(Activity.RESULT_OK,Intent().putExtra(Constants.LATLNG_DATA,map.cameraPosition.target))
                setResult(Activity.RESULT_OK,Intent().putExtra(Constants.LATLNG_DATA,map.cameraPosition.target))
                finish()
            }

            else->{
                finish()
            }
        }
    }

    override fun onMapReady(p0: GoogleMap?) {
        if (p0!=null) {
            map = p0;
            map.uiSettings.isMyLocationButtonEnabled = true
            if (isLocation()) {
                moveCamera(LatLng(location.latitude, location.longitude))
            }
            map.setOnCameraMoveListener(this)
            map.setOnCameraIdleListener(this)
        }
    }



    private fun createLocationRequest() {
        locationRequest = LocationRequest.create()
        locationRequest.interval = 500
        locationRequest.fastestInterval = 500
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        builder = LocationSettingsRequest.Builder().addLocationRequest(locationRequest)
        locationCallback= object : LocationCallback() {
            override fun onLocationResult(p0: LocationResult?) {
                if (p0 != null) {
                    if (!isMap()) {
                        initMap()
                    }
                    if (isMap()){
                        if (map.cameraPosition.zoom==0f){
                            if (isLocation()){
                                moveCamera(getCurrentLatLng())
                            }
                        }
                    }

                    location = p0.lastLocation
                }
            }

        }
        locationRequestTask()
    }

    private fun locationRequestTask(){
        val client: SettingsClient = LocationServices.getSettingsClient(this)
        val task: Task<LocationSettingsResponse> = client.checkLocationSettings(builder.build())
        task.addOnSuccessListener {
            requestLocation()
        }

        task.addOnFailureListener { exception ->
            if (exception is ResolvableApiException){
                try {
                    exception.startResolutionForResult(this@MapActivity, REQUEST_CHECK_SETTINGS)
                }

                catch (sendEx: IntentSender.SendIntentException) {

                }
            }
        }
    }

    private fun isLocation():Boolean{
        return ::location.isInitialized
    }

    override fun onResume() {
        super.onResume()
            if (isLocationCallback()){
                requestLocation()
            }


    }

    override fun onStop() {
        super.onStop()
        if (::locationCallback.isInitialized){
            removeRequestLocation()
        }
    }

    private fun requestLocation(){
        LocationServices.getFusedLocationProviderClient(this).requestLocationUpdates(locationRequest,locationCallback, Looper.getMainLooper())

    }

    private fun removeRequestLocation(){
        LocationServices.getFusedLocationProviderClient(this).removeLocationUpdates(locationCallback)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode==REQUEST_CHECK_SETTINGS) {
            if (Utility.checkLocationServices(this)) {
                requestLocation()
            } else {
                locationRequestTask()
            }
        }
        else if (resultCode == Activity.RESULT_OK && requestCode == AUTOCOMPLETE_REQUEST_CODE){
            val place = Autocomplete.getPlaceFromIntent(data!!)
            binding.autocompleteTV.setText(place.address)
            moveCamera(place.latLng!!)
            Utility.showLog("Place " + place.address)
        }

    }

    private fun moveCamera(latlng:LatLng){
        if (isMap()){
            map.animateCamera(CameraUpdateFactory.newLatLngZoom(latlng,currentZoom))
            binding.toolbar.toolbar.title=Utility.getAddress(this,latlng)
            if (!binding.pin.isVisible) {
                binding.pin.visibility = View.VISIBLE
            }
            if (!binding.confirmBtn.isVisible){
                binding.confirmBtn.visibility=View.VISIBLE
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (Utility.hasLocationPermission(this)){
            createLocationRequest()
        }
        else{
            handleLocationPermissionResult()
        }

    }

    override fun OnCancelPermissioDialog() {
        finish()
    }

    override fun OnPermissionApproved() {

    }

    private fun isMap():Boolean{
        return ::map.isInitialized
    }

    private fun isLocationCallback():Boolean{
        return ::locationCallback.isInitialized
    }

    override fun afterTextChanged(s: Editable?) {
        if (s!=null && s.isNotEmpty()){
            binding.autocompleteTV.isCursorVisible=true
            search(s.toString())
        }
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
//        if (s!=null && s.length>0){
//            binding.autocompleteTV.isCursorVisible=true
//            search(s.toString())
//        }
    }


    private fun search(q: String) {
        val bounds=RectangularBounds.newInstance(LatLng(73.381, 30.796), LatLng( 75.425, 32.201))
        val request =
            FindAutocompletePredictionsRequest.builder()
                .setSessionToken(Utility.getPlacesSession())
                .setCountry("pk")
                .setQuery(q)
                .setTypeFilter(TypeFilter.ADDRESS)
                .setLocationRestriction(bounds)
                .build()
        Utility.getPlacesClient(this).findAutocompletePredictions(request)
            .addOnSuccessListener { response: FindAutocompletePredictionsResponse ->
                val predictions= mutableListOf<String>()
                for (items in response.autocompletePredictions){
                    predictions.add(items.getPrimaryText(null).toString())
                }
                val adapter= ArrayAdapter<String>(this,android.R.layout.simple_expandable_list_item_1,predictions)
                binding.autocompleteTV.setAdapter(adapter)
            }.addOnFailureListener { exception: Exception? ->

            }

        binding.autocompleteTV.onItemClickListener =
            AdapterView.OnItemClickListener { parent, view, position, id ->
                hideSearch()
                val prediction=binding.autocompleteTV.adapter.getItem(position) as String
                binding.autocompleteTV.setText(prediction)
                if (Utility.getLatLng(this@MapActivity,prediction)!=null){
                    moveCamera(Utility.getLatLng(this@MapActivity,prediction)!!)
                }

            }
    }

    private fun getCurrentLatLng():LatLng{
        return LatLng(location.latitude,location.longitude)
    }

    override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean {
        if (EditorInfo.IME_ACTION_DONE==actionId){
            hideSearch()

        }
    return false
    }

    private fun hideSearch() {
        binding.autocompleteTV.isCursorVisible=false
        hideSoftKeyboard()
        getSearchToolbar().visibility=View.GONE

    }


    override fun onMenuItemClick(item: MenuItem?): Boolean {
        when (item?.itemId){
            R.id.search_location->{
//                getSearchToolbar().visibility=View.VISIBLE
//                binding.autocompleteTV.isCursorVisible=true
//                binding.autocompleteTV.requestFocus()
                // Set the fields to specify which types of place data to
                // return after the user has made a selection.
                val bounds=RectangularBounds.newInstance(LatLng(73.381, 30.796), LatLng( 75.425, 32.201))
                val fields = Arrays.asList(Place.Field.ID, Place.Field.NAME,Place.Field.ADDRESS,Place.Field.LAT_LNG);
                val intent: Intent = Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, fields).setLocationBias(bounds)
                    .setCountry("pk")
                    .build(this)
                startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE)
            }
            R.id.current_location->{
                if (isLocation()) {
                    moveCamera(getCurrentLatLng())
                }
                }
            R.id.clear_search->{
                binding.autocompleteTV.setText("")
            }
            else->{

            }

        }
        return true
    }

    private fun getSearchToolbar():Toolbar{
        return binding.toolbarSearch
    }

    override fun onBackPressed() {
        if (getSearchToolbar().isVisible) {
            hideSearch()
        } else {
            super.onBackPressed()
        }
    }

    override fun onCameraMove() {
        deactivateButton()
    }

    override fun onCameraIdle() {

        activateButton()
    }

    private fun activateButton(){
        binding.toolbar.toolbar.title=Utility.getAddress(this,map.cameraPosition.target)
        binding.confirmBtn.isClickable=true
        binding.confirmBtn.background=resources.getDrawable(R.drawable.basic_button,theme)
        binding.confirmBtn.text = getString(R.string.confirm_location)
    }
    private fun deactivateButton(){
        binding.confirmBtn.isClickable=false
        binding.confirmBtn.background=resources.getDrawable(R.drawable.disabled_button,theme)
        binding.confirmBtn.text = getString(R.string.loading)
    }
}