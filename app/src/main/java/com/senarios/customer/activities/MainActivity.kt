package com.senarios.customer.activities

import android.Manifest
import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.content.IntentSender
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.os.Looper
import android.util.Log
import android.view.Gravity
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import androidx.core.app.ActivityCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.facebook.login.LoginManager
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.tasks.Task
import com.google.android.material.navigation.NavigationView
import com.senarios.rylad.BaseDialogeFragment
import com.senarios.customer.R
import com.senarios.customer.callbacks.FragmentChanger
import com.senarios.customer.costants.Constants
import com.senarios.customer.costants.SPConstants
import com.senarios.customer.costants.Tags
import com.senarios.customer.databinding.ActivityMainBinding
import com.senarios.customer.di.Component
import com.senarios.customer.dialogfragments.*
import com.senarios.customer.fragments.HomeFragment
import com.senarios.customer.models.Directory
import com.senarios.customer.models.INIModel
import com.senarios.customer.models.OrderModel
import com.senarios.customer.utility.Utility
import com.senarios.customer.viewmodel.SharedVM
import javax.inject.Inject

/*the first activity which will be showin after the launch of application*/
class MainActivity : BaseAcivity() ,FragmentChanger,DialogeCallback, AdapterView.OnItemSelectedListener ,View.OnClickListener,NavigationView.OnNavigationItemSelectedListener {
    private val REQUEST_CHECK_SETTINGS: Int=1000
    private lateinit var binding: ActivityMainBinding
    private val TAG:String=javaClass.name
    private lateinit var locationRequest:LocationRequest
    private lateinit var locationCallback: LocationCallback
    private lateinit var location: Location
    private lateinit var builder:LocationSettingsRequest.Builder
    private lateinit var sharedVM: SharedVM
    @Inject lateinit var directory: Directory
    private var dialog:RatingFragmentDialog? = null
    private val selectionList= arrayOf("Current Location","Other Location")


    override fun setComponent(component: Component) {
        component.inject(this)
    }

    override fun setBinding() {
        binding=DataBindingUtil.setContentView(this,R.layout.activity_main)
    }

    override fun onResume() {
        super.onResume()
        OnChange(HomeFragment(),Tags.HOME)
        inflateMenu()
        if (!Utility.hasLocationPermission(this)){
            checkLocationPermission()
        }
        else{
            if (isLocationCallback()){
                requestLocation()
            }
            else{
                createLocationRequest()
            }
        }

        if (Utility.getPreference(this).contains(SPConstants.ADDRESS_DATA)){
            setSelection(1)
        }
        else{
            setSelection(0)
        }

        if (dialog!=null && !dialog!!.isVisible){
            showDialog(dialog!!)
        }

    }



    override fun onBackPressed() {
        Log.v(TAG,"back")
        super.onBackPressed()
    }

    override fun OnChange(fragment: Fragment, tag: String) {
        supportFragmentManager.beginTransaction().replace(binding.container.id,fragment,tag).commitAllowingStateLoss()
    }

    override fun OnChange(fragment: BaseDialogeFragment) {
        fragment.show(supportFragmentManager,"dialog")
    }

    override fun OnCancelPermissioDialog() {

    }

    override fun OnPermissionApproved() {

    }

    override fun OnTrigger() {
        dialog=null;
    }



    override fun init(){
        binding.mainScreenDeliverToSpinner.setOnClickListener(this)
        sharedVM= ViewModelProvider(this).get(SharedVM::class.java)
        binding.navView.setNavigationItemSelectedListener(this)
        binding.toolbar.setNavigationOnClickListener(this)
        if(this::directory.isInitialized){
            Utility.deleteAllFiles(directory.MEDIA_PICKER+directory.MEDIA_PICKER_IMAGES)
        }


    }

    private fun handleReviewBox() {
        if (!(Utility.getPreferences(this,Constants.ORDER,"") as String).isEmpty()) {
            val order: OrderModel = Utility.getgson().fromJson(Utility.getPreferences(this, Constants.ORDER, "") as String, OrderModel::class.java)
            if (order != null) {
                if (order.is_rated == 0) {
                    dialog = RatingFragmentDialog()
                    val bundle = Bundle()
                    bundle.putParcelable(Constants.ORDER_DATA, order)
                    dialog!!.arguments = bundle
                    dialog!!.isCancelable=false
                    OnChange(dialog!!)
                } else {
                    Utility.removeSpValue(this, Constants.ORDER)
                }
            } else {
                Utility.removeSpValue(this, Constants.ORDER)
            }

        }
    }


    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        binding.drawer.closeDrawers()
        when(item.itemId){
            R.id.order->{
                startActivity(Intent(this,MyOrdersActivity::class.java))
            }
            R.id.profile->{
                startActivity(Intent(this,EditProfileActivity::class.java))
            }
            R.id.address->{
               startActivity(Intent(this,AddressActivity::class.java))
            }
            R.id.tc->{
                startActivity(Intent(this,TermsConditionActivity::class.java))
            }
            R.id.contact_us->{
                startActivity(Intent(this,ContactUsActivity::class.java))
            }
            R.id.faq->{
                     startActivity(Intent(this,FAQActivity::class.java))
            }
            R.id.logout->{
                logout()
            }
            R.id.login->{
                OnChange(LoginSignupDialog())
            }


        }
        return true
    }



    private fun logout() {
        LoginManager.getInstance().logOut()
        binding.navView.menu.clear()
        binding.navView.inflateMenu(R.menu.main_drawer_not_logged)
        val fcm= Utility.getFCM(this)
        val instant= INIModel.getModelFromSP(this)
        val lat=Utility.getPreferences(this,Constants.LAT,"") as String
        val lng=Utility.getPreferences(this,Constants.LNG,"")
        Utility.getPreference(this).edit().clear().apply()
        Utility.setPreferences(this,Constants.INSTANT,Utility.getgson().toJson(instant!!))
        Utility.setPreferences(this,SPConstants.FCM,fcm)
        Utility.setPreferences(this,Constants.LAT,lat)
        Utility.setPreferences(this,Constants.LNG,lng)
        setSelection(0)
    }

    @SuppressLint("WrongConstant")
    override fun onClick(v: View?) {
        if (v==binding.mainScreenDeliverToSpinner){
            showSelectionDialog()
        }
        else{
            binding.drawer.openDrawer(Gravity.START)
        }

    }

    fun getbinding():ActivityMainBinding{
        return binding
    }


fun inflateMenu(){
    if (sharedVM.getLoggedUser().value!=null){
        binding.navView.menu.clear()
        binding.navView.inflateMenu(R.menu.main_drawer_menu)
    }
    else{
        binding.navView.menu.clear()
        binding.navView.inflateMenu(R.menu.main_drawer_not_logged)
    }
}


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode==REQUEST_CHECK_SETTINGS) {
            if (Utility.checkLocationServices(this)) {
                requestLocation()
            } else {
                locationRequestTask()
            }
        }
    }

    private fun createLocationRequest() {
        locationRequest = LocationRequest.create()
        locationRequest.interval = 500
        locationRequest.fastestInterval = 500
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        builder = LocationSettingsRequest.Builder().addLocationRequest(locationRequest)
        locationCallback= object : LocationCallback() {
            override fun onLocationResult(p0: LocationResult?) {
                if (p0 != null) {
                    location = p0.lastLocation
                    Utility.setPreferences(this@MainActivity,Constants.LAT,location.latitude.toString())
                    Utility.setPreferences(this@MainActivity,Constants.LNG,location.longitude.toString())
                }
            }

        }
        locationRequestTask()
    }

    private fun locationRequestTask(){
        val client: SettingsClient = LocationServices.getSettingsClient(this)
        val task: Task<LocationSettingsResponse> = client.checkLocationSettings(builder.build())
        task.addOnSuccessListener {
            if (dialog==null) {
                handleReviewBox()
            }
            requestLocation()

        }

        task.addOnFailureListener { exception ->
            if (exception is ResolvableApiException){
                try {
                    exception.startResolutionForResult(this@MainActivity, REQUEST_CHECK_SETTINGS)
                }

                catch (sendEx: IntentSender.SendIntentException) {

                }
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (Utility.hasLocationPermission(this)){
            createLocationRequest()
        }
        else{
            handleLocationPermissionResult()
        }

    }

    private fun isLocation():Boolean{
        return ::location.isInitialized
    }

    override fun onStop() {
        super.onStop()
        if (::locationCallback.isInitialized){
            removeRequestLocation()
        }
    }

    private fun requestLocation(){
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return
        }
        LocationServices.getFusedLocationProviderClient(this).requestLocationUpdates(locationRequest,locationCallback, Looper.getMainLooper())

    }

    private fun removeRequestLocation(){
        LocationServices.getFusedLocationProviderClient(this).removeLocationUpdates(locationCallback)

    }

    private fun isLocationCallback():Boolean{
        return ::locationCallback.isInitialized
    }



    override fun onNothingSelected(parent: AdapterView<*>?) {
        Utility.showLog("main screen spinner onNothingSelected called")
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        val list=resources.getStringArray(R.array.address_type)

    }

    private fun setSelection(selection:Int){
        binding.mainScreenDeliverToSpinner.text = selectionList[selection]
    }

    private fun showSelectionDialog() {
        var selection:Int=0;
        if (Utility.getPreference(this).contains(SPConstants.ADDRESS_DATA)){
            selection=1;
        }
        val dialoge:AlertDialog.Builder=AlertDialog.Builder(this)
        dialoge.setSingleChoiceItems(selectionList,selection) { dialog, which ->
            dialog!!.dismiss()
            if (which!=selection){
                sortSelection(which)
            }
        }.show()

    }

    private fun sortSelection(selection: Int){
        if (selection==0){
            if (Utility.getDB().getCartItems().size>0) {
                Utility.getAlertDialoge(this, "Cart will be cleared", "By changing the location any item in your cart will be removed!").setPositiveButton("Ok") { dialog, which ->
                    Utility.getDB().clearCartTable()
                    Utility.removeSpValue(this, SPConstants.ADDRESS_DATA)
                    setSelection(0)
                }.setNegativeButton("no") { dialog, which ->
                    dialog!!.dismiss()
                }.show()
                return
            }
            setSelection(0)
            Utility.removeSpValue(this, SPConstants.ADDRESS_DATA)
        }
        else{
            if (Utility.getPreference(this).contains(SPConstants.USER)) {
                    startActivity(Intent(this@MainActivity, AddressActivity::class.java))
                }
                else{
                    showDialog(LoginSignupDialog())
                }
        }
    }

}
