package com.senarios.customer.activities

import android.content.Intent
import android.os.Handler
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.google.gson.reflect.TypeToken
import com.senarios.customer.R
import com.senarios.customer.costants.Constants
import com.senarios.customer.databinding.ActivitySplashBinding
import com.senarios.customer.models.INIModel
import com.senarios.customer.models.MainCategoryModel
import com.senarios.customer.models.ModelBaseResponse
import com.senarios.customer.retrofit.APIConstants
import com.senarios.customer.retrofit.ApiResponse
import com.senarios.customer.retrofit.NetworkCall
import com.senarios.customer.utility.Utility
import retrofit2.Response
import java.lang.reflect.Type

class SplashActivity : BaseAcivity(), ApiResponse, Observer<MutableList<MainCategoryModel>>{
    private lateinit var binding:ActivitySplashBinding



    override fun setBinding() {
        binding=DataBindingUtil.setContentView(this,R.layout.activity_splash)

    }

    override fun init() {
        Handler().postDelayed({
            NetworkCall.callAPI(this,getService().getInstant(),this,false,APIConstants.GET_INSTANT)
        },2000)

    }

    override fun OnCancelPermissioDialog() {

    }

    override fun OnPermissionApproved() {

    }

    override fun OnTrigger() {

    }

    private fun observeDB(){
        Utility.getDB().getMainCategories().observe(this,this)
    }

    override fun OnSuccess(endpoint: String, t: Response<ModelBaseResponse>, response: String) {
        try{
            if (endpoint.equals(APIConstants.GET_INSTANT)) {
                val itemType = object : TypeToken<MutableList<INIModel>>() {}.type
                val list=Utility.getgson().fromJson<MutableList<INIModel>>(response,itemType)
                Utility.setPreferences(this, Constants.INSTANT,Utility.getgson().toJson(list[0]))
                NetworkCall.callAPI(this,getService().getMainCategories(),this,false, APIConstants.GET_MAIN_CATEGORIES)
            }
            else if (endpoint.equals(APIConstants.GET_MAIN_CATEGORIES)){
                val listType: Type = object : TypeToken<List<MainCategoryModel?>?>() {}.getType()
                val list = Utility.getgson().fromJson<MutableList<MainCategoryModel>>(response, listType)
                if (list.isNotEmpty()) {
                    Utility.getDB().clearMainCategoryTable()
                    Utility.getDB().insertMainCategories(list)
                }
                observeDB()
            }

        }
        catch (e:Exception){
            Utility.showLog("Ecommerce Activity"+e.message)
        }

    }

    override fun OnStatusfalse(endpoint: String, t: Response<ModelBaseResponse>, response: String, message: String) {
        apiFailed(endpoint)
    }

    override fun OnError(endpoint: String, code: Int, message: String) {
        apiFailed(endpoint)
    }

    override fun OnException(endpoint: String, exception: Throwable) {
        apiFailed(endpoint)
     }

    override fun OnNetworkError(endpoint: String, message: String) {
        apiFailed(endpoint)
    }

    override fun onChanged(t: MutableList<MainCategoryModel>?) {
        if (t!!.isNotEmpty()) {
            finish()
            startActivity(Intent(this@SplashActivity,MainActivity::class.java))
        }
        else{
            Utility.showToast(this,"Something went wrong!")
        }

    }

    private fun apiFailed(endpoint: String){
        if (endpoint.equals(APIConstants.GET_INSTANT)) {
            NetworkCall.callAPI(this,getService().getMainCategories(),this,false, APIConstants.GET_MAIN_CATEGORIES)
        }
        else if (endpoint.equals(APIConstants.GET_MAIN_CATEGORIES)){
            observeDB()
        }
    }
}
