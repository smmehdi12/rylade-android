package com.senarios.customer.viewmodel

import android.app.Application
import android.content.SharedPreferences
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.senarios.customer.Rylade
import com.senarios.customer.costants.SPConstants
import com.senarios.customer.costants.SPConstants.Companion.OTP
import com.senarios.customer.models.*
import com.senarios.customer.utility.Utility.Companion.getgson

/*view model class which sole purpose is to prepare and provide real time data to activity and fragment its connected to.*/
class SharedVM(application: Application) : AndroidViewModel(application) {
    private lateinit var preference:SharedPreferences;
    private var user:MutableLiveData<User> = MutableLiveData()
    private var newUser:MutableLiveData<User> = MutableLiveData()
    private var address:MutableLiveData<Address> = MutableLiveData()
    private var otp:MutableLiveData<String> = MutableLiveData()
    private  var vendor:MutableLiveData<Vendor> = MutableLiveData()
    private  var comments:MutableLiveData<ArrayList<CommentsItem>> = MutableLiveData()
    private var email:MutableLiveData<String> = MutableLiveData()

    init {
        getSharedPreference()
        getLoggedUser()
    }


    fun getSharedPreference():SharedPreferences{
        preference=getApplication<Rylade>().getSharedPreferences(SPConstants.SP,SPConstants.MODE)
        return preference
    }

    fun setLoggedUser(user:User){
        this.user.value=user
    }

    fun getLoggedUser():LiveData<User>{
        try{
            val response=preference.getString(SPConstants.USER,"")
            user.value=getgson().fromJson(response,User::class.java)
            return user
        }
        catch (e:Exception){
            Log.v(javaClass.name,e.localizedMessage!!)
            return user
        }


    }

    fun setUser(user: User){
        this.newUser.value=user
    }

    fun getUser():LiveData<User>{
        return newUser
    }

    fun getAddress():LiveData<Address>{
        return address
    }

    fun setAddress( address : Address){
        this.address.value=address
    }


    fun getOTP():LiveData<String>{
        otp.value=preference.getString(OTP,"")
        return otp
    }


    //only needed for autofilling email on sign and login screens
    fun getEmail():LiveData<String>{
        return email
    }

    fun setEmail(email:String) {
        this.email.value = email
    }


    fun getComments():LiveData<ArrayList<CommentsItem>>{
        return comments
    }

    fun setComments(list:ArrayList<CommentsItem>){
        this.comments.value=list
    }

    fun getVendor():LiveData<Vendor>{
        return vendor
    }

    fun setVendor(vendor: Vendor){
        this.vendor.value=vendor
    }



}