package com.senarios.customer.services

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.util.Log
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.senarios.customer.activities.MainActivity
import com.senarios.customer.costants.Constants
import com.senarios.customer.models.ModelCommunication
import io.karn.notify.Notify
import org.greenrobot.eventbus.EventBus
import java.lang.Exception

class FirebaseNotificationService : FirebaseMessagingService() {
    private val TAG=javaClass.name

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)
        Log.v(TAG,"message received from server")

        val set=remoteMessage.data.keys

        if (set.size>0){
            if (set.contains("title") && set.contains("message")){
                showFirebaseNotification(remoteMessage.data)
            }
            if (set.contains(Constants.USER) && set.contains(Constants.RIDE_ORDER) && set.contains(Constants.ORDER)  ){
                val list= mutableListOf<ModelCommunication>()
                list.add(ModelCommunication(Constants.USER,remoteMessage.data.get(Constants.USER).toString()))
                list.add(ModelCommunication(Constants.RIDE_ORDER,remoteMessage.data.get(Constants.RIDE_ORDER).toString()))
                list.add(ModelCommunication(Constants.ORDER,remoteMessage.data.get(Constants.ORDER).toString()))
                EventBus.getDefault().post(list)
            }
            else if(set.contains(Constants.ORDER)){
                    com.senarios.customer.utility.Utility.setPreferences(this,Constants.ORDER,remoteMessage.data.get(Constants.ORDER).toString())
            }

        }


    }

    private fun showFirebaseNotification(data: Map<String, String>) {
        try {
            val title = data.get("title")
            val message = data.get("message")
            val manager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                val channel = NotificationChannel("0", "Rylade", NotificationManager.IMPORTANCE_HIGH)
                manager.createNotificationChannel(channel)
            }
            val pending= PendingIntent.getActivity(this,0, Intent(this, MainActivity::class.java), PendingIntent.FLAG_ONE_SHOT)
            val builder = Notify.with(this).asBuilder().setContentIntent(pending).setContentTitle(title).setContentText(message).setChannelId("0").build()
            manager.notify(0, builder)
        }
        catch (e:Exception){
         Log.v(TAG,""+e.message)
        }


    }


}