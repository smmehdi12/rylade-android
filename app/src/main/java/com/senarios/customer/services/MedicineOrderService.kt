package com.senarios.customer.services

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.Service
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.IBinder
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import com.senarios.customer.R
import com.senarios.customer.activities.MyOrdersActivity
import com.senarios.customer.costants.Constants
import com.senarios.customer.models.Address
import com.senarios.customer.models.MedicineModel
import com.senarios.customer.models.ModelBaseResponse
import com.senarios.customer.retrofit.APIConstants
import com.senarios.customer.retrofit.ApiResponse
import com.senarios.customer.retrofit.NetworkCall
import com.senarios.customer.utility.Utility
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.json.JSONObject
import retrofit2.Response
import java.io.File
import java.util.*

class MedicineOrderService : Service(), ApiResponse {
    private val TAG = javaClass.name
    private var id: Int = -1
    private var position: Int = 0
    private var medicines: ArrayList<MedicineModel>? = null
    private var address: Address? = null


    override fun onBind(intent: Intent?): IBinder? {
        return null
    }


    override fun onCreate() {
        super.onCreate()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startMyOwnForeground()
        } else {
            startForeground(1, Notification())
        }
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        try {
            if ((intent != null && intent.extras != null) && intent.extras!!.keySet().contains(Constants.MEDICINE_ORDER_DATA)) {
                medicines = intent.extras!!.getParcelableArrayList<MedicineModel>(Constants.MEDICINE_ORDER_DATA) as ArrayList<MedicineModel>
                address = Utility.getgson().fromJson<Address>(intent.getStringExtra(Constants.ADDRESS_DATA), Address::class.java)
                if (medicines!!.size > 0 && address != null) {
                    fireOrder()

                } else {
                    stopService()
                }
            }


        } catch (e: Exception) {
            Log.v(TAG, e.localizedMessage!!)
            stopService()
        }

        return START_REDELIVER_INTENT
    }

    private fun fireOrder() {
        val item = medicines!![position]
        if (item.photo.isNullOrEmpty()) {
            createApi(Utility.getUser(this).token, null, Utility.getUser(this).id, id, item.name!!, item.quantity!!.toInt(), item.dose, item.instruction, item.type!!, address!!.id, item.instance,item.orderDate,item.orderTime)
        } else {
            createApi(Utility.getUser(this).token, getPart(item.photo!!), Utility.getUser(this).id, id, item.name!!, item.quantity!!.toInt(), item.dose, item.instruction, item.type!!, address!!.id, item.instance,item.orderDate,item.orderTime)
        }


    }

    private fun createApi(token: String, part: MultipartBody.Part?, user_id: Int, id: Int, name: String, quantity: Int, dose: String?, instruction: String?, type: String, address_id: Int, instance: Boolean, orderDate: String?, orderTime:String?) {

        position++
        if (part == null) {
            NetworkCall.callAPI(this, Utility.getService().order_Medicine(APIConstants.BEARER + token, user_id, id, name, quantity, dose, instruction, type, address_id, instance, orderDate,orderTime), this, false, APIConstants.ORDER_MEDICINE)

        } else {
            NetworkCall.callAPI(this, Utility.getService().order_Medicine(APIConstants.BEARER + token, part, user_id, id, name, quantity, dose, instruction, type, address_id,instance,orderDate,orderTime), this, false, APIConstants.ORDER_MEDICINE)
        }
    }


    override fun OnSuccess(endpoint: String, t: Response<ModelBaseResponse>, response: String) {
        try {
            val json = JSONObject(response)
            if (json.has("id")) {
                id = json.getInt("id")
            }
            if (position < medicines!!.size) {
                fireOrder()
            } else if (position == medicines!!.size) {
                Utility.showFirebaseNotification(this, "Rylade Order", "Your Order has been placed Successfully",Intent(this, MyOrdersActivity::class.java))
                stopService()
                Log.v(TAG, "Orders Successfull")
            }


        } catch (e: java.lang.Exception) {
            OnException(endpoint, e)
        }

    }

    override fun OnStatusfalse(endpoint: String, t: Response<ModelBaseResponse>, response: String, message: String) {
        Log.v(TAG, "Orders failed")
        Utility.showNotification(this, "Rylade Medicine Order", "Order Failed")
        stopService()
    }

    override fun OnError(endpoint: String, code: Int, message: String) {
        Log.v(TAG, "Orders failed")
        Utility.showNotification(this, "Rylade Medicine Order", "Order Failed")
        stopService()

    }

    override fun OnException(endpoint: String, exception: Throwable) {
        Log.v(TAG, "Orders failed" + exception.message)
        Utility.showNotification(this, "Rylade Medicine Order", "Order Failed")
        stopService()
    }

    override fun OnNetworkError(endpoint: String, message: String) {

        Log.v(TAG, "Orders failed")
        Utility.showNotification(this, "Rylade Medicine Order", "Order Failed")
        stopService()
    }

    @RequiresApi(api = Build.VERSION_CODES.O) private fun startMyOwnForeground() {
        val NOTIFICATION_CHANNEL_ID = applicationContext.packageName
        val channelName = getString(R.string.rylade_notification)
        val chan = NotificationChannel(NOTIFICATION_CHANNEL_ID, channelName, NotificationManager.IMPORTANCE_HIGH)
        chan.lightColor = Color.BLUE
        chan.lockscreenVisibility = Notification.VISIBILITY_PRIVATE
        val manager = (getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager)
        manager.createNotificationChannel(chan)
        val notificationBuilder = NotificationCompat.Builder(applicationContext, NOTIFICATION_CHANNEL_ID)
        val notification = notificationBuilder.setOngoing(true).setSmallIcon(R.drawable.rylade).setContentTitle("App is running in background").setPriority(NotificationManager.IMPORTANCE_HIGH).setCategory(Notification.CATEGORY_SERVICE).build()
        startForeground(1, notification)
    }

    private fun getPart(data: String): MultipartBody.Part {
        val file = File(data)
        val filepart: RequestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file)
        return MultipartBody.Part.createFormData("photo", file.name, filepart)
    }

    private fun createRequest(data: String): RequestBody {
        return RequestBody.create(MediaType.parse("text/plain"), data)
    }

    private fun stopService() {
        stopForeground(true)
        stopSelf()
    }

}