package com.senarios.customer.services

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.provider.Telephony
import android.util.Log
import com.senarios.customer.costants.EventBusTags.Companion.OTP_RECEIVER_SMS
import com.senarios.customer.models.ModelCommunication
import org.greenrobot.eventbus.EventBus


class OTPReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {

        try{
            val data = intent!!.extras
            val set=data?.keySet()
            Log.v(javaClass.name,set?.size.toString())


          if (set!=null && set.size>0){
            if (  set.contains("pdus")) {
                val smsMessage = Telephony.Sms.Intents.getMessagesFromIntent(intent);
                if ((smsMessage!=null && smsMessage.size>0)) {
                    val sender = smsMessage[0].displayOriginatingAddress
                    val messageBody = smsMessage[0].messageBody
                    val model = ModelCommunication(OTP_RECEIVER_SMS, messageBody)
                    EventBus.getDefault().postSticky(model)
                }
              }
            }
        }catch (e:Exception){
            Log.v(javaClass.name,e.message.toString())
        }


    }
}