package com.senarios.customer.retrofit

import android.app.Activity
import android.content.Context
import android.util.Log
import com.google.gson.Gson
import com.senarios.customer.RyladeDialoge
import com.senarios.customer.costants.Messages
import com.senarios.customer.costants.Messages.Companion.NULL_BODY_ERROR
import com.senarios.customer.models.ModelBaseResponse
import com.senarios.customer.utility.Utility
import io.reactivex.Single
import io.reactivex.SingleObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import retrofit2.Response

/*base class for calling apis , this calls gives callback using api response interface based on fetched response.*/
class NetworkCall {

    companion object {
        private val TAG=javaClass::class.java.name

            fun  callAPI(context: Context, single: Single<Response<ModelBaseResponse>>, api: ApiResponse, isDialog: Boolean, endpoint: String) {
                try {
                    if (Utility.isNetworkConnected(context)) {
                        val dialog = RyladeDialoge(context)
                        if (isDialog) {
                            if (context is Activity && !context.isFinishing) {
                                dialog.show()
                            }

                        }
                        single.observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io()).subscribe(object : SingleObserver<Response<ModelBaseResponse>> {
                            override fun onSuccess(t: Response<ModelBaseResponse>) {

                                Log.v(TAG, "body is" + t.body().toString())
                                Log.v(TAG, "code : " + t.code())
                                if (t.isSuccessful) {
                                    if (t.code() == 200) {
                                        if (t.body() != null) {
                                            Log.v("tag", Gson().toJson(t.body()))
                                            if (t.body()!!.status && t.body() != null) {
                                                Log.v("tag", t.body()!!.data.toString())
                                                api.OnSuccess(endpoint, t, Utility.getgson().toJson(t.body()!!.data))
                                            } else if (t.body()!!.data != null) {
                                                api.OnStatusfalse(endpoint, t, Utility.getgson().toJson(t.body()!!.data), t.body()!!.message)
                                            } else if (t.body()!!.message != null) {
                                                api.OnError(endpoint, t.code(), t.body()!!.message)
                                            } else {
                                                api.OnError(endpoint, t.code(), NULL_BODY_ERROR)
                                            }
                                        } else {
                                            api.OnError(endpoint, t.code(), t.errorBody().toString())
                                        }
                                    } else {
                                        api.OnError(endpoint, t.code(), t.errorBody().toString())
                                    }

                                } else {
                                    api.OnError(endpoint, t.code(), t.errorBody().toString())
                                }

                                if (isDialog) {
                                    if (context is Activity && !(context as Activity).isFinishing) {
                                        dialog.dismiss()
                                    }
                                }
                            }

                            override fun onSubscribe(d: Disposable) {

                            }

                            override fun onError(e: Throwable) {
                                Log.v(TAG, "exception : " + e.localizedMessage)

                                if (isDialog) {
                                    if (context is Activity && !(context as Activity).isFinishing) {
                                        dialog.dismiss()
                                    }
                                }
                                api.OnException(endpoint, e)
                            }

                        })
                    } else {
                        api.OnNetworkError(endpoint, Messages.NETWORK_ERROR)
                    }
                }
                catch (e:Exception){
                    api.OnException(endpoint,e)
                }

            }


        }

}