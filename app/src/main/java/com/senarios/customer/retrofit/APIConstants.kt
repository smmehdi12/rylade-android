package com.senarios.customer.retrofit

interface APIConstants {
    //    http://rylade.senarios.co
    companion object {

        const val BASE_GOOGLE = "https://maps.googleapis.com/maps/api/"
        const val BASE_URL = "https://rylade.com/api/auth/"
        const val BASE_PARCEL_IMAGE = "https://rylade.com/parcel_orders/"
        const val BASE_MEDICINE_IMAGE = "https://rylade.com/medicine_prescriptions/"

        ///
        const val BASE_PROFILE_IMAGE = "https://rylade.com/"
        const val BASE_VENDOR_PHOTO = "https://rylade.com/attachments/"
        const val BASE_PRODUCT_PHOTO = "https://rylade.com/product_images/"


        //rylade endpoints
        const val REGISTER = "signup"
        const val LOGIN = "login"
        const val VERIFY_OTP = "verifyOTP"
        const val CHECKMAIL = "checkEmailExist"
        const val ADD_ADDRESS = "insertUserAddress"
        const val GET_ADDRESS = "getUserAddress"
        const val DELETE_ADDRESS = "deleteAddress"
        const val SENDOTP = "sendOTP"
        const val EDITPROFILE = "updateProfile"
        const val CHANGEPASSWORD = "updatePassword"
        const val FACEBOOKLOGIN = "facebook_login_signup"
        const val ORDER_MEDICINE = "create_medicine_order"
        const val ORDER_PARCEL = "create_parcel_order"
        const val GET_ORDER = "getorders"
        const val GET_ORDER_DETAIl = "getorderdetails"
        const val REQUEST_RIDE = "create_ride_order"
        const val CANCEL_ORDER = "customer_cancel_order"
        const val RATING = "rating"
        const val GET_REVIEWS = "get_comments"
        const val INSERT_COMMENT = "insert_comment"
        const val GET_CONTACT_US = "get_contacts"
        const val GET_FAQ = "get_faqs"
        const val GET_TAC = "get_tac"
        const val CHECK_RIDE_STATUS = "check_user_ride_status"
        const val GET_MAIN_CATEGORIES = "get_main_categories"
        const val GET_VENDORS = "get_vendor_by_id"
        const val GET_VENDOR_CATEGORIES = "get_categories"
        const val GET_OTP_FROM = "send_facebook_otp"
        const val CONFIRM_PASSWORD = "forgot_password"
        const val GET_VENDOR_PRODUCTS = "get_products_by_category_id"
        const val CHECK_PROMO = "promo_code_check"
        const val ORDER_ECOMMERCE = "insert_ecommerce_order"
        const val GET_INSTANT = "get_instances"
        const val SEARCH = "search"
        const val GET_VENDOR = "get_vendor"
        const val GET_PRODUCTS_BY_ORDER_ID = "get_products_by_order_id"


        //google endpoints
        const val DIRECTION = "distancematrix/json"

        //config
        const val CONTENT_TYPE = "Content-Type: application/json"
        const val AUTH = "Authorization"
        const val BEARER = "Bearer "
    }

}