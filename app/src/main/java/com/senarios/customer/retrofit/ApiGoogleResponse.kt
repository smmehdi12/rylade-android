package com.senarios.customer.retrofit

import com.senarios.customer.models.GoogleDistanceApiResponse
import retrofit2.Response

interface ApiGoogleResponse {
    fun OnGoogleSuccess(endpoint:String, t: Response<GoogleDistanceApiResponse> )
    fun OnGoogleError(endpoint:String, code:Int, message: String)
    fun OnGoogleException(endpoint:String, exception:Throwable)
    fun OnNetworkError(endpoint:String,message:String)

}