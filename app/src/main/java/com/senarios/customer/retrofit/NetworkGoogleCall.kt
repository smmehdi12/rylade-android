package com.senarios.customer.retrofit


import android.content.Context
import android.util.Log
import com.google.gson.Gson
import com.senarios.customer.RyladeDialoge
import com.senarios.customer.costants.Messages
import com.senarios.customer.costants.Messages.Companion.NULL_BODY_ERROR
import com.senarios.customer.models.GoogleDistanceApiResponse
import com.senarios.customer.utility.Utility
import io.reactivex.Single
import io.reactivex.SingleObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import retrofit2.Response

/*base class for calling apis , this calls gives callback using api response interface based on fetched response.*/
class NetworkGoogleCall {

    companion object {
        private val TAG=javaClass::class.java.name
        fun callAPI(context: Context, single: Single<Response<GoogleDistanceApiResponse>>, api: ApiGoogleResponse, isDialog:Boolean, endpoint:String) {
            if (Utility.isNetworkConnected(context)) {
                val dialog = RyladeDialoge(context)
                if (isDialog) {
                    dialog.show()
                }
                single.observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(object : SingleObserver<Response<GoogleDistanceApiResponse>> {
                        override fun onSuccess(t: Response<GoogleDistanceApiResponse>) {

                            Log.v(TAG, "body is" + t.body().toString())
                            Log.v(TAG, "code : " + t.code())
                            if (t.isSuccessful) {
                                if (t.code() == 200) {
                                    if (t.body() != null) {
                                        Log.v("tag", Gson().toJson(t.body()))
                                        if (t.body()!!.rows[0].elements[0].status.equals("OK",true)){
                                            api.OnGoogleSuccess(endpoint,t)
                                        }
                                        else{
                                            api.OnGoogleError(endpoint,t.code(),NULL_BODY_ERROR)
                                        }
                                    } else {
                                        api.OnGoogleError(endpoint,t.code(),t.errorBody().toString())
                                    }
                                } else {
                                    api.OnGoogleError(endpoint,t.code(),t.errorBody().toString())
                                }

                            } else {
                                api.OnGoogleError(endpoint,t.code(),t.errorBody().toString())
                            }

                            if (isDialog) {
                                dialog.dismiss()
                            }
                        }

                        override fun onSubscribe(d: Disposable) {

                        }

                        override fun onError(e: Throwable) {
                            Log.v(TAG, "exception : " + e.localizedMessage)
                            api.OnGoogleException(endpoint,e)
                            if (isDialog) {
                                dialog.dismiss()
                            }
                        }

                    })
            }
            else{
                api.OnNetworkError(endpoint,Messages.NETWORK_ERROR)
            }


        }
    }

}