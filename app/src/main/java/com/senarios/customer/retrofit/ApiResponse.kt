package com.senarios.customer.retrofit

import com.senarios.customer.models.ModelBaseResponse
import retrofit2.Response

interface ApiResponse {
    fun OnSuccess(endpoint:String,t: Response<ModelBaseResponse>, response:String ){

    }
    fun OnStatusfalse(endpoint:String,t: Response<ModelBaseResponse>,response:String,message:String){

    }
    fun OnError(endpoint:String,code:Int,message: String){

    }
    fun OnException(endpoint:String,exception:Throwable){

    }
    fun OnNetworkError(endpoint:String,message:String){

    }

}