package com.senarios.customer.retrofit

import com.google.gson.JsonObject
import com.senarios.customer.costants.Constants
import com.senarios.customer.models.GoogleDistanceApiResponse
import com.senarios.customer.models.ModelBaseResponse
import io.reactivex.Single
import okhttp3.MultipartBody
import retrofit2.Response
import retrofit2.http.*

/*interface which has abstract functions , these functions will be implemented by Retrofit */
interface DataService  {

    @Headers(APIConstants.CONTENT_TYPE)
    @POST(APIConstants.REGISTER)
    fun register( @Body json:JsonObject):Single<Response<ModelBaseResponse>>

    @Headers(APIConstants.CONTENT_TYPE)
    @POST(APIConstants.LOGIN)
    fun login( @Body json:JsonObject):Single<Response<ModelBaseResponse>>

    @Headers(APIConstants.CONTENT_TYPE)
    @POST(APIConstants.CHECKMAIL)
    fun checkMail(@Body json:JsonObject):Single<Response<ModelBaseResponse>>

    @Headers(APIConstants.CONTENT_TYPE)
    @POST(APIConstants.VERIFY_OTP)
    fun verifyUser( @Body json:JsonObject):Single<Response<ModelBaseResponse>>

    @Headers(APIConstants.CONTENT_TYPE)
    @POST(APIConstants.ADD_ADDRESS)
    fun addAddress( @Header(APIConstants.AUTH)token:String, @Body json:JsonObject):Single<Response<ModelBaseResponse>>

    @Headers(APIConstants.CONTENT_TYPE)
    @POST(APIConstants.GET_ADDRESS)
    fun getAddress( @Header(APIConstants.AUTH)token:String, @Body json:JsonObject):Single<Response<ModelBaseResponse>>

    @Headers(APIConstants.CONTENT_TYPE)
    @POST(APIConstants.DELETE_ADDRESS)
    fun deleteAddress( @Header(APIConstants.AUTH)token:String, @Body json:JsonObject):Single<Response<ModelBaseResponse>>


    @Headers(APIConstants.CONTENT_TYPE)
    @POST(APIConstants.SENDOTP)
    fun sendOTP( @Body json:JsonObject):Single<Response<ModelBaseResponse>>

    @Headers(APIConstants.CONTENT_TYPE)
    @POST(APIConstants.EDITPROFILE)
    fun editprofile( @Header(APIConstants.AUTH)token:String,@Body json:JsonObject):Single<Response<ModelBaseResponse>>


    @Headers(APIConstants.CONTENT_TYPE)
    @POST(APIConstants.CHANGEPASSWORD)
    fun changepassword( @Header(APIConstants.AUTH)token:String,@Body json:JsonObject):Single<Response<ModelBaseResponse>>

    @Headers(APIConstants.CONTENT_TYPE)
    @POST(APIConstants.FACEBOOKLOGIN)
    fun login_Signup_Facebook( @Body json:JsonObject):Single<Response<ModelBaseResponse>>

    @Multipart
    @POST(APIConstants.ORDER_MEDICINE)
    fun order_Medicine(@Header(APIConstants.AUTH)token:String,
                       @Part file:MultipartBody.Part?,
                       @Query(Constants.USER_ID) user_id:Int,
                       @Query(Constants.ID) id:Int,
                       @Query(Constants.NAME) name:String,
                       @Query(Constants.MEDICINE_QUANTITY) quantity:Int,
                       @Query(Constants.MEDICINE_DOSE) dose:String?,
                       @Query(Constants.MEDICINE_INSTRUCTION) instructions:String?,
                       @Query(Constants.MEDICINE_TYPE) type:String,
                       @Query(Constants.MEDICINE_ADDRESS_ID) address_id:Int,
                       @Query("instance") instance:Boolean,
                       @Query(Constants.ORDER_DATE) orderDate:String?,
                       @Query(Constants.ORDER_TIME) orderTime:String?
                        ):Single<Response<ModelBaseResponse>>


    @POST(APIConstants.ORDER_MEDICINE)
    fun order_Medicine(@Header(APIConstants.AUTH)token:String,
        @Query(Constants.USER_ID) user_id:Int,
        @Query(Constants.ID) id:Int,
        @Query(Constants.NAME) name:String,
        @Query(Constants.MEDICINE_QUANTITY) quantity:Int,
        @Query(Constants.MEDICINE_DOSE) dose:String?,
        @Query(Constants.MEDICINE_INSTRUCTION) instructions:String?,
        @Query(Constants.MEDICINE_TYPE) type:String,
        @Query(Constants.MEDICINE_ADDRESS_ID) address_id:Int,
        @Query("instance") instance:Boolean,
        @Query(Constants.ORDER_DATE) orderDate:String?,
        @Query(Constants.ORDER_TIME) orderTime:String?
    ):Single<Response<ModelBaseResponse>>


    @Multipart
    @POST(APIConstants.ORDER_PARCEL)
    fun order_Parcel(@Header(APIConstants.AUTH)token:String,
                     @Part file:MultipartBody.Part,
                      @Query(Constants.USER_ID) user_ID:Int,
                      @Query(Constants.ADDRESS_ID) address_ID:Int,
                      @Query(Constants.PARCEL_ITEM_NAME) item_name:String,
                      @Query(Constants.PARCEL_ITEM_WEIGHT) item_Weight:String,
                      @Query(Constants.PARCEL_SENDER_NAME) sender_name:String,
                      @Query(Constants.PARCEL_SENDER_PHONE) sender_phone:String,
                      @Query(Constants.PARCEL_SENDER_ADDRESS) sender_address:String,
                      @Query(Constants.SENDER_LAT) sender_lat:String,
                      @Query(Constants.SENDER_LNG) sender_long:String,
                      @Query(Constants.PARCEL_RECEIVER_NAME) receiver_name:String,
                      @Query(Constants.PARCEL_RECEIVER_PHONE) receiver_phone:String,
                      @Query(Constants.PARCEL_RECEIVER_ADDRESS) receiver_address:String,
                      @Query(Constants.PARCEL_RECEIVER_LAT) receiver_lat:String,
                      @Query(Constants.PARCEL_RECEIVER_LNG) receiver_long:String,
                      @Query(Constants.ORDER_DATE) orderDate: String,
                      @Query(Constants.ORDER_TIME) orderTime: String,
                      @Query(Constants.MANUAL_SENDER_ADDRESS) manual_sender:String,
                      @Query(Constants.MANUAL_RECEIVER_ADDRESS) manual_receiver:String,
        @Query(Constants.AMOUNT) amount:String,
        @Query("instance") instance:Boolean

        ):Single<Response<ModelBaseResponse>>


    //google API call
    @GET(APIConstants.DIRECTION)
    fun getDirectionAPI(
        @Query(Constants.UNIT) unit:String,
        @Query(Constants.ORIGIN) origin:String,
        @Query(Constants.DESTINATION) destination:String,
        @Query(Constants.KEY) key:String

    ):Single<Response<GoogleDistanceApiResponse>>

    @Headers(APIConstants.CONTENT_TYPE)
    @POST(APIConstants.GET_ORDER)
    fun getOrders( @Header(APIConstants.AUTH)token:String,@Body json:JsonObject):Single<Response<ModelBaseResponse>>

    @Headers(APIConstants.CONTENT_TYPE)
    @POST(APIConstants.GET_ORDER_DETAIl)
    fun getOrderDetails( @Header(APIConstants.AUTH)token:String,@Body json:JsonObject):Single<Response<ModelBaseResponse>>

    @Headers(APIConstants.CONTENT_TYPE)
    @POST(APIConstants.REQUEST_RIDE)
    fun postRideOrder( @Header(APIConstants.AUTH)token:String,@Body json:JsonObject):Single<Response<ModelBaseResponse>>

    @Headers(APIConstants.CONTENT_TYPE)
    @POST(APIConstants.CANCEL_ORDER)
    fun cancelOrder( @Header(APIConstants.AUTH)token:String,@Body json:JsonObject):Single<Response<ModelBaseResponse>>

    @Headers(APIConstants.CONTENT_TYPE)
    @POST(APIConstants.RATING)
    fun rateOrder( @Header(APIConstants.AUTH)token:String,@Body json:JsonObject):Single<Response<ModelBaseResponse>>

    @POST(APIConstants.GET_CONTACT_US)
    fun getContactUS():Single<Response<ModelBaseResponse>>

    @POST(APIConstants.GET_FAQ)
    fun getFAQ():Single<Response<ModelBaseResponse>>

    @POST(APIConstants.GET_TAC)
    fun getTAC():Single<Response<ModelBaseResponse>>

    @Headers(APIConstants.CONTENT_TYPE)
    @POST(APIConstants.CHECK_RIDE_STATUS)
    fun checkRideStatus(@Header(APIConstants.AUTH)token:String, @Body json:JsonObject):Single<Response<ModelBaseResponse>>

    @Headers(APIConstants.CONTENT_TYPE)
    @GET(APIConstants.GET_MAIN_CATEGORIES)
    fun getMainCategories():Single<Response<ModelBaseResponse>>

    @Headers(APIConstants.CONTENT_TYPE)
    @POST(APIConstants.GET_VENDORS)
    fun getVendors(@Body json:JsonObject):Single<Response<ModelBaseResponse>>


    @Headers(APIConstants.CONTENT_TYPE)
    @POST(APIConstants.GET_OTP_FROM)
    fun verifymailcheckfromserver(@Body json: JsonObject):Single<Response<ModelBaseResponse>>
    ////
    @Headers(APIConstants.CONTENT_TYPE)
    @POST(APIConstants.SENDOTP)
    fun forgetpassword(@Body json: JsonObject):Single<Response<ModelBaseResponse>>

    @Headers(APIConstants.CONTENT_TYPE)
    @POST(APIConstants.CONFIRM_PASSWORD)
    fun changepassword(@Body json: JsonObject):Single<Response<ModelBaseResponse>>

    @Headers(APIConstants.CONTENT_TYPE)
    @POST(APIConstants.GET_VENDOR_CATEGORIES)
    fun getVendorCategories(@Body json:JsonObject):Single<Response<ModelBaseResponse>>

    @Headers(APIConstants.CONTENT_TYPE)
    @POST(APIConstants.GET_VENDOR_PRODUCTS)
    fun getVendorProducts(@Body json:JsonObject):Single<Response<ModelBaseResponse>>



    @Headers(APIConstants.CONTENT_TYPE)
    @POST(APIConstants.GET_REVIEWS)
    fun getReviewsFromUser(@Body json: JsonObject):Single<Response<ModelBaseResponse>>

    @POST(APIConstants.INSERT_COMMENT)
    fun InsertReviews(@Header(APIConstants.AUTH)token: String,@Body json: JsonObject):Single<Response<ModelBaseResponse>>

    @POST(APIConstants.CHECK_PROMO)
    fun checkPromo(@Body json:JsonObject):Single<Response<ModelBaseResponse>>

    @POST(APIConstants.GET_INSTANT)
    fun getInstant():Single<Response<ModelBaseResponse>>

    @Headers(APIConstants.CONTENT_TYPE)
    @POST(APIConstants.ORDER_ECOMMERCE)
    fun order_Ecommerce(@Header(APIConstants.AUTH)token:String, @Body json:JsonObject):Single<Response<ModelBaseResponse>>

    @Headers(APIConstants.CONTENT_TYPE)
    @POST(APIConstants.SEARCH)
    fun search(@Body json:JsonObject):Single<Response<ModelBaseResponse>>

    @Headers(APIConstants.CONTENT_TYPE)
    @POST(APIConstants.GET_VENDOR)
    fun getVendor(@Body json:JsonObject):Single<Response<ModelBaseResponse>>

    @Headers(APIConstants.CONTENT_TYPE)
    @POST(APIConstants.GET_PRODUCTS_BY_ORDER_ID)
    fun getOrderProducts(@Header(APIConstants.AUTH)token:String,@Body json:JsonObject):Single<Response<ModelBaseResponse>>

}