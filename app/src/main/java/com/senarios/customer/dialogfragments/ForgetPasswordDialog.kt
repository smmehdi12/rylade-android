package com.senarios.customer.dialogfragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.senarios.rylad.BaseDialogeFragment
import com.senarios.customer.R
import com.senarios.customer.costants.Constants
import com.senarios.customer.costants.Messages
import com.senarios.customer.costants.SPConstants
import com.senarios.customer.databinding.FragmentForgetPasswordBinding
import com.senarios.customer.models.ModelBaseResponse
import com.senarios.customer.models.ModelCommunication
import com.senarios.customer.models.User
import com.senarios.customer.retrofit.APIConstants
import com.senarios.customer.retrofit.ApiResponse
import com.senarios.customer.retrofit.NetworkCall
import com.senarios.customer.utility.Utility
import retrofit2.Response

class ForgetPasswordDialog: BaseDialogeFragment(),View.OnClickListener,View.OnFocusChangeListener,ApiResponse {



    private lateinit var binding:FragmentForgetPasswordBinding
    private val list= mutableListOf<ModelCommunication>()
    override fun init() {
        binding.phoneEditText.onFocusChangeListener=this
        binding.backImageView.setOnClickListener(this)
        binding.buttonForget.setOnClickListener(this)
        binding.phoneEditText.requestFocus()


    }

    override fun getFragmentView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val  view=inflater.inflate(R.layout.fragment_forget_password,container,false)
        binding=DataBindingUtil.bind(view)!!

        return view
    }

    override fun OnBackPressed() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onClick(p0: View?) {
        when(p0?.id)
        {
            binding.buttonForget.id->{

                validate_input()
            }
            binding.backImageView.id->{

                dismiss()
                getDialogCB().OnChange(LoginSignupDialog())
            }



        }


    }

    private fun validate_input() {
        val text_field=Utility.getTextET(binding.phoneEditText)
        if(text_field.isEmpty())
        {
             binding.phoneEditText.error=Messages.EMPTY_FIELD
        }
        else
        {
            dismiss()
            list.clear()
            list.add(ModelCommunication(Constants.EMAIL,text_field))
            NetworkCall.callAPI(context!!,getService().forgetpassword(Utility.getJson(list)),this,true,APIConstants.SENDOTP)


        }

    }

    override fun onFocusChange(p0: View?, p1: Boolean) {

    }

    override fun OnSuccess(endpoint: String, t: Response<ModelBaseResponse>, response: String) {
        val user = Utility.getgson().fromJson<User>(response, User::class.java)
        getViewModel().setUser(user)
        Utility.setPreferences(getReference(),SPConstants.IS_FORGET_PASSWORD,true)
        dismiss()
        getDialogCB().OnChange(VerifyOTPDialog())

    }

    override fun OnStatusfalse(
        endpoint: String,
        t: Response<ModelBaseResponse>,
        response: String,
        message: String
    ) {
        Utility.showErrorDialog(getReference(),R.layout.dialog_error_505,Messages.ERROR_TITLE,message)
        //To change body of created functions use File | Settings | File Templates.
    }

    override fun OnError(endpoint: String, code: Int, message: String) {
        Utility.showErrorDialog(getReference(),R.layout.dialog_error_505,Messages.ERROR_TITLE,message)
    }

    override fun OnException(endpoint: String, exception: Throwable) {
        Utility.showErrorDialog(getReference(),R.layout.dialog_error,Messages.ERROR_TITLE,exception.localizedMessage!!)


    }

    override fun OnNetworkError(endpoint: String, message: String) {
        Utility.showNErrorDialog(getReference())

    }
}