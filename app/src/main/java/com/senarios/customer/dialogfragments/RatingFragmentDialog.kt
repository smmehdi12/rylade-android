package com.senarios.customer.dialogfragments

import android.content.res.Resources
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.muddzdev.styleabletoast.StyleableToast
import com.senarios.rylad.BaseDialogeFragment
import com.senarios.customer.R
import com.senarios.customer.adaptors.ProductReviewAdapter
import com.senarios.customer.adaptors.RecyclerViewCallback
import com.senarios.customer.costants.Constants
import com.senarios.customer.costants.Messages
import com.senarios.customer.databinding.FragmentRatingDialogBinding
import com.senarios.customer.models.*
import com.senarios.customer.retrofit.APIConstants
import com.senarios.customer.retrofit.ApiResponse
import com.senarios.customer.retrofit.NetworkCall
import com.senarios.customer.utility.Utility
import retrofit2.Response
import kotlin.math.roundToInt

class RatingFragmentDialog : BaseDialogeFragment(), View.OnClickListener, ApiResponse ,RecyclerViewCallback{
    private lateinit var binding: FragmentRatingDialogBinding
    private lateinit var order: OrderModel
    private lateinit var adaptor:ProductReviewAdapter


    override fun init() {
        binding.backImageView.setOnClickListener(this)
        binding.ratingDoneButton.setOnClickListener(this)
        if (arguments != null) {
            order = arguments!!.getParcelable(Constants.ORDER_DATA)!!
            val list: MutableList<ModelCommunication> = mutableListOf()
            list.add(ModelCommunication(Constants.ORDER_ID, order.id.toString()))
            NetworkCall.callAPI(context!!, getService().getOrderProducts(APIConstants.BEARER + Utility.getUser(context!!).token, Utility.getJson(list)), this, false, APIConstants.GET_PRODUCTS_BY_ORDER_ID)
        }


    }

    private fun callApi() {
            val token = Utility.getUser(context!!).token
            val list: MutableList<ModelCommunication> = mutableListOf()
            list.add(ModelCommunication("rider_id", order.rider_id.toString()))
            list.add(ModelCommunication("user_id", order.user_id.toString()))
            list.add(ModelCommunication("rider_rating", binding.listitemratingRider.rating.roundToInt().toString()))
            list.add(ModelCommunication("products",Utility.getReviews(requireContext(),order.id,adaptor.getData())!!,true))
            list.add(ModelCommunication(Constants.ORDER_ID, order.id.toString()))
            NetworkCall.callAPI(context!!, getService().InsertReviews(APIConstants.BEARER + token,Utility.getJson(list) ), this, true, APIConstants.INSERT_COMMENT)
    }

    override fun getFragmentView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_rating_dialog, container, false)
        binding = DataBindingUtil.bind(view)!!
        expand()
        return view
    }

    override fun OnBackPressed() {

    }

    override fun onClick(p0: View?) {
        when (p0?.id) {
            binding.ratingDoneButton.id -> {
                if (isComment(adaptor.getData())) {
                    callApi()
                }
            }
            binding.backImageView.id->{
                dismiss()
            }


        }

    }

    override fun OnSuccess(endpoint: String, t: Response<ModelBaseResponse>, response: String) {
        if (endpoint.equals(APIConstants.INSERT_COMMENT)) {
            getDialogCB().OnTrigger()
            dismiss()
            Utility.removeSpValue(requireContext(), Constants.ORDER)
        }
        else{
            val data=Utility.getgson().fromJson(response,ResponseProducts::class.java)
            if (!data.products.isNullOrEmpty()){
                adaptor= ProductReviewAdapter(requireContext(),data.products as ArrayList<ProductModel>,this)
                binding.recyclerview.adapter=adaptor
                binding.group.visibility=View.VISIBLE
                binding.progressBar.visibility=View.GONE
            }

        }
    }

    override fun OnStatusfalse(endpoint: String, t: Response<ModelBaseResponse>, response: String, message: String) {
        binding.progressBar.visibility=View.GONE
        Utility.showErrorDialog(context!!, R.layout.dialog_error_505, Messages.ERROR_TITLE, message)

    }

    override fun OnError(endpoint: String, code: Int, message: String) {
        binding.progressBar.visibility=View.GONE
        Utility.showErrorDialog(context!!, R.layout.dialog_error_505, Messages.ERROR_TITLE, message)

    }

    override fun OnException(endpoint: String, exception: Throwable) {
        binding.progressBar.visibility=View.GONE
        Utility.showErrorDialog(context!!, R.layout.dialog_error, Messages.ERROR_TITLE, exception.localizedMessage!!)

    }

    override fun OnNetworkError(endpoint: String, message: String) {
        binding.progressBar.visibility=View.GONE
        Utility.showNErrorDialog(context!!)

    }

    private fun isComment(list:ArrayList<ProductModel>):Boolean{
        for (item in list){
            if (item.comment.isNullOrEmpty() || item.rating.isNullOrEmpty()){
                StyleableToast.makeText(requireContext(),"Please Add Comments",R.style.mytoast).show()
                return false
            }

        }
        return true
    }
}