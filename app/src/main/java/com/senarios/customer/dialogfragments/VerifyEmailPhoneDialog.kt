package com.senarios.customer.dialogfragments

import android.os.Bundle
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.senarios.rylad.BaseDialogeFragment
import com.senarios.customer.R
import com.senarios.customer.costants.Constants
import com.senarios.customer.costants.Messages
import com.senarios.customer.costants.SPConstants
import com.senarios.customer.databinding.FragmentVerifyEmailPhoneBinding
import com.senarios.customer.models.ModelBaseResponse
import com.senarios.customer.models.ModelCommunication
import com.senarios.customer.models.User
import com.senarios.customer.retrofit.APIConstants
import com.senarios.customer.retrofit.ApiResponse
import com.senarios.customer.retrofit.NetworkCall
import com.senarios.customer.utility.Utility
import retrofit2.Response


class VerifyEmailPhoneDialog : BaseDialogeFragment(),Observer<User> ,View.OnFocusChangeListener, View.OnClickListener, ApiResponse {

    private lateinit var binding: FragmentVerifyEmailPhoneBinding


    override fun init() {
        binding.buttonVerify.setOnClickListener(this)
        binding.emailVerifyEditText.onFocusChangeListener = this
        binding.inputLayout.onFocusChangeListener = this
        binding.emailVerifyEditText.requestFocus()
        binding.backImageView.setOnClickListener(this)

        getViewModel().getUser().observe(this,this)

    }

    override fun getFragmentView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {


        val view = inflater.inflate(R.layout.fragment_verify_email_phone, container, false)
        binding = DataBindingUtil.bind(view)!!

        return view



    }

    override fun OnBackPressed() {

    }


    override fun onClick(p0: View?) {
        when (p0?.id) {
            binding.backImageView.id -> {
                dismiss()
                getDialogCB().OnChange(LoginSignupDialog())
            }
            binding.buttonVerify.id -> {
                validateInputs()
            }
        }

    }

    private fun validateInputs() {

        val email_text = Utility.getTextET(binding.emailVerifyEditText)
        val password_text = Utility.getTextET(binding.inputLayout)
        if (email_text.isEmpty()) {
            binding.emailVerifyEditText.error = Messages.EMPTY_FIELD


        } else if (password_text.isEmpty()) {
            binding.inputLayout.error = Messages.EMPTY_FIELD

        } else if (password_text.length > 11) {
            binding.inputLayout.error = Messages.PASSWORD_ERROR
        } else if (!Patterns.EMAIL_ADDRESS.matcher(Utility.getTextET(binding.emailVerifyEditText)).matches()) {
            binding.inputLayout.error = Messages.INVALID_EMAIL
        } else {
            val list_of_mail = mutableListOf<ModelCommunication>()
            list_of_mail.add(ModelCommunication(Constants.EMAIL, email_text))
            list_of_mail.add(ModelCommunication(Constants.PHONE, password_text))
            list_of_mail.add(ModelCommunication(Constants.ID, getViewModel().getUser().value!!.id.toString()))
            NetworkCall.callAPI(context!!, getService().verifymailcheckfromserver(Utility.getJson(list_of_mail)), this, true, APIConstants.GET_OTP_FROM)
        }


    }

    override fun onFocusChange(p0: View?, p1: Boolean) {
        if (binding.emailVerifyEditText.hasFocus()) {
            set_activated(true, false)
        } else if (binding.inputLayout.hasFocus()) {
            set_activated(false, true)

        }
    }

    private fun set_activated(email_verify: Boolean, password_verify: Boolean) {

        if (email_verify) {

            setdrawable_green(binding.emailVerifyEditText)
            setdrawablegray(binding.inputLayout)

        } else {
            setdrawablegray(binding.emailVerifyEditText)
            setdrawable_green(binding.inputLayout)

        }


    }

    private fun setdrawablegray(passwordVerifyEditText: EditText?) {
        if (passwordVerifyEditText != null) {
            passwordVerifyEditText.background = resources.getDrawable(R.drawable.email_grey, context?.theme)
        }

    }

    private fun setdrawable_green(emailVerifyEditText: EditText?) {
        if (emailVerifyEditText != null) {
            emailVerifyEditText.background = resources.getDrawable(R.drawable.email_green, context?.theme)
        }

    }
    override fun OnSuccess(endpoint: String, t: Response<ModelBaseResponse>, response: String) {
        try {
            dismiss()
            val user = Utility.getgson().fromJson<User>(response, User::class.java)
            getViewModel().setUser(user)
            Utility.showNotification(getReference(), "OTP", user.otp)
            Utility.setPreferences(getReference(),SPConstants.IS_FACEBOOK_VERIFY,true)
            getDialogCB().OnChange(VerifyOTPDialog())
        }
        catch (e:Exception){
            OnException(endpoint,e)
        }


    }

    override fun OnStatusfalse(endpoint: String, t: Response<ModelBaseResponse>, response: String, message: String) {
        Utility.showErrorDialog(getReference(), R.layout.dialog_error_505, Messages.ERROR_TITLE, message) //To change body of created functions use File | Settings | File Templates.
    }

    override fun OnError(endpoint: String, code: Int, message: String) {
        Utility.showErrorDialog(getReference(), R.layout.dialog_error_505, Messages.ERROR_TITLE, message)
    }

    override fun OnException(endpoint: String, exception: Throwable) {
        Utility.showToast(getReference(), "message" + exception.message) //To change body of created functions use File | Settings | File Templates.
    }

    override fun OnNetworkError(endpoint: String, message: String) {
        Utility.showNErrorDialog(getReference())
    }

    override fun onChanged(t: User?) {
        if (t!=null) {
            if (t.email != null) {
                binding.emailVerifyEditText.setText(t.email)
            }
        }
    }


}