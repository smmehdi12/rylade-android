package com.senarios.customer.dialogfragments

import com.senarios.rylad.BaseDialogeFragment
/*interface which helps with communication between dialoge and acitvity*/
interface DialogeCallback {
    fun OnChange(fragment: BaseDialogeFragment)
    fun OnTrigger()
}