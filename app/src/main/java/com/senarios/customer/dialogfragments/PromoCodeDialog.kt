package com.senarios.customer.dialogfragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.senarios.rylad.BaseDialogeFragment
import com.senarios.customer.R
import com.senarios.customer.costants.Constants
import com.senarios.customer.databinding.FragmentPromoCodeBinding
import com.senarios.customer.models.ModelBaseResponse
import com.senarios.customer.models.ModelCommunication
import com.senarios.customer.models.PromoModel
import com.senarios.customer.retrofit.APIConstants
import com.senarios.customer.retrofit.ApiResponse
import com.senarios.customer.retrofit.NetworkCall
import com.senarios.customer.utility.Utility
import org.greenrobot.eventbus.EventBus
import retrofit2.Response
import java.lang.Exception

class PromoCodeDialog : BaseDialogeFragment(),ApiResponse,View.OnClickListener{
    private lateinit var binding:FragmentPromoCodeBinding



    override fun init() {
        binding.backImageView.setOnClickListener(this)
        binding.btnDone.setOnClickListener(this)

    }

    override fun getFragmentView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view=inflater.inflate(R.layout.fragment_promo_code, container,false)
        binding= DataBindingUtil.bind(view)!!
        return view
    }

    override fun OnBackPressed() {

    }

    override fun onClick(v: View?) {
        when (v?.id){
            binding.backImageView.id->{
                dismiss()
            }
            binding.btnDone.id->{
                validateInputs()
            }
        }
    }

    private fun validateInputs() {
        val promoText=Utility.getTextET(binding.promoEditText)
        if (promoText.isEmpty()){
            Utility.setErrorET(binding.promoEditText)
        }
        else{
            val list= mutableListOf<ModelCommunication>()
            list.add(ModelCommunication(Constants.PROMO,promoText))
            list.add(ModelCommunication(Constants.VENDOR_ID,Utility.getDB().getVendorIDFromCart().toString()))
            NetworkCall.callAPI(context!!,getService().checkPromo(Utility.getJson(list)),this,true,APIConstants.CHECK_PROMO)
        }

    }

    override fun OnSuccess(endpoint: String, t: Response<ModelBaseResponse>, response: String) {
        try{
            val promoModel=Utility.getgson().fromJson(response,PromoModel::class.java)
            EventBus.getDefault().post(promoModel)
            Utility.setPreferences(context!!,Constants.PROMO_DATA,response)
            dismiss()
        }
        catch (e:Exception){
            OnException(endpoint,e)
        }
    }

    override fun OnStatusfalse(endpoint: String, t: Response<ModelBaseResponse>, response: String, message: String) {
        sendError(message)
    }

    override fun OnError(endpoint: String, code: Int, message: String) {
        sendError(message)
    }

    override fun OnException(endpoint: String, exception: Throwable) {
        sendError(""+exception.message)
    }

    override fun OnNetworkError(endpoint: String, message: String) {
       sendError(message)
    }

    private fun sendError(message:String){
        dismiss()
        EventBus.getDefault().post(ModelCommunication("Error",message))
    }
}