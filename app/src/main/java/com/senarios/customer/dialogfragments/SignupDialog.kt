package com.senarios.customer.dialogfragments

import android.os.Bundle
import android.util.Patterns
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.google.gson.Gson
import com.senarios.rylad.BaseDialogeFragment
import com.senarios.customer.R
import com.senarios.customer.costants.Constants
import com.senarios.customer.costants.Messages
import com.senarios.customer.costants.SPConstants
import com.senarios.customer.databinding.FragmentSignupBinding
import com.senarios.customer.models.ModelBaseResponse
import com.senarios.customer.models.ModelCommunication
import com.senarios.customer.models.User
import com.senarios.customer.retrofit.APIConstants
import com.senarios.customer.retrofit.ApiResponse
import com.senarios.customer.retrofit.NetworkCall
import com.senarios.customer.utility.Utility
import retrofit2.Response

class SignupDialog(): BaseDialogeFragment(), Observer<String>, View.OnClickListener,TextView.OnEditorActionListener,View.OnFocusChangeListener,ApiResponse {
    private lateinit var binding:FragmentSignupBinding

    override fun getFragmentView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
      val view=inflater.inflate(R.layout.fragment_signup,container,false)
        binding=DataBindingUtil.bind(view)!!
        return view
    }

    override fun init(){

    binding.backImageView.setOnClickListener(this)
        binding.button.setOnClickListener(this)
        binding.firstNameEditText.requestFocus()
        binding.firstNameEditText.onFocusChangeListener = this
        binding.lastNameEditText.onFocusChangeListener = this
        binding.passwordEditText.onFocusChangeListener = this
        binding.emailEditText.onFocusChangeListener = this
        binding.numberEditText.onFocusChangeListener = this
        binding.numberEditText.setOnEditorActionListener(this)


        getViewModel().getEmail().observe(this,this)
    }
    override fun OnBackPressed() {

    }

    override fun onClick(v: View?) {
        when(v?.id){
            binding.backImageView.id->{
                dismiss()
                getDialogCB().OnChange(LoginSignupDialog())
            }
            binding.button.id->{
                validateInputs()

            }
        }
    }

    private fun validateInputs() {
        val fname=Utility.getTextET(binding.firstNameEditText)
        val lname=Utility.getTextET(binding.lastNameEditText)
        val email=Utility.getTextET(binding.emailEditText)
        val password=Utility.getTextET(binding.passwordEditText)
        val number=Utility.getTextET(binding.numberEditText)
        if (fname.isEmpty()){
            Utility.setErrorOnET(binding.firstNameEditText,Messages.EMPTY_FIELD)
        }
        else if (lname.isEmpty()){
            Utility.setErrorOnET(binding.lastNameEditText,Messages.EMPTY_FIELD)
        }
        else if (email.isEmpty()){
            Utility.setErrorOnET(binding.emailEditText,Messages.EMPTY_FIELD)
        }
        else if (password.isEmpty()){
            Utility.setErrorOnET(binding.passwordEditText,Messages.EMPTY_FIELD)
        }
        else if (number.isEmpty()){
            Utility.setErrorOnET(binding.numberEditText,Messages.PASSWORD_ERROR)
        }
        else if (password.length<8){
            Utility.setErrorOnET(binding.passwordEditText,"Password must be 8 characters atleast!")
        }
        else if (!Patterns.EMAIL_ADDRESS.matcher(Utility.getTextET(binding.emailEditText)).matches()){
            binding.emailEditText.error= Messages.INVALID_EMAIL
        }
//        else if (!Utility.hasPermissions(this.context, arrayOf(android.Manifest.permission.RECEIVE_SMS))){
//            checkSMSPermission()
//        }
        else {
            val list= mutableListOf<ModelCommunication>()
            list.add(ModelCommunication(Constants.F_NAME,fname))
            list.add(ModelCommunication(Constants.L_NAME,lname))
            list.add(ModelCommunication(Constants.EMAIL,email))
            list.add(ModelCommunication(Constants.PASSWORD,password))
            list.add(ModelCommunication(Constants.PHONE,"+92"+number))
            list.add(ModelCommunication(Constants.TYPE,"customer"))
            list.add(ModelCommunication(SPConstants.FCM,Utility.getFCM(this.context)))
            NetworkCall.callAPI(context!!,getService().register(Utility.getJson(list)),this,true,APIConstants.REGISTER)
        }

    }

    override fun onFocusChange(v: View?, hasFocus: Boolean) {
        if (binding.firstNameEditText.hasFocus()){
            setActive(true,false,false,false,false)
        }
        else if(binding.lastNameEditText.hasFocus()){
            setActive(false,true,false,false,false)
        }
        else if(binding.emailEditText.hasFocus()){
            setActive(false,false,true,false,false)
        }
        else if(binding.passwordEditText.hasFocus()){
            setActive(false,false,false,true,false)
        }
        else if(binding.numberEditText.hasFocus()){
            setActive(false,false,false,false,true)
        }
    }



    override fun OnSuccess(endpoint:String,t: Response<ModelBaseResponse>,response:String) {
        try{
            Utility.getPreference(getReference()).edit().remove(SPConstants.IS_FORGET_PASSWORD).apply()
            val data=Gson().fromJson<User>(response,User::class.java)
            getViewModel().setUser(data)
            dismiss()
            Utility.showNotification(getReference(),"OTP",data.otp)
            getDialogCB().OnChange(VerifyOTPDialog())
        }
        catch (e:Exception){
            OnException(endpoint,e)
        }



    }

    override fun OnStatusfalse(endpoint:String,t: Response<ModelBaseResponse>, response: String, message: String) {

    }


    override fun OnError(endpoint:String,code: Int, message: String) {
            Utility.showErrorDialog(context!!,R.layout.dialog_error_505,Messages.ERROR_TITLE,message)
    }


    override fun OnException(endpoint:String,exception: Throwable) {
        Utility.showErrorDialog(context!!,R.layout.dialog_error,Messages.ERROR_TITLE,exception.localizedMessage!!)
    }

    override fun OnNetworkError(endpoint:String,message: String) {
        Utility.showNErrorDialog(context!!)
    }



    private fun setActive(fname: Boolean,lname: Boolean,email: Boolean,password: Boolean,number: Boolean){
        if (fname){
            setGreenDrawable(binding.firstNameEditText)
            setGrayDrawable(binding.lastNameEditText)
            setGrayDrawable(binding.emailEditText)
            setGrayDrawable(binding.passwordEditText)
            setGrayDrawable(binding.numberEditText)


        }
        else if (lname){
            setGrayDrawable(binding.firstNameEditText)
            setGreenDrawable(binding.lastNameEditText)
            setGrayDrawable(binding.emailEditText)
            setGrayDrawable(binding.passwordEditText)
            setGrayDrawable(binding.numberEditText)
        }
        else if (email){
            setGrayDrawable(binding.firstNameEditText)
            setGrayDrawable(binding.lastNameEditText)
            setGreenDrawable(binding.emailEditText)
            setGrayDrawable(binding.passwordEditText)
            setGrayDrawable(binding.numberEditText)
        }
        else if (password){
            setGrayDrawable(binding.firstNameEditText)
            setGrayDrawable(binding.lastNameEditText)
            setGrayDrawable(binding.emailEditText)
            setGreenDrawable(binding.passwordEditText)
            setGrayDrawable(binding.numberEditText)
        }
        else if (number){
            setGrayDrawable(binding.firstNameEditText)
            setGrayDrawable(binding.lastNameEditText)
            setGrayDrawable(binding.emailEditText)
            setGrayDrawable(binding.passwordEditText)
            setGreenDrawable(binding.numberEditText)
        }



    }
    private fun setGrayDrawable(editText: EditText){
        editText.background=resources.getDrawable(R.drawable.email_grey,context?.theme)

    }
    private fun setGreenDrawable(editText: EditText){
        editText.background=resources.getDrawable(R.drawable.email_green,context?.theme)
    }

    override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean {
        if (actionId== EditorInfo.IME_ACTION_DONE){
            validateInputs()
            return true
        }
        return true
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    override fun onChanged(t: String?) {
        if (t != null && t.length > 0) {
            binding.emailEditText.setText(t)
        }
    }

}