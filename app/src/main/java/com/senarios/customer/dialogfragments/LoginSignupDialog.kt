package com.senarios.customer.dialogfragments


import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.util.Patterns
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.facebook.*
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.senarios.rylad.BaseDialogeFragment
import com.senarios.customer.R
import com.senarios.customer.activities.MainActivity
import com.senarios.customer.costants.Constants
import com.senarios.customer.costants.Messages
import com.senarios.customer.costants.Messages.Companion.EMAIL_NO_EXIST
import com.senarios.customer.costants.SPConstants
import com.senarios.customer.databinding.FragmentLoginSignupBinding
import com.senarios.customer.models.ModelBaseResponse
import com.senarios.customer.models.ModelCommunication
import com.senarios.customer.models.User
import com.senarios.customer.retrofit.APIConstants
import com.senarios.customer.retrofit.ApiResponse
import com.senarios.customer.retrofit.NetworkCall
import com.senarios.customer.utility.Utility
import org.json.JSONObject
import retrofit2.Response


/**
 * A simple [Fragment] subclass.
 */
class LoginSignupDialog : BaseDialogeFragment(), GraphRequest.GraphJSONObjectCallback,FacebookCallback<LoginResult>,TextView.OnEditorActionListener,View.OnFocusChangeListener,View.OnClickListener,ApiResponse{
    lateinit var binding:FragmentLoginSignupBinding
    private val callback= CallbackManager.Factory.create()
    private lateinit var graphRequest: GraphRequest
    private lateinit var dialog: AlertDialog
    private val TAG=javaClass.name

    override fun getFragmentView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
       val view=inflater.inflate(R.layout.fragment_login_signup, container,false)
        binding=DataBindingUtil.bind(view)!!
       return view
    }




   override  fun init() {
        binding.emailEditText.setOnFocusChangeListener(this)
         binding.backImageView.setOnClickListener(this)
         binding.continueTextView.setOnClickListener(this)
       binding.emailEditText.setOnEditorActionListener(this)

       //facebook button
       binding.facebookButton.setPermissions("email", "public_profile")
       binding.facebookButton.fragment=this
       binding.facebookButton.registerCallback(callback,this)



    }

    override fun OnBackPressed() {
        dismiss()
    }

    override fun onFocusChange(v: View?, hasFocus: Boolean) {
        expand()
    }

    override fun onClick(v: View?) {

        when(v?.id){
        binding.backImageView.id -> {
            dismiss()
        }
            binding.continueTextView.id -> {
              validateInputs()
            }

    }

    }

    private fun validateInputs() {
        val email=Utility.getTextET(binding.emailEditText)
        if (Utility.validateET(binding.emailEditText)){
            binding.emailEditText.error=Messages.EMPTY_EMAIL
        }
        else if (!Patterns.EMAIL_ADDRESS.matcher(Utility.getTextET(binding.emailEditText)).matches()){
            binding.emailEditText.error=Messages.INVALID_EMAIL
        }
        else {
            dismiss()
            val list= mutableListOf<ModelCommunication>()
            list.add(ModelCommunication(Constants.EMAIL,email))
            NetworkCall.callAPI(context!!,getService().checkMail(Utility.getJson(list)),this,true,APIConstants.CHECKMAIL)
        }
    }

    override fun OnSuccess(endpoint:String,t: Response<ModelBaseResponse>, response: String) {

        try {
            dismiss()
            if (endpoint.equals(APIConstants.CHECKMAIL)) {
                getDialogCB().OnChange(LoginDialog())
                getViewModel().setEmail(Utility.getTextET(binding.emailEditText)
                )
            }
            else if(endpoint.equals(APIConstants.FACEBOOKLOGIN)){
                val user = Utility.getgson().fromJson<User>(response,User::class.java)
                if (user.is_verified) {
                    Utility.setPreferences(context!!, SPConstants.USER, response)
                    if (activity is MainActivity) {
                        val activity = activity as MainActivity
                        activity.getbinding().navView.menu.clear()
                        activity.getbinding().navView.inflateMenu(R.menu.main_drawer_menu)
                    } else {
                        getDialogCB().OnTrigger()
                    }
                    Utility.removeEvents()
                    Utility.removeStringEvents()
                }
                else if (user.phone_number.isNullOrEmpty() || user.email.isNullOrEmpty()){
                    getViewModel().setUser(user)
                    LoginManager.getInstance().logOut()
                    getDialogCB().OnChange(VerifyEmailPhoneDialog())

                }
            }
        }
        catch (e:Exception){
            OnException(endpoint,e)
        }
    }

    override fun OnStatusfalse(endpoint:String,t: Response<ModelBaseResponse>, response: String, message: String) {
        if(endpoint.equals(APIConstants.FACEBOOKLOGIN)){
            LoginManager.getInstance().logOut()
        }
    }

    override fun OnError(endpoint:String,code: Int, message: String) {
       try{
            if (message.equals(EMAIL_NO_EXIST,true)) {
                dismiss()
                getViewModel().setEmail(Utility.getTextET(binding.emailEditText))
                 getDialogCB().OnChange(SignupDialog())
            }
            else{
                if (endpoint.equals(APIConstants.FACEBOOKLOGIN)){
                    LoginManager.getInstance().logOut()
                }
                    Utility.showErrorDialog(this.context!!,R.layout.dialog_error_505,Messages.ERROR_TITLE,message)

            }
        }
        catch (e:Exception){
            OnException(endpoint,e)
        }
    }

    override fun OnException(endpoint:String,exception: Throwable) {
        if (endpoint.equals(APIConstants.FACEBOOKLOGIN)){
            LoginManager.getInstance().logOut()
        }
        Utility.showErrorDialog(this.context!!,R.layout.dialog_error,Messages.ERROR_TITLE,exception.localizedMessage!!)

    }

    override fun OnNetworkError(endpoint:String,message: String) {
        if (endpoint.equals(APIConstants.FACEBOOKLOGIN)){
            LoginManager.getInstance().logOut()
        }
        Utility.showNErrorDialog(this.context!!)
    }

    override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean {
        if (actionId== EditorInfo.IME_ACTION_DONE){
            validateInputs()
            return true
        }
        return true
    }

    override fun onCancel() {
        Log.v(javaClass.name,"Facebook Cancelled")

    }

    override fun onSuccess(result: LoginResult?) {
        Log.v(javaClass.name,"Facebook Success"+Utility.getgson().toJson(result))
        graphRequest= GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(),this)
        val parameters = Bundle()
        parameters.putString("fields", "id,name,email,gender,birthday")
        graphRequest.setParameters(parameters)
        graphRequest.executeAsync()

    }

    override fun onResume() {
        super.onResume()
        if (::dialog.isInitialized){
            dialog.dismiss()
        }

    }

    override fun onError(error: FacebookException?) {
        Log.v(javaClass.name,""+error!!.message)
        dialog= Utility.showErrorDialog(this.context,R.layout.dialog_error, Messages.ERROR_TITLE,error.localizedMessage)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        callback.onActivityResult(requestCode, resultCode, data);

    }

    override fun onCompleted(json: JSONObject?, response: GraphResponse?) {
        var email:String?=""
        var name:String?=""
        var id:String?=""
        val list= mutableListOf<ModelCommunication>()
        try{
            if (response!=null){
                if(json!=null && json.length()>0){
                    Log.v(javaClass.name,"Facebook Success"+response.toString())
                    if (json.has(Constants.EMAIL)) {
                         email = json.getString(Constants.EMAIL)
                    }
                    if (json.has(Constants.ID)){
                        id=json.getString(Constants.ID)
                    }
                    if (json.has(Constants.NAME)){
                        name=json.getString(Constants.NAME)
                    }
                    list.add(ModelCommunication(SPConstants.FCM,Utility.getFCM(this.context)))
                    list.add(ModelCommunication(Constants.EMAIL,email!!))
                    list.add(ModelCommunication(Constants.F_NAME,name!!.split(" ")[0]))
                    list.add(ModelCommunication(Constants.L_NAME,name.split(" ")[1]))
                    list.add(ModelCommunication(Constants.FB_ID,id!!))
                    NetworkCall.callAPI(this.context!!,getService().login_Signup_Facebook(Utility.getJson(list)),this,true,APIConstants.FACEBOOKLOGIN)


                }

            }
        }
        catch (e:Exception){
            Log.v(TAG,e.message!!)
        }

    }
}
