package com.senarios.customer.dialogfragments

import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.google.gson.Gson
import com.senarios.rylad.BaseDialogeFragment
import com.senarios.customer.R
import com.senarios.customer.adaptors.RecyclerViewCallback
import com.senarios.customer.adaptors.ReviewAdapter
import com.senarios.customer.costants.Constants
import com.senarios.customer.costants.Messages
import com.senarios.customer.databinding.FragmentDisplayReviewsBinding
import com.senarios.customer.models.*
import com.senarios.customer.retrofit.APIConstants
import com.senarios.customer.retrofit.ApiResponse
import com.senarios.customer.retrofit.NetworkCall
import com.senarios.customer.utility.Utility
import retrofit2.Response
import java.text.DecimalFormat

class ReviewFragmentDialog:BaseDialogeFragment(),RecyclerViewCallback,View.OnClickListener{


    private lateinit var binding:FragmentDisplayReviewsBinding
    private  var list= mutableListOf<ModelCommunication>()
    private lateinit var adapter: ReviewAdapter
    private lateinit var review:ReviewModel
    private lateinit var productModel: ProductModel

    override fun init() {
        binding.toolbar.setNavigationOnClickListener(this)
        binding.imageViewReview.setOnClickListener(this)
        getViewModel().getComments().observe(this, Observer {
            if (!it.isNullOrEmpty()){
                binding.recyclerView.adapter=ReviewAdapter(requireContext(),it,this)
            }

        })
        getViewModel().getVendor().observe(this, Observer {
            if (it!=null){
                binding.vendor=it
            }

        })
    }



    override fun getFragmentView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view=inflater.inflate(R.layout.fragment_display_reviews,container,false)
        binding=DataBindingUtil.bind(view)!!
        return view

    }

    override fun OnBackPressed() {

    }

    override fun onClick(p0: View?) {
        when(p0?.id)
        {
            binding.imageViewReview.id->{
                Utility.loadFullScreenImageView(context!!,APIConstants.BASE_VENDOR_PHOTO+binding.vendor?.image.toString())


            }

        }

        dismiss()
    }


    override fun onDismiss(dialog: DialogInterface) {

    }

}