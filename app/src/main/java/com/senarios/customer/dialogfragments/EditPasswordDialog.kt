package com.senarios.customer.dialogfragments

import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import com.senarios.rylad.BaseDialogeFragment
import com.senarios.customer.R
import com.senarios.customer.costants.Messages
import com.senarios.customer.databinding.FragmentEditPasswordBinding
import com.senarios.customer.models.ModelBaseResponse
import com.senarios.customer.models.ModelCommunication
import com.senarios.customer.retrofit.APIConstants
import com.senarios.customer.retrofit.ApiResponse
import com.senarios.customer.retrofit.NetworkCall
import com.senarios.customer.utility.Utility
import com.senarios.customer.utility.Utility.Companion.getUserToken
import retrofit2.Response

class EditPasswordDialog : BaseDialogeFragment(),View.OnFocusChangeListener ,ApiResponse,View.OnClickListener,TextView.OnEditorActionListener{
    private lateinit var binding:FragmentEditPasswordBinding
    private lateinit var dialog: AlertDialog
    override fun init() {
        binding.passwordEditText.requestFocus()
        binding.passwordEditText.onFocusChangeListener=this
        binding.newPasswordEditText.onFocusChangeListener=this
        binding.newPasswordEditText.setOnEditorActionListener(this)
        binding.doneTxt.setOnClickListener(this)
        binding.backImageView.setOnClickListener(this)
    }

    override fun getFragmentView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view=inflater.inflate(R.layout.fragment_edit_password,container,false)
        binding=DataBindingUtil.bind(view)!!
        return binding.root
    }

    override fun OnBackPressed() {
    dismiss()
    }

    override fun onClick(v: View?) {
        when(v?.id){
            binding.doneTxt.id->{
                validateInput()
            }
            binding.backImageView.id->{
                dismiss()
            }

        }
    }

    override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean {
        if (actionId== EditorInfo.IME_ACTION_DONE){
            validateInput()
            return true
        }
        return true
     }

    private fun validateInput() {
        val password=Utility.getTextET(binding.passwordEditText)
        val new_password=Utility.getTextET(binding.newPasswordEditText)
        if (password.isEmpty()){
            Utility.setErrorET(binding.passwordEditText)
        }
        else if(new_password.isEmpty()){
            Utility.setErrorET(binding.newPasswordEditText)
        }
        else if (password.length<8){
            Utility.setErrorOnET(binding.passwordEditText,Messages.PASSWORD_ERROR)
        }
        else if (new_password.length<8){
            Utility.setRErrorET(binding.newPasswordEditText,Messages.PASSWORD_ERROR)
        }
        else {
            val list= mutableListOf<ModelCommunication>()
            list.add(ModelCommunication("old_password",password))
            list.add(ModelCommunication("new_password",new_password))
            list.add(ModelCommunication("user_id",getViewModel().getLoggedUser().value!!.id.toString()))
            NetworkCall.callAPI(context!!,getService().changepassword(getUserToken(getViewModel()),Utility.getJson(list)),this,true,APIConstants.CHANGEPASSWORD)

        }

    }

    override fun OnSuccess(endpoint: String, t: Response<ModelBaseResponse>, response: String) {
        try {
            showToast(this.context!!,"Password Changed Successfully")
            dismiss()

        }
        catch (e:Exception){
            OnException(endpoint,e)
        }
    }

    override fun OnStatusfalse(endpoint:String,t: Response<ModelBaseResponse>, response: String, message: String) {
        dialog=  Utility.showErrorDialog(this.context,R.layout.dialog_error_505,
            Messages.ERROR_TITLE,message)
    }

    override fun OnError(endpoint:String,code: Int, message: String) {
        dialog=  Utility.showErrorDialog(this.context,R.layout.dialog_error_505,
            Messages.ERROR_TITLE,message)
    }


    override fun OnException(endpoint:String,exception: Throwable) {
        dialog=  Utility.showErrorDialog(this.context,R.layout.dialog_error, Messages.ERROR_TITLE,exception.localizedMessage!!)
    }

    override fun OnNetworkError(endpoint:String,message: String) {
        dialog=  Utility.showNErrorDialog(this.context)
    }

    fun setGrayDrawable(editText: EditText){
        editText.background=resources.getDrawable(R.drawable.email_grey,context?.theme)

    }
    fun setGreenDrawable(editText: EditText){
        editText.background=resources.getDrawable(R.drawable.email_green,context?.theme)
    }

    override fun onFocusChange(v: View?, hasFocus: Boolean) {
        if (binding.passwordEditText.hasFocus()){
            setGreenDrawable(binding.passwordEditText)
            setGrayDrawable(binding.newPasswordEditText)
        }
        else if (binding.newPasswordEditText.hasFocus()){
            setGrayDrawable(binding.passwordEditText)
            setGreenDrawable(binding.newPasswordEditText)
        }
    }
    override fun onResume() {
        super.onResume()
        if (::dialog.isInitialized){
            dialog.dismiss()
        }
    }
}