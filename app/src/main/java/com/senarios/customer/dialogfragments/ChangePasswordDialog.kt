package com.senarios.customer.dialogfragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.databinding.DataBindingUtil
import com.senarios.rylad.BaseDialogeFragment
import com.senarios.customer.R
import com.senarios.customer.costants.Constants
import com.senarios.customer.costants.Messages
import com.senarios.customer.utility.Utility
import com.senarios.customer.databinding.FragmentPasswordChangeBinding
import com.senarios.customer.models.ModelBaseResponse
import com.senarios.customer.models.ModelCommunication
import com.senarios.customer.retrofit.APIConstants
import com.senarios.customer.retrofit.ApiResponse
import com.senarios.customer.retrofit.NetworkCall
import retrofit2.Response

class ChangePasswordDialog:BaseDialogeFragment(),View.OnClickListener,View.OnFocusChangeListener ,ApiResponse{



    private lateinit var binding:FragmentPasswordChangeBinding
    private var list= mutableListOf<ModelCommunication>()
    override fun init() {
        isCancelable=false

        binding.newPasswordEditText.onFocusChangeListener=this
        binding.confirmPasswordEdittext.onFocusChangeListener=this
        binding.newPasswordEditText.requestFocus()
        binding.buttonChangePassword.setOnClickListener(this)
        binding.backImageView.setOnClickListener(this)


    }

    override fun getFragmentView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val v=inflater.inflate(R.layout.fragment_password_change,container,false)
        binding=DataBindingUtil.bind(v)!!
        return v

    }

    override fun OnBackPressed() {
         //To change body of created functions use File | Settings | File Templates.
    }

    override fun onFocusChange(p0: View?, p1: Boolean) {
        if(binding.newPasswordEditText.hasFocus())
        {
            setactivated(true,false)
        }
        else
        {
            setactivated(false,true)

        }
    }

    private fun setactivated(new_password: Boolean, confirm_password: Boolean) {
        if(new_password)
        {
            setdrawable_green(binding.newPasswordEditText)
            setdrawablegray(binding.confirmPasswordEdittext)
        }
        else
        {
            setdrawablegray(binding.newPasswordEditText)
            setdrawable_green(binding.confirmPasswordEdittext)


        }



    }

    ///////
    private fun setdrawablegray(passwordVerifyEditText: EditText?) {
        if (passwordVerifyEditText != null) {
            passwordVerifyEditText.background=resources.getDrawable(R.drawable.email_grey,context?.theme)
        }

    }

    private fun setdrawable_green(emailVerifyEditText: EditText?) {
        if (emailVerifyEditText != null) {
            emailVerifyEditText.background=resources.getDrawable(R.drawable.email_green,context?.theme)
        }

    }

    override fun onClick(p0: View?) {
       when(p0?.id)
       {
           binding.backImageView.id->
           {
               dismiss()
               getDialogCB().OnChange(LoginDialog())


           }
            binding.buttonChangePassword.id->
            {
                validate_input_fields()
            }


       }
    }

    private fun validate_input_fields() {
        val new_pass=Utility.getTextET(binding.newPasswordEditText)
        val confirm_pass=Utility.getTextET(binding.confirmPasswordEdittext)

        if(new_pass.isEmpty())
        {
            binding.newPasswordEditText.error=Messages.EMPTY_FIELD
        }
        else if(confirm_pass.isEmpty()) {
            binding.confirmPasswordEdittext.error = Messages.EMPTY_FIELD
        }
        else if(new_pass.length<8)
        {
            binding.newPasswordEditText.error=Messages.PASSWORD_ERROR

        }
        else if(confirm_pass.length<8)
        {
            binding.confirmPasswordEdittext.error=Messages.PASSWORD_ERROR

        }
        else if(!new_pass.equals(confirm_pass))
        {
            binding.confirmPasswordEdittext.error=Messages.INPUT_MISMATCH


        }
        else
        {

            dismiss()
            list.clear()
            list.add(ModelCommunication(Constants.EMAIL,getViewModel().getUser().value!!.email.toString()))
            list.add(ModelCommunication(Constants.PASSWORD,new_pass))
            NetworkCall.callAPI(context!!,getService().changepassword(Utility.getJson(list)),this,true,APIConstants.CONFIRM_PASSWORD)


        }






    }

    override fun OnSuccess(endpoint: String, t: Response<ModelBaseResponse>, response: String) {
         getDialogCB().OnChange(LoginDialog())


    }

    override fun OnStatusfalse(
        endpoint: String,
        t: Response<ModelBaseResponse>,
        response: String,
        message: String
    ) {
        Utility.showErrorDialog(context!!,R.layout.dialog_error_505,Messages.ERROR_TITLE,message)
         //To change body of created functions use File | Settings | File Templates.
    }

    override fun OnError(endpoint: String, code: Int, message: String) {
        Utility.showErrorDialog(context!!,R.layout.dialog_error_505,Messages.ERROR_TITLE,message)
        //To change body of created functions use File | Settings | File Templates.
    }

    override fun OnException(endpoint: String, exception: Throwable) {
        Utility.showErrorDialog(context!!,R.layout.dialog_error,Messages.ERROR_TITLE,exception.localizedMessage!!)
        //To change body of created functions use File | Settings | File Templates.
    }

    override fun OnNetworkError(endpoint: String, message: String) {
        Utility.showNErrorDialog(context!!)
         //To change body of created functions use File | Settings | File Templates.
    }
}