package com.senarios.customer.dialogfragments

import android.content.Intent
import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.google.android.gms.maps.model.LatLng
import com.senarios.rylad.BaseDialogeFragment
import com.senarios.customer.R
import com.senarios.customer.activities.MapActivity
import com.senarios.customer.costants.Codes
import com.senarios.customer.costants.Constants
import com.senarios.customer.costants.Messages
import com.senarios.customer.databinding.FragmentAddNewAddressBinding
import com.senarios.customer.models.Address
import com.senarios.customer.models.ModelBaseResponse
import com.senarios.customer.models.ModelCommunication
import com.senarios.customer.retrofit.APIConstants
import com.senarios.customer.retrofit.ApiResponse
import com.senarios.customer.retrofit.NetworkCall
import com.senarios.customer.utility.Utility
import com.senarios.customer.utility.Utility.Companion.setErrorET
import retrofit2.Response

/**/
/*dialog which manages everything related to adding address*/
class AddAddressDialog : BaseDialogeFragment() , TextView.OnEditorActionListener,View.OnFocusChangeListener,View.OnClickListener, ApiResponse,Observer<Address> {
    private lateinit var binding:FragmentAddNewAddressBinding
    private val types= mutableListOf("home","work","other")
    private var type="home"
    private lateinit var address: Address
    private lateinit var latLng: LatLng

    override fun init() {
        binding.home.setOnClickListener(this)
        binding.work.setOnClickListener(this)
        binding.other.setOnClickListener(this)
        binding.backImageView.setOnClickListener(this)
        binding.tvAdd.setOnClickListener(this)
        binding.address.setOnClickListener(this)




        binding.otherTitle.onFocusChangeListener = this
        binding.address.onFocusChangeListener = this
        binding.manualAddress.onFocusChangeListener = this


        //binding.address.requestFocus()
       // setActive(false,true,false,false)
        setActiveButton(true,false,false)

        editFlow()

    }

    private fun editFlow() {
        getViewModel().getAddress().observe(this,this)
    }

    override fun getFragmentView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
   val view=inflater.inflate(R.layout.fragment_add_new_address,container,false)
        binding=DataBindingUtil.bind(view)!!
        return  view
    }



    override fun OnBackPressed() {
        dismiss()
    }



    override fun onClick(v: View?) {
        when(v?.id){
            binding.home.id->{
                type=types[0]
                setActiveButton(true,false,false)
            }
            binding.work.id->{
                type=types[1]
                setActiveButton(false,true,false)
            }
            binding.other.id->{
                type=types[2]
                setActiveButton(false,false,true)
            }
            binding.backImageView.id->{
                dismiss()
            }
            binding.tvAdd.id->{
                validateInputs()
            }
            binding.address.id->{
                startActivityForResult(Intent(this.context,MapActivity::class.java),Codes.LATLNG_REQUEST_CODE)
            }

        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode==Codes.LATLNG_REQUEST_CODE&&data!=null){
            latLng=data.getParcelableExtra(Constants.LATLNG_DATA)!!
            binding.address.setText(Utility.getAddress(this.context,latLng = latLng))
        }
    }

    private fun validateInputs() {
        val otherTitle=Utility.getTextET(binding.otherTitle)
        val address=Utility.getTextET(binding.address)
        val street=Utility.getTextET(binding.manualAddress)
        if (binding.otherTitle.isVisible && otherTitle.isEmpty()){
            setErrorET(binding.otherTitle)
        }
        else if(address.isEmpty()){
            setErrorET(binding.address)
        }
        else if(street.isEmpty()){
            setErrorET(binding.manualAddress)
        }
        else{

            val list= mutableListOf<ModelCommunication>()
            if (binding.otherTitle.isVisible){
                list.add(ModelCommunication("other_title",otherTitle))
            }
            else{
                list.add(ModelCommunication("other_title",""))
            }
            if(this::address.isInitialized && this.address.id!=-1){
                list.add(ModelCommunication("id",this.address.id.toString()))
                list.add(ModelCommunication("lat",this.address.lat))
                list.add(ModelCommunication("lon",this.address.lon))
            }
            else{
                list.add(ModelCommunication("lat",latLng.latitude.toString()))
                list.add(ModelCommunication("lon",latLng.longitude.toString()))
            }
            list.add(ModelCommunication("user_id",getViewModel().getLoggedUser().value!!.id.toString()))
            list.add(ModelCommunication("address_type",type))
            list.add(ModelCommunication("address",address))
            list.add(ModelCommunication("manual_address",street))
            NetworkCall.callAPI(context!!,getService().addAddress(APIConstants.BEARER +getViewModel().getLoggedUser().value!!.token,Utility.getJson(list)),this,true,APIConstants.ADD_ADDRESS)
            //Utility.showToast(context!!,"message")


        }


    }





    override fun onFocusChange(v: View?, hasFocus: Boolean) {
        if (binding.otherTitle.hasFocus()){
            setActive(true,false,false,false)
        }
        else if(binding.address.hasFocus()){
            setActive(false,true,false,false)
        }
        else if(binding.manualAddress.hasFocus()){
            setActive(false,false,true,false)
        }


    }

    override fun OnSuccess(endpoint:String,t: Response<ModelBaseResponse>, response: String) {
        try{
            getViewModel().setAddress(
                Address(-1,0,"","",""
                    ,"","","","","","","","","","",""))
            getDialogCB().OnTrigger()
            dismiss()
        }
        catch (e:Exception){
            OnException(endpoint,e)
        }
    }

    override fun OnStatusfalse(endpoint:String,t: Response<ModelBaseResponse>, response: String, message: String) {
        Utility.showErrorDialog(context!!,R.layout.dialog_error_505,
            Messages.ERROR_TITLE,message)
     }

    override fun OnError(endpoint:String,code: Int, message: String) {
        Utility.showErrorDialog(context!!,R.layout.dialog_error_505,
            Messages.ERROR_TITLE,message)
    }


    override fun OnException(endpoint:String,exception: Throwable) {
        Utility.showErrorDialog(context!!,R.layout.dialog_error, Messages.ERROR_TITLE,exception.localizedMessage!!)
    }

    override fun OnNetworkError(endpoint:String,message: String) {
        Utility.showNErrorDialog(context!!)
    }
    fun setActive(title: Boolean, address: Boolean, street: Boolean, town: Boolean){
        if (title){
            setGreenDrawable(binding.otherTitle)
            setGrayDrawable(binding.address)
            setGrayDrawable(binding.manualAddress)


        }
        else if (address){
            setGrayDrawable(binding.otherTitle)
            setGreenDrawable(binding.address)
            setGrayDrawable(binding.manualAddress)
        }
        else if (street){
            setGrayDrawable(binding.otherTitle)
            setGrayDrawable(binding.address)
            setGreenDrawable(binding.manualAddress)
        }
        else if (town){
            setGrayDrawable(binding.otherTitle)
            setGrayDrawable(binding.address)
            setGrayDrawable(binding.manualAddress)
        }


    }
    fun setActiveButton(home: Boolean, work: Boolean, other: Boolean){
        if (home){
            binding.otherTitle.visibility=View.GONE
            setGreenButton(R.drawable.homegreen,binding.homeImg,binding.homeTxt)
            setGrayButton(R.drawable.work,binding.workImg,binding.workTxt)
            setGrayButton(R.drawable.other,binding.otherImg,binding.otherTxt)

        }
        else if (work){
            binding.otherTitle.visibility=View.GONE
            setGrayButton(R.drawable.home,binding.homeImg,binding.homeTxt)
            setGreenButton(R.drawable.workgreen,binding.workImg,binding.workTxt)
            setGrayButton(R.drawable.other,binding.otherImg,binding.otherTxt)
        }
        else if (other){
            binding.otherTitle.visibility=View.VISIBLE
            binding.otherTitle.requestFocus()
            setGrayButton(R.drawable.home,binding.homeImg,binding.homeTxt)
            setGrayButton(R.drawable.work,binding.workImg,binding.workTxt)
            setGreenButton(R.drawable.othergreen,binding.otherImg,binding.otherTxt)
        }


    }

    fun setGrayDrawable(editText: EditText){
        editText.background=resources.getDrawable(R.drawable.email_grey,context?.theme)

    }
    fun setGreenDrawable(editText: EditText){
        editText.background=resources.getDrawable(R.drawable.email_green,context?.theme)
    }
    fun setGrayButton(id:Int,image: ImageView,textView: TextView){
        image.setImageResource(id)
        textView.setTextColor(resources.getColor(R.color.black))


    }
    fun setGreenButton(id:Int,image: ImageView,textView: TextView){
        image.setImageResource(id)
        textView.setTextColor(resources.getColor(R.color.colorPrimary))
    }

    override fun onChanged(t: Address?) {
        if (t?.id!=-1){
            address=t!!
            binding.data=address
            if (address.address_type.equals(types[0],true)){
                setActiveButton(true,false ,false)
            }
            else if (address.address_type.equals(types[1],true)){
                setActiveButton(false,true ,false)
            }
            else if (address.address_type.equals(types[2],true)){
                setActiveButton(false,false ,true)
            }
        }
    }

    override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean {
        if (actionId==EditorInfo.IME_ACTION_DONE){
            validateInputs()
            return true
        }
        return true
    }

}