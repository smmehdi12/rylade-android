package com.senarios.customer.dialogfragments

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import com.google.android.gms.auth.api.phone.SmsRetriever
import com.google.android.gms.tasks.OnFailureListener
import com.google.android.gms.tasks.OnSuccessListener
import com.google.android.gms.tasks.Task
import com.senarios.customer.R
import com.senarios.customer.activities.MainActivity
import com.senarios.customer.costants.EventBusTags.Companion.OTP_RECEIVER_SMS
import com.senarios.customer.costants.Messages
import com.senarios.customer.costants.SPConstants
import com.senarios.customer.databinding.FragmentOtpBinding
import com.senarios.customer.models.ModelBaseResponse
import com.senarios.customer.models.ModelCommunication
import com.senarios.customer.models.User
import com.senarios.customer.retrofit.APIConstants
import com.senarios.customer.retrofit.ApiResponse
import com.senarios.customer.retrofit.NetworkCall
import com.senarios.customer.utility.Utility
import com.senarios.rylad.BaseDialogeFragment
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import retrofit2.Response


class VerifyOTPDialog :  BaseDialogeFragment(),OnSuccessListener<Void>,OnFailureListener,TextView.OnEditorActionListener,View.OnClickListener,TextWatcher,View.OnFocusChangeListener,ApiResponse{
    private lateinit var binding:FragmentOtpBinding

    override fun getFragmentView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view=inflater.inflate(R.layout.fragment_otp,container,false)
        binding=DataBindingUtil.bind(view)!!
        return view
    }

   override fun init(){
       isCancelable=false

       EventBus.getDefault().register(this);
       binding.otpTextView.setText("Enter the 4 digit code sent to you at "+getViewModel().getUser().value!!.phone_number)
       binding.backImageView.setOnClickListener(this)


       binding.otpEditText.requestFocus()

        binding.otpEditText.addTextChangedListener(this)
        binding.otpEditText2.addTextChangedListener(this)
        binding.otpEditText3.addTextChangedListener(this)
        binding.otpEditText4.addTextChangedListener(this)

       binding.btnVerify.setOnClickListener(this)
       binding.otpEditText4.setOnEditorActionListener(this)

       val client = SmsRetriever.getClient(requireContext())

       val task: Task<Void> = SmsRetriever.getClient(requireContext()).startSmsUserConsent(null)

       task.addOnSuccessListener(this)
       task.addOnFailureListener(this)



    }

    override fun OnBackPressed() {


    }


    override fun onClick(v: View?) {
        when(v?.id){
            binding.backImageView.id->{
                dismiss()
//                getDialogCB().OnChange(SignupDialog())
            }
            binding.btnVerify.id->{
                validateInputs()
            }

        }

    }

    private fun validateInputs() {
        val otp=getViewModel().getUser().value?.otp.toString()
        val one=Utility.getTextET(binding.otpEditText)
        val two=Utility.getTextET(binding.otpEditText2)
        val three=Utility.getTextET(binding.otpEditText3)
        val four=Utility.getTextET(binding.otpEditText4)
        val entered_otp=one+two+three+four
        if (one.isEmpty()){
            binding.otpEditText.error=Messages.EMPTY_FIELD
        }
        else if (two.isEmpty()){
            binding.otpEditText2.error=Messages.EMPTY_FIELD
        }
        else if (three.isEmpty()){
            binding.otpEditText3.error=Messages.EMPTY_FIELD
        }
        else if (four.isEmpty()){
            binding.otpEditText4.error=Messages.EMPTY_FIELD
        }
        else if (!entered_otp.equals(otp,true)){
            showToast(context!!,"OTP Doesn't match!")
        }
        else{

            val list= mutableListOf<ModelCommunication>()
            list.add(ModelCommunication("user_id",getViewModel().getUser().value?.id!!.toString()))
            NetworkCall.callAPI(context!!,getService().verifyUser(Utility.getJson(list)),this,true,APIConstants.VERIFY_OTP)
        }

    }

    override fun afterTextChanged(s: Editable?) {
            if (s!!.isNotEmpty()) {
                if (Utility.getTextET(binding.otpEditText).length == 1 && binding.otpEditText.hasFocus()) {
                    binding.otpEditText2.requestFocus()
                } else if (Utility.getTextET(binding.otpEditText2).length == 1 && binding.otpEditText2.hasFocus()) {
                    binding.otpEditText3.requestFocus()
                } else if (Utility.getTextET(binding.otpEditText3).length == 1&& binding.otpEditText3.hasFocus()) {
                    binding.otpEditText4.requestFocus()
                } else if (Utility.getTextET(binding.otpEditText4).length == 1 && binding.otpEditText4.hasFocus()) {
                    showHideSoftKeyboard()
                }
            }
            else if (s.isEmpty()) {
                if (Utility.getTextET(binding.otpEditText).isEmpty() && binding.otpEditText.hasFocus()) {
                    showHideSoftKeyboard()
                } else if (Utility.getTextET(binding.otpEditText2).length == 0&& binding.otpEditText2.hasFocus()) {
                    binding.otpEditText.requestFocus()
                } else if (Utility.getTextET(binding.otpEditText3).length == 0&& binding.otpEditText3.hasFocus()) {
                    binding.otpEditText2.requestFocus()
                } else if (Utility.getTextET(binding.otpEditText4).length == 0&& binding.otpEditText4.hasFocus()) {
                    binding.otpEditText3.requestFocus()
                }
            }

    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {



    }

    override fun onFocusChange(v: View?, hasFocus: Boolean) {

    }

    override fun OnSuccess(endpoint:String,t: Response<ModelBaseResponse>, response: String) {
        try{
            val user = Utility.getgson().fromJson<User>(response, User::class.java)
            if(Utility.getPreferences(getReference(),SPConstants.IS_FORGET_PASSWORD,false) as Boolean) {
                getViewModel().setUser(user)
                getDialogCB().OnChange(ChangePasswordDialog())
            }
            else if (Utility.getPreferences(getReference(),SPConstants.IS_FACEBOOK_VERIFY,false) as Boolean){
                setLogin(response)
            }
            else{
                setLogin(response)
            }
            dismiss()

        }
        catch (e:Exception){
            OnException(endpoint,e)
        }
    }

    private fun setLogin(user:String) {
        Utility.setPreferences(getReference(),SPConstants.USER,user)
        if (activity!=null) {
            if ((activity is MainActivity)) {
                val activity = activity as MainActivity
                activity.getbinding().navView.menu.clear()
                activity.getbinding().navView.inflateMenu(R.menu.main_drawer_menu)
                Utility.removeSpValue(getReference(),SPConstants.IS_FACEBOOK_VERIFY)
            }
        }
    }

    override fun OnStatusfalse(endpoint:String,t: Response<ModelBaseResponse>, response: String, message: String) {
        Utility.showErrorDialog(context!!,R.layout.dialog_error_505,Messages.ERROR_TITLE,message)
    }

    override fun OnError(endpoint:String,code: Int, message: String) {
        Utility.showErrorDialog(context!!,R.layout.dialog_error_505,Messages.ERROR_TITLE,message)
    }


    override fun OnException(endpoint:String,exception: Throwable) {
        Utility.showErrorDialog(context!!,R.layout.dialog_error,Messages.ERROR_TITLE,exception.localizedMessage!!)
    }

    override fun OnNetworkError(endpoint:String,message: String) {
        Utility.showNErrorDialog(context!!)
    }

    @Subscribe(sticky = true,threadMode = ThreadMode.MAIN)
    fun getOtp(model:ModelCommunication){
        Log.v(javaClass.name,model.value)
        if (model.key.equals(OTP_RECEIVER_SMS,true)&& model.value.length==4){
        binding.otpEditText.setText(model.value.get(0).toString())
        binding.otpEditText2.setText(model.value.get(1).toString())
        binding.otpEditText3.setText(model.value.get(2).toString())
        binding.otpEditText4.setText(model.value.get(3).toString())
        }
        else{
            EventBus.getDefault().postSticky(model.value)
        }

    }


    override fun onDestroy() {
        super.onDestroy()
        EventBus.getDefault().unregister(this);
    }


    override fun onStart() {
        super.onStart()
    }

    override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean {
        if (actionId== EditorInfo.IME_ACTION_DONE){
            validateInputs()
            return true
        }
        return true
    }

    override fun onSuccess(p0: Void?) {
        Utility.showLog("Success: Waiting for sms")
    }

    override fun onFailure(p0: java.lang.Exception) {
        Utility.showLog("Failed "+p0.localizedMessage)
    }
}