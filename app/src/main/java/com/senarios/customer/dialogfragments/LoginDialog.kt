package com.senarios.customer.dialogfragments


import android.os.Bundle
import android.util.Patterns
import android.view.KeyEvent
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.google.gson.Gson
import com.muddzdev.styleabletoast.StyleableToast
import com.senarios.rylad.BaseDialogeFragment
import com.senarios.customer.R
import com.senarios.customer.activities.MainActivity
import com.senarios.customer.costants.Constants
import com.senarios.customer.costants.Messages
import com.senarios.customer.costants.Messages.Companion.UNVERIFIED_MESSAGE
import com.senarios.customer.costants.Messages.Companion.UNVERIFIED_TITLE
import com.senarios.customer.costants.SPConstants
import com.senarios.customer.costants.SPConstants.Companion.USER
import com.senarios.customer.databinding.FragmentLoginBinding
import com.senarios.customer.models.ModelBaseResponse
import com.senarios.customer.models.ModelCommunication
import com.senarios.customer.models.User
import com.senarios.customer.retrofit.APIConstants
import com.senarios.customer.retrofit.ApiResponse
import com.senarios.customer.retrofit.NetworkCall
import com.senarios.customer.utility.Utility
import retrofit2.Response

/**
 * A simple [Fragment] subclass.
 */
class LoginDialog : BaseDialogeFragment(), Observer<String>, TextView.OnEditorActionListener, View.OnClickListener, View.OnFocusChangeListener, ApiResponse {
    private lateinit var binding: FragmentLoginBinding
    private val list = mutableListOf<ModelCommunication>()

    override fun init() {
        binding.button.setOnClickListener(this)
        binding.emailEditText.onFocusChangeListener = this
        binding.passwordEditText.onFocusChangeListener = this
        binding.passwordEditText.requestFocus()
        binding.backImageView.setOnClickListener(this)
        binding.passwordEditText.setOnEditorActionListener(this)
        binding.textViewForegtPassword.setOnClickListener(this)

        getViewModel().getEmail().observe(this,this)
    }


    override fun getFragmentView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(R.layout.fragment_login, container, false)
        binding = DataBindingUtil.bind(view)!!
        return view
    }


    override fun OnBackPressed() {

    }

    override fun onFocusChange(v: View?, hasFocus: Boolean) {
        if (binding.emailEditText.hasFocus()) {
            setActive(true, false)
        } else if (binding.passwordEditText.hasFocus()) {
            setActive(false, true)
        }

    }


    override fun onClick(v: View?) {
        when (v?.id) {
            binding.backImageView.id -> {
                dismiss()
                getDialogCB().OnChange(LoginSignupDialog())
            }
            binding.button.id -> {
                validateInputs()
            }
            binding.textViewForegtPassword.id -> {

                dismiss()
                getDialogCB().OnChange(ForgetPasswordDialog())
            }
        }

    }

    private fun validateInputs() {

        val email = Utility.getTextET(binding.emailEditText)
        val password = Utility.getTextET(binding.passwordEditText)
        if (email.isEmpty()) {
            binding.emailEditText.error = Messages.EMPTY_FIELD
        } else if (password.isEmpty()) {
            binding.passwordEditText.error = Messages.EMPTY_FIELD
        } else if (password.length < 8) {
            binding.passwordEditText.error = Messages.PASSWORD_ERROR
        } else if (!Patterns.EMAIL_ADDRESS.matcher(Utility.getTextET(binding.emailEditText)).matches()) {
            binding.emailEditText.error = Messages.INVALID_EMAIL
        } else {


            list.clear()
            list.add(ModelCommunication(Constants.EMAIL, email))
            list.add(ModelCommunication(Constants.PASSWORD, password))
            list.add(ModelCommunication(SPConstants.FCM, Utility.getFCM(this.context)))
            NetworkCall.callAPI(context!!, getService().login(Utility.getJson(list)), this, true, APIConstants.LOGIN)

        }
    }

    override fun OnSuccess(endpoint: String, t: Response<ModelBaseResponse>, response: String) {
        try {
            dismiss()
            val data = Gson().fromJson(response, User::class.java)
            if (endpoint.equals(APIConstants.SENDOTP, true)) {
                getViewModel().setUser(data)
                Utility.showNotification(getReference(),"OTP",data.otp)
                dismiss()
                getDialogCB().OnChange(VerifyOTPDialog())
            } else {
                if (data.is_verified) {
                    Utility.setPreferences(context!!, USER, response)
                    if (activity is MainActivity) {
                        val activity = activity as MainActivity
                        activity.getbinding().navView.menu.clear()
                        activity.getbinding().navView.inflateMenu(R.menu.main_drawer_menu)
                    } else {
                        getDialogCB().OnTrigger()
                    }
                    Utility.removeEvents()
                    Utility.removeStringEvents()
                    Utility.getPreference(getReference()).edit().remove(SPConstants.IS_FORGET_PASSWORD).apply()
                    StyleableToast.makeText(requireContext(),"Logged In Successfully!",R.style.mytoast).show()
                    dismiss()
                } else {
                    getViewModel().setUser(data)
                    Utility.getAlertDialoge(context!!, UNVERIFIED_TITLE, UNVERIFIED_MESSAGE).setPositiveButton("Ok") { p0, p1 ->
                        p0?.dismiss()
                        list.clear()
                        list.add(ModelCommunication("email", getViewModel().getUser().value!!.email))
                        NetworkCall.callAPI(getReference(), getService().sendOTP(Utility.getJson(list)), this, true, APIConstants.SENDOTP)
                    }.setNegativeButton("Nah, I'm good") { p0, p1 ->
                            dismiss()

                        }.show()
                }
            }
        } catch (e: Exception) {
            OnException(endpoint, e)
        }

    }

    override fun OnStatusfalse(endpoint: String, t: Response<ModelBaseResponse>, response: String, message: String) {
        Utility.showErrorDialog(context!!, R.layout.dialog_error_505, Messages.ERROR_TITLE, message)
    }

    override fun OnError(endpoint: String, code: Int, message: String) {
        Utility.showErrorDialog(context!!, R.layout.dialog_error_505, Messages.ERROR_TITLE, message)

    }

    override fun OnException(endpoint: String, exception: Throwable) {
        Utility.showErrorDialog(context!!, R.layout.dialog_error, Messages.ERROR_TITLE, exception.localizedMessage!!)

    }

    override fun OnNetworkError(endpoint: String, message: String) {
        Utility.showNErrorDialog(context!!)
    }

    private fun setActive(email: Boolean, password: Boolean) {
        if (email) {
            setGreenDrawable(binding.emailEditText)
            setGrayDrawable(binding.passwordEditText)

        } else if (password) {
            setGrayDrawable(binding.emailEditText)
            setGreenDrawable(binding.passwordEditText)
        }


    }

    fun setGrayDrawable(editText: EditText) {
        editText.background = resources.getDrawable(R.drawable.email_grey, context?.theme)

    }

    fun setGreenDrawable(editText: EditText) {
        editText.background = resources.getDrawable(R.drawable.email_green, context?.theme)
    }

    override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean {
        if (actionId == EditorInfo.IME_ACTION_DONE) {
            validateInputs()
            return true
        }
        return true
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    override fun onChanged(t: String?) {
        if (t != null && t.length > 0) {
            binding.emailEditText.setText(t)
        }
    }

}
