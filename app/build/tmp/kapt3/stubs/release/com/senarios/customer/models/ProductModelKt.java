package com.senarios.customer.models;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 2, d1 = {"\u0000\u001e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0004\u001a\u0018\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0007\u001a\u0018\u0010\u0006\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0007\u001a5\u0010\u0007\u001a\u00020\u00012\b\u0010\u0002\u001a\u0004\u0018\u00010\u00032\b\u0010\b\u001a\u0004\u0018\u00010\t2\b\u0010\n\u001a\u0004\u0018\u00010\u00052\b\u0010\u000b\u001a\u0004\u0018\u00010\tH\u0007\u00a2\u0006\u0002\u0010\f\u00a8\u0006\r"}, d2 = {"loadProductImage", "", "view", "Landroid/view/View;", "image", "", "loadProductImageView", "setPrice", "price", "", "type", "isDiscount", "(Landroid/view/View;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Integer;)V", "app_release"})
public final class ProductModelKt {
    
    @androidx.databinding.BindingAdapter(value = {"loadProductImage"})
    public static final void loadProductImage(@org.jetbrains.annotations.NotNull()
    android.view.View view, @org.jetbrains.annotations.NotNull()
    java.lang.String image) {
    }
    
    @androidx.databinding.BindingAdapter(value = {"loadProductImageView"})
    public static final void loadProductImageView(@org.jetbrains.annotations.NotNull()
    android.view.View view, @org.jetbrains.annotations.NotNull()
    java.lang.String image) {
    }
    
    @androidx.databinding.BindingAdapter(requireAll = false, value = {"setprice", "pricetype", "isDiscount"})
    public static final void setPrice(@org.jetbrains.annotations.Nullable()
    android.view.View view, @org.jetbrains.annotations.Nullable()
    java.lang.Integer price, @org.jetbrains.annotations.Nullable()
    java.lang.String type, @org.jetbrains.annotations.Nullable()
    java.lang.Integer isDiscount) {
    }
}