package com.senarios.customer.activities;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u00a0\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0011\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0015\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\b\b&\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u00032\u00020\u0004B\u0005\u00a2\u0006\u0002\u0010\u0005J\b\u0010\u0010\u001a\u00020\u0011H\u0016J\u0010\u0010\u0012\u001a\u00020\u00112\u0006\u0010\u0013\u001a\u00020\u0014H\u0016J\b\u0010\u0015\u001a\u00020\u0011H&J\u0006\u0010\u0016\u001a\u00020\u0011J\u0006\u0010\u0017\u001a\u00020\u0011J\u0006\u0010\u0018\u001a\u00020\u0011J\u0006\u0010\u0019\u001a\u00020\u0011J\u0006\u0010\u001a\u001a\u00020\u0011J\u0006\u0010\u001b\u001a\u00020\u0011J\u0006\u0010\u001c\u001a\u00020\u0011J\u0010\u0010\u001d\u001a\u00020\u00112\u0006\u0010\b\u001a\u00020\tH\u0016J&\u0010\u001e\u001a\u0004\u0018\u00010\u001f2\b\u0010 \u001a\u0004\u0018\u00010!2\b\u0010\"\u001a\u0004\u0018\u00010#2\b\u0010$\u001a\u0004\u0018\u00010#J\u0006\u0010%\u001a\u00020\tJ\u0006\u0010&\u001a\u00020\'J\u0006\u0010(\u001a\u00020\u0011J\u0016\u0010)\u001a\u00020*2\u0006\u0010+\u001a\u00020#2\u0006\u0010,\u001a\u00020#J\u0006\u0010-\u001a\u00020.J\u0006\u0010/\u001a\u00020\rJ\u0006\u00100\u001a\u00020\u0011J\u0006\u00101\u001a\u00020\u0011J\b\u00102\u001a\u00020\u0011H\u0002J\u0006\u00103\u001a\u00020\u0011J\u0006\u00104\u001a\u00020\u0011J\u0006\u00105\u001a\u00020\u0011J\u0006\u00106\u001a\u00020\u0011J%\u00107\u001a\u0002082\b\u0010 \u001a\u0004\u0018\u00010!2\f\u00109\u001a\b\u0012\u0004\u0012\u00020#0:H\u0016\u00a2\u0006\u0002\u0010;J\b\u0010<\u001a\u00020\u0011H\u0016J\b\u0010=\u001a\u00020\u0011H$J\u0006\u0010>\u001a\u00020\u0011J\"\u0010?\u001a\u00020\u00112\u0006\u0010@\u001a\u00020\u00072\u0006\u0010A\u001a\u00020\u00072\b\u0010+\u001a\u0004\u0018\u00010BH\u0014J\u0012\u0010C\u001a\u00020\u00112\b\u0010D\u001a\u0004\u0018\u00010EH\u0014J-\u0010F\u001a\u00020\u00112\u0006\u0010@\u001a\u00020\u00072\u000e\u00109\u001a\n\u0012\u0006\b\u0001\u0012\u00020#0:2\u0006\u0010G\u001a\u00020HH\u0016\u00a2\u0006\u0002\u0010IJ\b\u0010J\u001a\u00020\u0011H&J\u0010\u0010K\u001a\u00020\u00112\u0006\u0010\n\u001a\u00020\u000bH\u0016J\u000e\u0010L\u001a\u00020\u00112\u0006\u0010M\u001a\u00020NJ9\u0010O\u001a\u00020\u00112\u0006\u0010\"\u001a\u00020#2\u0006\u0010$\u001a\u00020#2\f\u0010P\u001a\b\u0012\u0004\u0012\u00020#0:2\u0006\u0010Q\u001a\u0002082\u0006\u0010R\u001a\u00020\u0007\u00a2\u0006\u0002\u0010SJ\b\u0010T\u001a\u00020\u0011H\u0016J\u0018\u0010U\u001a\u00020\u00112\b\u0010 \u001a\u0004\u0018\u00010!2\u0006\u0010$\u001a\u00020#R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\rX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006V"}, d2 = {"Lcom/senarios/customer/activities/BaseAcivity;", "Landroidx/appcompat/app/AppCompatActivity;", "Lcom/senarios/customer/callbacks/ActivityFragment;", "Lcom/senarios/customer/dialogfragments/DialogeCallback;", "Lcom/senarios/customer/utility/Utility;", "()V", "SMS_CONSENT_REQUEST", "", "activityStates", "Lcom/senarios/customer/callbacks/ActivityStates;", "component", "Lcom/senarios/customer/di/Component;", "sharedVM", "Lcom/senarios/customer/viewmodel/SharedVM;", "smsVerificationReceiver", "Landroid/content/BroadcastReceiver;", "OnCancelPermissioDialog", "", "OnChange", "fragment", "Lcom/senarios/rylad/BaseDialogeFragment;", "OnPermissionApproved", "checkCameraPermission", "checkContactPermission", "checkLocationPermission", "checkMYPermission", "checkMediaPermission", "checkSMSPermission", "checkStoragePermission", "get", "getAlertDialoge", "Landroid/app/AlertDialog$Builder;", "context", "Landroid/content/Context;", "title", "", "message", "getCallback", "getDAO", "Lcom/senarios/customer/db/DAO;", "getMissingFieldDialog", "getPart", "Lokhttp3/MultipartBody$Part;", "data", "name", "getService", "Lcom/senarios/customer/retrofit/DataService;", "getViewModel", "handleAllPermissionResult", "handleCameraPermissionResult", "handleContactPermissionResult", "handleLocationPermissionResult", "handleMediaPermissionResult", "handleSmsPermissionResult", "handleStoragePermissionResult", "hasPermissions", "", "permissions", "", "(Landroid/content/Context;[Ljava/lang/String;)Z", "hideSoftKeyboard", "init", "keepScreenOn", "onActivityResult", "requestCode", "resultCode", "Landroid/content/Intent;", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onRequestPermissionsResult", "grantResults", "", "(I[Ljava/lang/String;[I)V", "setBinding", "setComponent", "showDialog", "dialogFragment", "Landroidx/fragment/app/DialogFragment;", "showPermissionDialog", "permission", "isSetting", "code", "(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;ZI)V", "showSoftKeyboard", "showToast", "app_release"})
public abstract class BaseAcivity extends androidx.appcompat.app.AppCompatActivity implements com.senarios.customer.callbacks.ActivityFragment, com.senarios.customer.dialogfragments.DialogeCallback, com.senarios.customer.utility.Utility {
    private com.senarios.customer.callbacks.ActivityStates activityStates;
    private com.senarios.customer.di.Component component;
    private final int SMS_CONSENT_REQUEST = 2;
    private com.senarios.customer.viewmodel.SharedVM sharedVM;
    private final android.content.BroadcastReceiver smsVerificationReceiver = null;
    private java.util.HashMap _$_findViewCache;
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.senarios.customer.viewmodel.SharedVM getViewModel() {
        return null;
    }
    
    @java.lang.Override()
    protected void onActivityResult(int requestCode, int resultCode, @org.jetbrains.annotations.Nullable()
    android.content.Intent data) {
    }
    
    public void setComponent(@org.jetbrains.annotations.NotNull()
    com.senarios.customer.di.Component component) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.senarios.customer.db.DAO getDAO() {
        return null;
    }
    
    public abstract void setBinding();
    
    protected abstract void init();
    
    public final void keepScreenOn() {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final android.app.AlertDialog.Builder getAlertDialoge(@org.jetbrains.annotations.Nullable()
    android.content.Context context, @org.jetbrains.annotations.Nullable()
    java.lang.String title, @org.jetbrains.annotations.Nullable()
    java.lang.String message) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.senarios.customer.callbacks.ActivityStates getCallback() {
        return null;
    }
    
    @java.lang.Override()
    public void get(@org.jetbrains.annotations.NotNull()
    com.senarios.customer.callbacks.ActivityStates activityStates) {
    }
    
    public final void showToast(@org.jetbrains.annotations.Nullable()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    java.lang.String message) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.senarios.customer.retrofit.DataService getService() {
        return null;
    }
    
    public final void showDialog(@org.jetbrains.annotations.NotNull()
    androidx.fragment.app.DialogFragment dialogFragment) {
    }
    
    public void hideSoftKeyboard() {
    }
    
    public void showSoftKeyboard() {
    }
    
    @java.lang.Override()
    public void OnChange(@org.jetbrains.annotations.NotNull()
    com.senarios.rylad.BaseDialogeFragment fragment) {
    }
    
    @java.lang.Override()
    public void onRequestPermissionsResult(int requestCode, @org.jetbrains.annotations.NotNull()
    java.lang.String[] permissions, @org.jetbrains.annotations.NotNull()
    int[] grantResults) {
    }
    
    public final void showPermissionDialog(@org.jetbrains.annotations.NotNull()
    java.lang.String title, @org.jetbrains.annotations.NotNull()
    java.lang.String message, @org.jetbrains.annotations.NotNull()
    java.lang.String[] permission, boolean isSetting, int code) {
    }
    
    public void OnCancelPermissioDialog() {
    }
    
    public abstract void OnPermissionApproved();
    
    public boolean hasPermissions(@org.jetbrains.annotations.Nullable()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    java.lang.String[] permissions) {
        return false;
    }
    
    public final void checkMYPermission() {
    }
    
    public final void checkSMSPermission() {
    }
    
    public final void checkContactPermission() {
    }
    
    public final void checkLocationPermission() {
    }
    
    public final void checkStoragePermission() {
    }
    
    public final void checkCameraPermission() {
    }
    
    public final void checkMediaPermission() {
    }
    
    public final void handleAllPermissionResult() {
    }
    
    public final void handleLocationPermissionResult() {
    }
    
    public final void handleMediaPermissionResult() {
    }
    
    public final void handleCameraPermissionResult() {
    }
    
    public final void handleStoragePermissionResult() {
    }
    
    public final void handleSmsPermissionResult() {
    }
    
    private final void handleContactPermissionResult() {
    }
    
    public final void getMissingFieldDialog() {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final okhttp3.MultipartBody.Part getPart(@org.jetbrains.annotations.NotNull()
    java.lang.String data, @org.jetbrains.annotations.NotNull()
    java.lang.String name) {
        return null;
    }
    
    public BaseAcivity() {
        super();
    }
}