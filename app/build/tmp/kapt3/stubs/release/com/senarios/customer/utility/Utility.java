package com.senarios.customer.utility;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\bf\u0018\u0000 \u00022\u00020\u0001:\u0001\u0002\u00a8\u0006\u0003"}, d2 = {"Lcom/senarios/customer/utility/Utility;", "", "Companion", "app_release"})
public abstract interface Utility {
    public static final com.senarios.customer.utility.Utility.Companion Companion = null;
    
    @kotlin.Suppress(names = {"RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS"})
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0082\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0011\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0018\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0003\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0016\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fJ\u0016\u0010\r\u001a\u00020\u000e2\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000f\u001a\u00020\u0010J\u0016\u0010\u0011\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u0012\u001a\u00020\u0004J\u0010\u0010\u0013\u001a\u00020\u00142\b\u0010\t\u001a\u0004\u0018\u00010\nJ\u0010\u0010\u0015\u001a\u00020\u00142\b\u0010\t\u001a\u0004\u0018\u00010\nJ\u0010\u0010\u0016\u001a\u00020\b2\b\u0010\u0017\u001a\u0004\u0018\u00010\u0004J\u001a\u0010\u0018\u001a\u0004\u0018\u00010\u00042\b\u0010\t\u001a\u0004\u0018\u00010\n2\u0006\u0010\u0019\u001a\u00020\u001aJ$\u0010\u001b\u001a\u00020\u001c2\b\u0010\t\u001a\u0004\u0018\u00010\n2\b\u0010\u001d\u001a\u0004\u0018\u00010\u00042\b\u0010\u001e\u001a\u0004\u0018\u00010\u0004J\u0006\u0010\u001f\u001a\u00020 J\u0010\u0010!\u001a\u00020\u00042\b\u0010\t\u001a\u0004\u0018\u00010\nJ\u0006\u0010\"\u001a\u00020#J\u0014\u0010$\u001a\u00020%2\f\u0010&\u001a\b\u0012\u0004\u0012\u00020(0\'J\u0016\u0010)\u001a\u0004\u0018\u00010*2\f\u0010&\u001a\b\u0012\u0004\u0012\u00020,0+J\u0014\u0010-\u001a\u00020%2\f\u0010&\u001a\b\u0012\u0004\u0012\u00020(0\'J\u001a\u0010.\u001a\u0004\u0018\u00010\u001a2\b\u0010\t\u001a\u0004\u0018\u00010\n2\u0006\u0010/\u001a\u00020\u0004J\u000e\u00100\u001a\u0002012\u0006\u00102\u001a\u000203J\u000e\u00104\u001a\u00020\u00042\u0006\u00105\u001a\u000206J\u000e\u00107\u001a\u0002082\u0006\u00109\u001a\u000208J\u0011\u0010:\u001a\b\u0012\u0004\u0012\u00020\u00040;\u00a2\u0006\u0002\u0010<J\u0010\u0010=\u001a\u00020>2\b\u0010\t\u001a\u0004\u0018\u00010\nJ\u0006\u0010?\u001a\u00020\u0006J\u0010\u0010@\u001a\u00020A2\b\u0010\t\u001a\u0004\u0018\u00010\nJ&\u0010B\u001a\u0004\u0018\u00010\u00012\b\u0010\t\u001a\u0004\u0018\u00010\n2\b\u0010C\u001a\u0004\u0018\u00010\u00042\b\u0010D\u001a\u0004\u0018\u00010\u0001J(\u0010E\u001a\u0004\u0018\u00010*2\b\u0010\t\u001a\u0004\u0018\u00010\n2\u0006\u0010F\u001a\u00020\u00102\f\u0010&\u001a\b\u0012\u0004\u0012\u00020G0+J\u0012\u0010H\u001a\u0004\u0018\u00010I2\b\u0010\t\u001a\u0004\u0018\u00010\nJ\u0006\u0010J\u001a\u00020#J\u0010\u0010K\u001a\u00020\u00042\b\u0010L\u001a\u0004\u0018\u00010MJ\u0010\u0010N\u001a\u00020O2\b\u0010\t\u001a\u0004\u0018\u00010\nJ\u000e\u0010P\u001a\u00020\u00042\u0006\u00105\u001a\u000206J\u0006\u0010Q\u001a\u00020RJ\u0010\u0010S\u001a\u00020\b2\b\u0010\t\u001a\u0004\u0018\u00010\nJ\u0010\u0010T\u001a\u00020\u00142\b\u0010\t\u001a\u0004\u0018\u00010\nJ\u0010\u0010U\u001a\u00020\u00142\b\u0010\t\u001a\u0004\u0018\u00010\nJ\u0010\u0010V\u001a\u00020\u00142\b\u0010\t\u001a\u0004\u0018\u00010\nJ#\u0010W\u001a\u00020\u00142\b\u0010\t\u001a\u0004\u0018\u00010\n2\f\u0010X\u001a\b\u0012\u0004\u0012\u00020\u00040;\u00a2\u0006\u0002\u0010YJ\u0010\u0010Z\u001a\u00020\u00142\b\u0010\t\u001a\u0004\u0018\u00010\nJ\u000e\u0010[\u001a\u00020\u00142\u0006\u0010\t\u001a\u00020\nJ\u0018\u0010\\\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\b\u0010]\u001a\u0004\u0018\u00010\u0004J\u0006\u0010^\u001a\u00020\bJ\u0018\u0010_\u001a\u00020\b2\b\u0010\t\u001a\u0004\u0018\u00010\n2\u0006\u0010C\u001a\u00020\u0004J\u0006\u0010`\u001a\u00020\bJ\u0016\u0010a\u001a\u00020\b2\u0006\u0010b\u001a\u00020\n2\u0006\u0010c\u001a\u00020IJ\u000e\u0010d\u001a\u00020\b2\u0006\u0010L\u001a\u00020MJ\u0016\u0010e\u001a\u00020\b2\u0006\u0010L\u001a\u00020M2\u0006\u0010\u001e\u001a\u00020\u0004J$\u0010f\u001a\u00020\b2\b\u0010\t\u001a\u0004\u0018\u00010\n2\b\u0010C\u001a\u0004\u0018\u00010\u00042\b\u0010D\u001a\u0004\u0018\u00010\u0001J\u0016\u0010g\u001a\u00020\b2\u0006\u0010L\u001a\u00020M2\u0006\u0010\u001e\u001a\u00020\u0004J \u0010h\u001a\u00020\b2\b\u0010\t\u001a\u0004\u0018\u00010\n2\u0006\u0010\u001d\u001a\u00020\u00042\u0006\u0010\u001e\u001a\u00020\u0004J\u0016\u0010i\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\u0006\u0010j\u001a\u00020kJ\u000e\u0010l\u001a\u00020\b2\u0006\u0010m\u001a\u00020nJ*\u0010o\u001a\u00020p2\b\u0010\t\u001a\u0004\u0018\u00010\n2\u0006\u0010q\u001a\u00020\u00102\b\u0010\u001d\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u001e\u001a\u00020\u0004J*\u0010r\u001a\u00020s2\b\u0010\t\u001a\u0004\u0018\u00010\n2\u0006\u0010q\u001a\u00020\u00102\b\u0010\u001d\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u001e\u001a\u00020\u0004J*\u0010t\u001a\u00020\b2\b\u0010\t\u001a\u0004\u0018\u00010\n2\u0006\u0010u\u001a\u00020\u00042\u0006\u0010v\u001a\u00020\u00042\b\u0010w\u001a\u0004\u0018\u00010xJ\u000e\u0010y\u001a\u00020\b2\u0006\u0010/\u001a\u00020\u0004J\u0010\u0010z\u001a\u00020p2\b\u0010\t\u001a\u0004\u0018\u00010\nJ\u0010\u0010{\u001a\u00020p2\b\u0010\t\u001a\u0004\u0018\u00010\nJ \u0010|\u001a\u00020\b2\b\u0010\t\u001a\u0004\u0018\u00010\n2\u0006\u0010u\u001a\u00020\u00042\u0006\u0010v\u001a\u00020\u0004J\u000e\u0010}\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nJ\u000e\u0010~\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nJ\u0007\u0010\u007f\u001a\u00030\u0080\u0001J\u0019\u0010\u0081\u0001\u001a\u00020\b2\b\u0010\t\u001a\u0004\u0018\u00010\n2\u0006\u0010\u001e\u001a\u00020\u0004J\u000f\u0010\u0082\u0001\u001a\u00020\u00142\u0006\u0010L\u001a\u00020MR\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0083\u0001"}, d2 = {"Lcom/senarios/customer/utility/Utility$Companion;", "", "()V", "TAG", "", "sessionInstance", "Lcom/google/android/libraries/places/api/model/AutocompleteSessionToken;", "Time_Picker_Dialog", "", "context", "Landroid/content/Context;", "timePickerDialog", "Landroid/app/TimePickerDialog$OnTimeSetListener;", "bitmapDescriptorFromVector", "Lcom/google/android/gms/maps/model/BitmapDescriptor;", "vectorResId", "", "callIntent", "number", "checkIfUserLogged", "", "checkLocationServices", "deleteAllFiles", "path", "getAddress", "latLng", "Lcom/google/android/gms/maps/model/LatLng;", "getAlertDialoge", "Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;", "title", "message", "getDB", "Lcom/senarios/customer/db/DAO;", "getFCM", "getGoogleService", "Lcom/senarios/customer/retrofit/DataService;", "getJson", "Lcom/google/gson/JsonObject;", "list", "", "Lcom/senarios/customer/models/ModelCommunication;", "getJsonArrayFromList", "Lcom/google/gson/JsonArray;", "", "Lcom/senarios/customer/models/CartModel;", "getJsonWithInt", "getLatLng", "text", "getMediaBuilder", "Lnet/alhazmy13/mediapicker/Image/ImagePicker$Builder;", "activity", "Landroid/app/Activity;", "getNotloggedToken", "sharedVM", "Lcom/senarios/customer/viewmodel/SharedVM;", "getObservable", "Lio/reactivex/Completable;", "completeable", "getPermissions", "", "()[Ljava/lang/String;", "getPlacesClient", "Lcom/google/android/libraries/places/api/net/PlacesClient;", "getPlacesSession", "getPreference", "Landroid/content/SharedPreferences;", "getPreferences", "key", "object", "getReviews", "order_id", "Lcom/senarios/customer/models/ProductModel;", "getSelectedAddress", "Lcom/senarios/customer/models/Address;", "getService", "getTextET", "editText", "Landroid/widget/EditText;", "getUser", "Lcom/senarios/customer/models/User;", "getUserToken", "getgson", "Lcom/google/gson/Gson;", "goToSettings", "hasContactPermission", "hasLocationPermission", "hasMediaPermission", "hasPermissions", "permissions", "(Landroid/content/Context;[Ljava/lang/String;)Z", "hasSmSPermission", "isNetworkConnected", "loadFullScreenImageView", "uri", "removeEvents", "removeSpValue", "removeStringEvents", "setDefaultAddress", "mContext", "mAddress", "setErrorET", "setErrorOnET", "setPreferences", "setRErrorET", "showAlerter", "showDatePickerDialog", "datepickercallback", "Landroid/app/DatePickerDialog$OnDateSetListener;", "showELog", "e", "", "showErrorDialog", "Landroidx/appcompat/app/AlertDialog;", "layour_ID", "showErrorDialogBuilder", "Landroidx/appcompat/app/AlertDialog$Builder;", "showFirebaseNotification", "tl", "tex", "intent", "Landroid/content/Intent;", "showLog", "showNErrorDialog", "showNErrorDialogwithFinish", "showNotification", "showOrderNotification", "showParcelOrder", "showRating", "Lcom/stepstone/apprating/AppRatingDialog$Builder;", "showToast", "validateET", "app_release"})
    public static final class Companion {
        private static com.google.android.libraries.places.api.model.AutocompleteSessionToken sessionInstance;
        private static final java.lang.String TAG = "UTL";
        
        public final void showToast(@org.jetbrains.annotations.Nullable()
        android.content.Context context, @org.jetbrains.annotations.NotNull()
        java.lang.String message) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.google.android.material.dialog.MaterialAlertDialogBuilder getAlertDialoge(@org.jetbrains.annotations.Nullable()
        android.content.Context context, @org.jetbrains.annotations.Nullable()
        java.lang.String title, @org.jetbrains.annotations.Nullable()
        java.lang.String message) {
            return null;
        }
        
        public final boolean validateET(@org.jetbrains.annotations.NotNull()
        android.widget.EditText editText) {
            return false;
        }
        
        public final void setErrorET(@org.jetbrains.annotations.NotNull()
        android.widget.EditText editText) {
        }
        
        public final void setRErrorET(@org.jetbrains.annotations.NotNull()
        android.widget.EditText editText, @org.jetbrains.annotations.NotNull()
        java.lang.String message) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getTextET(@org.jetbrains.annotations.Nullable()
        android.widget.EditText editText) {
            return null;
        }
        
        public final boolean isNetworkConnected(@org.jetbrains.annotations.NotNull()
        android.content.Context context) {
            return false;
        }
        
        public final void setErrorOnET(@org.jetbrains.annotations.NotNull()
        android.widget.EditText editText, @org.jetbrains.annotations.NotNull()
        java.lang.String message) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.google.gson.JsonObject getJson(@org.jetbrains.annotations.NotNull()
        java.util.List<com.senarios.customer.models.ModelCommunication> list) {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.google.gson.JsonObject getJsonWithInt(@org.jetbrains.annotations.NotNull()
        java.util.List<com.senarios.customer.models.ModelCommunication> list) {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final androidx.appcompat.app.AlertDialog showErrorDialog(@org.jetbrains.annotations.Nullable()
        android.content.Context context, int layour_ID, @org.jetbrains.annotations.Nullable()
        java.lang.String title, @org.jetbrains.annotations.NotNull()
        java.lang.String message) {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final androidx.appcompat.app.AlertDialog showNErrorDialog(@org.jetbrains.annotations.Nullable()
        android.content.Context context) {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final androidx.appcompat.app.AlertDialog.Builder showErrorDialogBuilder(@org.jetbrains.annotations.Nullable()
        android.content.Context context, int layour_ID, @org.jetbrains.annotations.Nullable()
        java.lang.String title, @org.jetbrains.annotations.NotNull()
        java.lang.String message) {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final androidx.appcompat.app.AlertDialog showNErrorDialogwithFinish(@org.jetbrains.annotations.Nullable()
        android.content.Context context) {
            return null;
        }
        
        public final void setPreferences(@org.jetbrains.annotations.Nullable()
        android.content.Context context, @org.jetbrains.annotations.Nullable()
        java.lang.String key, @org.jetbrains.annotations.Nullable()
        java.lang.Object object) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Object getPreferences(@org.jetbrains.annotations.Nullable()
        android.content.Context context, @org.jetbrains.annotations.Nullable()
        java.lang.String key, @org.jetbrains.annotations.Nullable()
        java.lang.Object object) {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.google.gson.Gson getgson() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getUserToken(@org.jetbrains.annotations.NotNull()
        com.senarios.customer.viewmodel.SharedVM sharedVM) {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getNotloggedToken(@org.jetbrains.annotations.NotNull()
        com.senarios.customer.viewmodel.SharedVM sharedVM) {
            return null;
        }
        
        public final void removeEvents() {
        }
        
        public final void removeStringEvents() {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final net.alhazmy13.mediapicker.Image.ImagePicker.Builder getMediaBuilder(@org.jetbrains.annotations.NotNull()
        android.app.Activity activity) {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String[] getPermissions() {
            return null;
        }
        
        public final boolean hasPermissions(@org.jetbrains.annotations.Nullable()
        android.content.Context context, @org.jetbrains.annotations.NotNull()
        java.lang.String[] permissions) {
            return false;
        }
        
        public final void goToSettings(@org.jetbrains.annotations.Nullable()
        android.content.Context context) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final android.content.SharedPreferences getPreference(@org.jetbrains.annotations.Nullable()
        android.content.Context context) {
            return null;
        }
        
        public final boolean checkIfUserLogged(@org.jetbrains.annotations.Nullable()
        android.content.Context context) {
            return false;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.senarios.customer.retrofit.DataService getService() {
            return null;
        }
        
        public final boolean hasLocationPermission(@org.jetbrains.annotations.Nullable()
        android.content.Context context) {
            return false;
        }
        
        public final boolean hasMediaPermission(@org.jetbrains.annotations.Nullable()
        android.content.Context context) {
            return false;
        }
        
        public final boolean hasSmSPermission(@org.jetbrains.annotations.Nullable()
        android.content.Context context) {
            return false;
        }
        
        public final boolean hasContactPermission(@org.jetbrains.annotations.Nullable()
        android.content.Context context) {
            return false;
        }
        
        public final boolean checkLocationServices(@org.jetbrains.annotations.Nullable()
        android.content.Context context) {
            return false;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.senarios.customer.models.User getUser(@org.jetbrains.annotations.Nullable()
        android.content.Context context) {
            return null;
        }
        
        public final void showFirebaseNotification(@org.jetbrains.annotations.Nullable()
        android.content.Context context, @org.jetbrains.annotations.NotNull()
        java.lang.String tl, @org.jetbrains.annotations.NotNull()
        java.lang.String tex, @org.jetbrains.annotations.Nullable()
        android.content.Intent intent) {
        }
        
        public final void showNotification(@org.jetbrains.annotations.Nullable()
        android.content.Context context, @org.jetbrains.annotations.NotNull()
        java.lang.String tl, @org.jetbrains.annotations.NotNull()
        java.lang.String tex) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.google.android.libraries.places.api.model.AutocompleteSessionToken getPlacesSession() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.google.android.libraries.places.api.net.PlacesClient getPlacesClient(@org.jetbrains.annotations.Nullable()
        android.content.Context context) {
            return null;
        }
        
        @org.jetbrains.annotations.Nullable()
        public final com.google.android.gms.maps.model.LatLng getLatLng(@org.jetbrains.annotations.Nullable()
        android.content.Context context, @org.jetbrains.annotations.NotNull()
        java.lang.String text) {
            return null;
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getAddress(@org.jetbrains.annotations.Nullable()
        android.content.Context context, @org.jetbrains.annotations.NotNull()
        com.google.android.gms.maps.model.LatLng latLng) {
            return null;
        }
        
        public final void showDatePickerDialog(@org.jetbrains.annotations.NotNull()
        android.content.Context context, @org.jetbrains.annotations.NotNull()
        android.app.DatePickerDialog.OnDateSetListener datepickercallback) {
        }
        
        public final void Time_Picker_Dialog(@org.jetbrains.annotations.NotNull()
        android.content.Context context, @org.jetbrains.annotations.NotNull()
        android.app.TimePickerDialog.OnTimeSetListener timePickerDialog) {
        }
        
        public final void showOrderNotification(@org.jetbrains.annotations.NotNull()
        android.content.Context context) {
        }
        
        public final void showParcelOrder(@org.jetbrains.annotations.NotNull()
        android.content.Context context) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.senarios.customer.retrofit.DataService getGoogleService() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.google.android.gms.maps.model.BitmapDescriptor bitmapDescriptorFromVector(@org.jetbrains.annotations.NotNull()
        android.content.Context context, int vectorResId) {
            return null;
        }
        
        public final void loadFullScreenImageView(@org.jetbrains.annotations.NotNull()
        android.content.Context context, @org.jetbrains.annotations.Nullable()
        java.lang.String uri) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.stepstone.apprating.AppRatingDialog.Builder showRating() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getFCM(@org.jetbrains.annotations.Nullable()
        android.content.Context context) {
            return null;
        }
        
        public final void showLog(@org.jetbrains.annotations.NotNull()
        java.lang.String text) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.senarios.customer.db.DAO getDB() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final io.reactivex.Completable getObservable(@org.jetbrains.annotations.NotNull()
        io.reactivex.Completable completeable) {
            return null;
        }
        
        @org.jetbrains.annotations.Nullable()
        public final com.google.gson.JsonArray getJsonArrayFromList(@org.jetbrains.annotations.NotNull()
        java.util.List<com.senarios.customer.models.CartModel> list) {
            return null;
        }
        
        public final void showAlerter(@org.jetbrains.annotations.Nullable()
        android.content.Context context, @org.jetbrains.annotations.NotNull()
        java.lang.String title, @org.jetbrains.annotations.NotNull()
        java.lang.String message) {
        }
        
        public final void removeSpValue(@org.jetbrains.annotations.Nullable()
        android.content.Context context, @org.jetbrains.annotations.NotNull()
        java.lang.String key) {
        }
        
        public final void callIntent(@org.jetbrains.annotations.NotNull()
        android.content.Context context, @org.jetbrains.annotations.NotNull()
        java.lang.String number) {
        }
        
        public final void deleteAllFiles(@org.jetbrains.annotations.Nullable()
        java.lang.String path) {
        }
        
        public final void showELog(@org.jetbrains.annotations.NotNull()
        java.lang.Throwable e) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final com.google.gson.JsonArray getReviews(@org.jetbrains.annotations.Nullable()
        android.content.Context context, int order_id, @org.jetbrains.annotations.NotNull()
        java.util.List<com.senarios.customer.models.ProductModel> list) {
            return null;
        }
        
        @org.jetbrains.annotations.Nullable()
        public final com.senarios.customer.models.Address getSelectedAddress(@org.jetbrains.annotations.Nullable()
        android.content.Context context) {
            return null;
        }
        
        public final void setDefaultAddress(@org.jetbrains.annotations.NotNull()
        android.content.Context mContext, @org.jetbrains.annotations.NotNull()
        com.senarios.customer.models.Address mAddress) {
        }
        
        private Companion() {
            super();
        }
    }
}