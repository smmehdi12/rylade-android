package com.senarios.customer.retrofit;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u0000 \t2\u00020\u0001:\u0001\tB\u000f\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0006\u0010\u0007\u001a\u00020\bR\u000e\u0010\u0005\u001a\u00020\u0006X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\n"}, d2 = {"Lcom/senarios/customer/retrofit/Retrofit;", "", "url", "", "(Ljava/lang/String;)V", "retrofit", "Lretrofit2/Retrofit;", "getService", "Lcom/senarios/customer/retrofit/DataService;", "Companion", "app_release"})
public final class Retrofit {
    private retrofit2.Retrofit retrofit;
    private final java.lang.String url = null;
    public static final com.senarios.customer.retrofit.Retrofit.Companion Companion = null;
    
    @org.jetbrains.annotations.NotNull()
    public final com.senarios.customer.retrofit.DataService getService() {
        return null;
    }
    
    private Retrofit(java.lang.String url) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006\u00a8\u0006\u0007"}, d2 = {"Lcom/senarios/customer/retrofit/Retrofit$Companion;", "", "()V", "getinstance", "Lcom/senarios/customer/retrofit/Retrofit;", "url", "", "app_release"})
    public static final class Companion {
        
        @org.jetbrains.annotations.NotNull()
        public final synchronized com.senarios.customer.retrofit.Retrofit getinstance(@org.jetbrains.annotations.NotNull()
        java.lang.String url) {
            return null;
        }
        
        private Companion() {
            super();
        }
    }
}