package com.senarios.customer.costants;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\bf\u0018\u0000 \u00022\u00020\u0001:\u0001\u0002\u00a8\u0006\u0003"}, d2 = {"Lcom/senarios/customer/costants/SPConstants;", "", "Companion", "app_release"})
public abstract interface SPConstants {
    public static final com.senarios.customer.costants.SPConstants.Companion Companion = null;
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String SP = "Rylade";
    public static final int MODE = android.content.Context.MODE_PRIVATE;
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String USER = "user_data";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String OTP = "otpsms";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String IS_RIDE = "is_ride";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String FCM = "fcm";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String IS_FORGET_PASSWORD = "is forget password";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String IS_FACEBOOK_VERIFY = "is facebook verify";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String ADDRESS_DATA = "sp_address";
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0010\b\n\u0002\b\u0004\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000e"}, d2 = {"Lcom/senarios/customer/costants/SPConstants$Companion;", "", "()V", "ADDRESS_DATA", "", "FCM", "IS_FACEBOOK_VERIFY", "IS_FORGET_PASSWORD", "IS_RIDE", "MODE", "", "OTP", "SP", "USER", "app_release"})
    public static final class Companion {
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String SP = "Rylade";
        public static final int MODE = android.content.Context.MODE_PRIVATE;
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String USER = "user_data";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String OTP = "otpsms";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String IS_RIDE = "is_ride";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String FCM = "fcm";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String IS_FORGET_PASSWORD = "is forget password";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String IS_FACEBOOK_VERIFY = "is facebook verify";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String ADDRESS_DATA = "sp_address";
        
        private Companion() {
            super();
        }
    }
}