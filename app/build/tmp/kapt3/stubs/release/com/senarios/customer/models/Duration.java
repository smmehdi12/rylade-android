package com.senarios.customer.models;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u0006\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006R\u0016\u0010\u0002\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0016\u0010\u0004\u001a\u00020\u00058\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n\u00a8\u0006\u000b"}, d2 = {"Lcom/senarios/customer/models/Duration;", "", "text", "", "value", "", "(Ljava/lang/String;I)V", "getText", "()Ljava/lang/String;", "getValue", "()I", "app_release"})
public final class Duration {
    @org.jetbrains.annotations.NotNull()
    @com.google.gson.annotations.SerializedName(value = "text")
    private final java.lang.String text = null;
    @com.google.gson.annotations.SerializedName(value = "value")
    private final int value = 0;
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getText() {
        return null;
    }
    
    public final int getValue() {
        return 0;
    }
    
    public Duration(@org.jetbrains.annotations.NotNull()
    java.lang.String text, int value) {
        super();
    }
}