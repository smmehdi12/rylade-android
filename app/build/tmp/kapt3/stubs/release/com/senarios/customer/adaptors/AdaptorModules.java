package com.senarios.customer.adaptors;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\f\u0012\b\u0012\u00060\u0002R\u00020\u00000\u0001:\u0001\u0015B#\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006\u0012\u0006\u0010\b\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\b\u0010\u0011\u001a\u00020\u0012H\u0016J\u001c\u0010\u0013\u001a\u00020\u00142\n\u0010\u0015\u001a\u00060\u0002R\u00020\u00002\u0006\u0010\u0016\u001a\u00020\u0012H\u0016J\u001c\u0010\u0017\u001a\u00060\u0002R\u00020\u00002\u0006\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u0012H\u0016R\u0011\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0017\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u0011\u0010\b\u001a\u00020\t\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010\u00a8\u0006\u001b"}, d2 = {"Lcom/senarios/customer/adaptors/AdaptorModules;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lcom/senarios/customer/adaptors/AdaptorModules$holder;", "context", "Landroid/content/Context;", "list", "", "Lcom/senarios/customer/models/Modules;", "listener", "Lcom/senarios/customer/adaptors/RecyclerViewCallback;", "(Landroid/content/Context;Ljava/util/List;Lcom/senarios/customer/adaptors/RecyclerViewCallback;)V", "getContext", "()Landroid/content/Context;", "getList", "()Ljava/util/List;", "getListener", "()Lcom/senarios/customer/adaptors/RecyclerViewCallback;", "getItemCount", "", "onBindViewHolder", "", "holder", "position", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "app_release"})
public final class AdaptorModules extends androidx.recyclerview.widget.RecyclerView.Adapter<com.senarios.customer.adaptors.AdaptorModules.holder> {
    @org.jetbrains.annotations.NotNull()
    private final android.content.Context context = null;
    @org.jetbrains.annotations.NotNull()
    private final java.util.List<com.senarios.customer.models.Modules> list = null;
    @org.jetbrains.annotations.NotNull()
    private final com.senarios.customer.adaptors.RecyclerViewCallback listener = null;
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.senarios.customer.adaptors.AdaptorModules.holder onCreateViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup parent, int viewType) {
        return null;
    }
    
    @java.lang.Override()
    public int getItemCount() {
        return 0;
    }
    
    @java.lang.Override()
    public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
    com.senarios.customer.adaptors.AdaptorModules.holder holder, int position) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.content.Context getContext() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.senarios.customer.models.Modules> getList() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.senarios.customer.adaptors.RecyclerViewCallback getListener() {
        return null;
    }
    
    public AdaptorModules(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    java.util.List<com.senarios.customer.models.Modules> list, @org.jetbrains.annotations.NotNull()
    com.senarios.customer.adaptors.RecyclerViewCallback listener) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\b\u0086\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\u0007"}, d2 = {"Lcom/senarios/customer/adaptors/AdaptorModules$holder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "binding", "Lcom/senarios/customer/databinding/ItemMainActivityBinding;", "(Lcom/senarios/customer/adaptors/AdaptorModules;Lcom/senarios/customer/databinding/ItemMainActivityBinding;)V", "getBinding", "()Lcom/senarios/customer/databinding/ItemMainActivityBinding;", "app_release"})
    public final class holder extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
        @org.jetbrains.annotations.NotNull()
        private final com.senarios.customer.databinding.ItemMainActivityBinding binding = null;
        
        @org.jetbrains.annotations.NotNull()
        public final com.senarios.customer.databinding.ItemMainActivityBinding getBinding() {
            return null;
        }
        
        public holder(@org.jetbrains.annotations.NotNull()
        com.senarios.customer.databinding.ItemMainActivityBinding binding) {
            super(null);
        }
    }
}