package com.senarios.customer.models;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\f\u0018\u00002\u00020\u0001B-\u0012\u0016\u0010\u0002\u001a\u0012\u0012\u0004\u0012\u00020\u00040\u0003j\b\u0012\u0004\u0012\u00020\u0004`\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u00a2\u0006\u0002\u0010\nR\u001e\u0010\u0006\u001a\u00020\u00078\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000eR&\u0010\u0002\u001a\u0012\u0012\u0004\u0012\u00020\u00040\u0003j\b\u0012\u0004\u0012\u00020\u0004`\u00058\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u001e\u0010\b\u001a\u00020\t8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0011\u0010\u0012\"\u0004\b\u0013\u0010\u0014\u00a8\u0006\u0015"}, d2 = {"Lcom/senarios/customer/models/ReviewModel;", "", "list_comments", "Ljava/util/ArrayList;", "Lcom/senarios/customer/models/Comment;", "Lkotlin/collections/ArrayList;", "average_score", "", "object_val", "Lcom/senarios/customer/models/VendorModel;", "(Ljava/util/ArrayList;Ljava/lang/String;Lcom/senarios/customer/models/VendorModel;)V", "getAverage_score", "()Ljava/lang/String;", "setAverage_score", "(Ljava/lang/String;)V", "getList_comments", "()Ljava/util/ArrayList;", "getObject_val", "()Lcom/senarios/customer/models/VendorModel;", "setObject_val", "(Lcom/senarios/customer/models/VendorModel;)V", "app_release"})
public final class ReviewModel {
    @org.jetbrains.annotations.NotNull()
    @com.google.gson.annotations.SerializedName(value = "comments")
    private final java.util.ArrayList<com.senarios.customer.models.Comment> list_comments = null;
    @org.jetbrains.annotations.NotNull()
    @com.google.gson.annotations.SerializedName(value = "average")
    private java.lang.String average_score;
    @org.jetbrains.annotations.NotNull()
    @com.google.gson.annotations.SerializedName(value = "vendor")
    private com.senarios.customer.models.VendorModel object_val;
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.ArrayList<com.senarios.customer.models.Comment> getList_comments() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getAverage_score() {
        return null;
    }
    
    public final void setAverage_score(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.senarios.customer.models.VendorModel getObject_val() {
        return null;
    }
    
    public final void setObject_val(@org.jetbrains.annotations.NotNull()
    com.senarios.customer.models.VendorModel p0) {
    }
    
    public ReviewModel(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.senarios.customer.models.Comment> list_comments, @org.jetbrains.annotations.NotNull()
    java.lang.String average_score, @org.jetbrains.annotations.NotNull()
    com.senarios.customer.models.VendorModel object_val) {
        super();
    }
}