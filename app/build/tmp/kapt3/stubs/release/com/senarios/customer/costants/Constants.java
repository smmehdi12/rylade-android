package com.senarios.customer.costants;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\bf\u0018\u0000 \u00022\u00020\u0001:\u0001\u0002\u00a8\u0006\u0003"}, d2 = {"Lcom/senarios/customer/costants/Constants;", "", "Companion", "app_release"})
public abstract interface Constants {
    public static final com.senarios.customer.costants.Constants.Companion Companion = null;
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String PASSWORD = "password";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String EMAIL = "email";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String USER_ID = "user_id";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String USER_NAME = "user_name";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String ADDRESS_ID = "address_id";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String ID = "id";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String NAME = "name";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String F_NAME = "f_name";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String L_NAME = "l_name";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String PHONE = "phone_number";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String TYPE = "user_type";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String UPDATED_AT = "updated_at";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String CREATED_AT = "created_at";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String MANUAL_SENDER_ADDRESS = "manual_sender_address";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String MANUAL_RECEIVER_ADDRESS = "manual_receiever_address";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String FB_ID = "fb_id";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String MODULE_MED = "Medicine";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String MODULE_RIDE = "Ride";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String MODULE_DEL = "Delivery";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String MODULE_FAV_PRODUCT = "Favourite Products";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String AMOUNT = "amount";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String STATUS = "status";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String RIDER_ID = "rider_id";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String MEDICINE_ORDER_DATA = "medicine order data";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String ADDRESS_DATA = "address";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String LATLNG_DATA = "latlng";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String MEDICINE_QUANTITY = "quantity";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String MEDICINE_DOSE = "dose";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String MEDICINE_TYPE = "type";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String MEDICINE_INSTRUCTION = "instructions";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String MEDICINE_ADDRESS_ID = "address_id";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String PARCEL_SENDER_NAME = "senders_name";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String PARCEL_SENDER_PHONE = "senders_contact";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String PARCEL_SENDER_ADDRESS = "senders_address";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String SENDER_LAT = "senders_lat";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String SENDER_LNG = "senders_long";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String PARCEL_RECEIVER_NAME = "receivers_name";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String PARCEL_RECEIVER_PHONE = "receivers_contact";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String PARCEL_RECEIVER_ADDRESS = "receivers_address";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String PARCEL_RECEIVER_LAT = "receivers_lat";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String PARCEL_RECEIVER_LNG = "receivers_long";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String PARCEL_ITEM_NAME = "item_name";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String PARCEL_ITEM_WEIGHT = "weight";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String PARCEL_ITEM_PHOTO = "photo";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String ORIGIN = "origins";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String DESTINATION = "destinations";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String KEY = "key";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String UNIT = "units";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String IMPERIAL = "imperial";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String RIDE_PICKUP_ADDRESS = "pickup_address";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String RIDE_DROP0FF_ADDRESS = "dropoff_address";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String RIDE_DROP0FF_LAT = "dropoff_lat";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String RIDE_DROP0FF_LNG = "dropoff_lon";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String RIDE_DISTANCE = "distance_km";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String RIDE_PICKUP_LAT = "pickup_lat";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String RIDE_PICKUP_LNG = "pickup_lon";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String ORDER_ID = "order_id";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String ORDER_DATA = "order_data";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String ORDER_TYPE = "order_type";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String ORDER_STATUS_PENDING = "pending";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String ORDER_STATUS_APPROVED = "approved";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String ORDER_STATUS_PFA = "pending_for_approval";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String ORDER_STATUS_PV = "pending_vendor";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String ORDER_STATUS_RBV = "rejected_by_vendor";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String ORDER_STATUS_CANCELLED = "cancelled";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String ORDER_STATUS_ASSIGNED = "assigned";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String ORDER_STATUS_COMPLETED = "completed";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String ORDER_STATUS_WAITING = "waiting";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String RATING = "rating";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String IS_RATED = "is_rated";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String ORDER_DATE = "order_date";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String ORDER_TIME = "order_time";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String IS_VENDOR = "is_vendor";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String ESTIMATE_FARE = "estimated_fare";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String INITIAL_FARE = "initial_fare";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String CALCULATED_FARE = "calculated_fare";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String WAITING_TIME = "waiting_time";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String WAITING_COST = "waiting_cost";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String LAST_TIME = "last_time";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String TOTAL_FARE = "total_fare";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String LAST_LAT = "last_lat";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String LAST_LNG = "last_long";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String TOTAL_TIME = "total_time";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String ESTIMATE_TIME = "estimated_time";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String ESTIMATE_DISTANCE = "estimated_distance";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String USER = "rider";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String RIDE_ORDER = "ride_order";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String ORDER = "order";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String ON_SITE = "Arrived";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String START_RIDE = "Start Ride";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String END_RIDE = "End Ride";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String MAIN_CATEGORY = "main_category_id";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String VENDOR_ID = "vendor_id";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String CATEGORY_ID = "category_id";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String CATEGORY_DATA = "category_Data";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String PRODUCT_DATA = "product data";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String SALES_TAX = "sales_tax";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String PRODUCT_IMAGE = "image";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String PRODUCT_ID = "product_id";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String PRODUCT_QUANTITY = "quantity";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String PRODUCT_PRICE = "price";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String DISCOUNT_PRICE = "discounted_price";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String IS_DISCOUNT = "iss_discount";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String PRODUCT_NAME = "product_name";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String PRODUCT_CATEGORY_ID = "category_id";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String PRODUCT_COMMENT = "comment";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String PRODUCT_RATING = "review";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String PROMO = "promo_code_name";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String PROMO_DATA = "promo code model";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String SUB_TOTAL = "sub_total";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String TOTAL_AMOUNT = "total_amount";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String DELIVERY_FEES = "delivery_fees";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String DELIVERY_FEE = "delivery_fee";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String IS_PROMO = "is_promo";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String PROMO_AMOUNT = "promo_amount";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String PROMO_ID = "promo_id";
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b}\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0019\u001a\u00020\u0004X\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001a\u0010\u001bR\u000e\u0010\u001c\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001d\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001e\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001f\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010 \u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010!\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\"\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010#\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u0014\u0010$\u001a\u00020\u0004X\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b%\u0010\u001bR\u000e\u0010&\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u0014\u0010\'\u001a\u00020\u0004X\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b(\u0010\u001bR\u0014\u0010)\u001a\u00020\u0004X\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b*\u0010\u001bR\u000e\u0010+\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010,\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010-\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010.\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010/\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u00100\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u00101\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u00102\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u00103\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u00104\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u00105\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u00106\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u00107\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u00108\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u00109\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010:\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010;\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010<\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010=\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010>\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010?\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010@\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010A\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010B\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010C\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010D\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010E\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010F\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010G\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010H\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010I\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010J\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010K\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010L\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010M\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010N\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010O\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010P\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010Q\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010R\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010S\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010T\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010U\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010V\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010W\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010X\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010Y\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010Z\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010[\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\\\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010]\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010^\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010_\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010`\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010a\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010b\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010c\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010d\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010e\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010f\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010g\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010h\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010i\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010j\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010k\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010l\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010m\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010n\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010o\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010p\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010q\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010r\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010s\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010t\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010u\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010v\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010w\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010x\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010y\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010z\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010{\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010|\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010}\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010~\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u007f\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000f\u0010\u0080\u0001\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0081\u0001"}, d2 = {"Lcom/senarios/customer/costants/Constants$Companion;", "", "()V", "ADDRESS_DATA", "", "ADDRESS_ID", "AMOUNT", "CALCULATED_FARE", "CATEGORY_DATA", "CATEGORY_ID", "CREATED_AT", "DELIVERY_FEE", "DELIVERY_FEES", "DESTINATION", "DISCOUNT_PRICE", "EMAIL", "END_RIDE", "ESTIMATE_DISTANCE", "ESTIMATE_FARE", "ESTIMATE_TIME", "FB_ID", "F_NAME", "ID", "IMPERIAL", "INITIAL_FARE", "INSTANT", "getINSTANT", "()Ljava/lang/String;", "IS_DISCOUNT", "IS_PROMO", "IS_RATED", "IS_VENDOR", "KEY", "LAST_LAT", "LAST_LNG", "LAST_TIME", "LAT", "getLAT", "LATLNG_DATA", "LNG", "getLNG", "LOCATION", "getLOCATION", "L_NAME", "MAIN_CATEGORY", "MANUAL_RECEIVER_ADDRESS", "MANUAL_SENDER_ADDRESS", "MEDICINE_ADDRESS_ID", "MEDICINE_DOSE", "MEDICINE_INSTRUCTION", "MEDICINE_ORDER_DATA", "MEDICINE_QUANTITY", "MEDICINE_TYPE", "MODULE_DEL", "MODULE_FAV_PRODUCT", "MODULE_MED", "MODULE_RIDE", "NAME", "ON_SITE", "ORDER", "ORDER_DATA", "ORDER_DATE", "ORDER_ID", "ORDER_STATUS_APPROVED", "ORDER_STATUS_ASSIGNED", "ORDER_STATUS_CANCELLED", "ORDER_STATUS_COMPLETED", "ORDER_STATUS_PENDING", "ORDER_STATUS_PFA", "ORDER_STATUS_PV", "ORDER_STATUS_RBV", "ORDER_STATUS_WAITING", "ORDER_TIME", "ORDER_TYPE", "ORIGIN", "PARCEL_ITEM_NAME", "PARCEL_ITEM_PHOTO", "PARCEL_ITEM_WEIGHT", "PARCEL_RECEIVER_ADDRESS", "PARCEL_RECEIVER_LAT", "PARCEL_RECEIVER_LNG", "PARCEL_RECEIVER_NAME", "PARCEL_RECEIVER_PHONE", "PARCEL_SENDER_ADDRESS", "PARCEL_SENDER_NAME", "PARCEL_SENDER_PHONE", "PASSWORD", "PHONE", "PRODUCT_CATEGORY_ID", "PRODUCT_COMMENT", "PRODUCT_DATA", "PRODUCT_ID", "PRODUCT_IMAGE", "PRODUCT_NAME", "PRODUCT_PRICE", "PRODUCT_QUANTITY", "PRODUCT_RATING", "PROMO", "PROMO_AMOUNT", "PROMO_DATA", "PROMO_ID", "RATING", "RIDER_ID", "RIDE_DISTANCE", "RIDE_DROP0FF_ADDRESS", "RIDE_DROP0FF_LAT", "RIDE_DROP0FF_LNG", "RIDE_ORDER", "RIDE_PICKUP_ADDRESS", "RIDE_PICKUP_LAT", "RIDE_PICKUP_LNG", "SALES_TAX", "SENDER_LAT", "SENDER_LNG", "START_RIDE", "STATUS", "SUB_TOTAL", "TOTAL_AMOUNT", "TOTAL_FARE", "TOTAL_TIME", "TYPE", "UNIT", "UPDATED_AT", "USER", "USER_ID", "USER_NAME", "VENDOR_ID", "WAITING_COST", "WAITING_TIME", "app_release"})
    public static final class Companion {
        @org.jetbrains.annotations.NotNull()
        private static final java.lang.String LAT = "lat";
        @org.jetbrains.annotations.NotNull()
        private static final java.lang.String LNG = "long";
        @org.jetbrains.annotations.NotNull()
        private static final java.lang.String LOCATION = "locationobj";
        @org.jetbrains.annotations.NotNull()
        private static final java.lang.String INSTANT = "instant model";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String PASSWORD = "password";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String EMAIL = "email";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String USER_ID = "user_id";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String USER_NAME = "user_name";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String ADDRESS_ID = "address_id";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String ID = "id";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String NAME = "name";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String F_NAME = "f_name";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String L_NAME = "l_name";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String PHONE = "phone_number";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String TYPE = "user_type";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String UPDATED_AT = "updated_at";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String CREATED_AT = "created_at";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String MANUAL_SENDER_ADDRESS = "manual_sender_address";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String MANUAL_RECEIVER_ADDRESS = "manual_receiever_address";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String FB_ID = "fb_id";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String MODULE_MED = "Medicine";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String MODULE_RIDE = "Ride";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String MODULE_DEL = "Delivery";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String MODULE_FAV_PRODUCT = "Favourite Products";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String AMOUNT = "amount";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String STATUS = "status";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String RIDER_ID = "rider_id";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String MEDICINE_ORDER_DATA = "medicine order data";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String ADDRESS_DATA = "address";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String LATLNG_DATA = "latlng";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String MEDICINE_QUANTITY = "quantity";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String MEDICINE_DOSE = "dose";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String MEDICINE_TYPE = "type";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String MEDICINE_INSTRUCTION = "instructions";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String MEDICINE_ADDRESS_ID = "address_id";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String PARCEL_SENDER_NAME = "senders_name";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String PARCEL_SENDER_PHONE = "senders_contact";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String PARCEL_SENDER_ADDRESS = "senders_address";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String SENDER_LAT = "senders_lat";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String SENDER_LNG = "senders_long";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String PARCEL_RECEIVER_NAME = "receivers_name";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String PARCEL_RECEIVER_PHONE = "receivers_contact";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String PARCEL_RECEIVER_ADDRESS = "receivers_address";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String PARCEL_RECEIVER_LAT = "receivers_lat";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String PARCEL_RECEIVER_LNG = "receivers_long";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String PARCEL_ITEM_NAME = "item_name";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String PARCEL_ITEM_WEIGHT = "weight";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String PARCEL_ITEM_PHOTO = "photo";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String ORIGIN = "origins";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String DESTINATION = "destinations";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String KEY = "key";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String UNIT = "units";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String IMPERIAL = "imperial";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String RIDE_PICKUP_ADDRESS = "pickup_address";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String RIDE_DROP0FF_ADDRESS = "dropoff_address";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String RIDE_DROP0FF_LAT = "dropoff_lat";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String RIDE_DROP0FF_LNG = "dropoff_lon";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String RIDE_DISTANCE = "distance_km";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String RIDE_PICKUP_LAT = "pickup_lat";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String RIDE_PICKUP_LNG = "pickup_lon";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String ORDER_ID = "order_id";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String ORDER_DATA = "order_data";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String ORDER_TYPE = "order_type";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String ORDER_STATUS_PENDING = "pending";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String ORDER_STATUS_APPROVED = "approved";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String ORDER_STATUS_PFA = "pending_for_approval";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String ORDER_STATUS_PV = "pending_vendor";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String ORDER_STATUS_RBV = "rejected_by_vendor";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String ORDER_STATUS_CANCELLED = "cancelled";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String ORDER_STATUS_ASSIGNED = "assigned";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String ORDER_STATUS_COMPLETED = "completed";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String ORDER_STATUS_WAITING = "waiting";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String RATING = "rating";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String IS_RATED = "is_rated";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String ORDER_DATE = "order_date";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String ORDER_TIME = "order_time";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String IS_VENDOR = "is_vendor";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String ESTIMATE_FARE = "estimated_fare";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String INITIAL_FARE = "initial_fare";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String CALCULATED_FARE = "calculated_fare";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String WAITING_TIME = "waiting_time";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String WAITING_COST = "waiting_cost";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String LAST_TIME = "last_time";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String TOTAL_FARE = "total_fare";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String LAST_LAT = "last_lat";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String LAST_LNG = "last_long";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String TOTAL_TIME = "total_time";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String ESTIMATE_TIME = "estimated_time";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String ESTIMATE_DISTANCE = "estimated_distance";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String USER = "rider";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String RIDE_ORDER = "ride_order";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String ORDER = "order";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String ON_SITE = "Arrived";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String START_RIDE = "Start Ride";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String END_RIDE = "End Ride";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String MAIN_CATEGORY = "main_category_id";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String VENDOR_ID = "vendor_id";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String CATEGORY_ID = "category_id";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String CATEGORY_DATA = "category_Data";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String PRODUCT_DATA = "product data";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String SALES_TAX = "sales_tax";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String PRODUCT_IMAGE = "image";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String PRODUCT_ID = "product_id";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String PRODUCT_QUANTITY = "quantity";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String PRODUCT_PRICE = "price";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String DISCOUNT_PRICE = "discounted_price";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String IS_DISCOUNT = "iss_discount";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String PRODUCT_NAME = "product_name";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String PRODUCT_CATEGORY_ID = "category_id";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String PRODUCT_COMMENT = "comment";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String PRODUCT_RATING = "review";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String PROMO = "promo_code_name";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String PROMO_DATA = "promo code model";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String SUB_TOTAL = "sub_total";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String TOTAL_AMOUNT = "total_amount";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String DELIVERY_FEES = "delivery_fees";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String DELIVERY_FEE = "delivery_fee";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String IS_PROMO = "is_promo";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String PROMO_AMOUNT = "promo_amount";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String PROMO_ID = "promo_id";
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getLAT() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getLNG() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getLOCATION() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getINSTANT() {
            return null;
        }
        
        private Companion() {
            super();
        }
    }
}