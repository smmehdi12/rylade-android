package com.senarios.customer.models;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u00020\u0001B%\u0012\u000e\u0010\u0002\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u0003\u0012\u000e\u0010\u0005\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\u0006R\u001e\u0010\u0005\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u00038\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u001e\u0010\u0002\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u00038\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\b\u00a8\u0006\n"}, d2 = {"Lcom/senarios/customer/models/OrderListsModel;", "", "pending", "", "Lcom/senarios/customer/models/OrderModel;", "completed", "(Ljava/util/List;Ljava/util/List;)V", "getCompleted", "()Ljava/util/List;", "getPending", "app_release"})
public final class OrderListsModel {
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.SerializedName(value = "pending")
    private final java.util.List<com.senarios.customer.models.OrderModel> pending = null;
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.SerializedName(value = "completed")
    private final java.util.List<com.senarios.customer.models.OrderModel> completed = null;
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<com.senarios.customer.models.OrderModel> getPending() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<com.senarios.customer.models.OrderModel> getCompleted() {
        return null;
    }
    
    public OrderListsModel(@org.jetbrains.annotations.Nullable()
    java.util.List<com.senarios.customer.models.OrderModel> pending, @org.jetbrains.annotations.Nullable()
    java.util.List<com.senarios.customer.models.OrderModel> completed) {
        super();
    }
}