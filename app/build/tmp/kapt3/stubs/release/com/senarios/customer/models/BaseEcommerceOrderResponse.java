package com.senarios.customer.models;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\n\u0018\u00002\u00020\u0001B\u001b\u0012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007R\u001e\u0010\u0005\u001a\u00020\u00068\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\b\u0010\t\"\u0004\b\n\u0010\u000bR$\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\f\u0010\r\"\u0004\b\u000e\u0010\u000f\u00a8\u0006\u0010"}, d2 = {"Lcom/senarios/customer/models/BaseEcommerceOrderResponse;", "", "list", "", "Lcom/senarios/customer/models/CartModel;", "address", "Lcom/senarios/customer/models/Address;", "(Ljava/util/List;Lcom/senarios/customer/models/Address;)V", "getAddress", "()Lcom/senarios/customer/models/Address;", "setAddress", "(Lcom/senarios/customer/models/Address;)V", "getList", "()Ljava/util/List;", "setList", "(Ljava/util/List;)V", "app_release"})
public final class BaseEcommerceOrderResponse {
    @org.jetbrains.annotations.NotNull()
    @com.google.gson.annotations.SerializedName(value = "items")
    private java.util.List<com.senarios.customer.models.CartModel> list;
    @org.jetbrains.annotations.NotNull()
    @com.google.gson.annotations.SerializedName(value = "address")
    private com.senarios.customer.models.Address address;
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.senarios.customer.models.CartModel> getList() {
        return null;
    }
    
    public final void setList(@org.jetbrains.annotations.NotNull()
    java.util.List<com.senarios.customer.models.CartModel> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.senarios.customer.models.Address getAddress() {
        return null;
    }
    
    public final void setAddress(@org.jetbrains.annotations.NotNull()
    com.senarios.customer.models.Address p0) {
    }
    
    public BaseEcommerceOrderResponse(@org.jetbrains.annotations.NotNull()
    java.util.List<com.senarios.customer.models.CartModel> list, @org.jetbrains.annotations.NotNull()
    com.senarios.customer.models.Address address) {
        super();
    }
}