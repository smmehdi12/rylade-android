package com.senarios.customer.activities;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u008c\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0010\u0003\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u00032\u00020\u00042\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00070\u00060\u0005B\u0005\u00a2\u0006\u0002\u0010\bJ\b\u0010\u0013\u001a\u00020\u0014H\u0016J\u0018\u0010\u0015\u001a\u00020\u00142\u0006\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u0019H\u0016J \u0010\u001a\u001a\u00020\u00142\u0006\u0010\u001b\u001a\u00020\u00192\u0006\u0010\u001c\u001a\u00020\u00122\u0006\u0010\u001d\u001a\u00020\u0019H\u0016J\u0018\u0010\u001e\u001a\u00020\u00142\u0006\u0010\u001b\u001a\u00020\u00192\u0006\u0010\u001f\u001a\u00020 H\u0016J\u0018\u0010!\u001a\u00020\u00142\u0006\u0010\u001b\u001a\u00020\u00192\u0006\u0010\u001d\u001a\u00020\u0019H\u0016J\b\u0010\"\u001a\u00020\u0014H\u0016J.\u0010#\u001a\u00020\u00142\u0006\u0010\u001b\u001a\u00020\u00192\f\u0010$\u001a\b\u0012\u0004\u0012\u00020&0%2\u0006\u0010\'\u001a\u00020\u00192\u0006\u0010\u001d\u001a\u00020\u0019H\u0016J&\u0010(\u001a\u00020\u00142\u0006\u0010\u001b\u001a\u00020\u00192\f\u0010$\u001a\b\u0012\u0004\u0012\u00020&0%2\u0006\u0010\'\u001a\u00020\u0019H\u0016J\b\u0010)\u001a\u00020\u0014H\u0016J\b\u0010*\u001a\u00020\u0014H\u0002J\u0012\u0010+\u001a\u0004\u0018\u00010,2\u0006\u0010-\u001a\u00020\u0007H\u0002J\b\u0010.\u001a\u00020\u0014H\u0014J\b\u0010/\u001a\u000200H\u0002J\b\u00101\u001a\u00020\u0014H\u0002J\b\u00102\u001a\u00020\u0014H\u0002J\u0016\u00103\u001a\u00020\u00142\f\u0010$\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006H\u0016J\u0012\u00104\u001a\u00020\u00142\b\u00105\u001a\u0004\u0018\u000106H\u0016J\u0012\u00107\u001a\u0002002\b\u00108\u001a\u0004\u0018\u000109H\u0016J\u0010\u0010:\u001a\u0002002\u0006\u0010-\u001a\u00020\u0010H\u0016J\b\u0010;\u001a\u00020\u0014H\u0016J\b\u0010<\u001a\u00020\u0014H\u0002J\u0010\u0010=\u001a\u00020\u00142\u0006\u0010>\u001a\u00020\u0012H\u0002R\u000e\u0010\t\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006?"}, d2 = {"Lcom/senarios/customer/activities/VendorProductsActivity;", "Lcom/senarios/customer/activities/BaseAcivity;", "Landroid/view/View$OnClickListener;", "Lcom/senarios/customer/callbacks/FragmentChanger;", "Lcom/senarios/customer/retrofit/ApiResponse;", "Landroidx/lifecycle/Observer;", "", "Lcom/senarios/customer/models/VendorCategoryModel;", "()V", "adapter", "Lcom/ogaclejapan/smarttablayout/utils/v4/FragmentPagerItemAdapter;", "binding", "Lcom/senarios/customer/databinding/ActivityVendorProductsBinding;", "creator", "Lcom/ogaclejapan/smarttablayout/utils/v4/FragmentPagerItems$Creator;", "menuItem", "Landroid/view/MenuItem;", "vendorID", "", "OnCancelPermissioDialog", "", "OnChange", "fragment", "Landroidx/fragment/app/Fragment;", "tag", "", "OnError", "endpoint", "code", "message", "OnException", "exception", "", "OnNetworkError", "OnPermissionApproved", "OnStatusfalse", "t", "Lretrofit2/Response;", "Lcom/senarios/customer/models/ModelBaseResponse;", "response", "OnSuccess", "OnTrigger", "callApi", "getBundle", "Landroid/os/Bundle;", "item", "init", "isMenu", "", "observeCart", "observeDB", "onChanged", "onClick", "v", "Landroid/view/View;", "onCreateOptionsMenu", "menu", "Landroid/view/Menu;", "onOptionsItemSelected", "setBinding", "setToolbar", "updateMenuItem", "count", "app_release"})
public final class VendorProductsActivity extends com.senarios.customer.activities.BaseAcivity implements android.view.View.OnClickListener, com.senarios.customer.callbacks.FragmentChanger, com.senarios.customer.retrofit.ApiResponse, androidx.lifecycle.Observer<java.util.List<com.senarios.customer.models.VendorCategoryModel>> {
    private com.senarios.customer.databinding.ActivityVendorProductsBinding binding;
    private com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItemAdapter adapter;
    private com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems.Creator creator;
    private int vendorID = -1;
    private android.view.MenuItem menuItem;
    private java.util.HashMap _$_findViewCache;
    
    @java.lang.Override()
    public void setBinding() {
    }
    
    @java.lang.Override()
    protected void init() {
    }
    
    private final void setToolbar() {
    }
    
    private final void observeCart() {
    }
    
    private final void callApi() {
    }
    
    @java.lang.Override()
    public void OnCancelPermissioDialog() {
    }
    
    @java.lang.Override()
    public void OnPermissionApproved() {
    }
    
    @java.lang.Override()
    public void OnTrigger() {
    }
    
    @java.lang.Override()
    public void OnSuccess(@org.jetbrains.annotations.NotNull()
    java.lang.String endpoint, @org.jetbrains.annotations.NotNull()
    retrofit2.Response<com.senarios.customer.models.ModelBaseResponse> t, @org.jetbrains.annotations.NotNull()
    java.lang.String response) {
    }
    
    @java.lang.Override()
    public void OnStatusfalse(@org.jetbrains.annotations.NotNull()
    java.lang.String endpoint, @org.jetbrains.annotations.NotNull()
    retrofit2.Response<com.senarios.customer.models.ModelBaseResponse> t, @org.jetbrains.annotations.NotNull()
    java.lang.String response, @org.jetbrains.annotations.NotNull()
    java.lang.String message) {
    }
    
    @java.lang.Override()
    public void OnError(@org.jetbrains.annotations.NotNull()
    java.lang.String endpoint, int code, @org.jetbrains.annotations.NotNull()
    java.lang.String message) {
    }
    
    @java.lang.Override()
    public void OnException(@org.jetbrains.annotations.NotNull()
    java.lang.String endpoint, @org.jetbrains.annotations.NotNull()
    java.lang.Throwable exception) {
    }
    
    @java.lang.Override()
    public void OnNetworkError(@org.jetbrains.annotations.NotNull()
    java.lang.String endpoint, @org.jetbrains.annotations.NotNull()
    java.lang.String message) {
    }
    
    private final android.os.Bundle getBundle(com.senarios.customer.models.VendorCategoryModel item) {
        return null;
    }
    
    @java.lang.Override()
    public void OnChange(@org.jetbrains.annotations.NotNull()
    androidx.fragment.app.Fragment fragment, @org.jetbrains.annotations.NotNull()
    java.lang.String tag) {
    }
    
    private final void observeDB() {
    }
    
    @java.lang.Override()
    public void onChanged(@org.jetbrains.annotations.NotNull()
    java.util.List<com.senarios.customer.models.VendorCategoryModel> t) {
    }
    
    private final void updateMenuItem(int count) {
    }
    
    @java.lang.Override()
    public boolean onCreateOptionsMenu(@org.jetbrains.annotations.Nullable()
    android.view.Menu menu) {
        return false;
    }
    
    @java.lang.Override()
    public boolean onOptionsItemSelected(@org.jetbrains.annotations.NotNull()
    android.view.MenuItem item) {
        return false;
    }
    
    private final boolean isMenu() {
        return false;
    }
    
    @java.lang.Override()
    public void onClick(@org.jetbrains.annotations.Nullable()
    android.view.View v) {
    }
    
    public VendorProductsActivity() {
        super();
    }
}