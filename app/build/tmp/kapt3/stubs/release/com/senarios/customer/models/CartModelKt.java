package com.senarios.customer.models;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 2, d1 = {"\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\u001a\u0018\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0007\u00a8\u0006\u0006"}, d2 = {"setoutstock", "", "view", "Landroid/view/View;", "isOutStock", "", "app_release"})
public final class CartModelKt {
    
    @androidx.databinding.BindingAdapter(value = {"setoutstock"})
    public static final void setoutstock(@org.jetbrains.annotations.NotNull()
    android.view.View view, boolean isOutStock) {
    }
}