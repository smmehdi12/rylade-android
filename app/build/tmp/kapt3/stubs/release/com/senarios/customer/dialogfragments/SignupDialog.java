package com.senarios.customer.dialogfragments;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0084\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u0003\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u00012\b\u0012\u0004\u0012\u00020\u00030\u00022\u00020\u00042\u00020\u00052\u00020\u00062\u00020\u0007B\u0005\u00a2\u0006\u0002\u0010\bJ\b\u0010\u000b\u001a\u00020\fH\u0016J \u0010\r\u001a\u00020\f2\u0006\u0010\u000e\u001a\u00020\u00032\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0003H\u0016J\u0018\u0010\u0012\u001a\u00020\f2\u0006\u0010\u000e\u001a\u00020\u00032\u0006\u0010\u0013\u001a\u00020\u0014H\u0016J\u0018\u0010\u0015\u001a\u00020\f2\u0006\u0010\u000e\u001a\u00020\u00032\u0006\u0010\u0011\u001a\u00020\u0003H\u0016J.\u0010\u0016\u001a\u00020\f2\u0006\u0010\u000e\u001a\u00020\u00032\f\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\u00190\u00182\u0006\u0010\u001a\u001a\u00020\u00032\u0006\u0010\u0011\u001a\u00020\u0003H\u0016J&\u0010\u001b\u001a\u00020\f2\u0006\u0010\u000e\u001a\u00020\u00032\f\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\u00190\u00182\u0006\u0010\u001a\u001a\u00020\u0003H\u0016J&\u0010\u001c\u001a\u0004\u0018\u00010\u001d2\u0006\u0010\u001e\u001a\u00020\u001f2\b\u0010 \u001a\u0004\u0018\u00010!2\b\u0010\"\u001a\u0004\u0018\u00010#H\u0016J\b\u0010$\u001a\u00020\fH\u0014J\u0012\u0010%\u001a\u00020\f2\b\u0010\u0017\u001a\u0004\u0018\u00010\u0003H\u0016J\u0012\u0010&\u001a\u00020\f2\b\u0010\'\u001a\u0004\u0018\u00010\u001dH\u0016J\b\u0010(\u001a\u00020\fH\u0016J$\u0010)\u001a\u00020*2\b\u0010\'\u001a\u0004\u0018\u00010+2\u0006\u0010,\u001a\u00020\u00102\b\u0010-\u001a\u0004\u0018\u00010.H\u0016J\u001a\u0010/\u001a\u00020\f2\b\u0010\'\u001a\u0004\u0018\u00010\u001d2\u0006\u00100\u001a\u00020*H\u0016J0\u00101\u001a\u00020\f2\u0006\u00102\u001a\u00020*2\u0006\u00103\u001a\u00020*2\u0006\u00104\u001a\u00020*2\u0006\u00105\u001a\u00020*2\u0006\u00106\u001a\u00020*H\u0002J\u0010\u00107\u001a\u00020\f2\u0006\u00108\u001a\u000209H\u0002J\u0010\u0010:\u001a\u00020\f2\u0006\u00108\u001a\u000209H\u0002J\b\u0010;\u001a\u00020\fH\u0002R\u000e\u0010\t\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006<"}, d2 = {"Lcom/senarios/customer/dialogfragments/SignupDialog;", "Lcom/senarios/rylad/BaseDialogeFragment;", "Landroidx/lifecycle/Observer;", "", "Landroid/view/View$OnClickListener;", "Landroid/widget/TextView$OnEditorActionListener;", "Landroid/view/View$OnFocusChangeListener;", "Lcom/senarios/customer/retrofit/ApiResponse;", "()V", "binding", "Lcom/senarios/customer/databinding/FragmentSignupBinding;", "OnBackPressed", "", "OnError", "endpoint", "code", "", "message", "OnException", "exception", "", "OnNetworkError", "OnStatusfalse", "t", "Lretrofit2/Response;", "Lcom/senarios/customer/models/ModelBaseResponse;", "response", "OnSuccess", "getFragmentView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "init", "onChanged", "onClick", "v", "onDestroy", "onEditorAction", "", "Landroid/widget/TextView;", "actionId", "event", "Landroid/view/KeyEvent;", "onFocusChange", "hasFocus", "setActive", "fname", "lname", "email", "password", "number", "setGrayDrawable", "editText", "Landroid/widget/EditText;", "setGreenDrawable", "validateInputs", "app_release"})
public final class SignupDialog extends com.senarios.rylad.BaseDialogeFragment implements androidx.lifecycle.Observer<java.lang.String>, android.view.View.OnClickListener, android.widget.TextView.OnEditorActionListener, android.view.View.OnFocusChangeListener, com.senarios.customer.retrofit.ApiResponse {
    private com.senarios.customer.databinding.FragmentSignupBinding binding;
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public android.view.View getFragmentView(@org.jetbrains.annotations.NotNull()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    @java.lang.Override()
    protected void init() {
    }
    
    @java.lang.Override()
    public void OnBackPressed() {
    }
    
    @java.lang.Override()
    public void onClick(@org.jetbrains.annotations.Nullable()
    android.view.View v) {
    }
    
    private final void validateInputs() {
    }
    
    @java.lang.Override()
    public void onFocusChange(@org.jetbrains.annotations.Nullable()
    android.view.View v, boolean hasFocus) {
    }
    
    @java.lang.Override()
    public void OnSuccess(@org.jetbrains.annotations.NotNull()
    java.lang.String endpoint, @org.jetbrains.annotations.NotNull()
    retrofit2.Response<com.senarios.customer.models.ModelBaseResponse> t, @org.jetbrains.annotations.NotNull()
    java.lang.String response) {
    }
    
    @java.lang.Override()
    public void OnStatusfalse(@org.jetbrains.annotations.NotNull()
    java.lang.String endpoint, @org.jetbrains.annotations.NotNull()
    retrofit2.Response<com.senarios.customer.models.ModelBaseResponse> t, @org.jetbrains.annotations.NotNull()
    java.lang.String response, @org.jetbrains.annotations.NotNull()
    java.lang.String message) {
    }
    
    @java.lang.Override()
    public void OnError(@org.jetbrains.annotations.NotNull()
    java.lang.String endpoint, int code, @org.jetbrains.annotations.NotNull()
    java.lang.String message) {
    }
    
    @java.lang.Override()
    public void OnException(@org.jetbrains.annotations.NotNull()
    java.lang.String endpoint, @org.jetbrains.annotations.NotNull()
    java.lang.Throwable exception) {
    }
    
    @java.lang.Override()
    public void OnNetworkError(@org.jetbrains.annotations.NotNull()
    java.lang.String endpoint, @org.jetbrains.annotations.NotNull()
    java.lang.String message) {
    }
    
    private final void setActive(boolean fname, boolean lname, boolean email, boolean password, boolean number) {
    }
    
    private final void setGrayDrawable(android.widget.EditText editText) {
    }
    
    private final void setGreenDrawable(android.widget.EditText editText) {
    }
    
    @java.lang.Override()
    public boolean onEditorAction(@org.jetbrains.annotations.Nullable()
    android.widget.TextView v, int actionId, @org.jetbrains.annotations.Nullable()
    android.view.KeyEvent event) {
        return false;
    }
    
    @java.lang.Override()
    public void onDestroy() {
    }
    
    @java.lang.Override()
    public void onChanged(@org.jetbrains.annotations.Nullable()
    java.lang.String t) {
    }
    
    public SignupDialog() {
        super();
    }
}