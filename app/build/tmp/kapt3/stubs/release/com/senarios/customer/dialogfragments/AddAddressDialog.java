package com.senarios.customer.dialogfragments;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u00a6\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010!\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u0003\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\r\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u00032\u00020\u00042\u00020\u00052\b\u0012\u0004\u0012\u00020\u00070\u0006B\u0005\u00a2\u0006\u0002\u0010\bJ\b\u0010\u0012\u001a\u00020\u0013H\u0016J \u0010\u0014\u001a\u00020\u00132\u0006\u0010\u0015\u001a\u00020\u000f2\u0006\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u000fH\u0016J\u0018\u0010\u0019\u001a\u00020\u00132\u0006\u0010\u0015\u001a\u00020\u000f2\u0006\u0010\u001a\u001a\u00020\u001bH\u0016J\u0018\u0010\u001c\u001a\u00020\u00132\u0006\u0010\u0015\u001a\u00020\u000f2\u0006\u0010\u0018\u001a\u00020\u000fH\u0016J.\u0010\u001d\u001a\u00020\u00132\u0006\u0010\u0015\u001a\u00020\u000f2\f\u0010\u001e\u001a\b\u0012\u0004\u0012\u00020 0\u001f2\u0006\u0010!\u001a\u00020\u000f2\u0006\u0010\u0018\u001a\u00020\u000fH\u0016J&\u0010\"\u001a\u00020\u00132\u0006\u0010\u0015\u001a\u00020\u000f2\f\u0010\u001e\u001a\b\u0012\u0004\u0012\u00020 0\u001f2\u0006\u0010!\u001a\u00020\u000fH\u0016J\b\u0010#\u001a\u00020\u0013H\u0002J&\u0010$\u001a\u0004\u0018\u00010%2\u0006\u0010&\u001a\u00020\'2\b\u0010(\u001a\u0004\u0018\u00010)2\b\u0010*\u001a\u0004\u0018\u00010+H\u0016J\b\u0010,\u001a\u00020\u0013H\u0014J\"\u0010-\u001a\u00020\u00132\u0006\u0010.\u001a\u00020\u00172\u0006\u0010/\u001a\u00020\u00172\b\u00100\u001a\u0004\u0018\u000101H\u0016J\u0012\u00102\u001a\u00020\u00132\b\u0010\u001e\u001a\u0004\u0018\u00010\u0007H\u0016J\u0012\u00103\u001a\u00020\u00132\b\u00104\u001a\u0004\u0018\u00010%H\u0016J$\u00105\u001a\u0002062\b\u00104\u001a\u0004\u0018\u0001072\u0006\u00108\u001a\u00020\u00172\b\u00109\u001a\u0004\u0018\u00010:H\u0016J\u001a\u0010;\u001a\u00020\u00132\b\u00104\u001a\u0004\u0018\u00010%2\u0006\u0010<\u001a\u000206H\u0016J&\u0010=\u001a\u00020\u00132\u0006\u0010>\u001a\u0002062\u0006\u0010\t\u001a\u0002062\u0006\u0010?\u001a\u0002062\u0006\u0010@\u001a\u000206J\u001e\u0010A\u001a\u00020\u00132\u0006\u0010B\u001a\u0002062\u0006\u0010C\u001a\u0002062\u0006\u0010D\u001a\u000206J\u001e\u0010E\u001a\u00020\u00132\u0006\u0010F\u001a\u00020\u00172\u0006\u0010G\u001a\u00020H2\u0006\u0010I\u001a\u000207J\u000e\u0010J\u001a\u00020\u00132\u0006\u0010K\u001a\u00020LJ\u001e\u0010M\u001a\u00020\u00132\u0006\u0010F\u001a\u00020\u00172\u0006\u0010G\u001a\u00020H2\u0006\u0010I\u001a\u000207J\u000e\u0010N\u001a\u00020\u00132\u0006\u0010K\u001a\u00020LJ\b\u0010O\u001a\u00020\u0013H\u0002R\u000e\u0010\t\u001a\u00020\u0007X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\rX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u000f0\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006P"}, d2 = {"Lcom/senarios/customer/dialogfragments/AddAddressDialog;", "Lcom/senarios/rylad/BaseDialogeFragment;", "Landroid/widget/TextView$OnEditorActionListener;", "Landroid/view/View$OnFocusChangeListener;", "Landroid/view/View$OnClickListener;", "Lcom/senarios/customer/retrofit/ApiResponse;", "Landroidx/lifecycle/Observer;", "Lcom/senarios/customer/models/Address;", "()V", "address", "binding", "Lcom/senarios/customer/databinding/FragmentAddNewAddressBinding;", "latLng", "Lcom/google/android/gms/maps/model/LatLng;", "type", "", "types", "", "OnBackPressed", "", "OnError", "endpoint", "code", "", "message", "OnException", "exception", "", "OnNetworkError", "OnStatusfalse", "t", "Lretrofit2/Response;", "Lcom/senarios/customer/models/ModelBaseResponse;", "response", "OnSuccess", "editFlow", "getFragmentView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "init", "onActivityResult", "requestCode", "resultCode", "data", "Landroid/content/Intent;", "onChanged", "onClick", "v", "onEditorAction", "", "Landroid/widget/TextView;", "actionId", "event", "Landroid/view/KeyEvent;", "onFocusChange", "hasFocus", "setActive", "title", "street", "town", "setActiveButton", "home", "work", "other", "setGrayButton", "id", "image", "Landroid/widget/ImageView;", "textView", "setGrayDrawable", "editText", "Landroid/widget/EditText;", "setGreenButton", "setGreenDrawable", "validateInputs", "app_release"})
public final class AddAddressDialog extends com.senarios.rylad.BaseDialogeFragment implements android.widget.TextView.OnEditorActionListener, android.view.View.OnFocusChangeListener, android.view.View.OnClickListener, com.senarios.customer.retrofit.ApiResponse, androidx.lifecycle.Observer<com.senarios.customer.models.Address> {
    private com.senarios.customer.databinding.FragmentAddNewAddressBinding binding;
    private final java.util.List<java.lang.String> types = null;
    private java.lang.String type = "home";
    private com.senarios.customer.models.Address address;
    private com.google.android.gms.maps.model.LatLng latLng;
    private java.util.HashMap _$_findViewCache;
    
    @java.lang.Override()
    protected void init() {
    }
    
    private final void editFlow() {
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public android.view.View getFragmentView(@org.jetbrains.annotations.NotNull()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    @java.lang.Override()
    public void OnBackPressed() {
    }
    
    @java.lang.Override()
    public void onClick(@org.jetbrains.annotations.Nullable()
    android.view.View v) {
    }
    
    @java.lang.Override()
    public void onActivityResult(int requestCode, int resultCode, @org.jetbrains.annotations.Nullable()
    android.content.Intent data) {
    }
    
    private final void validateInputs() {
    }
    
    @java.lang.Override()
    public void onFocusChange(@org.jetbrains.annotations.Nullable()
    android.view.View v, boolean hasFocus) {
    }
    
    @java.lang.Override()
    public void OnSuccess(@org.jetbrains.annotations.NotNull()
    java.lang.String endpoint, @org.jetbrains.annotations.NotNull()
    retrofit2.Response<com.senarios.customer.models.ModelBaseResponse> t, @org.jetbrains.annotations.NotNull()
    java.lang.String response) {
    }
    
    @java.lang.Override()
    public void OnStatusfalse(@org.jetbrains.annotations.NotNull()
    java.lang.String endpoint, @org.jetbrains.annotations.NotNull()
    retrofit2.Response<com.senarios.customer.models.ModelBaseResponse> t, @org.jetbrains.annotations.NotNull()
    java.lang.String response, @org.jetbrains.annotations.NotNull()
    java.lang.String message) {
    }
    
    @java.lang.Override()
    public void OnError(@org.jetbrains.annotations.NotNull()
    java.lang.String endpoint, int code, @org.jetbrains.annotations.NotNull()
    java.lang.String message) {
    }
    
    @java.lang.Override()
    public void OnException(@org.jetbrains.annotations.NotNull()
    java.lang.String endpoint, @org.jetbrains.annotations.NotNull()
    java.lang.Throwable exception) {
    }
    
    @java.lang.Override()
    public void OnNetworkError(@org.jetbrains.annotations.NotNull()
    java.lang.String endpoint, @org.jetbrains.annotations.NotNull()
    java.lang.String message) {
    }
    
    public final void setActive(boolean title, boolean address, boolean street, boolean town) {
    }
    
    public final void setActiveButton(boolean home, boolean work, boolean other) {
    }
    
    public final void setGrayDrawable(@org.jetbrains.annotations.NotNull()
    android.widget.EditText editText) {
    }
    
    public final void setGreenDrawable(@org.jetbrains.annotations.NotNull()
    android.widget.EditText editText) {
    }
    
    public final void setGrayButton(int id, @org.jetbrains.annotations.NotNull()
    android.widget.ImageView image, @org.jetbrains.annotations.NotNull()
    android.widget.TextView textView) {
    }
    
    public final void setGreenButton(int id, @org.jetbrains.annotations.NotNull()
    android.widget.ImageView image, @org.jetbrains.annotations.NotNull()
    android.widget.TextView textView) {
    }
    
    @java.lang.Override()
    public void onChanged(@org.jetbrains.annotations.Nullable()
    com.senarios.customer.models.Address t) {
    }
    
    @java.lang.Override()
    public boolean onEditorAction(@org.jetbrains.annotations.Nullable()
    android.widget.TextView v, int actionId, @org.jetbrains.annotations.Nullable()
    android.view.KeyEvent event) {
        return false;
    }
    
    public AddAddressDialog() {
        super();
    }
}