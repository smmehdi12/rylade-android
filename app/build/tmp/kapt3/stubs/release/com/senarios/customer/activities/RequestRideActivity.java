package com.senarios.customer.activities;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u00d6\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0007\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0006\n\u0002\u0010\u0003\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0011\n\u0000\n\u0002\u0010\u0015\n\u0002\b\n\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u00032\b\u0012\u0004\u0012\u00020\u00050\u00042\u00020\u00062\u00020\u0007B\u0005\u00a2\u0006\u0002\u0010\bJ\b\u0010$\u001a\u00020%H\u0016J \u0010&\u001a\u00020%2\u0006\u0010\'\u001a\u00020\f2\u0006\u0010(\u001a\u00020\n2\u0006\u0010)\u001a\u00020\fH\u0016J\u0018\u0010*\u001a\u00020%2\u0006\u0010\'\u001a\u00020\f2\u0006\u0010+\u001a\u00020,H\u0016J \u0010-\u001a\u00020%2\u0006\u0010\'\u001a\u00020\f2\u0006\u0010(\u001a\u00020\n2\u0006\u0010)\u001a\u00020\fH\u0016J\u0018\u0010.\u001a\u00020%2\u0006\u0010\'\u001a\u00020\f2\u0006\u0010+\u001a\u00020,H\u0016J\u001e\u0010/\u001a\u00020%2\u0006\u0010\'\u001a\u00020\f2\f\u00100\u001a\b\u0012\u0004\u0012\u00020201H\u0016J\u0018\u00103\u001a\u00020%2\u0006\u0010\'\u001a\u00020\f2\u0006\u0010)\u001a\u00020\fH\u0016J\b\u00104\u001a\u00020%H\u0016J.\u00105\u001a\u00020%2\u0006\u0010\'\u001a\u00020\f2\f\u00100\u001a\b\u0012\u0004\u0012\u000206012\u0006\u00107\u001a\u00020\f2\u0006\u0010)\u001a\u00020\fH\u0016J&\u00108\u001a\u00020%2\u0006\u0010\'\u001a\u00020\f2\f\u00100\u001a\b\u0012\u0004\u0012\u000206012\u0006\u00107\u001a\u00020\fH\u0016J\b\u00109\u001a\u00020%H\u0016J\b\u0010:\u001a\u00020%H\u0002J\b\u0010;\u001a\u00020%H\u0002J\b\u0010<\u001a\u00020%H\u0002J\b\u0010=\u001a\u00020%H\u0002J \u0010>\u001a\u00020%2\u0006\u0010?\u001a\u00020@2\u0006\u0010A\u001a\u00020\f2\u0006\u0010B\u001a\u00020\nH\u0002J\b\u0010C\u001a\u00020DH\u0002J\b\u0010E\u001a\u00020@H\u0002J\u0016\u0010F\u001a\u00020%2\f\u0010G\u001a\b\u0012\u0004\u0012\u00020H0\u001bH\u0007J\b\u0010I\u001a\u00020%H\u0002J\b\u0010J\u001a\u00020%H\u0002J\b\u0010K\u001a\u00020%H\u0014J\b\u0010L\u001a\u00020%H\u0002J\b\u0010M\u001a\u00020NH\u0002J\b\u0010O\u001a\u00020NH\u0002J\b\u0010P\u001a\u00020NH\u0002J\b\u0010Q\u001a\u00020%H\u0002J\u0010\u0010R\u001a\u00020%2\u0006\u0010S\u001a\u00020@H\u0002J\u0010\u0010R\u001a\u00020%2\u0006\u0010T\u001a\u00020DH\u0002J\"\u0010U\u001a\u00020%2\u0006\u0010V\u001a\u00020\n2\u0006\u0010W\u001a\u00020\n2\b\u0010X\u001a\u0004\u0018\u00010YH\u0014J\u0012\u0010Z\u001a\u00020%2\b\u0010[\u001a\u0004\u0018\u00010\\H\u0016J\u0012\u0010]\u001a\u00020%2\b\u0010^\u001a\u0004\u0018\u00010\u0019H\u0016J-\u0010_\u001a\u00020%2\u0006\u0010V\u001a\u00020\n2\u000e\u0010`\u001a\n\u0012\u0006\b\u0001\u0012\u00020\f0a2\u0006\u0010b\u001a\u00020cH\u0016\u00a2\u0006\u0002\u0010dJ\b\u0010e\u001a\u00020%H\u0014J\b\u0010f\u001a\u00020%H\u0014J\b\u0010g\u001a\u00020%H\u0014J\u0012\u0010h\u001a\u00020%2\b\u0010^\u001a\u0004\u0018\u00010\u0005H\u0016J\b\u0010i\u001a\u00020%H\u0002J\b\u0010j\u001a\u00020%H\u0002J\b\u0010k\u001a\u00020%H\u0002J\b\u0010l\u001a\u00020%H\u0016R\u000e\u0010\t\u001a\u00020\nX\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0005X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0017X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0019X\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\u001c0\u001bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001d\u001a\u00020\u001eX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001f\u001a\u00020 X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010!\u001a\u00020 X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\"\u001a\u00020#X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006m"}, d2 = {"Lcom/senarios/customer/activities/RequestRideActivity;", "Lcom/senarios/customer/activities/BaseAcivity;", "Lcom/google/android/gms/maps/OnMapReadyCallback;", "Lcom/senarios/customer/retrofit/ApiResponse;", "Lcom/google/android/gms/tasks/OnSuccessListener;", "Landroid/location/Location;", "Landroid/view/View$OnClickListener;", "Lcom/senarios/customer/retrofit/ApiGoogleResponse;", "()V", "REQUEST_CHECK_SETTINGS", "", "TAG", "", "binding", "Lcom/senarios/customer/databinding/FragmentRequestRideBinding;", "builder", "Lcom/google/android/gms/location/LocationSettingsRequest$Builder;", "currentZoom", "", "location", "locationCallback", "Lcom/google/android/gms/location/LocationCallback;", "locationRequest", "Lcom/google/android/gms/location/LocationRequest;", "map", "Lcom/google/android/gms/maps/GoogleMap;", "markers", "", "Lcom/google/android/gms/maps/model/MarkerOptions;", "orderModel", "Lcom/senarios/customer/models/OrderModel;", "ride", "Lcom/senarios/customer/models/RideModel;", "rideModel", "riderModel", "Lcom/senarios/customer/models/User;", "OnCancelPermissioDialog", "", "OnError", "endpoint", "code", "message", "OnException", "exception", "", "OnGoogleError", "OnGoogleException", "OnGoogleSuccess", "t", "Lretrofit2/Response;", "Lcom/senarios/customer/models/GoogleDistanceApiResponse;", "OnNetworkError", "OnPermissionApproved", "OnStatusfalse", "Lcom/senarios/customer/models/ModelBaseResponse;", "response", "OnSuccess", "OnTrigger", "addMarkers", "callCancelAPI", "callRideAPI", "createLocationRequest", "createMarker", "latLng", "Lcom/google/android/gms/maps/model/LatLng;", "type", "index", "getBounds", "Lcom/google/android/gms/maps/model/LatLngBounds;", "getCurrentLatLng", "getValues", "list", "Lcom/senarios/customer/models/ModelCommunication;", "hideShowConfirmationCV", "hideShowLocationCV", "init", "initMap", "isLocation", "", "isLocationCallback", "isMap", "locationRequestTask", "moveCamera", "latlng", "bounds", "onActivityResult", "requestCode", "resultCode", "data", "Landroid/content/Intent;", "onClick", "v", "Landroid/view/View;", "onMapReady", "p0", "onRequestPermissionsResult", "permissions", "", "grantResults", "", "(I[Ljava/lang/String;[I)V", "onResume", "onStart", "onStop", "onSuccess", "removeRequestLocation", "requestLocation", "restoreRideState", "setBinding", "app_release"})
public final class RequestRideActivity extends com.senarios.customer.activities.BaseAcivity implements com.google.android.gms.maps.OnMapReadyCallback, com.senarios.customer.retrofit.ApiResponse, com.google.android.gms.tasks.OnSuccessListener<android.location.Location>, android.view.View.OnClickListener, com.senarios.customer.retrofit.ApiGoogleResponse {
    private com.senarios.customer.databinding.FragmentRequestRideBinding binding;
    private com.senarios.customer.models.RideModel ride;
    private final int REQUEST_CHECK_SETTINGS = 1000;
    private com.google.android.gms.maps.GoogleMap map;
    private com.google.android.gms.location.LocationRequest locationRequest;
    private com.google.android.gms.location.LocationCallback locationCallback;
    private android.location.Location location;
    private com.senarios.customer.models.RideModel rideModel;
    private com.senarios.customer.models.OrderModel orderModel;
    private com.senarios.customer.models.User riderModel;
    private com.google.android.gms.location.LocationSettingsRequest.Builder builder;
    private final float currentZoom = 16.0F;
    private java.util.List<com.google.android.gms.maps.model.MarkerOptions> markers;
    private final java.lang.String TAG = null;
    private java.util.HashMap _$_findViewCache;
    
    @java.lang.Override()
    public void setBinding() {
    }
    
    @java.lang.Override()
    protected void init() {
    }
    
    @java.lang.Override()
    public void OnCancelPermissioDialog() {
    }
    
    @java.lang.Override()
    public void OnPermissionApproved() {
    }
    
    @java.lang.Override()
    public void OnTrigger() {
    }
    
    @java.lang.Override()
    public void onClick(@org.jetbrains.annotations.Nullable()
    android.view.View v) {
    }
    
    private final void callCancelAPI() {
    }
    
    private final void callRideAPI() {
    }
    
    @java.lang.Override()
    protected void onActivityResult(int requestCode, int resultCode, @org.jetbrains.annotations.Nullable()
    android.content.Intent data) {
    }
    
    private final void createMarker(com.google.android.gms.maps.model.LatLng latLng, java.lang.String type, int index) {
    }
    
    private final void addMarkers() {
    }
    
    private final void initMap() {
    }
    
    @java.lang.Override()
    public void onMapReady(@org.jetbrains.annotations.Nullable()
    com.google.android.gms.maps.GoogleMap p0) {
    }
    
    private final void createLocationRequest() {
    }
    
    private final void locationRequestTask() {
    }
    
    private final boolean isLocation() {
        return false;
    }
    
    @java.lang.Override()
    protected void onResume() {
    }
    
    @java.lang.Override()
    protected void onStop() {
    }
    
    private final void requestLocation() {
    }
    
    private final void removeRequestLocation() {
    }
    
    private final void moveCamera(com.google.android.gms.maps.model.LatLng latlng) {
    }
    
    private final void moveCamera(com.google.android.gms.maps.model.LatLngBounds bounds) {
    }
    
    @java.lang.Override()
    public void onRequestPermissionsResult(int requestCode, @org.jetbrains.annotations.NotNull()
    java.lang.String[] permissions, @org.jetbrains.annotations.NotNull()
    int[] grantResults) {
    }
    
    private final com.google.android.gms.maps.model.LatLng getCurrentLatLng() {
        return null;
    }
    
    private final boolean isMap() {
        return false;
    }
    
    private final boolean isLocationCallback() {
        return false;
    }
    
    private final com.google.android.gms.maps.model.LatLngBounds getBounds() {
        return null;
    }
    
    private final void hideShowConfirmationCV() {
    }
    
    private final void hideShowLocationCV() {
    }
    
    @java.lang.Override()
    public void OnGoogleSuccess(@org.jetbrains.annotations.NotNull()
    java.lang.String endpoint, @org.jetbrains.annotations.NotNull()
    retrofit2.Response<com.senarios.customer.models.GoogleDistanceApiResponse> t) {
    }
    
    @java.lang.Override()
    public void OnGoogleError(@org.jetbrains.annotations.NotNull()
    java.lang.String endpoint, int code, @org.jetbrains.annotations.NotNull()
    java.lang.String message) {
    }
    
    @java.lang.Override()
    public void OnGoogleException(@org.jetbrains.annotations.NotNull()
    java.lang.String endpoint, @org.jetbrains.annotations.NotNull()
    java.lang.Throwable exception) {
    }
    
    @java.lang.Override()
    public void OnSuccess(@org.jetbrains.annotations.NotNull()
    java.lang.String endpoint, @org.jetbrains.annotations.NotNull()
    retrofit2.Response<com.senarios.customer.models.ModelBaseResponse> t, @org.jetbrains.annotations.NotNull()
    java.lang.String response) {
    }
    
    @java.lang.Override()
    public void OnStatusfalse(@org.jetbrains.annotations.NotNull()
    java.lang.String endpoint, @org.jetbrains.annotations.NotNull()
    retrofit2.Response<com.senarios.customer.models.ModelBaseResponse> t, @org.jetbrains.annotations.NotNull()
    java.lang.String response, @org.jetbrains.annotations.NotNull()
    java.lang.String message) {
    }
    
    @java.lang.Override()
    public void OnError(@org.jetbrains.annotations.NotNull()
    java.lang.String endpoint, int code, @org.jetbrains.annotations.NotNull()
    java.lang.String message) {
    }
    
    @java.lang.Override()
    public void OnException(@org.jetbrains.annotations.NotNull()
    java.lang.String endpoint, @org.jetbrains.annotations.NotNull()
    java.lang.Throwable exception) {
    }
    
    @java.lang.Override()
    public void OnNetworkError(@org.jetbrains.annotations.NotNull()
    java.lang.String endpoint, @org.jetbrains.annotations.NotNull()
    java.lang.String message) {
    }
    
    @java.lang.Override()
    protected void onStart() {
    }
    
    @org.greenrobot.eventbus.Subscribe(threadMode = org.greenrobot.eventbus.ThreadMode.MAIN)
    public final void getValues(@org.jetbrains.annotations.NotNull()
    java.util.List<com.senarios.customer.models.ModelCommunication> list) {
    }
    
    private final void restoreRideState() {
    }
    
    @java.lang.Override()
    public void onSuccess(@org.jetbrains.annotations.Nullable()
    android.location.Location p0) {
    }
    
    public RequestRideActivity() {
        super();
    }
}