package com.senarios.customer.models;

import java.lang.System;

@androidx.room.Entity(tableName = "Cart")
@kotlinx.android.parcel.Parcelize()
@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0011\n\u0002\u0010\u0006\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b@\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0007\u0018\u00002\u00020\u0001B\u00fb\u0001\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u0012\b\u0010\b\u001a\u0004\u0018\u00010\u0007\u0012\b\u0010\t\u001a\u0004\u0018\u00010\u0007\u0012\b\u0010\n\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010\u000b\u001a\u0004\u0018\u00010\u0007\u0012\b\u0010\f\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010\r\u001a\u0004\u0018\u00010\u0007\u0012\b\u0010\u000e\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010\u000f\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010\u0010\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010\u0011\u001a\u0004\u0018\u00010\u0007\u0012\b\u0010\u0012\u001a\u0004\u0018\u00010\u0007\u0012\b\u0010\u0013\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010\u0014\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010\u0015\u001a\u0004\u0018\u00010\u0007\u0012\b\u0010\u0016\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010\u0017\u001a\u0004\u0018\u00010\u0005\u0012\u0006\u0010\u0018\u001a\u00020\u0019\u0012\b\u0010\u001a\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010\u001b\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010\u001c\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010\u001d\u001a\u0004\u0018\u00010\u0005\u0012\u0006\u0010\u001e\u001a\u00020\u001f\u00a2\u0006\u0002\u0010 J\t\u0010^\u001a\u00020\u0005H\u00d6\u0001J\u0019\u0010_\u001a\u00020`2\u0006\u0010a\u001a\u00020b2\u0006\u0010c\u001a\u00020\u0005H\u00d6\u0001R\"\u0010\u0004\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0010\n\u0002\u0010%\u001a\u0004\b!\u0010\"\"\u0004\b#\u0010$R \u0010\u0006\u001a\u0004\u0018\u00010\u00078\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b&\u0010\'\"\u0004\b(\u0010)R \u0010\b\u001a\u0004\u0018\u00010\u00078\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b*\u0010\'\"\u0004\b+\u0010)R \u0010\t\u001a\u0004\u0018\u00010\u00078\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b,\u0010\'\"\u0004\b-\u0010)R\"\u0010\u001d\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0010\n\u0002\u0010%\u001a\u0004\b.\u0010\"\"\u0004\b/\u0010$R\"\u0010\u001a\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0010\n\u0002\u0010%\u001a\u0004\b0\u0010\"\"\u0004\b1\u0010$R\"\u0010\u0002\u001a\u0004\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0010\n\u0002\u00106\u001a\u0004\b2\u00103\"\u0004\b4\u00105R \u0010\u000b\u001a\u0004\u0018\u00010\u00078\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b7\u0010\'\"\u0004\b8\u0010)R\u001e\u0010\u001e\u001a\u00020\u001f8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001e\u00109\"\u0004\b:\u0010;R\"\u0010\u001b\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0010\n\u0002\u0010%\u001a\u0004\b<\u0010\"\"\u0004\b=\u0010$R\"\u0010\u0017\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0010\n\u0002\u0010%\u001a\u0004\b>\u0010\"\"\u0004\b?\u0010$R\"\u0010\u001c\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0010\n\u0002\u0010%\u001a\u0004\b@\u0010\"\"\u0004\bA\u0010$R\"\u0010\f\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0010\n\u0002\u0010%\u001a\u0004\bB\u0010\"\"\u0004\bC\u0010$R \u0010\r\u001a\u0004\u0018\u00010\u00078\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bD\u0010\'\"\u0004\bE\u0010)R\"\u0010\n\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0010\n\u0002\u0010%\u001a\u0004\bF\u0010\"\"\u0004\bG\u0010$R\"\u0010\u000e\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0010\n\u0002\u0010%\u001a\u0004\bH\u0010\"\"\u0004\bI\u0010$R\"\u0010\u0016\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0010\n\u0002\u0010%\u001a\u0004\bJ\u0010\"\"\u0004\bK\u0010$R\"\u0010\u000f\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0010\n\u0002\u0010%\u001a\u0004\bL\u0010\"\"\u0004\bM\u0010$R\u001e\u0010\u0018\u001a\u00020\u00198\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bN\u0010O\"\u0004\bP\u0010QR\"\u0010\u0010\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0010\n\u0002\u0010%\u001a\u0004\bR\u0010\"\"\u0004\bS\u0010$R \u0010\u0011\u001a\u0004\u0018\u00010\u00078\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bT\u0010\'\"\u0004\bU\u0010)R \u0010\u0012\u001a\u0004\u0018\u00010\u00078\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bV\u0010\'\"\u0004\bW\u0010)R\"\u0010\u0014\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0010\n\u0002\u0010%\u001a\u0004\bX\u0010\"\"\u0004\bY\u0010$R \u0010\u0015\u001a\u0004\u0018\u00010\u00078\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bZ\u0010\'\"\u0004\b[\u0010)R\"\u0010\u0013\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0010\n\u0002\u0010%\u001a\u0004\b\\\u0010\"\"\u0004\b]\u0010$\u00a8\u0006d"}, d2 = {"Lcom/senarios/customer/models/CartModel;", "Landroid/os/Parcelable;", "id", "", "categoryId", "", "categoryName", "", "createdAt", "description", "product_id", "image", "price", "productName", "promoCodeId", "quantityAvailable", "status", "subCategoryName", "updatedAt", "weight", "vendorId", "vendor_name", "quantity", "order_id", "sales_tax", "", "fixed_discount", "iss_discount", "originalPrice", "discounted_price", "isOutStock", "", "(Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;DLjava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Z)V", "getCategoryId", "()Ljava/lang/Integer;", "setCategoryId", "(Ljava/lang/Integer;)V", "Ljava/lang/Integer;", "getCategoryName", "()Ljava/lang/String;", "setCategoryName", "(Ljava/lang/String;)V", "getCreatedAt", "setCreatedAt", "getDescription", "setDescription", "getDiscounted_price", "setDiscounted_price", "getFixed_discount", "setFixed_discount", "getId", "()Ljava/lang/Long;", "setId", "(Ljava/lang/Long;)V", "Ljava/lang/Long;", "getImage", "setImage", "()Z", "setOutStock", "(Z)V", "getIss_discount", "setIss_discount", "getOrder_id", "setOrder_id", "getOriginalPrice", "setOriginalPrice", "getPrice", "setPrice", "getProductName", "setProductName", "getProduct_id", "setProduct_id", "getPromoCodeId", "setPromoCodeId", "getQuantity", "setQuantity", "getQuantityAvailable", "setQuantityAvailable", "getSales_tax", "()D", "setSales_tax", "(D)V", "getStatus", "setStatus", "getSubCategoryName", "setSubCategoryName", "getUpdatedAt", "setUpdatedAt", "getVendorId", "setVendorId", "getVendor_name", "setVendor_name", "getWeight", "setWeight", "describeContents", "writeToParcel", "", "parcel", "Landroid/os/Parcel;", "flags", "app_release"})
public final class CartModel implements android.os.Parcelable {
    @org.jetbrains.annotations.Nullable()
    @androidx.room.ColumnInfo(name = "id")
    @androidx.room.PrimaryKey(autoGenerate = true)
    private java.lang.Long id;
    @org.jetbrains.annotations.Nullable()
    @androidx.room.ColumnInfo(name = "category_id")
    @com.google.gson.annotations.SerializedName(value = "category_id")
    private java.lang.Integer categoryId;
    @org.jetbrains.annotations.Nullable()
    @androidx.room.ColumnInfo(name = "category_name")
    @com.google.gson.annotations.SerializedName(value = "category_name")
    private java.lang.String categoryName;
    @org.jetbrains.annotations.Nullable()
    @androidx.room.ColumnInfo(name = "created_at")
    @com.google.gson.annotations.SerializedName(value = "created_at")
    private java.lang.String createdAt;
    @org.jetbrains.annotations.Nullable()
    @androidx.room.ColumnInfo(name = "description")
    @com.google.gson.annotations.SerializedName(value = "description")
    private java.lang.String description;
    @org.jetbrains.annotations.Nullable()
    @androidx.room.ColumnInfo(name = "product_id")
    @com.google.gson.annotations.SerializedName(value = "product_id")
    private java.lang.Integer product_id;
    @org.jetbrains.annotations.Nullable()
    @androidx.room.ColumnInfo(name = "image")
    @com.google.gson.annotations.SerializedName(value = "image")
    private java.lang.String image;
    @org.jetbrains.annotations.Nullable()
    @androidx.room.ColumnInfo(name = "price")
    @com.google.gson.annotations.SerializedName(value = "price")
    private java.lang.Integer price;
    @org.jetbrains.annotations.Nullable()
    @androidx.room.ColumnInfo(name = "product_name")
    @com.google.gson.annotations.SerializedName(value = "product_name")
    private java.lang.String productName;
    @org.jetbrains.annotations.Nullable()
    @androidx.room.ColumnInfo(name = "promo_code_id")
    @com.google.gson.annotations.SerializedName(value = "promo_code_id")
    private java.lang.Integer promoCodeId;
    @org.jetbrains.annotations.Nullable()
    @androidx.room.ColumnInfo(name = "quantity_available")
    @com.google.gson.annotations.SerializedName(value = "quantity_available")
    private java.lang.Integer quantityAvailable;
    @org.jetbrains.annotations.Nullable()
    @androidx.room.ColumnInfo(name = "status")
    @com.google.gson.annotations.SerializedName(value = "status")
    private java.lang.Integer status;
    @org.jetbrains.annotations.Nullable()
    @androidx.room.ColumnInfo(name = "sub_category_name")
    @com.google.gson.annotations.SerializedName(value = "sub_category_name")
    private java.lang.String subCategoryName;
    @org.jetbrains.annotations.Nullable()
    @androidx.room.ColumnInfo(name = "updated_at")
    @com.google.gson.annotations.SerializedName(value = "updated_at")
    private java.lang.String updatedAt;
    @org.jetbrains.annotations.Nullable()
    @androidx.room.ColumnInfo(name = "weight")
    @com.google.gson.annotations.SerializedName(value = "weight")
    private java.lang.Integer weight;
    @org.jetbrains.annotations.Nullable()
    @androidx.room.ColumnInfo(name = "vendor_id")
    @com.google.gson.annotations.SerializedName(value = "vendor_id")
    private java.lang.Integer vendorId;
    @org.jetbrains.annotations.Nullable()
    @androidx.room.ColumnInfo(name = "vendor_name")
    @com.google.gson.annotations.SerializedName(value = "vendor_name")
    private java.lang.String vendor_name;
    @org.jetbrains.annotations.Nullable()
    @androidx.room.ColumnInfo(name = "quantity")
    @com.google.gson.annotations.SerializedName(value = "quantity")
    private java.lang.Integer quantity;
    @org.jetbrains.annotations.Nullable()
    @androidx.room.ColumnInfo(name = "order_id")
    @com.google.gson.annotations.SerializedName(value = "order_id")
    private java.lang.Integer order_id;
    @androidx.room.ColumnInfo(name = "sales_tax")
    @com.google.gson.annotations.SerializedName(value = "sales_tax")
    private double sales_tax;
    @org.jetbrains.annotations.Nullable()
    @androidx.room.ColumnInfo(name = "fixed_discount")
    @com.google.gson.annotations.SerializedName(value = "fixed_discount")
    private java.lang.Integer fixed_discount;
    @org.jetbrains.annotations.Nullable()
    @androidx.room.ColumnInfo(name = "iss_discount")
    @com.google.gson.annotations.SerializedName(value = "iss_discount")
    private java.lang.Integer iss_discount;
    @org.jetbrains.annotations.Nullable()
    @androidx.room.ColumnInfo(name = "originalPrice")
    private java.lang.Integer originalPrice;
    @org.jetbrains.annotations.Nullable()
    @androidx.room.ColumnInfo(name = "discounted_price")
    @com.google.gson.annotations.SerializedName(value = "discounted_price")
    private java.lang.Integer discounted_price;
    @androidx.room.ColumnInfo(name = "isOutStock")
    private boolean isOutStock;
    public static final android.os.Parcelable.Creator CREATOR = null;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Long getId() {
        return null;
    }
    
    public final void setId(@org.jetbrains.annotations.Nullable()
    java.lang.Long p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getCategoryId() {
        return null;
    }
    
    public final void setCategoryId(@org.jetbrains.annotations.Nullable()
    java.lang.Integer p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getCategoryName() {
        return null;
    }
    
    public final void setCategoryName(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getCreatedAt() {
        return null;
    }
    
    public final void setCreatedAt(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getDescription() {
        return null;
    }
    
    public final void setDescription(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getProduct_id() {
        return null;
    }
    
    public final void setProduct_id(@org.jetbrains.annotations.Nullable()
    java.lang.Integer p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getImage() {
        return null;
    }
    
    public final void setImage(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getPrice() {
        return null;
    }
    
    public final void setPrice(@org.jetbrains.annotations.Nullable()
    java.lang.Integer p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getProductName() {
        return null;
    }
    
    public final void setProductName(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getPromoCodeId() {
        return null;
    }
    
    public final void setPromoCodeId(@org.jetbrains.annotations.Nullable()
    java.lang.Integer p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getQuantityAvailable() {
        return null;
    }
    
    public final void setQuantityAvailable(@org.jetbrains.annotations.Nullable()
    java.lang.Integer p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getStatus() {
        return null;
    }
    
    public final void setStatus(@org.jetbrains.annotations.Nullable()
    java.lang.Integer p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getSubCategoryName() {
        return null;
    }
    
    public final void setSubCategoryName(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getUpdatedAt() {
        return null;
    }
    
    public final void setUpdatedAt(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getWeight() {
        return null;
    }
    
    public final void setWeight(@org.jetbrains.annotations.Nullable()
    java.lang.Integer p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getVendorId() {
        return null;
    }
    
    public final void setVendorId(@org.jetbrains.annotations.Nullable()
    java.lang.Integer p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getVendor_name() {
        return null;
    }
    
    public final void setVendor_name(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getQuantity() {
        return null;
    }
    
    public final void setQuantity(@org.jetbrains.annotations.Nullable()
    java.lang.Integer p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getOrder_id() {
        return null;
    }
    
    public final void setOrder_id(@org.jetbrains.annotations.Nullable()
    java.lang.Integer p0) {
    }
    
    public final double getSales_tax() {
        return 0.0;
    }
    
    public final void setSales_tax(double p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getFixed_discount() {
        return null;
    }
    
    public final void setFixed_discount(@org.jetbrains.annotations.Nullable()
    java.lang.Integer p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getIss_discount() {
        return null;
    }
    
    public final void setIss_discount(@org.jetbrains.annotations.Nullable()
    java.lang.Integer p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getOriginalPrice() {
        return null;
    }
    
    public final void setOriginalPrice(@org.jetbrains.annotations.Nullable()
    java.lang.Integer p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getDiscounted_price() {
        return null;
    }
    
    public final void setDiscounted_price(@org.jetbrains.annotations.Nullable()
    java.lang.Integer p0) {
    }
    
    public final boolean isOutStock() {
        return false;
    }
    
    public final void setOutStock(boolean p0) {
    }
    
    public CartModel(@org.jetbrains.annotations.Nullable()
    java.lang.Long id, @org.jetbrains.annotations.Nullable()
    java.lang.Integer categoryId, @org.jetbrains.annotations.Nullable()
    java.lang.String categoryName, @org.jetbrains.annotations.Nullable()
    java.lang.String createdAt, @org.jetbrains.annotations.Nullable()
    java.lang.String description, @org.jetbrains.annotations.Nullable()
    java.lang.Integer product_id, @org.jetbrains.annotations.Nullable()
    java.lang.String image, @org.jetbrains.annotations.Nullable()
    java.lang.Integer price, @org.jetbrains.annotations.Nullable()
    java.lang.String productName, @org.jetbrains.annotations.Nullable()
    java.lang.Integer promoCodeId, @org.jetbrains.annotations.Nullable()
    java.lang.Integer quantityAvailable, @org.jetbrains.annotations.Nullable()
    java.lang.Integer status, @org.jetbrains.annotations.Nullable()
    java.lang.String subCategoryName, @org.jetbrains.annotations.Nullable()
    java.lang.String updatedAt, @org.jetbrains.annotations.Nullable()
    java.lang.Integer weight, @org.jetbrains.annotations.Nullable()
    java.lang.Integer vendorId, @org.jetbrains.annotations.Nullable()
    java.lang.String vendor_name, @org.jetbrains.annotations.Nullable()
    java.lang.Integer quantity, @org.jetbrains.annotations.Nullable()
    java.lang.Integer order_id, double sales_tax, @org.jetbrains.annotations.Nullable()
    java.lang.Integer fixed_discount, @org.jetbrains.annotations.Nullable()
    java.lang.Integer iss_discount, @org.jetbrains.annotations.Nullable()
    java.lang.Integer originalPrice, @org.jetbrains.annotations.Nullable()
    java.lang.Integer discounted_price, boolean isOutStock) {
        super();
    }
    
    @java.lang.Override()
    public int describeContents() {
        return 0;
    }
    
    @java.lang.Override()
    public void writeToParcel(@org.jetbrains.annotations.NotNull()
    android.os.Parcel parcel, int flags) {
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 3)
    public static final class Creator implements android.os.Parcelable.Creator {
        
        public Creator() {
            super();
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.Object[] newArray(int size) {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.Object createFromParcel(@org.jetbrains.annotations.NotNull()
        android.os.Parcel in) {
            return null;
        }
    }
}