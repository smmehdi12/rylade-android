package com.senarios.customer.models;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u0015\u0012\u000e\u0010\u0002\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\u0005R\u0019\u0010\u0002\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007\u00a8\u0006\b"}, d2 = {"Lcom/senarios/customer/models/CategoryListsModel;", "", "mainCategories", "", "Lcom/senarios/customer/models/MainCategoryModel;", "(Ljava/util/List;)V", "getMainCategories", "()Ljava/util/List;", "app_release"})
public final class CategoryListsModel {
    @org.jetbrains.annotations.Nullable()
    private final java.util.List<com.senarios.customer.models.MainCategoryModel> mainCategories = null;
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<com.senarios.customer.models.MainCategoryModel> getMainCategories() {
        return null;
    }
    
    public CategoryListsModel(@org.jetbrains.annotations.Nullable()
    java.util.List<com.senarios.customer.models.MainCategoryModel> mainCategories) {
        super();
    }
}