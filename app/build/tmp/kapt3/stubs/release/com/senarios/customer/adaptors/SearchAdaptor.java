package com.senarios.customer.adaptors;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\f\u0012\b\u0012\u00060\u0002R\u00020\u00000\u0001:\u0001\u0017B\u001b\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006\u00a2\u0006\u0002\u0010\bJ\b\u0010\r\u001a\u00020\u000eH\u0016J\u001c\u0010\u000f\u001a\u00020\u00102\n\u0010\u0011\u001a\u00060\u0002R\u00020\u00002\u0006\u0010\u0012\u001a\u00020\u000eH\u0016J\u001c\u0010\u0013\u001a\u00060\u0002R\u00020\u00002\u0006\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u000eH\u0016R\u0017\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0011\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\f\u00a8\u0006\u0018"}, d2 = {"Lcom/senarios/customer/adaptors/SearchAdaptor;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lcom/senarios/customer/adaptors/SearchAdaptor$Holder;", "ref", "Landroid/content/Context;", "list", "", "Lcom/senarios/customer/models/ProductModel;", "(Landroid/content/Context;Ljava/util/List;)V", "getList", "()Ljava/util/List;", "getRef", "()Landroid/content/Context;", "getItemCount", "", "onBindViewHolder", "", "holder", "position", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "Holder", "app_release"})
public final class SearchAdaptor extends androidx.recyclerview.widget.RecyclerView.Adapter<com.senarios.customer.adaptors.SearchAdaptor.Holder> {
    @org.jetbrains.annotations.NotNull()
    private final android.content.Context ref = null;
    @org.jetbrains.annotations.NotNull()
    private final java.util.List<com.senarios.customer.models.ProductModel> list = null;
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.senarios.customer.adaptors.SearchAdaptor.Holder onCreateViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup parent, int viewType) {
        return null;
    }
    
    @java.lang.Override()
    public int getItemCount() {
        return 0;
    }
    
    @java.lang.Override()
    public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
    com.senarios.customer.adaptors.SearchAdaptor.Holder holder, int position) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.content.Context getRef() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.senarios.customer.models.ProductModel> getList() {
        return null;
    }
    
    public SearchAdaptor(@org.jetbrains.annotations.NotNull()
    android.content.Context ref, @org.jetbrains.annotations.NotNull()
    java.util.List<com.senarios.customer.models.ProductModel> list) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\b\u0086\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\u0007"}, d2 = {"Lcom/senarios/customer/adaptors/SearchAdaptor$Holder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "binding", "Lcom/senarios/customer/databinding/ItemSearchProductBinding;", "(Lcom/senarios/customer/adaptors/SearchAdaptor;Lcom/senarios/customer/databinding/ItemSearchProductBinding;)V", "getBinding", "()Lcom/senarios/customer/databinding/ItemSearchProductBinding;", "app_release"})
    public final class Holder extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
        @org.jetbrains.annotations.NotNull()
        private final com.senarios.customer.databinding.ItemSearchProductBinding binding = null;
        
        @org.jetbrains.annotations.NotNull()
        public final com.senarios.customer.databinding.ItemSearchProductBinding getBinding() {
            return null;
        }
        
        public Holder(@org.jetbrains.annotations.NotNull()
        com.senarios.customer.databinding.ItemSearchProductBinding binding) {
            super(null);
        }
    }
}