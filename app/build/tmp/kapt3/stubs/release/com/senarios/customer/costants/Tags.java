package com.senarios.customer.costants;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\bf\u0018\u0000 \u00022\u00020\u0001:\u0001\u0002\u00a8\u0006\u0003"}, d2 = {"Lcom/senarios/customer/costants/Tags;", "", "Companion", "app_release"})
public abstract interface Tags {
    public static final com.senarios.customer.costants.Tags.Companion Companion = null;
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String ADDRESS = "Address";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String HOME = "Home Screen";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String LOGIN_SIGN_UP = "Login Signup Dialog Fragment";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String LOGIN = "Login Dialog Fragment";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String REGISTER = "Register Dialog Fragment";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String VERIFY_OTP = "Verify OTP Dialog Fragment";
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0006\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\n"}, d2 = {"Lcom/senarios/customer/costants/Tags$Companion;", "", "()V", "ADDRESS", "", "HOME", "LOGIN", "LOGIN_SIGN_UP", "REGISTER", "VERIFY_OTP", "app_release"})
    public static final class Companion {
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String ADDRESS = "Address";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String HOME = "Home Screen";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String LOGIN_SIGN_UP = "Login Signup Dialog Fragment";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String LOGIN = "Login Dialog Fragment";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String REGISTER = "Register Dialog Fragment";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String VERIFY_OTP = "Verify OTP Dialog Fragment";
        
        private Companion() {
            super();
        }
    }
}