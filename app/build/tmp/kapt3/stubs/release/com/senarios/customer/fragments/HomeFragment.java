package com.senarios.customer.fragments;

import java.lang.System;

/**
 * A simple [Fragment] subclass.
 */
@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000|\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\u0003\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u00032\u00020\u00042\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00070\u00060\u0005B\u0005\u00a2\u0006\u0002\u0010\bJ\b\u0010\u000b\u001a\u00020\fH\u0016J\u0018\u0010\r\u001a\u00020\f2\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u0011H\u0016J \u0010\u0012\u001a\u00020\f2\u0006\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u000f2\u0006\u0010\u0016\u001a\u00020\u0014H\u0016J\u0018\u0010\u0017\u001a\u00020\f2\u0006\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0018\u001a\u00020\u0019H\u0016J\u0018\u0010\u001a\u001a\u00020\f2\u0006\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0016\u001a\u00020\u0014H\u0016J.\u0010\u001b\u001a\u00020\f2\u0006\u0010\u0013\u001a\u00020\u00142\f\u0010\u001c\u001a\b\u0012\u0004\u0012\u00020\u001e0\u001d2\u0006\u0010\u001f\u001a\u00020\u00142\u0006\u0010\u0016\u001a\u00020\u0014H\u0016J&\u0010 \u001a\u00020\f2\u0006\u0010\u0013\u001a\u00020\u00142\f\u0010\u001c\u001a\b\u0012\u0004\u0012\u00020\u001e0\u001d2\u0006\u0010\u001f\u001a\u00020\u0014H\u0016J&\u0010!\u001a\u0004\u0018\u00010\"2\u0006\u0010#\u001a\u00020$2\b\u0010%\u001a\u0004\u0018\u00010&2\b\u0010\'\u001a\u0004\u0018\u00010(H\u0016J\u001c\u0010)\u001a\b\u0012\u0004\u0012\u00020*0\u00062\f\u0010\u001c\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006H\u0002J\u0006\u0010+\u001a\u00020\fJ\b\u0010,\u001a\u00020\fH\u0002J\u0018\u0010-\u001a\u00020\f2\u000e\u0010\u001c\u001a\n\u0012\u0004\u0012\u00020\u0007\u0018\u00010\u0006H\u0016J\u0012\u0010.\u001a\u00020\f2\b\u0010/\u001a\u0004\u0018\u00010\"H\u0016R\u000e\u0010\t\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u00060"}, d2 = {"Lcom/senarios/customer/fragments/HomeFragment;", "Lcom/senarios/customer/fragments/BaseFragment;", "Landroid/view/View$OnClickListener;", "Lcom/senarios/customer/retrofit/ApiResponse;", "Lcom/senarios/customer/adaptors/RecyclerViewCallback;", "Landroidx/lifecycle/Observer;", "", "Lcom/senarios/customer/models/MainCategoryModel;", "()V", "binding", "Lcom/senarios/customer/databinding/FragmentHomeBinding;", "OnBackPressed", "", "OnClick", "position", "", "any", "", "OnError", "endpoint", "", "code", "message", "OnException", "exception", "", "OnNetworkError", "OnStatusfalse", "t", "Lretrofit2/Response;", "Lcom/senarios/customer/models/ModelBaseResponse;", "response", "OnSuccess", "getFragmentView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "getModulesList", "Lcom/senarios/customer/models/Modules;", "init", "observeDB", "onChanged", "onClick", "v", "app_release"})
public final class HomeFragment extends com.senarios.customer.fragments.BaseFragment implements android.view.View.OnClickListener, com.senarios.customer.retrofit.ApiResponse, com.senarios.customer.adaptors.RecyclerViewCallback, androidx.lifecycle.Observer<java.util.List<com.senarios.customer.models.MainCategoryModel>> {
    private com.senarios.customer.databinding.FragmentHomeBinding binding;
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public android.view.View getFragmentView(@org.jetbrains.annotations.NotNull()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    public final void init() {
    }
    
    private final void observeDB() {
    }
    
    @java.lang.Override()
    public void OnBackPressed() {
    }
    
    @java.lang.Override()
    public void onClick(@org.jetbrains.annotations.Nullable()
    android.view.View v) {
    }
    
    @java.lang.Override()
    public void OnSuccess(@org.jetbrains.annotations.NotNull()
    java.lang.String endpoint, @org.jetbrains.annotations.NotNull()
    retrofit2.Response<com.senarios.customer.models.ModelBaseResponse> t, @org.jetbrains.annotations.NotNull()
    java.lang.String response) {
    }
    
    @java.lang.Override()
    public void OnStatusfalse(@org.jetbrains.annotations.NotNull()
    java.lang.String endpoint, @org.jetbrains.annotations.NotNull()
    retrofit2.Response<com.senarios.customer.models.ModelBaseResponse> t, @org.jetbrains.annotations.NotNull()
    java.lang.String response, @org.jetbrains.annotations.NotNull()
    java.lang.String message) {
    }
    
    @java.lang.Override()
    public void OnError(@org.jetbrains.annotations.NotNull()
    java.lang.String endpoint, int code, @org.jetbrains.annotations.NotNull()
    java.lang.String message) {
    }
    
    @java.lang.Override()
    public void OnException(@org.jetbrains.annotations.NotNull()
    java.lang.String endpoint, @org.jetbrains.annotations.NotNull()
    java.lang.Throwable exception) {
    }
    
    @java.lang.Override()
    public void OnNetworkError(@org.jetbrains.annotations.NotNull()
    java.lang.String endpoint, @org.jetbrains.annotations.NotNull()
    java.lang.String message) {
    }
    
    private final java.util.List<com.senarios.customer.models.Modules> getModulesList(java.util.List<com.senarios.customer.models.MainCategoryModel> t) {
        return null;
    }
    
    @java.lang.Override()
    public void onChanged(@org.jetbrains.annotations.Nullable()
    java.util.List<com.senarios.customer.models.MainCategoryModel> t) {
    }
    
    @java.lang.Override()
    public void OnClick(int position, @org.jetbrains.annotations.NotNull()
    java.lang.Object any) {
    }
    
    public HomeFragment() {
        super();
    }
    
    public void OnEdit(int position, @org.jetbrains.annotations.NotNull()
    java.lang.Object any) {
    }
    
    public void OnDelete(int position, @org.jetbrains.annotations.NotNull()
    java.lang.Object any) {
    }
    
    public void OnScroll() {
    }
    
    public void openGallery(int position) {
    }
    
    public void getPosition(int position) {
    }
    
    public void updateCartItem(int position, @org.jetbrains.annotations.NotNull()
    com.senarios.customer.models.CartModel cartModel, int quantity, boolean flag) {
    }
    
    public void setDefaultAddress(int position, @org.jetbrains.annotations.NotNull()
    com.senarios.customer.models.Address mAddress) {
    }
}