package com.senarios.customer.retrofit;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0018\u0002\n\u0002\b\u0015\n\u0002\u0010\b\n\u0002\b\b\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u001a\bf\u0018\u00002\u00020\u0001J(\u0010\u0002\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\u00040\u00032\b\b\u0001\u0010\u0006\u001a\u00020\u00072\b\b\u0001\u0010\b\u001a\u00020\tH\'J(\u0010\n\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\u00040\u00032\b\b\u0001\u0010\u0006\u001a\u00020\u00072\b\b\u0001\u0010\b\u001a\u00020\tH\'J(\u0010\u000b\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\u00040\u00032\b\b\u0001\u0010\u0006\u001a\u00020\u00072\b\b\u0001\u0010\b\u001a\u00020\tH\'J\u001e\u0010\f\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\u00040\u00032\b\b\u0001\u0010\b\u001a\u00020\tH\'J(\u0010\f\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\u00040\u00032\b\b\u0001\u0010\u0006\u001a\u00020\u00072\b\b\u0001\u0010\b\u001a\u00020\tH\'J\u001e\u0010\r\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\u00040\u00032\b\b\u0001\u0010\b\u001a\u00020\tH\'J\u001e\u0010\u000e\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\u00040\u00032\b\b\u0001\u0010\b\u001a\u00020\tH\'J(\u0010\u000f\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\u00040\u00032\b\b\u0001\u0010\u0006\u001a\u00020\u00072\b\b\u0001\u0010\b\u001a\u00020\tH\'J(\u0010\u0010\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\u00040\u00032\b\b\u0001\u0010\u0006\u001a\u00020\u00072\b\b\u0001\u0010\b\u001a\u00020\tH\'J(\u0010\u0011\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\u00040\u00032\b\b\u0001\u0010\u0006\u001a\u00020\u00072\b\b\u0001\u0010\b\u001a\u00020\tH\'J\u001e\u0010\u0012\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\u00040\u00032\b\b\u0001\u0010\b\u001a\u00020\tH\'J(\u0010\u0013\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\u00040\u00032\b\b\u0001\u0010\u0006\u001a\u00020\u00072\b\b\u0001\u0010\b\u001a\u00020\tH\'J\u0014\u0010\u0014\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\u00040\u0003H\'J<\u0010\u0015\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00160\u00040\u00032\b\b\u0001\u0010\u0017\u001a\u00020\u00072\b\b\u0001\u0010\u0018\u001a\u00020\u00072\b\b\u0001\u0010\u0019\u001a\u00020\u00072\b\b\u0001\u0010\u001a\u001a\u00020\u0007H\'J\u0014\u0010\u001b\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\u00040\u0003H\'J\u0014\u0010\u001c\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\u00040\u0003H\'J\u0014\u0010\u001d\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\u00040\u0003H\'J(\u0010\u001e\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\u00040\u00032\b\b\u0001\u0010\u0006\u001a\u00020\u00072\b\b\u0001\u0010\b\u001a\u00020\tH\'J(\u0010\u001f\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\u00040\u00032\b\b\u0001\u0010\u0006\u001a\u00020\u00072\b\b\u0001\u0010\b\u001a\u00020\tH\'J(\u0010 \u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\u00040\u00032\b\b\u0001\u0010\u0006\u001a\u00020\u00072\b\b\u0001\u0010\b\u001a\u00020\tH\'J\u001e\u0010!\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\u00040\u00032\b\b\u0001\u0010\b\u001a\u00020\tH\'J\u0014\u0010\"\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\u00040\u0003H\'J\u001e\u0010#\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\u00040\u00032\b\b\u0001\u0010\b\u001a\u00020\tH\'J\u001e\u0010$\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\u00040\u00032\b\b\u0001\u0010\b\u001a\u00020\tH\'J\u001e\u0010%\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\u00040\u00032\b\b\u0001\u0010\b\u001a\u00020\tH\'J\u001e\u0010&\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\u00040\u00032\b\b\u0001\u0010\b\u001a\u00020\tH\'J\u001e\u0010\'\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\u00040\u00032\b\b\u0001\u0010\b\u001a\u00020\tH\'J\u001e\u0010(\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\u00040\u00032\b\b\u0001\u0010\b\u001a\u00020\tH\'J(\u0010)\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\u00040\u00032\b\b\u0001\u0010\u0006\u001a\u00020\u00072\b\b\u0001\u0010\b\u001a\u00020\tH\'J\u0094\u0001\u0010*\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\u00040\u00032\b\b\u0001\u0010\u0006\u001a\u00020\u00072\b\b\u0001\u0010+\u001a\u00020,2\b\b\u0001\u0010-\u001a\u00020,2\b\b\u0001\u0010.\u001a\u00020\u00072\b\b\u0001\u0010/\u001a\u00020,2\n\b\u0001\u00100\u001a\u0004\u0018\u00010\u00072\n\b\u0001\u00101\u001a\u0004\u0018\u00010\u00072\b\b\u0001\u00102\u001a\u00020\u00072\b\b\u0001\u00103\u001a\u00020,2\b\b\u0001\u00104\u001a\u0002052\n\b\u0001\u00106\u001a\u0004\u0018\u00010\u00072\n\b\u0001\u00107\u001a\u0004\u0018\u00010\u0007H\'J\u00a0\u0001\u0010*\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\u00040\u00032\b\b\u0001\u0010\u0006\u001a\u00020\u00072\n\b\u0001\u00108\u001a\u0004\u0018\u0001092\b\b\u0001\u0010+\u001a\u00020,2\b\b\u0001\u0010-\u001a\u00020,2\b\b\u0001\u0010.\u001a\u00020\u00072\b\b\u0001\u0010/\u001a\u00020,2\n\b\u0001\u00100\u001a\u0004\u0018\u00010\u00072\n\b\u0001\u00101\u001a\u0004\u0018\u00010\u00072\b\b\u0001\u00102\u001a\u00020\u00072\b\b\u0001\u00103\u001a\u00020,2\b\b\u0001\u00104\u001a\u0002052\n\b\u0001\u00106\u001a\u0004\u0018\u00010\u00072\n\b\u0001\u00107\u001a\u0004\u0018\u00010\u0007H\'J\u00f0\u0001\u0010:\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\u00040\u00032\b\b\u0001\u0010\u0006\u001a\u00020\u00072\b\b\u0001\u00108\u001a\u0002092\b\b\u0001\u0010;\u001a\u00020,2\b\b\u0001\u0010<\u001a\u00020,2\b\b\u0001\u0010=\u001a\u00020\u00072\b\b\u0001\u0010>\u001a\u00020\u00072\b\b\u0001\u0010?\u001a\u00020\u00072\b\b\u0001\u0010@\u001a\u00020\u00072\b\b\u0001\u0010A\u001a\u00020\u00072\b\b\u0001\u0010B\u001a\u00020\u00072\b\b\u0001\u0010C\u001a\u00020\u00072\b\b\u0001\u0010D\u001a\u00020\u00072\b\b\u0001\u0010E\u001a\u00020\u00072\b\b\u0001\u0010F\u001a\u00020\u00072\b\b\u0001\u0010G\u001a\u00020\u00072\b\b\u0001\u0010H\u001a\u00020\u00072\b\b\u0001\u00106\u001a\u00020\u00072\b\b\u0001\u00107\u001a\u00020\u00072\b\b\u0001\u0010I\u001a\u00020\u00072\b\b\u0001\u0010J\u001a\u00020\u00072\b\b\u0001\u0010K\u001a\u00020\u00072\b\b\u0001\u00104\u001a\u000205H\'J(\u0010L\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\u00040\u00032\b\b\u0001\u0010\u0006\u001a\u00020\u00072\b\b\u0001\u0010\b\u001a\u00020\tH\'J(\u0010M\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\u00040\u00032\b\b\u0001\u0010\u0006\u001a\u00020\u00072\b\b\u0001\u0010\b\u001a\u00020\tH\'J\u001e\u0010N\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\u00040\u00032\b\b\u0001\u0010\b\u001a\u00020\tH\'J\u001e\u0010O\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\u00040\u00032\b\b\u0001\u0010\b\u001a\u00020\tH\'J\u001e\u0010P\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\u00040\u00032\b\b\u0001\u0010\b\u001a\u00020\tH\'J\u001e\u0010Q\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\u00040\u00032\b\b\u0001\u0010\b\u001a\u00020\tH\'J\u001e\u0010R\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\u00040\u00032\b\b\u0001\u0010\b\u001a\u00020\tH\'\u00a8\u0006S"}, d2 = {"Lcom/senarios/customer/retrofit/DataService;", "", "InsertReviews", "Lio/reactivex/Single;", "Lretrofit2/Response;", "Lcom/senarios/customer/models/ModelBaseResponse;", "token", "", "json", "Lcom/google/gson/JsonObject;", "addAddress", "cancelOrder", "changepassword", "checkMail", "checkPromo", "checkRideStatus", "deleteAddress", "editprofile", "forgetpassword", "getAddress", "getContactUS", "getDirectionAPI", "Lcom/senarios/customer/models/GoogleDistanceApiResponse;", "unit", "origin", "destination", "key", "getFAQ", "getInstant", "getMainCategories", "getOrderDetails", "getOrderProducts", "getOrders", "getReviewsFromUser", "getTAC", "getVendor", "getVendorCategories", "getVendorProducts", "getVendors", "login", "login_Signup_Facebook", "order_Ecommerce", "order_Medicine", "user_id", "", "id", "name", "quantity", "dose", "instructions", "type", "address_id", "instance", "", "orderDate", "orderTime", "file", "Lokhttp3/MultipartBody$Part;", "order_Parcel", "user_ID", "address_ID", "item_name", "item_Weight", "sender_name", "sender_phone", "sender_address", "sender_lat", "sender_long", "receiver_name", "receiver_phone", "receiver_address", "receiver_lat", "receiver_long", "manual_sender", "manual_receiver", "amount", "postRideOrder", "rateOrder", "register", "search", "sendOTP", "verifyUser", "verifymailcheckfromserver", "app_release"})
public abstract interface DataService {
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "signup")
    @retrofit2.http.Headers(value = {"Content-Type: application/json"})
    public abstract io.reactivex.Single<retrofit2.Response<com.senarios.customer.models.ModelBaseResponse>> register(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    com.google.gson.JsonObject json);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "login")
    @retrofit2.http.Headers(value = {"Content-Type: application/json"})
    public abstract io.reactivex.Single<retrofit2.Response<com.senarios.customer.models.ModelBaseResponse>> login(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    com.google.gson.JsonObject json);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "checkEmailExist")
    @retrofit2.http.Headers(value = {"Content-Type: application/json"})
    public abstract io.reactivex.Single<retrofit2.Response<com.senarios.customer.models.ModelBaseResponse>> checkMail(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    com.google.gson.JsonObject json);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "verifyOTP")
    @retrofit2.http.Headers(value = {"Content-Type: application/json"})
    public abstract io.reactivex.Single<retrofit2.Response<com.senarios.customer.models.ModelBaseResponse>> verifyUser(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    com.google.gson.JsonObject json);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "insertUserAddress")
    @retrofit2.http.Headers(value = {"Content-Type: application/json"})
    public abstract io.reactivex.Single<retrofit2.Response<com.senarios.customer.models.ModelBaseResponse>> addAddress(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String token, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    com.google.gson.JsonObject json);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "getUserAddress")
    @retrofit2.http.Headers(value = {"Content-Type: application/json"})
    public abstract io.reactivex.Single<retrofit2.Response<com.senarios.customer.models.ModelBaseResponse>> getAddress(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String token, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    com.google.gson.JsonObject json);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "deleteAddress")
    @retrofit2.http.Headers(value = {"Content-Type: application/json"})
    public abstract io.reactivex.Single<retrofit2.Response<com.senarios.customer.models.ModelBaseResponse>> deleteAddress(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String token, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    com.google.gson.JsonObject json);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "sendOTP")
    @retrofit2.http.Headers(value = {"Content-Type: application/json"})
    public abstract io.reactivex.Single<retrofit2.Response<com.senarios.customer.models.ModelBaseResponse>> sendOTP(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    com.google.gson.JsonObject json);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "updateProfile")
    @retrofit2.http.Headers(value = {"Content-Type: application/json"})
    public abstract io.reactivex.Single<retrofit2.Response<com.senarios.customer.models.ModelBaseResponse>> editprofile(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String token, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    com.google.gson.JsonObject json);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "updatePassword")
    @retrofit2.http.Headers(value = {"Content-Type: application/json"})
    public abstract io.reactivex.Single<retrofit2.Response<com.senarios.customer.models.ModelBaseResponse>> changepassword(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String token, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    com.google.gson.JsonObject json);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "facebook_login_signup")
    @retrofit2.http.Headers(value = {"Content-Type: application/json"})
    public abstract io.reactivex.Single<retrofit2.Response<com.senarios.customer.models.ModelBaseResponse>> login_Signup_Facebook(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    com.google.gson.JsonObject json);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "create_medicine_order")
    @retrofit2.http.Multipart()
    public abstract io.reactivex.Single<retrofit2.Response<com.senarios.customer.models.ModelBaseResponse>> order_Medicine(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String token, @org.jetbrains.annotations.Nullable()
    @retrofit2.http.Part()
    okhttp3.MultipartBody.Part file, @retrofit2.http.Query(value = "user_id")
    int user_id, @retrofit2.http.Query(value = "id")
    int id, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "name")
    java.lang.String name, @retrofit2.http.Query(value = "quantity")
    int quantity, @org.jetbrains.annotations.Nullable()
    @retrofit2.http.Query(value = "dose")
    java.lang.String dose, @org.jetbrains.annotations.Nullable()
    @retrofit2.http.Query(value = "instructions")
    java.lang.String instructions, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "type")
    java.lang.String type, @retrofit2.http.Query(value = "address_id")
    int address_id, @retrofit2.http.Query(value = "instance")
    boolean instance, @org.jetbrains.annotations.Nullable()
    @retrofit2.http.Query(value = "order_date")
    java.lang.String orderDate, @org.jetbrains.annotations.Nullable()
    @retrofit2.http.Query(value = "order_time")
    java.lang.String orderTime);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "create_medicine_order")
    public abstract io.reactivex.Single<retrofit2.Response<com.senarios.customer.models.ModelBaseResponse>> order_Medicine(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String token, @retrofit2.http.Query(value = "user_id")
    int user_id, @retrofit2.http.Query(value = "id")
    int id, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "name")
    java.lang.String name, @retrofit2.http.Query(value = "quantity")
    int quantity, @org.jetbrains.annotations.Nullable()
    @retrofit2.http.Query(value = "dose")
    java.lang.String dose, @org.jetbrains.annotations.Nullable()
    @retrofit2.http.Query(value = "instructions")
    java.lang.String instructions, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "type")
    java.lang.String type, @retrofit2.http.Query(value = "address_id")
    int address_id, @retrofit2.http.Query(value = "instance")
    boolean instance, @org.jetbrains.annotations.Nullable()
    @retrofit2.http.Query(value = "order_date")
    java.lang.String orderDate, @org.jetbrains.annotations.Nullable()
    @retrofit2.http.Query(value = "order_time")
    java.lang.String orderTime);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "create_parcel_order")
    @retrofit2.http.Multipart()
    public abstract io.reactivex.Single<retrofit2.Response<com.senarios.customer.models.ModelBaseResponse>> order_Parcel(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String token, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Part()
    okhttp3.MultipartBody.Part file, @retrofit2.http.Query(value = "user_id")
    int user_ID, @retrofit2.http.Query(value = "address_id")
    int address_ID, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "item_name")
    java.lang.String item_name, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "weight")
    java.lang.String item_Weight, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "senders_name")
    java.lang.String sender_name, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "senders_contact")
    java.lang.String sender_phone, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "senders_address")
    java.lang.String sender_address, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "senders_lat")
    java.lang.String sender_lat, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "senders_long")
    java.lang.String sender_long, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "receivers_name")
    java.lang.String receiver_name, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "receivers_contact")
    java.lang.String receiver_phone, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "receivers_address")
    java.lang.String receiver_address, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "receivers_lat")
    java.lang.String receiver_lat, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "receivers_long")
    java.lang.String receiver_long, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "order_date")
    java.lang.String orderDate, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "order_time")
    java.lang.String orderTime, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "manual_sender_address")
    java.lang.String manual_sender, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "manual_receiever_address")
    java.lang.String manual_receiver, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "amount")
    java.lang.String amount, @retrofit2.http.Query(value = "instance")
    boolean instance);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "distancematrix/json")
    public abstract io.reactivex.Single<retrofit2.Response<com.senarios.customer.models.GoogleDistanceApiResponse>> getDirectionAPI(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "units")
    java.lang.String unit, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "origins")
    java.lang.String origin, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "destinations")
    java.lang.String destination, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "key")
    java.lang.String key);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "getorders")
    @retrofit2.http.Headers(value = {"Content-Type: application/json"})
    public abstract io.reactivex.Single<retrofit2.Response<com.senarios.customer.models.ModelBaseResponse>> getOrders(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String token, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    com.google.gson.JsonObject json);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "getorderdetails")
    @retrofit2.http.Headers(value = {"Content-Type: application/json"})
    public abstract io.reactivex.Single<retrofit2.Response<com.senarios.customer.models.ModelBaseResponse>> getOrderDetails(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String token, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    com.google.gson.JsonObject json);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "create_ride_order")
    @retrofit2.http.Headers(value = {"Content-Type: application/json"})
    public abstract io.reactivex.Single<retrofit2.Response<com.senarios.customer.models.ModelBaseResponse>> postRideOrder(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String token, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    com.google.gson.JsonObject json);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "customer_cancel_order")
    @retrofit2.http.Headers(value = {"Content-Type: application/json"})
    public abstract io.reactivex.Single<retrofit2.Response<com.senarios.customer.models.ModelBaseResponse>> cancelOrder(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String token, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    com.google.gson.JsonObject json);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "rating")
    @retrofit2.http.Headers(value = {"Content-Type: application/json"})
    public abstract io.reactivex.Single<retrofit2.Response<com.senarios.customer.models.ModelBaseResponse>> rateOrder(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String token, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    com.google.gson.JsonObject json);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "get_contacts")
    public abstract io.reactivex.Single<retrofit2.Response<com.senarios.customer.models.ModelBaseResponse>> getContactUS();
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "get_faqs")
    public abstract io.reactivex.Single<retrofit2.Response<com.senarios.customer.models.ModelBaseResponse>> getFAQ();
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "get_tac")
    public abstract io.reactivex.Single<retrofit2.Response<com.senarios.customer.models.ModelBaseResponse>> getTAC();
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "check_user_ride_status")
    @retrofit2.http.Headers(value = {"Content-Type: application/json"})
    public abstract io.reactivex.Single<retrofit2.Response<com.senarios.customer.models.ModelBaseResponse>> checkRideStatus(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String token, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    com.google.gson.JsonObject json);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "get_main_categories")
    @retrofit2.http.Headers(value = {"Content-Type: application/json"})
    public abstract io.reactivex.Single<retrofit2.Response<com.senarios.customer.models.ModelBaseResponse>> getMainCategories();
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "get_vendor_by_id")
    @retrofit2.http.Headers(value = {"Content-Type: application/json"})
    public abstract io.reactivex.Single<retrofit2.Response<com.senarios.customer.models.ModelBaseResponse>> getVendors(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    com.google.gson.JsonObject json);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "send_facebook_otp")
    @retrofit2.http.Headers(value = {"Content-Type: application/json"})
    public abstract io.reactivex.Single<retrofit2.Response<com.senarios.customer.models.ModelBaseResponse>> verifymailcheckfromserver(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    com.google.gson.JsonObject json);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "sendOTP")
    @retrofit2.http.Headers(value = {"Content-Type: application/json"})
    public abstract io.reactivex.Single<retrofit2.Response<com.senarios.customer.models.ModelBaseResponse>> forgetpassword(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    com.google.gson.JsonObject json);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "forgot_password")
    @retrofit2.http.Headers(value = {"Content-Type: application/json"})
    public abstract io.reactivex.Single<retrofit2.Response<com.senarios.customer.models.ModelBaseResponse>> changepassword(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    com.google.gson.JsonObject json);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "get_categories")
    @retrofit2.http.Headers(value = {"Content-Type: application/json"})
    public abstract io.reactivex.Single<retrofit2.Response<com.senarios.customer.models.ModelBaseResponse>> getVendorCategories(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    com.google.gson.JsonObject json);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "get_products_by_category_id")
    @retrofit2.http.Headers(value = {"Content-Type: application/json"})
    public abstract io.reactivex.Single<retrofit2.Response<com.senarios.customer.models.ModelBaseResponse>> getVendorProducts(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    com.google.gson.JsonObject json);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "get_comments")
    @retrofit2.http.Headers(value = {"Content-Type: application/json"})
    public abstract io.reactivex.Single<retrofit2.Response<com.senarios.customer.models.ModelBaseResponse>> getReviewsFromUser(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    com.google.gson.JsonObject json);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "insert_comment")
    public abstract io.reactivex.Single<retrofit2.Response<com.senarios.customer.models.ModelBaseResponse>> InsertReviews(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String token, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    com.google.gson.JsonObject json);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "promo_code_check")
    public abstract io.reactivex.Single<retrofit2.Response<com.senarios.customer.models.ModelBaseResponse>> checkPromo(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    com.google.gson.JsonObject json);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "get_instances")
    public abstract io.reactivex.Single<retrofit2.Response<com.senarios.customer.models.ModelBaseResponse>> getInstant();
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "insert_ecommerce_order")
    @retrofit2.http.Headers(value = {"Content-Type: application/json"})
    public abstract io.reactivex.Single<retrofit2.Response<com.senarios.customer.models.ModelBaseResponse>> order_Ecommerce(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String token, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    com.google.gson.JsonObject json);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "search")
    @retrofit2.http.Headers(value = {"Content-Type: application/json"})
    public abstract io.reactivex.Single<retrofit2.Response<com.senarios.customer.models.ModelBaseResponse>> search(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    com.google.gson.JsonObject json);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "get_vendor")
    @retrofit2.http.Headers(value = {"Content-Type: application/json"})
    public abstract io.reactivex.Single<retrofit2.Response<com.senarios.customer.models.ModelBaseResponse>> getVendor(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    com.google.gson.JsonObject json);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "get_products_by_order_id")
    @retrofit2.http.Headers(value = {"Content-Type: application/json"})
    public abstract io.reactivex.Single<retrofit2.Response<com.senarios.customer.models.ModelBaseResponse>> getOrderProducts(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Header(value = "Authorization")
    java.lang.String token, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    com.google.gson.JsonObject json);
}