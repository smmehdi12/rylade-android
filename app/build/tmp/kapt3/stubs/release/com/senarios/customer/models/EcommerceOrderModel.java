package com.senarios.customer.models;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\u0006\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\u001c\n\u0002\u0010\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0006\u00103\u001a\u000204R&\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u00048G@FX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0006\u0010\u0007\"\u0004\b\b\u0010\tR&\u0010\u000b\u001a\u00020\n2\u0006\u0010\u0003\u001a\u00020\n8G@FX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\f\u0010\r\"\u0004\b\u000e\u0010\u000fR&\u0010\u0011\u001a\u00020\u00102\u0006\u0010\u0003\u001a\u00020\u00108G@FX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0012\u0010\u0013\"\u0004\b\u0014\u0010\u0015R\u001c\u0010\u0016\u001a\u00020\u00178GX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0016\u0010\u0018\"\u0004\b\u0019\u0010\u001aR&\u0010\u001b\u001a\u00020\u00102\u0006\u0010\u0003\u001a\u00020\u00108G@FX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001c\u0010\u0013\"\u0004\b\u001d\u0010\u0015R&\u0010\u001e\u001a\u00020\n2\u0006\u0010\u0003\u001a\u00020\n8G@FX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001f\u0010\r\"\u0004\b \u0010\u000fR&\u0010!\u001a\u00020\u00102\u0006\u0010\u0003\u001a\u00020\u00108G@FX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\"\u0010\u0013\"\u0004\b#\u0010\u0015R&\u0010$\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u00048G@FX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b%\u0010\u0007\"\u0004\b&\u0010\tR&\u0010\'\u001a\u00020\u00102\u0006\u0010\u0003\u001a\u00020\u00108G@FX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b(\u0010\u0013\"\u0004\b)\u0010\u0015R&\u0010*\u001a\u00020\u00102\u0006\u0010\u0003\u001a\u00020\u00108G@FX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b+\u0010\u0013\"\u0004\b,\u0010\u0015R&\u0010-\u001a\u00020\u00102\u0006\u0010\u0003\u001a\u00020\u00108G@FX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b.\u0010\u0013\"\u0004\b/\u0010\u0015R&\u00100\u001a\u00020\n2\u0006\u0010\u0003\u001a\u00020\n8G@FX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b1\u0010\r\"\u0004\b2\u0010\u000f\u00a8\u00065"}, d2 = {"Lcom/senarios/customer/models/EcommerceOrderModel;", "Landroidx/databinding/BaseObservable;", "()V", "value", "", "address", "getAddress", "()Ljava/lang/String;", "setAddress", "(Ljava/lang/String;)V", "", "addressID", "getAddressID", "()I", "setAddressID", "(I)V", "", "deliveryFee", "getDeliveryFee", "()D", "setDeliveryFee", "(D)V", "isPromo", "", "()Z", "setPromo", "(Z)V", "promoAmount", "getPromoAmount", "setPromoAmount", "promoID", "getPromoID", "setPromoID", "promoPercent", "getPromoPercent", "setPromoPercent", "promoTitle", "getPromoTitle", "setPromoTitle", "subTotal", "getSubTotal", "setSubTotal", "taxAmount", "getTaxAmount", "setTaxAmount", "totalAmount", "getTotalAmount", "setTotalAmount", "vendorID", "getVendorID", "setVendorID", "reset", "", "app_release"})
public final class EcommerceOrderModel extends androidx.databinding.BaseObservable {
    private double totalAmount = 0.0;
    private double subTotal = 0.0;
    private boolean isPromo = false;
    private int promoID = 0;
    private double promoAmount = 0.0;
    private int addressID = 0;
    private int vendorID = 0;
    @org.jetbrains.annotations.NotNull()
    private java.lang.String address = "";
    private double deliveryFee = 0.0;
    @org.jetbrains.annotations.NotNull()
    private java.lang.String promoTitle = "";
    private double taxAmount = 0.0;
    private double promoPercent = 0.0;
    
    @androidx.databinding.Bindable()
    public final double getTotalAmount() {
        return 0.0;
    }
    
    public final void setTotalAmount(double value) {
    }
    
    @androidx.databinding.Bindable()
    public final double getSubTotal() {
        return 0.0;
    }
    
    public final void setSubTotal(double value) {
    }
    
    @androidx.databinding.Bindable()
    public final boolean isPromo() {
        return false;
    }
    
    public final void setPromo(boolean p0) {
    }
    
    @androidx.databinding.Bindable()
    public final int getPromoID() {
        return 0;
    }
    
    public final void setPromoID(int value) {
    }
    
    @androidx.databinding.Bindable()
    public final double getPromoAmount() {
        return 0.0;
    }
    
    public final void setPromoAmount(double value) {
    }
    
    @androidx.databinding.Bindable()
    public final int getAddressID() {
        return 0;
    }
    
    public final void setAddressID(int value) {
    }
    
    @androidx.databinding.Bindable()
    public final int getVendorID() {
        return 0;
    }
    
    public final void setVendorID(int value) {
    }
    
    @org.jetbrains.annotations.NotNull()
    @androidx.databinding.Bindable()
    public final java.lang.String getAddress() {
        return null;
    }
    
    public final void setAddress(@org.jetbrains.annotations.NotNull()
    java.lang.String value) {
    }
    
    @androidx.databinding.Bindable()
    public final double getDeliveryFee() {
        return 0.0;
    }
    
    public final void setDeliveryFee(double value) {
    }
    
    @org.jetbrains.annotations.NotNull()
    @androidx.databinding.Bindable()
    public final java.lang.String getPromoTitle() {
        return null;
    }
    
    public final void setPromoTitle(@org.jetbrains.annotations.NotNull()
    java.lang.String value) {
    }
    
    @androidx.databinding.Bindable()
    public final double getTaxAmount() {
        return 0.0;
    }
    
    public final void setTaxAmount(double value) {
    }
    
    @androidx.databinding.Bindable()
    public final double getPromoPercent() {
        return 0.0;
    }
    
    public final void setPromoPercent(double value) {
    }
    
    public final void reset() {
    }
    
    public EcommerceOrderModel() {
        super();
    }
}