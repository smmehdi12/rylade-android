package com.senarios.customer.activities;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0092\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u00032\u00020\u00042\u00020\u00052\u00020\u0006B\u0005\u00a2\u0006\u0002\u0010\u0007J\b\u0010\u0017\u001a\u00020\u0018H\u0016J\u0010\u0010\u0019\u001a\u00020\u00182\u0006\u0010\u001a\u001a\u00020\u001bH\u0016J\u0018\u0010\u001c\u001a\u00020\u00182\u0006\u0010\u001d\u001a\u00020\u000f2\u0006\u0010\u001e\u001a\u00020\u001fH\u0016J\b\u0010 \u001a\u00020\u0018H\u0016J\b\u0010!\u001a\u00020\u0018H\u0016J\b\u0010\"\u001a\u00020\u0018H\u0016J\b\u0010#\u001a\u00020\u0018H\u0002J\b\u0010$\u001a\u00020\u0018H\u0002J\u0016\u0010%\u001a\u00020&2\f\u0010\'\u001a\b\u0012\u0004\u0012\u00020)0(H\u0002J\b\u0010*\u001a\u00020\u0018H\u0014J\"\u0010+\u001a\u00020\u00182\u0006\u0010,\u001a\u00020\u000f2\u0006\u0010-\u001a\u00020\u000f2\b\u0010.\u001a\u0004\u0018\u00010/H\u0014J\b\u00100\u001a\u00020\u0018H\u0016J\u001a\u00101\u001a\u00020\u00182\b\u00102\u001a\u0004\u0018\u0001032\u0006\u00104\u001a\u00020&H\u0016J\u0012\u00105\u001a\u00020\u00182\b\u00106\u001a\u0004\u0018\u000107H\u0016J*\u00108\u001a\u00020\u00182\b\u00109\u001a\u0004\u0018\u00010:2\u0006\u0010;\u001a\u00020\u000f2\u0006\u0010<\u001a\u00020\u000f2\u0006\u0010=\u001a\u00020\u000fH\u0016J\"\u0010>\u001a\u00020\u00182\b\u00109\u001a\u0004\u0018\u00010?2\u0006\u0010;\u001a\u00020\u000f2\u0006\u0010<\u001a\u00020\u000fH\u0016J\u0010\u0010@\u001a\u00020\u00182\u0006\u0010\u001d\u001a\u00020\u000fH\u0016J\b\u0010A\u001a\u00020\u0018H\u0016J\b\u0010B\u001a\u00020\u0018H\u0002J\b\u0010C\u001a\u00020\u0018H\u0002J\b\u0010D\u001a\u00020\u0018H\u0002J\u0010\u0010E\u001a\u00020\u00182\u0006\u0010F\u001a\u00020/H\u0002R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\rX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0010\u001a\u00020\tX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0011\u0010\u0012\"\u0004\b\u0013\u0010\u0014R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006G"}, d2 = {"Lcom/senarios/customer/activities/OrderMedicineActivity;", "Lcom/senarios/customer/activities/BaseAcivity;", "Landroid/view/View$OnClickListener;", "Landroid/widget/CompoundButton$OnCheckedChangeListener;", "Landroid/app/DatePickerDialog$OnDateSetListener;", "Landroid/app/TimePickerDialog$OnTimeSetListener;", "Lcom/senarios/customer/adaptors/RecyclerViewCallback;", "()V", "TAG", "", "adaptor", "Lcom/senarios/customer/adaptors/AdaptorMedicine;", "binding", "Lcom/senarios/customer/databinding/FragmentMedicineBinding;", "clickedLayoutID", "", "proper_date", "getProper_date", "()Ljava/lang/String;", "setProper_date", "(Ljava/lang/String;)V", "sharedVM", "Lcom/senarios/customer/viewmodel/SharedVM;", "OnCancelPermissioDialog", "", "OnChange", "fragment", "Lcom/senarios/rylad/BaseDialogeFragment;", "OnEdit", "position", "any", "", "OnPermissionApproved", "OnScroll", "OnTrigger", "ShowInstantDialog", "Show_Date_Picker", "checkList", "", "items", "", "Lcom/senarios/customer/models/MedicineModel;", "init", "onActivityResult", "requestCode", "resultCode", "data", "Landroid/content/Intent;", "onBackPressed", "onCheckedChanged", "buttonView", "Landroid/widget/CompoundButton;", "isChecked", "onClick", "v", "Landroid/view/View;", "onDateSet", "p0", "Landroid/widget/DatePicker;", "p1", "p2", "p3", "onTimeSet", "Landroid/widget/TimePicker;", "openGallery", "setBinding", "showAddressDialog", "showAgreedDialog", "showImageNotSelectedDialog", "showOrderDialog", "intent", "app_release"})
public final class OrderMedicineActivity extends com.senarios.customer.activities.BaseAcivity implements android.view.View.OnClickListener, android.widget.CompoundButton.OnCheckedChangeListener, android.app.DatePickerDialog.OnDateSetListener, android.app.TimePickerDialog.OnTimeSetListener, com.senarios.customer.adaptors.RecyclerViewCallback {
    private final java.lang.String TAG = null;
    private com.senarios.customer.databinding.FragmentMedicineBinding binding;
    private com.senarios.customer.viewmodel.SharedVM sharedVM;
    private int clickedLayoutID = -1;
    private com.senarios.customer.adaptors.AdaptorMedicine adaptor;
    @org.jetbrains.annotations.NotNull()
    private java.lang.String proper_date = "";
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getProper_date() {
        return null;
    }
    
    public final void setProper_date(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    @java.lang.Override()
    public void setBinding() {
    }
    
    @java.lang.Override()
    public void OnChange(@org.jetbrains.annotations.NotNull()
    com.senarios.rylad.BaseDialogeFragment fragment) {
    }
    
    @java.lang.Override()
    public void OnCancelPermissioDialog() {
    }
    
    @java.lang.Override()
    public void OnPermissionApproved() {
    }
    
    @java.lang.Override()
    public void OnTrigger() {
    }
    
    @java.lang.Override()
    protected void init() {
    }
    
    @java.lang.Override()
    public void onClick(@org.jetbrains.annotations.Nullable()
    android.view.View v) {
    }
    
    @java.lang.Override()
    public void onBackPressed() {
    }
    
    private final void showAgreedDialog() {
    }
    
    private final void ShowInstantDialog() {
    }
    
    private final void Show_Date_Picker() {
    }
    
    private final void showImageNotSelectedDialog() {
    }
    
    private final void showAddressDialog() {
    }
    
    private final boolean checkList(java.util.List<com.senarios.customer.models.MedicineModel> items) {
        return false;
    }
    
    @java.lang.Override()
    protected void onActivityResult(int requestCode, int resultCode, @org.jetbrains.annotations.Nullable()
    android.content.Intent data) {
    }
    
    private final void showOrderDialog(android.content.Intent intent) {
    }
    
    @java.lang.Override()
    public void OnEdit(int position, @org.jetbrains.annotations.NotNull()
    java.lang.Object any) {
    }
    
    @java.lang.Override()
    public void OnScroll() {
    }
    
    @java.lang.Override()
    public void openGallery(int position) {
    }
    
    @java.lang.Override()
    public void onCheckedChanged(@org.jetbrains.annotations.Nullable()
    android.widget.CompoundButton buttonView, boolean isChecked) {
    }
    
    @java.lang.Override()
    public void onDateSet(@org.jetbrains.annotations.Nullable()
    android.widget.DatePicker p0, int p1, int p2, int p3) {
    }
    
    @java.lang.Override()
    public void onTimeSet(@org.jetbrains.annotations.Nullable()
    android.widget.TimePicker p0, int p1, int p2) {
    }
    
    public OrderMedicineActivity() {
        super();
    }
    
    public void OnDelete(int position, @org.jetbrains.annotations.NotNull()
    java.lang.Object any) {
    }
    
    public void OnClick(int position, @org.jetbrains.annotations.NotNull()
    java.lang.Object any) {
    }
    
    public void getPosition(int position) {
    }
    
    public void updateCartItem(int position, @org.jetbrains.annotations.NotNull()
    com.senarios.customer.models.CartModel cartModel, int quantity, boolean flag) {
    }
    
    public void setDefaultAddress(int position, @org.jetbrains.annotations.NotNull()
    com.senarios.customer.models.Address mAddress) {
    }
}