package com.senarios.customer.dialogfragments;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0084\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u0003\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u00032\u00020\u0004B\u0005\u00a2\u0006\u0002\u0010\u0005J\b\u0010\f\u001a\u00020\rH\u0016J \u0010\u000e\u001a\u00020\r2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0010H\u0016J\u0018\u0010\u0014\u001a\u00020\r2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0015\u001a\u00020\u0016H\u0016J\u0018\u0010\u0017\u001a\u00020\r2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0013\u001a\u00020\u0010H\u0016J.\u0010\u0018\u001a\u00020\r2\u0006\u0010\u000f\u001a\u00020\u00102\f\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\u001b0\u001a2\u0006\u0010\u001c\u001a\u00020\u00102\u0006\u0010\u0013\u001a\u00020\u0010H\u0016J&\u0010\u001d\u001a\u00020\r2\u0006\u0010\u000f\u001a\u00020\u00102\f\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\u001b0\u001a2\u0006\u0010\u001c\u001a\u00020\u0010H\u0016J\b\u0010\u001e\u001a\u00020\rH\u0002J&\u0010\u001f\u001a\u0004\u0018\u00010 2\u0006\u0010!\u001a\u00020\"2\b\u0010#\u001a\u0004\u0018\u00010$2\b\u0010%\u001a\u0004\u0018\u00010&H\u0016J\b\u0010\'\u001a\u00020\rH\u0014J \u0010(\u001a\u00020)2\u0016\u0010*\u001a\u0012\u0012\u0004\u0012\u00020,0+j\b\u0012\u0004\u0012\u00020,`-H\u0002J\u0012\u0010.\u001a\u00020\r2\b\u0010/\u001a\u0004\u0018\u00010 H\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u00060"}, d2 = {"Lcom/senarios/customer/dialogfragments/RatingFragmentDialog;", "Lcom/senarios/rylad/BaseDialogeFragment;", "Landroid/view/View$OnClickListener;", "Lcom/senarios/customer/retrofit/ApiResponse;", "Lcom/senarios/customer/adaptors/RecyclerViewCallback;", "()V", "adaptor", "Lcom/senarios/customer/adaptors/ProductReviewAdapter;", "binding", "Lcom/senarios/customer/databinding/FragmentRatingDialogBinding;", "order", "Lcom/senarios/customer/models/OrderModel;", "OnBackPressed", "", "OnError", "endpoint", "", "code", "", "message", "OnException", "exception", "", "OnNetworkError", "OnStatusfalse", "t", "Lretrofit2/Response;", "Lcom/senarios/customer/models/ModelBaseResponse;", "response", "OnSuccess", "callApi", "getFragmentView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "init", "isComment", "", "list", "Ljava/util/ArrayList;", "Lcom/senarios/customer/models/ProductModel;", "Lkotlin/collections/ArrayList;", "onClick", "p0", "app_release"})
public final class RatingFragmentDialog extends com.senarios.rylad.BaseDialogeFragment implements android.view.View.OnClickListener, com.senarios.customer.retrofit.ApiResponse, com.senarios.customer.adaptors.RecyclerViewCallback {
    private com.senarios.customer.databinding.FragmentRatingDialogBinding binding;
    private com.senarios.customer.models.OrderModel order;
    private com.senarios.customer.adaptors.ProductReviewAdapter adaptor;
    private java.util.HashMap _$_findViewCache;
    
    @java.lang.Override()
    protected void init() {
    }
    
    private final void callApi() {
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public android.view.View getFragmentView(@org.jetbrains.annotations.NotNull()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    @java.lang.Override()
    public void OnBackPressed() {
    }
    
    @java.lang.Override()
    public void onClick(@org.jetbrains.annotations.Nullable()
    android.view.View p0) {
    }
    
    @java.lang.Override()
    public void OnSuccess(@org.jetbrains.annotations.NotNull()
    java.lang.String endpoint, @org.jetbrains.annotations.NotNull()
    retrofit2.Response<com.senarios.customer.models.ModelBaseResponse> t, @org.jetbrains.annotations.NotNull()
    java.lang.String response) {
    }
    
    @java.lang.Override()
    public void OnStatusfalse(@org.jetbrains.annotations.NotNull()
    java.lang.String endpoint, @org.jetbrains.annotations.NotNull()
    retrofit2.Response<com.senarios.customer.models.ModelBaseResponse> t, @org.jetbrains.annotations.NotNull()
    java.lang.String response, @org.jetbrains.annotations.NotNull()
    java.lang.String message) {
    }
    
    @java.lang.Override()
    public void OnError(@org.jetbrains.annotations.NotNull()
    java.lang.String endpoint, int code, @org.jetbrains.annotations.NotNull()
    java.lang.String message) {
    }
    
    @java.lang.Override()
    public void OnException(@org.jetbrains.annotations.NotNull()
    java.lang.String endpoint, @org.jetbrains.annotations.NotNull()
    java.lang.Throwable exception) {
    }
    
    @java.lang.Override()
    public void OnNetworkError(@org.jetbrains.annotations.NotNull()
    java.lang.String endpoint, @org.jetbrains.annotations.NotNull()
    java.lang.String message) {
    }
    
    private final boolean isComment(java.util.ArrayList<com.senarios.customer.models.ProductModel> list) {
        return false;
    }
    
    public RatingFragmentDialog() {
        super();
    }
    
    public void OnEdit(int position, @org.jetbrains.annotations.NotNull()
    java.lang.Object any) {
    }
    
    public void OnDelete(int position, @org.jetbrains.annotations.NotNull()
    java.lang.Object any) {
    }
    
    public void OnClick(int position, @org.jetbrains.annotations.NotNull()
    java.lang.Object any) {
    }
    
    public void OnScroll() {
    }
    
    public void openGallery(int position) {
    }
    
    public void getPosition(int position) {
    }
    
    public void updateCartItem(int position, @org.jetbrains.annotations.NotNull()
    com.senarios.customer.models.CartModel cartModel, int quantity, boolean flag) {
    }
    
    public void setDefaultAddress(int position, @org.jetbrains.annotations.NotNull()
    com.senarios.customer.models.Address mAddress) {
    }
}