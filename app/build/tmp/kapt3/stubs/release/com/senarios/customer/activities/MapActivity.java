package com.senarios.customer.activities;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u00c4\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0007\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0011\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0015\n\u0002\b\f\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u00032\u00020\u00042\u00020\u00052\u00020\u00062\u00020\u00072\u00020\bB\u0005\u00a2\u0006\u0002\u0010\tJ\b\u0010\u001b\u001a\u00020\u001cH\u0016J\b\u0010\u001d\u001a\u00020\u001cH\u0016J\b\u0010\u001e\u001a\u00020\u001cH\u0016J\b\u0010\u001f\u001a\u00020\u001cH\u0002J\u0012\u0010 \u001a\u00020\u001c2\b\u0010!\u001a\u0004\u0018\u00010\"H\u0016J*\u0010#\u001a\u00020\u001c2\b\u0010!\u001a\u0004\u0018\u00010$2\u0006\u0010%\u001a\u00020\u000b2\u0006\u0010&\u001a\u00020\u000b2\u0006\u0010\'\u001a\u00020\u000bH\u0016J\b\u0010(\u001a\u00020\u001cH\u0002J\b\u0010)\u001a\u00020\u001cH\u0002J\b\u0010*\u001a\u00020+H\u0002J\b\u0010,\u001a\u00020-H\u0002J\b\u0010.\u001a\u00020\u001cH\u0002J\b\u0010/\u001a\u00020\u001cH\u0014J\b\u00100\u001a\u00020\u001cH\u0002J\b\u00101\u001a\u000202H\u0002J\b\u00103\u001a\u000202H\u0002J\b\u00104\u001a\u000202H\u0002J\b\u00105\u001a\u00020\u001cH\u0002J\u0010\u00106\u001a\u00020\u001c2\u0006\u00107\u001a\u00020+H\u0002J\"\u00108\u001a\u00020\u001c2\u0006\u00109\u001a\u00020\u000b2\u0006\u0010:\u001a\u00020\u000b2\b\u0010;\u001a\u0004\u0018\u00010<H\u0014J\b\u0010=\u001a\u00020\u001cH\u0016J\b\u0010>\u001a\u00020\u001cH\u0016J\b\u0010?\u001a\u00020\u001cH\u0016J\u0012\u0010@\u001a\u00020\u001c2\b\u0010A\u001a\u0004\u0018\u00010BH\u0016J$\u0010C\u001a\u0002022\b\u0010A\u001a\u0004\u0018\u00010D2\u0006\u0010E\u001a\u00020\u000b2\b\u0010F\u001a\u0004\u0018\u00010GH\u0016J\u0012\u0010H\u001a\u00020\u001c2\b\u0010I\u001a\u0004\u0018\u00010\u001aH\u0016J\u0012\u0010J\u001a\u0002022\b\u0010K\u001a\u0004\u0018\u00010LH\u0016J-\u0010M\u001a\u00020\u001c2\u0006\u00109\u001a\u00020\u000b2\u000e\u0010N\u001a\n\u0012\u0006\b\u0001\u0012\u00020P0O2\u0006\u0010Q\u001a\u00020RH\u0016\u00a2\u0006\u0002\u0010SJ\b\u0010T\u001a\u00020\u001cH\u0014J\b\u0010U\u001a\u00020\u001cH\u0014J*\u0010V\u001a\u00020\u001c2\b\u0010!\u001a\u0004\u0018\u00010$2\u0006\u0010%\u001a\u00020\u000b2\u0006\u0010W\u001a\u00020\u000b2\u0006\u0010&\u001a\u00020\u000bH\u0016J\b\u0010X\u001a\u00020\u001cH\u0002J\b\u0010Y\u001a\u00020\u001cH\u0002J\u0010\u0010Z\u001a\u00020\u001c2\u0006\u0010[\u001a\u00020PH\u0002J\b\u0010\\\u001a\u00020\u001cH\u0016J\u0010\u0010]\u001a\u00020\u001c2\u0006\u0010^\u001a\u00020_H\u0016R\u000e\u0010\n\u001a\u00020\u000bX\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u000bX\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0018X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0019\u001a\u00020\u001aX\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006`"}, d2 = {"Lcom/senarios/customer/activities/MapActivity;", "Lcom/senarios/customer/activities/BaseAcivity;", "Lcom/google/android/gms/maps/GoogleMap$OnCameraIdleListener;", "Lcom/google/android/gms/maps/GoogleMap$OnCameraMoveListener;", "Landroidx/appcompat/widget/Toolbar$OnMenuItemClickListener;", "Landroid/widget/TextView$OnEditorActionListener;", "Landroid/view/View$OnClickListener;", "Landroid/text/TextWatcher;", "Lcom/google/android/gms/maps/OnMapReadyCallback;", "()V", "AUTOCOMPLETE_REQUEST_CODE", "", "REQUEST_CHECK_SETTINGS", "binding", "Lcom/senarios/customer/databinding/FragmentMapBinding;", "builder", "Lcom/google/android/gms/location/LocationSettingsRequest$Builder;", "currentZoom", "", "location", "Landroid/location/Location;", "locationCallback", "Lcom/google/android/gms/location/LocationCallback;", "locationRequest", "Lcom/google/android/gms/location/LocationRequest;", "map", "Lcom/google/android/gms/maps/GoogleMap;", "OnCancelPermissioDialog", "", "OnPermissionApproved", "OnTrigger", "activateButton", "afterTextChanged", "s", "Landroid/text/Editable;", "beforeTextChanged", "", "start", "count", "after", "createLocationRequest", "deactivateButton", "getCurrentLatLng", "Lcom/google/android/gms/maps/model/LatLng;", "getSearchToolbar", "Landroidx/appcompat/widget/Toolbar;", "hideSearch", "init", "initMap", "isLocation", "", "isLocationCallback", "isMap", "locationRequestTask", "moveCamera", "latlng", "onActivityResult", "requestCode", "resultCode", "data", "Landroid/content/Intent;", "onBackPressed", "onCameraIdle", "onCameraMove", "onClick", "v", "Landroid/view/View;", "onEditorAction", "Landroid/widget/TextView;", "actionId", "event", "Landroid/view/KeyEvent;", "onMapReady", "p0", "onMenuItemClick", "item", "Landroid/view/MenuItem;", "onRequestPermissionsResult", "permissions", "", "", "grantResults", "", "(I[Ljava/lang/String;[I)V", "onResume", "onStop", "onTextChanged", "before", "removeRequestLocation", "requestLocation", "search", "q", "setBinding", "setComponent", "component", "Lcom/senarios/customer/di/Component;", "app_release"})
public final class MapActivity extends com.senarios.customer.activities.BaseAcivity implements com.google.android.gms.maps.GoogleMap.OnCameraIdleListener, com.google.android.gms.maps.GoogleMap.OnCameraMoveListener, androidx.appcompat.widget.Toolbar.OnMenuItemClickListener, android.widget.TextView.OnEditorActionListener, android.view.View.OnClickListener, android.text.TextWatcher, com.google.android.gms.maps.OnMapReadyCallback {
    private final int REQUEST_CHECK_SETTINGS = 1000;
    private com.senarios.customer.databinding.FragmentMapBinding binding;
    private com.google.android.gms.maps.GoogleMap map;
    private com.google.android.gms.location.LocationRequest locationRequest;
    private com.google.android.gms.location.LocationCallback locationCallback;
    private android.location.Location location;
    private com.google.android.gms.location.LocationSettingsRequest.Builder builder;
    private final float currentZoom = 18.0F;
    private final int AUTOCOMPLETE_REQUEST_CODE = 1;
    private java.util.HashMap _$_findViewCache;
    
    @java.lang.Override()
    public void setComponent(@org.jetbrains.annotations.NotNull()
    com.senarios.customer.di.Component component) {
    }
    
    @java.lang.Override()
    public void setBinding() {
    }
    
    @java.lang.Override()
    protected void init() {
    }
    
    private final void initMap() {
    }
    
    @java.lang.Override()
    public void OnTrigger() {
    }
    
    @java.lang.Override()
    public void onClick(@org.jetbrains.annotations.Nullable()
    android.view.View v) {
    }
    
    @java.lang.Override()
    public void onMapReady(@org.jetbrains.annotations.Nullable()
    com.google.android.gms.maps.GoogleMap p0) {
    }
    
    private final void createLocationRequest() {
    }
    
    private final void locationRequestTask() {
    }
    
    private final boolean isLocation() {
        return false;
    }
    
    @java.lang.Override()
    protected void onResume() {
    }
    
    @java.lang.Override()
    protected void onStop() {
    }
    
    private final void requestLocation() {
    }
    
    private final void removeRequestLocation() {
    }
    
    @java.lang.Override()
    protected void onActivityResult(int requestCode, int resultCode, @org.jetbrains.annotations.Nullable()
    android.content.Intent data) {
    }
    
    private final void moveCamera(com.google.android.gms.maps.model.LatLng latlng) {
    }
    
    @java.lang.Override()
    public void onRequestPermissionsResult(int requestCode, @org.jetbrains.annotations.NotNull()
    java.lang.String[] permissions, @org.jetbrains.annotations.NotNull()
    int[] grantResults) {
    }
    
    @java.lang.Override()
    public void OnCancelPermissioDialog() {
    }
    
    @java.lang.Override()
    public void OnPermissionApproved() {
    }
    
    private final boolean isMap() {
        return false;
    }
    
    private final boolean isLocationCallback() {
        return false;
    }
    
    @java.lang.Override()
    public void afterTextChanged(@org.jetbrains.annotations.Nullable()
    android.text.Editable s) {
    }
    
    @java.lang.Override()
    public void beforeTextChanged(@org.jetbrains.annotations.Nullable()
    java.lang.CharSequence s, int start, int count, int after) {
    }
    
    @java.lang.Override()
    public void onTextChanged(@org.jetbrains.annotations.Nullable()
    java.lang.CharSequence s, int start, int before, int count) {
    }
    
    private final void search(java.lang.String q) {
    }
    
    private final com.google.android.gms.maps.model.LatLng getCurrentLatLng() {
        return null;
    }
    
    @java.lang.Override()
    public boolean onEditorAction(@org.jetbrains.annotations.Nullable()
    android.widget.TextView v, int actionId, @org.jetbrains.annotations.Nullable()
    android.view.KeyEvent event) {
        return false;
    }
    
    private final void hideSearch() {
    }
    
    @java.lang.Override()
    public boolean onMenuItemClick(@org.jetbrains.annotations.Nullable()
    android.view.MenuItem item) {
        return false;
    }
    
    private final androidx.appcompat.widget.Toolbar getSearchToolbar() {
        return null;
    }
    
    @java.lang.Override()
    public void onBackPressed() {
    }
    
    @java.lang.Override()
    public void onCameraMove() {
    }
    
    @java.lang.Override()
    public void onCameraIdle() {
    }
    
    private final void activateButton() {
    }
    
    private final void deactivateButton() {
    }
    
    public MapActivity() {
        super();
    }
}