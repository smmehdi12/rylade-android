package com.senarios.customer.retrofit;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\u0018\u0000 \u00032\u00020\u0001:\u0001\u0003B\u0005\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0004"}, d2 = {"Lcom/senarios/customer/retrofit/NetworkGoogleCall;", "", "()V", "Companion", "app_release"})
public final class NetworkGoogleCall {
    private static final java.lang.String TAG = null;
    public static final com.senarios.customer.retrofit.NetworkGoogleCall.Companion Companion = null;
    
    public NetworkGoogleCall() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J:\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0012\u0010\t\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\f0\u000b0\n2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0004R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0012"}, d2 = {"Lcom/senarios/customer/retrofit/NetworkGoogleCall$Companion;", "", "()V", "TAG", "", "callAPI", "", "context", "Landroid/content/Context;", "single", "Lio/reactivex/Single;", "Lretrofit2/Response;", "Lcom/senarios/customer/models/GoogleDistanceApiResponse;", "api", "Lcom/senarios/customer/retrofit/ApiGoogleResponse;", "isDialog", "", "endpoint", "app_release"})
    public static final class Companion {
        
        public final void callAPI(@org.jetbrains.annotations.NotNull()
        android.content.Context context, @org.jetbrains.annotations.NotNull()
        io.reactivex.Single<retrofit2.Response<com.senarios.customer.models.GoogleDistanceApiResponse>> single, @org.jetbrains.annotations.NotNull()
        com.senarios.customer.retrofit.ApiGoogleResponse api, boolean isDialog, @org.jetbrains.annotations.NotNull()
        java.lang.String endpoint) {
        }
        
        private Companion() {
            super();
        }
    }
}