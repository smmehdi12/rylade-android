package com.senarios.customer.models;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u0013\u0012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0002\u0010\u0005R\u001c\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u00038\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007\u00a8\u0006\b"}, d2 = {"Lcom/senarios/customer/models/Rows;", "", "elements", "", "Lcom/senarios/customer/models/Elements;", "(Ljava/util/List;)V", "getElements", "()Ljava/util/List;", "app_release"})
public final class Rows {
    @org.jetbrains.annotations.NotNull()
    @com.google.gson.annotations.SerializedName(value = "elements")
    private final java.util.List<com.senarios.customer.models.Elements> elements = null;
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.senarios.customer.models.Elements> getElements() {
        return null;
    }
    
    public Rows(@org.jetbrains.annotations.NotNull()
    java.util.List<com.senarios.customer.models.Elements> elements) {
        super();
    }
}