package com.senarios.customer.retrofit;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\bf\u0018\u0000 \u00022\u00020\u0001:\u0001\u0002\u00a8\u0006\u0003"}, d2 = {"Lcom/senarios/customer/retrofit/APIConstants;", "", "Companion", "app_release"})
public abstract interface APIConstants {
    public static final com.senarios.customer.retrofit.APIConstants.Companion Companion = null;
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String BASE_GOOGLE = "https://maps.googleapis.com/maps/api/";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String BASE_URL = "https://rylade.com/api/auth/";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String BASE_PARCEL_IMAGE = "https://rylade.com/parcel_orders/";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String BASE_MEDICINE_IMAGE = "https://rylade.com/medicine_prescriptions/";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String BASE_PROFILE_IMAGE = "https://rylade.com/";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String BASE_VENDOR_PHOTO = "https://rylade.com/attachments/";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String BASE_PRODUCT_PHOTO = "https://rylade.com/product_images/";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String REGISTER = "signup";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String LOGIN = "login";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String VERIFY_OTP = "verifyOTP";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String CHECKMAIL = "checkEmailExist";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String ADD_ADDRESS = "insertUserAddress";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String GET_ADDRESS = "getUserAddress";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String DELETE_ADDRESS = "deleteAddress";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String SENDOTP = "sendOTP";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String EDITPROFILE = "updateProfile";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String CHANGEPASSWORD = "updatePassword";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String FACEBOOKLOGIN = "facebook_login_signup";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String ORDER_MEDICINE = "create_medicine_order";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String ORDER_PARCEL = "create_parcel_order";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String GET_ORDER = "getorders";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String GET_ORDER_DETAIl = "getorderdetails";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String REQUEST_RIDE = "create_ride_order";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String CANCEL_ORDER = "customer_cancel_order";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String RATING = "rating";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String GET_REVIEWS = "get_comments";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String INSERT_COMMENT = "insert_comment";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String GET_CONTACT_US = "get_contacts";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String GET_FAQ = "get_faqs";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String GET_TAC = "get_tac";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String CHECK_RIDE_STATUS = "check_user_ride_status";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String GET_MAIN_CATEGORIES = "get_main_categories";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String GET_VENDORS = "get_vendor_by_id";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String GET_VENDOR_CATEGORIES = "get_categories";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String GET_OTP_FROM = "send_facebook_otp";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String CONFIRM_PASSWORD = "forgot_password";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String GET_VENDOR_PRODUCTS = "get_products_by_category_id";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String CHECK_PROMO = "promo_code_check";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String ORDER_ECOMMERCE = "insert_ecommerce_order";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String GET_INSTANT = "get_instances";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String SEARCH = "search";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String GET_VENDOR = "get_vendor";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String GET_PRODUCTS_BY_ORDER_ID = "get_products_by_order_id";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String DIRECTION = "distancematrix/json";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String CONTENT_TYPE = "Content-Type: application/json";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String AUTH = "Authorization";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String BEARER = "Bearer ";
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b/\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0019\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001b\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001c\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001d\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001e\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001f\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010 \u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010!\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\"\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010#\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010$\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010%\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010&\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\'\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010(\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010)\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010*\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010+\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010,\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010-\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010.\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010/\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u00100\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u00101\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u00102\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u00063"}, d2 = {"Lcom/senarios/customer/retrofit/APIConstants$Companion;", "", "()V", "ADD_ADDRESS", "", "AUTH", "BASE_GOOGLE", "BASE_MEDICINE_IMAGE", "BASE_PARCEL_IMAGE", "BASE_PRODUCT_PHOTO", "BASE_PROFILE_IMAGE", "BASE_URL", "BASE_VENDOR_PHOTO", "BEARER", "CANCEL_ORDER", "CHANGEPASSWORD", "CHECKMAIL", "CHECK_PROMO", "CHECK_RIDE_STATUS", "CONFIRM_PASSWORD", "CONTENT_TYPE", "DELETE_ADDRESS", "DIRECTION", "EDITPROFILE", "FACEBOOKLOGIN", "GET_ADDRESS", "GET_CONTACT_US", "GET_FAQ", "GET_INSTANT", "GET_MAIN_CATEGORIES", "GET_ORDER", "GET_ORDER_DETAIl", "GET_OTP_FROM", "GET_PRODUCTS_BY_ORDER_ID", "GET_REVIEWS", "GET_TAC", "GET_VENDOR", "GET_VENDORS", "GET_VENDOR_CATEGORIES", "GET_VENDOR_PRODUCTS", "INSERT_COMMENT", "LOGIN", "ORDER_ECOMMERCE", "ORDER_MEDICINE", "ORDER_PARCEL", "RATING", "REGISTER", "REQUEST_RIDE", "SEARCH", "SENDOTP", "VERIFY_OTP", "app_release"})
    public static final class Companion {
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String BASE_GOOGLE = "https://maps.googleapis.com/maps/api/";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String BASE_URL = "https://rylade.com/api/auth/";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String BASE_PARCEL_IMAGE = "https://rylade.com/parcel_orders/";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String BASE_MEDICINE_IMAGE = "https://rylade.com/medicine_prescriptions/";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String BASE_PROFILE_IMAGE = "https://rylade.com/";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String BASE_VENDOR_PHOTO = "https://rylade.com/attachments/";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String BASE_PRODUCT_PHOTO = "https://rylade.com/product_images/";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String REGISTER = "signup";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String LOGIN = "login";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String VERIFY_OTP = "verifyOTP";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String CHECKMAIL = "checkEmailExist";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String ADD_ADDRESS = "insertUserAddress";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String GET_ADDRESS = "getUserAddress";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String DELETE_ADDRESS = "deleteAddress";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String SENDOTP = "sendOTP";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String EDITPROFILE = "updateProfile";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String CHANGEPASSWORD = "updatePassword";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String FACEBOOKLOGIN = "facebook_login_signup";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String ORDER_MEDICINE = "create_medicine_order";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String ORDER_PARCEL = "create_parcel_order";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String GET_ORDER = "getorders";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String GET_ORDER_DETAIl = "getorderdetails";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String REQUEST_RIDE = "create_ride_order";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String CANCEL_ORDER = "customer_cancel_order";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String RATING = "rating";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String GET_REVIEWS = "get_comments";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String INSERT_COMMENT = "insert_comment";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String GET_CONTACT_US = "get_contacts";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String GET_FAQ = "get_faqs";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String GET_TAC = "get_tac";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String CHECK_RIDE_STATUS = "check_user_ride_status";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String GET_MAIN_CATEGORIES = "get_main_categories";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String GET_VENDORS = "get_vendor_by_id";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String GET_VENDOR_CATEGORIES = "get_categories";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String GET_OTP_FROM = "send_facebook_otp";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String CONFIRM_PASSWORD = "forgot_password";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String GET_VENDOR_PRODUCTS = "get_products_by_category_id";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String CHECK_PROMO = "promo_code_check";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String ORDER_ECOMMERCE = "insert_ecommerce_order";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String GET_INSTANT = "get_instances";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String SEARCH = "search";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String GET_VENDOR = "get_vendor";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String GET_PRODUCTS_BY_ORDER_ID = "get_products_by_order_id";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String DIRECTION = "distancematrix/json";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String CONTENT_TYPE = "Content-Type: application/json";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String AUTH = "Authorization";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String BEARER = "Bearer ";
        
        private Companion() {
            super();
        }
    }
}