package com.senarios.customer.db;

import java.lang.System;

@androidx.room.Dao()
@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000`\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0010\t\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u000e\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0003\b\'\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0016\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010\u0006\u001a\u00020\u0007H\'J\b\u0010\b\u001a\u00020\tH\'J\b\u0010\n\u001a\u00020\tH\'J\b\u0010\u000b\u001a\u00020\tH\'J\b\u0010\f\u001a\u00020\tH\'J\b\u0010\r\u001a\u00020\tH\'J\u0010\u0010\u000e\u001a\u00020\t2\u0006\u0010\u0006\u001a\u00020\u000fH\'J\u0018\u0010\u0010\u001a\u00020\t2\u0006\u0010\u0011\u001a\u00020\u00072\u0006\u0010\u0012\u001a\u00020\u0007H\'J\u0010\u0010\u0013\u001a\u00020\t2\u0006\u0010\u0012\u001a\u00020\u0007H\'J\u0010\u0010\u0014\u001a\u00020\t2\u0006\u0010\u0011\u001a\u00020\u0007H\'J\u000e\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004H\'J\u0014\u0010\u0016\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00180\u00040\u0017H\'J\u000e\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\u00180\u0004H\'J\u0014\u0010\u001a\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u001b0\u00040\u0017H\'J$\u0010\u001c\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\u00040\u00172\u0006\u0010\u0011\u001a\u00020\u00072\u0006\u0010\u0012\u001a\u00020\u0007H\'J\u001c\u0010\u001d\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u001e0\u00040\u00172\u0006\u0010\u001f\u001a\u00020\u0007H\'J\u0016\u0010 \u001a\b\u0012\u0004\u0012\u00020\u001e0\u00042\u0006\u0010\u0012\u001a\u00020\u0007H\'J\u001c\u0010!\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\"0\u00040\u00172\u0006\u0010\u0012\u001a\u00020\u0007H\'J\u0016\u0010#\u001a\b\u0012\u0004\u0012\u00020\u00070\u00172\u0006\u0010\u0012\u001a\u00020\u0007H\'J\b\u0010$\u001a\u00020\u000fH\'J\u0010\u0010%\u001a\u00020\t2\u0006\u0010&\u001a\u00020\u0005H\'J\u0010\u0010\'\u001a\u00020\u000f2\u0006\u0010(\u001a\u00020\u0018H\'J\u0016\u0010)\u001a\u00020\t2\f\u0010*\u001a\b\u0012\u0004\u0012\u00020\u001b0\u0004H\'J\u0010\u0010+\u001a\u00020\t2\u0006\u0010(\u001a\u00020\u001eH\'J\u0016\u0010,\u001a\u00020\t2\f\u0010*\u001a\b\u0012\u0004\u0012\u00020\"0\u0004H\'J\u0016\u0010-\u001a\u00020\t2\f\u0010*\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004H\'J\u0016\u0010.\u001a\u00020\t2\f\u0010*\u001a\b\u0012\u0004\u0012\u00020\u001e0\u0004H\'J\u0016\u0010/\u001a\b\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u00100\u001a\u000201H\'J\u0014\u00102\u001a\u00020\t2\f\u00103\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004J\u0018\u00104\u001a\u00020\t2\u0006\u0010\u0006\u001a\u00020\u000f2\u0006\u00105\u001a\u000206H\'J\u0018\u00107\u001a\u00020\t2\u0006\u0010\u0006\u001a\u00020\u000f2\u0006\u00108\u001a\u00020\u0007H\'\u00a8\u00069"}, d2 = {"Lcom/senarios/customer/db/DAO;", "", "()V", "checkProductExist", "", "Lcom/senarios/customer/models/ProductModel;", "id", "", "clearCartTable", "", "clearMainCategoryTable", "clearProductsTable", "clearVendorCategoryTable", "clearVendorTable", "deleteCartItem", "", "deleteProducts", "category_id", "vendor_id", "deleteVendorCategory", "deleteVendors", "getAllProducts", "getCart", "Landroidx/lifecycle/LiveData;", "Lcom/senarios/customer/models/CartModel;", "getCartItems", "getMainCategories", "Lcom/senarios/customer/models/MainCategoryModel;", "getProducts", "getVendor", "Lcom/senarios/customer/models/Vendor;", "categoryid", "getVendorByID", "getVendorCategory", "Lcom/senarios/customer/models/VendorCategoryModel;", "getVendorDeliveryStatus", "getVendorIDFromCart", "inserProduct", "product", "insertCart", "model", "insertMainCategories", "list", "insertVendor", "insertVendorCategories", "insertVendorProducts", "insertVendors", "searchProducts", "product_name", "", "updateCart", "products", "updateOutOfOrder", "isOutStock", "", "updateQuantity", "quantity", "app_release"})
public abstract class DAO {
    
    @androidx.room.Insert(onConflict = androidx.room.OnConflictStrategy.REPLACE)
    public abstract void insertMainCategories(@org.jetbrains.annotations.NotNull()
    java.util.List<com.senarios.customer.models.MainCategoryModel> list);
    
    @androidx.room.Insert(onConflict = androidx.room.OnConflictStrategy.REPLACE)
    public abstract void insertVendors(@org.jetbrains.annotations.NotNull()
    java.util.List<com.senarios.customer.models.Vendor> list);
    
    @androidx.room.Insert(onConflict = androidx.room.OnConflictStrategy.REPLACE)
    public abstract void insertVendor(@org.jetbrains.annotations.NotNull()
    com.senarios.customer.models.Vendor model);
    
    @androidx.room.Insert(onConflict = androidx.room.OnConflictStrategy.REPLACE)
    public abstract void insertVendorCategories(@org.jetbrains.annotations.NotNull()
    java.util.List<com.senarios.customer.models.VendorCategoryModel> list);
    
    @androidx.room.Insert(onConflict = androidx.room.OnConflictStrategy.REPLACE)
    public abstract void insertVendorProducts(@org.jetbrains.annotations.NotNull()
    java.util.List<com.senarios.customer.models.ProductModel> list);
    
    @androidx.room.Insert(onConflict = androidx.room.OnConflictStrategy.REPLACE)
    public abstract void inserProduct(@org.jetbrains.annotations.NotNull()
    com.senarios.customer.models.ProductModel product);
    
    @androidx.room.Insert(onConflict = androidx.room.OnConflictStrategy.REPLACE)
    public abstract long insertCart(@org.jetbrains.annotations.NotNull()
    com.senarios.customer.models.CartModel model);
    
    @org.jetbrains.annotations.NotNull()
    @androidx.room.Query(value = "select * from MainCategory")
    public abstract androidx.lifecycle.LiveData<java.util.List<com.senarios.customer.models.MainCategoryModel>> getMainCategories();
    
    @org.jetbrains.annotations.NotNull()
    @androidx.room.Query(value = "select * from Vendor where category_id=:categoryid AND user_status==1")
    public abstract androidx.lifecycle.LiveData<java.util.List<com.senarios.customer.models.Vendor>> getVendor(int categoryid);
    
    @org.jetbrains.annotations.NotNull()
    @androidx.room.Query(value = "select is_delivery from Vendor where id=:vendor_id")
    public abstract androidx.lifecycle.LiveData<java.lang.Integer> getVendorDeliveryStatus(int vendor_id);
    
    @org.jetbrains.annotations.NotNull()
    @androidx.room.Query(value = "select * from VendorCategory where vendor_id=:vendor_id")
    public abstract androidx.lifecycle.LiveData<java.util.List<com.senarios.customer.models.VendorCategoryModel>> getVendorCategory(int vendor_id);
    
    @org.jetbrains.annotations.NotNull()
    @androidx.room.Query(value = "select * from Products where category_id=:category_id AND vendor_id=:vendor_id AND status==1")
    public abstract androidx.lifecycle.LiveData<java.util.List<com.senarios.customer.models.ProductModel>> getProducts(int category_id, int vendor_id);
    
    @org.jetbrains.annotations.NotNull()
    @androidx.room.Query(value = "select * from Cart")
    public abstract androidx.lifecycle.LiveData<java.util.List<com.senarios.customer.models.CartModel>> getCart();
    
    @org.jetbrains.annotations.NotNull()
    @androidx.room.Query(value = "select * from Cart")
    public abstract java.util.List<com.senarios.customer.models.CartModel> getCartItems();
    
    @org.jetbrains.annotations.NotNull()
    @androidx.room.Query(value = "select * from products where lower(product_name) LIKE lower(:product_name)")
    public abstract java.util.List<com.senarios.customer.models.ProductModel> searchProducts(@org.jetbrains.annotations.NotNull()
    java.lang.String product_name);
    
    @org.jetbrains.annotations.NotNull()
    @androidx.room.Query(value = "select * from products")
    public abstract java.util.List<com.senarios.customer.models.ProductModel> getAllProducts();
    
    @org.jetbrains.annotations.NotNull()
    @androidx.room.Query(value = "select * from products where id =:id")
    public abstract java.util.List<com.senarios.customer.models.ProductModel> checkProductExist(int id);
    
    @androidx.room.Query(value = "delete from MainCategory")
    public abstract void clearMainCategoryTable();
    
    @androidx.room.Query(value = "delete from Vendor")
    public abstract void clearVendorTable();
    
    @androidx.room.Query(value = "delete from VendorCategory")
    public abstract void clearVendorCategoryTable();
    
    @androidx.room.Query(value = "delete from Products")
    public abstract void clearProductsTable();
    
    @androidx.room.Query(value = "delete from Cart")
    public abstract void clearCartTable();
    
    @androidx.room.Query(value = "update cart set quantity=:quantity where id=:id")
    public abstract void updateQuantity(long id, int quantity);
    
    @androidx.room.Query(value = "delete from Cart where id=:id")
    public abstract void deleteCartItem(long id);
    
    @androidx.room.Query(value = "SELECT vendor_id FROM Cart ORDER BY id ASC LIMIT 1")
    public abstract long getVendorIDFromCart();
    
    @androidx.room.Query(value = "delete from VendorCategory where vendor_id=:vendor_id")
    public abstract void deleteVendorCategory(int vendor_id);
    
    @androidx.room.Query(value = "delete from Products where category_id=:category_id AND vendor_id=:vendor_id")
    public abstract void deleteProducts(int category_id, int vendor_id);
    
    @androidx.room.Query(value = "delete from Vendor where category_id=:category_id")
    public abstract void deleteVendors(int category_id);
    
    @org.jetbrains.annotations.NotNull()
    @androidx.room.Query(value = "select * from vendor where id=:vendor_id")
    public abstract java.util.List<com.senarios.customer.models.Vendor> getVendorByID(int vendor_id);
    
    @androidx.room.Query(value = "Update Cart set isOutStock=:isOutStock where id=:id")
    public abstract void updateOutOfOrder(long id, boolean isOutStock);
    
    public final void updateCart(@org.jetbrains.annotations.NotNull()
    java.util.List<com.senarios.customer.models.ProductModel> products) {
    }
    
    public DAO() {
        super();
    }
}