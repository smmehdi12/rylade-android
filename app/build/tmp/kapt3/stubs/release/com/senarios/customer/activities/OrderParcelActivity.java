package com.senarios.customer.activities;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u00ae\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u0003\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u00032\u00020\u00042\u00020\u00052\u00020\u00062\u00020\u00072\b\u0012\u0004\u0012\u00020\t0\bB\u0005\u00a2\u0006\u0002\u0010\nJ\b\u0010\u0019\u001a\u00020\u001aH\u0016J \u0010\u001b\u001a\u00020\u001a2\u0006\u0010\u001c\u001a\u00020\u00122\u0006\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020\u0012H\u0016J\u0018\u0010 \u001a\u00020\u001a2\u0006\u0010\u001c\u001a\u00020\u00122\u0006\u0010!\u001a\u00020\"H\u0016J\u0018\u0010#\u001a\u00020\u001a2\u0006\u0010\u001c\u001a\u00020\u00122\u0006\u0010\u001f\u001a\u00020\u0012H\u0016J\b\u0010$\u001a\u00020\u001aH\u0016J.\u0010%\u001a\u00020\u001a2\u0006\u0010\u001c\u001a\u00020\u00122\f\u0010&\u001a\b\u0012\u0004\u0012\u00020(0\'2\u0006\u0010)\u001a\u00020\u00122\u0006\u0010\u001f\u001a\u00020\u0012H\u0016J&\u0010*\u001a\u00020\u001a2\u0006\u0010\u001c\u001a\u00020\u00122\f\u0010&\u001a\b\u0012\u0004\u0012\u00020(0\'2\u0006\u0010)\u001a\u00020\u0012H\u0016J\b\u0010+\u001a\u00020\u001aH\u0016J\b\u0010,\u001a\u00020\u001aH\u0002J\u0012\u0010-\u001a\u00020\u001a2\b\u0010.\u001a\u0004\u0018\u00010/H\u0016J*\u00100\u001a\u00020\u001a2\b\u0010.\u001a\u0004\u0018\u0001012\u0006\u00102\u001a\u00020\u001e2\u0006\u00103\u001a\u00020\u001e2\u0006\u00104\u001a\u00020\u001eH\u0016J\u0010\u00105\u001a\u00020\u001a2\u0006\u0010\u001d\u001a\u00020\u001eH\u0002J\b\u00106\u001a\u00020\u001aH\u0014J\u0006\u00107\u001a\u000208J\"\u00109\u001a\u00020\u001a2\u0006\u0010:\u001a\u00020\u001e2\u0006\u0010;\u001a\u00020\u001e2\b\u0010<\u001a\u0004\u0018\u00010=H\u0014J\u0012\u0010>\u001a\u00020\u001a2\b\u0010&\u001a\u0004\u0018\u00010\tH\u0016J\u001a\u0010?\u001a\u00020\u001a2\b\u0010@\u001a\u0004\u0018\u00010A2\u0006\u0010B\u001a\u000208H\u0016J\u0012\u0010C\u001a\u00020\u001a2\b\u0010D\u001a\u0004\u0018\u00010EH\u0016J*\u0010F\u001a\u00020\u001a2\b\u0010G\u001a\u0004\u0018\u00010H2\u0006\u0010I\u001a\u00020\u001e2\u0006\u0010J\u001a\u00020\u001e2\u0006\u0010K\u001a\u00020\u001eH\u0016J\b\u0010L\u001a\u00020\u001aH\u0014J*\u0010M\u001a\u00020\u001a2\b\u0010.\u001a\u0004\u0018\u0001012\u0006\u00102\u001a\u00020\u001e2\u0006\u0010N\u001a\u00020\u001e2\u0006\u00103\u001a\u00020\u001eH\u0016J\"\u0010O\u001a\u00020\u001a2\b\u0010G\u001a\u0004\u0018\u00010P2\u0006\u0010I\u001a\u00020\u001e2\u0006\u0010J\u001a\u00020\u001eH\u0016J\b\u0010Q\u001a\u00020\u001aH\u0016J\b\u0010R\u001a\u00020\u001aH\u0002R\u000e\u0010\u000b\u001a\u00020\fX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000f\u001a\u0004\u0018\u00010\u0010X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0011\u001a\u00020\u0012X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0013\u0010\u0014\"\u0004\b\u0015\u0010\u0016R\u000e\u0010\u0017\u001a\u00020\u0018X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006S"}, d2 = {"Lcom/senarios/customer/activities/OrderParcelActivity;", "Lcom/senarios/customer/activities/BaseAcivity;", "Lcom/senarios/customer/retrofit/ApiResponse;", "Landroid/text/TextWatcher;", "Landroid/app/DatePickerDialog$OnDateSetListener;", "Landroid/app/TimePickerDialog$OnTimeSetListener;", "Landroid/widget/CompoundButton$OnCheckedChangeListener;", "Landroid/view/View$OnClickListener;", "Landroidx/lifecycle/Observer;", "Lcom/senarios/customer/models/User;", "()V", "binding", "Lcom/senarios/customer/databinding/FragmentParcelPickupDetailsBinding;", "dialog", "Landroidx/appcompat/app/AlertDialog;", "parcel", "Lcom/senarios/customer/models/ParcelModel;", "parcelDate", "", "getParcelDate", "()Ljava/lang/String;", "setParcelDate", "(Ljava/lang/String;)V", "sharedVM", "Lcom/senarios/customer/viewmodel/SharedVM;", "OnCancelPermissioDialog", "", "OnError", "endpoint", "code", "", "message", "OnException", "exception", "", "OnNetworkError", "OnPermissionApproved", "OnStatusfalse", "t", "Lretrofit2/Response;", "Lcom/senarios/customer/models/ModelBaseResponse;", "response", "OnSuccess", "OnTrigger", "ShowInstantDialog", "afterTextChanged", "s", "Landroid/text/Editable;", "beforeTextChanged", "", "start", "count", "after", "contactIntent", "init", "isDialog", "", "onActivityResult", "requestCode", "resultCode", "data", "Landroid/content/Intent;", "onChanged", "onCheckedChanged", "buttonView", "Landroid/widget/CompoundButton;", "isChecked", "onClick", "v", "Landroid/view/View;", "onDateSet", "p0", "Landroid/widget/DatePicker;", "p1", "p2", "p3", "onResume", "onTextChanged", "before", "onTimeSet", "Landroid/widget/TimePicker;", "setBinding", "validateInputs", "app_release"})
public final class OrderParcelActivity extends com.senarios.customer.activities.BaseAcivity implements com.senarios.customer.retrofit.ApiResponse, android.text.TextWatcher, android.app.DatePickerDialog.OnDateSetListener, android.app.TimePickerDialog.OnTimeSetListener, android.widget.CompoundButton.OnCheckedChangeListener, android.view.View.OnClickListener, androidx.lifecycle.Observer<com.senarios.customer.models.User> {
    private com.senarios.customer.databinding.FragmentParcelPickupDetailsBinding binding;
    private androidx.appcompat.app.AlertDialog dialog;
    private com.senarios.customer.viewmodel.SharedVM sharedVM;
    @org.jetbrains.annotations.NotNull()
    private java.lang.String parcelDate = "";
    private com.senarios.customer.models.ParcelModel parcel;
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getParcelDate() {
        return null;
    }
    
    public final void setParcelDate(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    @java.lang.Override()
    public void setBinding() {
    }
    
    @java.lang.Override()
    protected void init() {
    }
    
    private final void contactIntent(int code) {
    }
    
    @java.lang.Override()
    public void OnCancelPermissioDialog() {
    }
    
    @java.lang.Override()
    public void OnPermissionApproved() {
    }
    
    @java.lang.Override()
    public void OnTrigger() {
    }
    
    @java.lang.Override()
    protected void onActivityResult(int requestCode, int resultCode, @org.jetbrains.annotations.Nullable()
    android.content.Intent data) {
    }
    
    @java.lang.Override()
    public void onClick(@org.jetbrains.annotations.Nullable()
    android.view.View v) {
    }
    
    private final void ShowInstantDialog() {
    }
    
    private final void validateInputs() {
    }
    
    @java.lang.Override()
    public void afterTextChanged(@org.jetbrains.annotations.Nullable()
    android.text.Editable s) {
    }
    
    @java.lang.Override()
    public void beforeTextChanged(@org.jetbrains.annotations.Nullable()
    java.lang.CharSequence s, int start, int count, int after) {
    }
    
    @java.lang.Override()
    public void onTextChanged(@org.jetbrains.annotations.Nullable()
    java.lang.CharSequence s, int start, int before, int count) {
    }
    
    @java.lang.Override()
    public void OnSuccess(@org.jetbrains.annotations.NotNull()
    java.lang.String endpoint, @org.jetbrains.annotations.NotNull()
    retrofit2.Response<com.senarios.customer.models.ModelBaseResponse> t, @org.jetbrains.annotations.NotNull()
    java.lang.String response) {
    }
    
    @java.lang.Override()
    public void OnStatusfalse(@org.jetbrains.annotations.NotNull()
    java.lang.String endpoint, @org.jetbrains.annotations.NotNull()
    retrofit2.Response<com.senarios.customer.models.ModelBaseResponse> t, @org.jetbrains.annotations.NotNull()
    java.lang.String response, @org.jetbrains.annotations.NotNull()
    java.lang.String message) {
    }
    
    @java.lang.Override()
    public void OnError(@org.jetbrains.annotations.NotNull()
    java.lang.String endpoint, int code, @org.jetbrains.annotations.NotNull()
    java.lang.String message) {
    }
    
    @java.lang.Override()
    public void OnException(@org.jetbrains.annotations.NotNull()
    java.lang.String endpoint, @org.jetbrains.annotations.NotNull()
    java.lang.Throwable exception) {
    }
    
    @java.lang.Override()
    public void OnNetworkError(@org.jetbrains.annotations.NotNull()
    java.lang.String endpoint, @org.jetbrains.annotations.NotNull()
    java.lang.String message) {
    }
    
    public final boolean isDialog() {
        return false;
    }
    
    @java.lang.Override()
    protected void onResume() {
    }
    
    @java.lang.Override()
    public void onChanged(@org.jetbrains.annotations.Nullable()
    com.senarios.customer.models.User t) {
    }
    
    @java.lang.Override()
    public void onCheckedChanged(@org.jetbrains.annotations.Nullable()
    android.widget.CompoundButton buttonView, boolean isChecked) {
    }
    
    @java.lang.Override()
    public void onDateSet(@org.jetbrains.annotations.Nullable()
    android.widget.DatePicker p0, int p1, int p2, int p3) {
    }
    
    @java.lang.Override()
    public void onTimeSet(@org.jetbrains.annotations.Nullable()
    android.widget.TimePicker p0, int p1, int p2) {
    }
    
    public OrderParcelActivity() {
        super();
    }
}