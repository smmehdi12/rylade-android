package com.senarios.customer.adaptors;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\bf\u0018\u00002\u00020\u0001J\u0018\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0001H\u0016J\u0018\u0010\u0007\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0001H\u0016J\u0018\u0010\b\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0001H\u0016J\b\u0010\t\u001a\u00020\u0003H\u0016J\u0010\u0010\n\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016J\u0010\u0010\u000b\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016J\u0018\u0010\f\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\r\u001a\u00020\u000eH\u0016J(\u0010\u000f\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u00052\u0006\u0010\u0013\u001a\u00020\u0014H\u0016\u00a8\u0006\u0015"}, d2 = {"Lcom/senarios/customer/adaptors/RecyclerViewCallback;", "", "OnClick", "", "position", "", "any", "OnDelete", "OnEdit", "OnScroll", "getPosition", "openGallery", "setDefaultAddress", "mAddress", "Lcom/senarios/customer/models/Address;", "updateCartItem", "cartModel", "Lcom/senarios/customer/models/CartModel;", "quantity", "flag", "", "app_release"})
public abstract interface RecyclerViewCallback {
    
    public abstract void OnEdit(int position, @org.jetbrains.annotations.NotNull()
    java.lang.Object any);
    
    public abstract void OnDelete(int position, @org.jetbrains.annotations.NotNull()
    java.lang.Object any);
    
    public abstract void OnClick(int position, @org.jetbrains.annotations.NotNull()
    java.lang.Object any);
    
    public abstract void OnScroll();
    
    public abstract void openGallery(int position);
    
    public abstract void getPosition(int position);
    
    public abstract void updateCartItem(int position, @org.jetbrains.annotations.NotNull()
    com.senarios.customer.models.CartModel cartModel, int quantity, boolean flag);
    
    public abstract void setDefaultAddress(int position, @org.jetbrains.annotations.NotNull()
    com.senarios.customer.models.Address mAddress);
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 3)
    public final class DefaultImpls {
        
        public static void OnEdit(com.senarios.customer.adaptors.RecyclerViewCallback $this, int position, @org.jetbrains.annotations.NotNull()
        java.lang.Object any) {
        }
        
        public static void OnDelete(com.senarios.customer.adaptors.RecyclerViewCallback $this, int position, @org.jetbrains.annotations.NotNull()
        java.lang.Object any) {
        }
        
        public static void OnClick(com.senarios.customer.adaptors.RecyclerViewCallback $this, int position, @org.jetbrains.annotations.NotNull()
        java.lang.Object any) {
        }
        
        public static void OnScroll(com.senarios.customer.adaptors.RecyclerViewCallback $this) {
        }
        
        public static void openGallery(com.senarios.customer.adaptors.RecyclerViewCallback $this, int position) {
        }
        
        public static void getPosition(com.senarios.customer.adaptors.RecyclerViewCallback $this, int position) {
        }
        
        public static void updateCartItem(com.senarios.customer.adaptors.RecyclerViewCallback $this, int position, @org.jetbrains.annotations.NotNull()
        com.senarios.customer.models.CartModel cartModel, int quantity, boolean flag) {
        }
        
        public static void setDefaultAddress(com.senarios.customer.adaptors.RecyclerViewCallback $this, int position, @org.jetbrains.annotations.NotNull()
        com.senarios.customer.models.Address mAddress) {
        }
    }
}