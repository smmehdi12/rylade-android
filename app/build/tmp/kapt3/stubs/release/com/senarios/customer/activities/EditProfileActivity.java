package com.senarios.customer.activities;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000n\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u0003\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u00032\u00020\u0004B\u0005\u00a2\u0006\u0002\u0010\u0005J\b\u0010\f\u001a\u00020\rH\u0016J \u0010\u000e\u001a\u00020\r2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0010H\u0016J\u0018\u0010\u0014\u001a\u00020\r2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0015\u001a\u00020\u0016H\u0016J\u0018\u0010\u0017\u001a\u00020\r2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0013\u001a\u00020\u0010H\u0016J\b\u0010\u0018\u001a\u00020\rH\u0016J.\u0010\u0019\u001a\u00020\r2\u0006\u0010\u000f\u001a\u00020\u00102\f\u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\u001c0\u001b2\u0006\u0010\u001d\u001a\u00020\u00102\u0006\u0010\u0013\u001a\u00020\u0010H\u0016J&\u0010\u001e\u001a\u00020\r2\u0006\u0010\u000f\u001a\u00020\u00102\f\u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\u001c0\u001b2\u0006\u0010\u001d\u001a\u00020\u0010H\u0016J\b\u0010\u001f\u001a\u00020\rH\u0016J\b\u0010 \u001a\u00020\rH\u0014J\u0012\u0010!\u001a\u00020\r2\b\u0010\"\u001a\u0004\u0018\u00010#H\u0016J$\u0010$\u001a\u00020%2\b\u0010\"\u001a\u0004\u0018\u00010&2\u0006\u0010\'\u001a\u00020\u00122\b\u0010(\u001a\u0004\u0018\u00010)H\u0016J\b\u0010*\u001a\u00020\rH\u0014J\b\u0010+\u001a\u00020\rH\u0016J\b\u0010,\u001a\u00020\rH\u0002R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006-"}, d2 = {"Lcom/senarios/customer/activities/EditProfileActivity;", "Lcom/senarios/customer/activities/BaseAcivity;", "Landroid/widget/TextView$OnEditorActionListener;", "Lcom/senarios/customer/retrofit/ApiResponse;", "Landroid/view/View$OnClickListener;", "()V", "binding", "Lcom/senarios/customer/databinding/FragmentProfileBinding;", "dialog", "Landroidx/appcompat/app/AlertDialog;", "sharedVM", "Lcom/senarios/customer/viewmodel/SharedVM;", "OnCancelPermissioDialog", "", "OnError", "endpoint", "", "code", "", "message", "OnException", "exception", "", "OnNetworkError", "OnPermissionApproved", "OnStatusfalse", "t", "Lretrofit2/Response;", "Lcom/senarios/customer/models/ModelBaseResponse;", "response", "OnSuccess", "OnTrigger", "init", "onClick", "v", "Landroid/view/View;", "onEditorAction", "", "Landroid/widget/TextView;", "actionId", "event", "Landroid/view/KeyEvent;", "onResume", "setBinding", "validateInput", "app_release"})
public final class EditProfileActivity extends com.senarios.customer.activities.BaseAcivity implements android.widget.TextView.OnEditorActionListener, com.senarios.customer.retrofit.ApiResponse, android.view.View.OnClickListener {
    private com.senarios.customer.databinding.FragmentProfileBinding binding;
    private com.senarios.customer.viewmodel.SharedVM sharedVM;
    private androidx.appcompat.app.AlertDialog dialog;
    private java.util.HashMap _$_findViewCache;
    
    @java.lang.Override()
    public void setBinding() {
    }
    
    @java.lang.Override()
    protected void init() {
    }
    
    @java.lang.Override()
    public void OnCancelPermissioDialog() {
    }
    
    @java.lang.Override()
    public void OnPermissionApproved() {
    }
    
    @java.lang.Override()
    public void OnTrigger() {
    }
    
    @java.lang.Override()
    public boolean onEditorAction(@org.jetbrains.annotations.Nullable()
    android.widget.TextView v, int actionId, @org.jetbrains.annotations.Nullable()
    android.view.KeyEvent event) {
        return false;
    }
    
    private final void validateInput() {
    }
    
    @java.lang.Override()
    public void onClick(@org.jetbrains.annotations.Nullable()
    android.view.View v) {
    }
    
    @java.lang.Override()
    public void OnSuccess(@org.jetbrains.annotations.NotNull()
    java.lang.String endpoint, @org.jetbrains.annotations.NotNull()
    retrofit2.Response<com.senarios.customer.models.ModelBaseResponse> t, @org.jetbrains.annotations.NotNull()
    java.lang.String response) {
    }
    
    @java.lang.Override()
    public void OnStatusfalse(@org.jetbrains.annotations.NotNull()
    java.lang.String endpoint, @org.jetbrains.annotations.NotNull()
    retrofit2.Response<com.senarios.customer.models.ModelBaseResponse> t, @org.jetbrains.annotations.NotNull()
    java.lang.String response, @org.jetbrains.annotations.NotNull()
    java.lang.String message) {
    }
    
    @java.lang.Override()
    public void OnError(@org.jetbrains.annotations.NotNull()
    java.lang.String endpoint, int code, @org.jetbrains.annotations.NotNull()
    java.lang.String message) {
    }
    
    @java.lang.Override()
    public void OnException(@org.jetbrains.annotations.NotNull()
    java.lang.String endpoint, @org.jetbrains.annotations.NotNull()
    java.lang.Throwable exception) {
    }
    
    @java.lang.Override()
    public void OnNetworkError(@org.jetbrains.annotations.NotNull()
    java.lang.String endpoint, @org.jetbrains.annotations.NotNull()
    java.lang.String message) {
    }
    
    @java.lang.Override()
    protected void onResume() {
    }
    
    public EditProfileActivity() {
        super();
    }
}