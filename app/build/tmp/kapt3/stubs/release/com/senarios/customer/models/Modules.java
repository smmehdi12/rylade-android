package com.senarios.customer.models;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0011\u0018\u00002\u00020\u0001B\'\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\b\u0010\b\u001a\u0004\u0018\u00010\t\u00a2\u0006\u0002\u0010\nR\u001a\u0010\u0004\u001a\u00020\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000eR\u001a\u0010\u0006\u001a\u00020\u0007X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0006\u0010\u000f\"\u0004\b\u0010\u0010\u0011R\u001c\u0010\b\u001a\u0004\u0018\u00010\tX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0012\u0010\u0013\"\u0004\b\u0014\u0010\u0015R\u001a\u0010\u0002\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0016\u0010\u0017\"\u0004\b\u0018\u0010\u0019\u00a8\u0006\u001a"}, d2 = {"Lcom/senarios/customer/models/Modules;", "", "title", "", "icon", "", "isOffline", "", "mainCategoryModel", "Lcom/senarios/customer/models/MainCategoryModel;", "(Ljava/lang/String;IZLcom/senarios/customer/models/MainCategoryModel;)V", "getIcon", "()I", "setIcon", "(I)V", "()Z", "setOffline", "(Z)V", "getMainCategoryModel", "()Lcom/senarios/customer/models/MainCategoryModel;", "setMainCategoryModel", "(Lcom/senarios/customer/models/MainCategoryModel;)V", "getTitle", "()Ljava/lang/String;", "setTitle", "(Ljava/lang/String;)V", "app_release"})
public final class Modules {
    @org.jetbrains.annotations.NotNull()
    private java.lang.String title;
    private int icon;
    private boolean isOffline;
    @org.jetbrains.annotations.Nullable()
    private com.senarios.customer.models.MainCategoryModel mainCategoryModel;
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getTitle() {
        return null;
    }
    
    public final void setTitle(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    public final int getIcon() {
        return 0;
    }
    
    public final void setIcon(int p0) {
    }
    
    public final boolean isOffline() {
        return false;
    }
    
    public final void setOffline(boolean p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.senarios.customer.models.MainCategoryModel getMainCategoryModel() {
        return null;
    }
    
    public final void setMainCategoryModel(@org.jetbrains.annotations.Nullable()
    com.senarios.customer.models.MainCategoryModel p0) {
    }
    
    public Modules(@org.jetbrains.annotations.NotNull()
    java.lang.String title, int icon, boolean isOffline, @org.jetbrains.annotations.Nullable()
    com.senarios.customer.models.MainCategoryModel mainCategoryModel) {
        super();
    }
}