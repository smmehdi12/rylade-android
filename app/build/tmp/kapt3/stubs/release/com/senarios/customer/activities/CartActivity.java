package com.senarios.customer.activities;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u008e\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u0003\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u00032\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00060\u00050\u00042\u00020\u0007B\u0005\u00a2\u0006\u0002\u0010\bJ\b\u0010\u0014\u001a\u00020\u0015H\u0016J \u0010\u0016\u001a\u00020\u00152\u0006\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u0018H\u0016J\u0018\u0010\u001c\u001a\u00020\u00152\u0006\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u001d\u001a\u00020\u001eH\u0016J\u0018\u0010\u001f\u001a\u00020\u00152\u0006\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u001b\u001a\u00020\u0018H\u0016J\b\u0010 \u001a\u00020\u0015H\u0016J.\u0010!\u001a\u00020\u00152\u0006\u0010\u0017\u001a\u00020\u00182\f\u0010\"\u001a\b\u0012\u0004\u0012\u00020$0#2\u0006\u0010%\u001a\u00020\u00182\u0006\u0010\u001b\u001a\u00020\u0018H\u0016J&\u0010&\u001a\u00020\u00152\u0006\u0010\u0017\u001a\u00020\u00182\f\u0010\"\u001a\b\u0012\u0004\u0012\u00020$0#2\u0006\u0010%\u001a\u00020\u0018H\u0016J\b\u0010\'\u001a\u00020\u0015H\u0016J\u0010\u0010(\u001a\u00020\u00152\u0006\u0010)\u001a\u00020*H\u0002J\b\u0010+\u001a\u00020\u0015H\u0002J\b\u0010,\u001a\u00020\u0015H\u0002J\b\u0010-\u001a\u00020\u0015H\u0002J\u0010\u0010.\u001a\u00020\u00152\u0006\u0010\u0012\u001a\u00020\u0013H\u0007J\b\u0010/\u001a\u00020\u0015H\u0014J\"\u00100\u001a\u00020\u00152\u0006\u00101\u001a\u00020\u001a2\u0006\u00102\u001a\u00020\u001a2\b\u00103\u001a\u0004\u0018\u000104H\u0014J\u0018\u00105\u001a\u00020\u00152\u000e\u0010\"\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0005H\u0016J\u0012\u00106\u001a\u00020\u00152\b\u00107\u001a\u0004\u0018\u000108H\u0016J\b\u00109\u001a\u00020\u0015H\u0014J\b\u0010:\u001a\u00020\u0015H\u0014J\b\u0010;\u001a\u00020\u0015H\u0014J\b\u0010<\u001a\u00020\u0015H\u0002J\b\u0010=\u001a\u00020\u0015H\u0016J\b\u0010>\u001a\u00020\u0015H\u0002J\u0010\u0010?\u001a\u00020\u00152\u0006\u0010@\u001a\u00020AH\u0007J(\u0010B\u001a\u00020\u00152\u0006\u0010C\u001a\u00020\u001a2\u0006\u0010D\u001a\u00020\u00062\u0006\u0010E\u001a\u00020\u001a2\u0006\u0010F\u001a\u00020*H\u0016J(\u0010G\u001a\u00020\u00152\u0006\u0010C\u001a\u00020\u001a2\u0006\u0010D\u001a\u00020\u00062\u0006\u0010E\u001a\u00020\u001a2\u0006\u0010F\u001a\u00020*H\u0002R\u000e\u0010\t\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006H"}, d2 = {"Lcom/senarios/customer/activities/CartActivity;", "Lcom/senarios/customer/activities/BaseAcivity;", "Lcom/senarios/customer/retrofit/ApiResponse;", "Lcom/senarios/customer/adaptors/RecyclerViewCallback;", "Landroidx/lifecycle/Observer;", "", "Lcom/senarios/customer/models/CartModel;", "Landroid/view/View$OnClickListener;", "()V", "adaptor", "Lcom/senarios/customer/adaptors/AdaptorCartItems;", "address", "Lcom/senarios/customer/models/Address;", "binding", "Lcom/senarios/customer/databinding/ActivityCartBinding;", "cartList", "model", "Lcom/senarios/customer/models/EcommerceOrderModel;", "promoModel", "Lcom/senarios/customer/models/PromoModel;", "OnCancelPermissioDialog", "", "OnError", "endpoint", "", "code", "", "message", "OnException", "exception", "", "OnNetworkError", "OnPermissionApproved", "OnStatusfalse", "t", "Lretrofit2/Response;", "Lcom/senarios/customer/models/ModelBaseResponse;", "response", "OnSuccess", "OnTrigger", "applyCalculations", "removePromo", "", "clearCart", "createOrder", "createOrderDialog", "getPromo", "init", "onActivityResult", "requestCode", "resultCode", "data", "Landroid/content/Intent;", "onChanged", "onClick", "v", "Landroid/view/View;", "onResume", "onStart", "onStop", "setAddress", "setBinding", "showClearPromoDialog", "showError", "modelCommunication", "Lcom/senarios/customer/models/ModelCommunication;", "updateCartItem", "position", "cartModel", "quantity", "flag", "updateQuantity", "app_release"})
public final class CartActivity extends com.senarios.customer.activities.BaseAcivity implements com.senarios.customer.retrofit.ApiResponse, com.senarios.customer.adaptors.RecyclerViewCallback, androidx.lifecycle.Observer<java.util.List<com.senarios.customer.models.CartModel>>, android.view.View.OnClickListener {
    private com.senarios.customer.databinding.ActivityCartBinding binding;
    private com.senarios.customer.adaptors.AdaptorCartItems adaptor;
    private com.senarios.customer.models.Address address;
    private com.senarios.customer.models.PromoModel promoModel;
    private java.util.List<com.senarios.customer.models.CartModel> cartList;
    private final com.senarios.customer.models.EcommerceOrderModel model = null;
    private java.util.HashMap _$_findViewCache;
    
    @java.lang.Override()
    protected void onResume() {
    }
    
    @java.lang.Override()
    public void setBinding() {
    }
    
    @java.lang.Override()
    protected void init() {
    }
    
    @java.lang.Override()
    public void OnCancelPermissioDialog() {
    }
    
    @java.lang.Override()
    public void OnPermissionApproved() {
    }
    
    @java.lang.Override()
    public void OnTrigger() {
    }
    
    @java.lang.Override()
    public void updateCartItem(int position, @org.jetbrains.annotations.NotNull()
    com.senarios.customer.models.CartModel cartModel, int quantity, boolean flag) {
    }
    
    private final void updateQuantity(int position, com.senarios.customer.models.CartModel cartModel, int quantity, boolean flag) {
    }
    
    @java.lang.Override()
    public void onChanged(@org.jetbrains.annotations.Nullable()
    java.util.List<com.senarios.customer.models.CartModel> t) {
    }
    
    private final void applyCalculations(boolean removePromo) {
    }
    
    @java.lang.Override()
    protected void onActivityResult(int requestCode, int resultCode, @org.jetbrains.annotations.Nullable()
    android.content.Intent data) {
    }
    
    private final void setAddress() {
    }
    
    @java.lang.Override()
    public void onClick(@org.jetbrains.annotations.Nullable()
    android.view.View v) {
    }
    
    private final void showClearPromoDialog() {
    }
    
    private final void createOrderDialog() {
    }
    
    private final void createOrder() {
    }
    
    @org.greenrobot.eventbus.Subscribe(threadMode = org.greenrobot.eventbus.ThreadMode.MAIN)
    public final void getPromo(@org.jetbrains.annotations.NotNull()
    com.senarios.customer.models.PromoModel promoModel) {
    }
    
    @org.greenrobot.eventbus.Subscribe(threadMode = org.greenrobot.eventbus.ThreadMode.MAIN)
    public final void showError(@org.jetbrains.annotations.NotNull()
    com.senarios.customer.models.ModelCommunication modelCommunication) {
    }
    
    @java.lang.Override()
    protected void onStop() {
    }
    
    @java.lang.Override()
    protected void onStart() {
    }
    
    @java.lang.Override()
    public void OnSuccess(@org.jetbrains.annotations.NotNull()
    java.lang.String endpoint, @org.jetbrains.annotations.NotNull()
    retrofit2.Response<com.senarios.customer.models.ModelBaseResponse> t, @org.jetbrains.annotations.NotNull()
    java.lang.String response) {
    }
    
    @java.lang.Override()
    public void OnStatusfalse(@org.jetbrains.annotations.NotNull()
    java.lang.String endpoint, @org.jetbrains.annotations.NotNull()
    retrofit2.Response<com.senarios.customer.models.ModelBaseResponse> t, @org.jetbrains.annotations.NotNull()
    java.lang.String response, @org.jetbrains.annotations.NotNull()
    java.lang.String message) {
    }
    
    @java.lang.Override()
    public void OnError(@org.jetbrains.annotations.NotNull()
    java.lang.String endpoint, int code, @org.jetbrains.annotations.NotNull()
    java.lang.String message) {
    }
    
    @java.lang.Override()
    public void OnException(@org.jetbrains.annotations.NotNull()
    java.lang.String endpoint, @org.jetbrains.annotations.NotNull()
    java.lang.Throwable exception) {
    }
    
    @java.lang.Override()
    public void OnNetworkError(@org.jetbrains.annotations.NotNull()
    java.lang.String endpoint, @org.jetbrains.annotations.NotNull()
    java.lang.String message) {
    }
    
    private final void clearCart() {
    }
    
    public CartActivity() {
        super();
    }
    
    public void OnEdit(int position, @org.jetbrains.annotations.NotNull()
    java.lang.Object any) {
    }
    
    public void OnDelete(int position, @org.jetbrains.annotations.NotNull()
    java.lang.Object any) {
    }
    
    public void OnClick(int position, @org.jetbrains.annotations.NotNull()
    java.lang.Object any) {
    }
    
    public void OnScroll() {
    }
    
    public void openGallery(int position) {
    }
    
    public void getPosition(int position) {
    }
    
    public void setDefaultAddress(int position, @org.jetbrains.annotations.NotNull()
    com.senarios.customer.models.Address mAddress) {
    }
}