package com.senarios.customer.di;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\bg\u0018\u00002\u00020\u0001:\u0001\u0011J\b\u0010\u0002\u001a\u00020\u0000H&J\b\u0010\u0003\u001a\u00020\u0004H&J\b\u0010\u0005\u001a\u00020\u0006H&J\b\u0010\u0007\u001a\u00020\bH&J\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fH&J\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\r\u001a\u00020\u000eH&J\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u000f\u001a\u00020\u0010H&\u00a8\u0006\u0012"}, d2 = {"Lcom/senarios/customer/di/Component;", "", "getComponent", "getContext", "Landroid/content/Context;", "getDAO", "Lcom/senarios/customer/db/DAO;", "getDirectory", "Lcom/senarios/customer/models/Directory;", "inject", "", "mainActivity", "Lcom/senarios/customer/activities/MainActivity;", "mapActivity", "Lcom/senarios/customer/activities/MapActivity;", "homeFragment", "Lcom/senarios/customer/fragments/HomeFragment;", "Builder", "app_release"})
@dagger.Component(modules = {com.senarios.customer.MyModule.class, com.senarios.customer.DbModule.class, com.senarios.customer.di.ApiModule.class, com.senarios.customer.di.UtilityModule.class})
@javax.inject.Singleton()
public abstract interface Component {
    
    public abstract void inject(@org.jetbrains.annotations.NotNull()
    com.senarios.customer.activities.MainActivity mainActivity);
    
    public abstract void inject(@org.jetbrains.annotations.NotNull()
    com.senarios.customer.activities.MapActivity mapActivity);
    
    public abstract void inject(@org.jetbrains.annotations.NotNull()
    com.senarios.customer.fragments.HomeFragment homeFragment);
    
    @org.jetbrains.annotations.NotNull()
    public abstract com.senarios.customer.di.Component getComponent();
    
    @org.jetbrains.annotations.NotNull()
    public abstract android.content.Context getContext();
    
    @org.jetbrains.annotations.NotNull()
    public abstract com.senarios.customer.db.DAO getDAO();
    
    @org.jetbrains.annotations.NotNull()
    public abstract com.senarios.customer.models.Directory getDirectory();
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\bg\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H&J\u0010\u0010\u0004\u001a\u00020\u00002\u0006\u0010\u0005\u001a\u00020\u0006H\'\u00a8\u0006\u0007"}, d2 = {"Lcom/senarios/customer/di/Component$Builder;", "", "build", "Lcom/senarios/customer/di/Component;", "setApplication", "application", "Landroid/app/Application;", "app_release"})
    @dagger.Component.Builder()
    public static abstract interface Builder {
        
        @org.jetbrains.annotations.NotNull()
        @dagger.BindsInstance()
        public abstract com.senarios.customer.di.Component.Builder setApplication(@org.jetbrains.annotations.NotNull()
        android.app.Application application);
        
        @org.jetbrains.annotations.NotNull()
        public abstract com.senarios.customer.di.Component build();
    }
}