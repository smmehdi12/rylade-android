package com.senarios.customer.dialogfragments;

import java.lang.System;

/**
 * A simple [Fragment] subclass.
 */
@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u00b6\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u0003\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u00002\u00020\u00012\u00020\u00022\b\u0012\u0004\u0012\u00020\u00040\u00032\u00020\u00052\u00020\u00062\u00020\u00072\u00020\bB\u0005\u00a2\u0006\u0002\u0010\tJ\b\u0010\u0019\u001a\u00020\u001aH\u0016J \u0010\u001b\u001a\u00020\u001a2\u0006\u0010\u001c\u001a\u00020\u000b2\u0006\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020\u000bH\u0016J\u0018\u0010 \u001a\u00020\u001a2\u0006\u0010\u001c\u001a\u00020\u000b2\u0006\u0010!\u001a\u00020\"H\u0016J\u0018\u0010#\u001a\u00020\u001a2\u0006\u0010\u001c\u001a\u00020\u000b2\u0006\u0010\u001f\u001a\u00020\u000bH\u0016J.\u0010$\u001a\u00020\u001a2\u0006\u0010\u001c\u001a\u00020\u000b2\f\u0010%\u001a\b\u0012\u0004\u0012\u00020\'0&2\u0006\u0010(\u001a\u00020\u000b2\u0006\u0010\u001f\u001a\u00020\u000bH\u0016J&\u0010)\u001a\u00020\u001a2\u0006\u0010\u001c\u001a\u00020\u000b2\f\u0010%\u001a\b\u0012\u0004\u0012\u00020\'0&2\u0006\u0010(\u001a\u00020\u000bH\u0016J&\u0010*\u001a\u0004\u0018\u00010+2\u0006\u0010,\u001a\u00020-2\b\u0010.\u001a\u0004\u0018\u00010/2\b\u00100\u001a\u0004\u0018\u000101H\u0016J\b\u00102\u001a\u00020\u001aH\u0014J\"\u00103\u001a\u00020\u001a2\u0006\u00104\u001a\u00020\u001e2\u0006\u00105\u001a\u00020\u001e2\b\u00106\u001a\u0004\u0018\u000107H\u0016J\b\u00108\u001a\u00020\u001aH\u0016J\u0012\u00109\u001a\u00020\u001a2\b\u0010:\u001a\u0004\u0018\u00010+H\u0016J\u001c\u0010;\u001a\u00020\u001a2\b\u0010<\u001a\u0004\u0018\u00010=2\b\u0010(\u001a\u0004\u0018\u00010>H\u0016J$\u0010?\u001a\u00020@2\b\u0010:\u001a\u0004\u0018\u00010A2\u0006\u0010B\u001a\u00020\u001e2\b\u0010C\u001a\u0004\u0018\u00010DH\u0016J\u0012\u0010E\u001a\u00020\u001a2\b\u0010F\u001a\u0004\u0018\u00010GH\u0016J\u001a\u0010H\u001a\u00020\u001a2\b\u0010:\u001a\u0004\u0018\u00010+2\u0006\u0010I\u001a\u00020@H\u0016J\b\u0010J\u001a\u00020\u001aH\u0016J\u0012\u0010K\u001a\u00020\u001a2\b\u0010L\u001a\u0004\u0018\u00010\u0004H\u0016J\b\u0010M\u001a\u00020\u001aH\u0002R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\f\u001a\u00020\rX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000e\u0010\u000f\"\u0004\b\u0010\u0010\u0011R\u0016\u0010\u0012\u001a\n \u0014*\u0004\u0018\u00010\u00130\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0018X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006N"}, d2 = {"Lcom/senarios/customer/dialogfragments/LoginSignupDialog;", "Lcom/senarios/rylad/BaseDialogeFragment;", "Lcom/facebook/GraphRequest$GraphJSONObjectCallback;", "Lcom/facebook/FacebookCallback;", "Lcom/facebook/login/LoginResult;", "Landroid/widget/TextView$OnEditorActionListener;", "Landroid/view/View$OnFocusChangeListener;", "Landroid/view/View$OnClickListener;", "Lcom/senarios/customer/retrofit/ApiResponse;", "()V", "TAG", "", "binding", "Lcom/senarios/customer/databinding/FragmentLoginSignupBinding;", "getBinding", "()Lcom/senarios/customer/databinding/FragmentLoginSignupBinding;", "setBinding", "(Lcom/senarios/customer/databinding/FragmentLoginSignupBinding;)V", "callback", "Lcom/facebook/CallbackManager;", "kotlin.jvm.PlatformType", "dialog", "Landroidx/appcompat/app/AlertDialog;", "graphRequest", "Lcom/facebook/GraphRequest;", "OnBackPressed", "", "OnError", "endpoint", "code", "", "message", "OnException", "exception", "", "OnNetworkError", "OnStatusfalse", "t", "Lretrofit2/Response;", "Lcom/senarios/customer/models/ModelBaseResponse;", "response", "OnSuccess", "getFragmentView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "init", "onActivityResult", "requestCode", "resultCode", "data", "Landroid/content/Intent;", "onCancel", "onClick", "v", "onCompleted", "json", "Lorg/json/JSONObject;", "Lcom/facebook/GraphResponse;", "onEditorAction", "", "Landroid/widget/TextView;", "actionId", "event", "Landroid/view/KeyEvent;", "onError", "error", "Lcom/facebook/FacebookException;", "onFocusChange", "hasFocus", "onResume", "onSuccess", "result", "validateInputs", "app_release"})
public final class LoginSignupDialog extends com.senarios.rylad.BaseDialogeFragment implements com.facebook.GraphRequest.GraphJSONObjectCallback, com.facebook.FacebookCallback<com.facebook.login.LoginResult>, android.widget.TextView.OnEditorActionListener, android.view.View.OnFocusChangeListener, android.view.View.OnClickListener, com.senarios.customer.retrofit.ApiResponse {
    @org.jetbrains.annotations.NotNull()
    public com.senarios.customer.databinding.FragmentLoginSignupBinding binding;
    private final com.facebook.CallbackManager callback = null;
    private com.facebook.GraphRequest graphRequest;
    private androidx.appcompat.app.AlertDialog dialog;
    private final java.lang.String TAG = null;
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.NotNull()
    public final com.senarios.customer.databinding.FragmentLoginSignupBinding getBinding() {
        return null;
    }
    
    public final void setBinding(@org.jetbrains.annotations.NotNull()
    com.senarios.customer.databinding.FragmentLoginSignupBinding p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public android.view.View getFragmentView(@org.jetbrains.annotations.NotNull()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    @java.lang.Override()
    protected void init() {
    }
    
    @java.lang.Override()
    public void OnBackPressed() {
    }
    
    @java.lang.Override()
    public void onFocusChange(@org.jetbrains.annotations.Nullable()
    android.view.View v, boolean hasFocus) {
    }
    
    @java.lang.Override()
    public void onClick(@org.jetbrains.annotations.Nullable()
    android.view.View v) {
    }
    
    private final void validateInputs() {
    }
    
    @java.lang.Override()
    public void OnSuccess(@org.jetbrains.annotations.NotNull()
    java.lang.String endpoint, @org.jetbrains.annotations.NotNull()
    retrofit2.Response<com.senarios.customer.models.ModelBaseResponse> t, @org.jetbrains.annotations.NotNull()
    java.lang.String response) {
    }
    
    @java.lang.Override()
    public void OnStatusfalse(@org.jetbrains.annotations.NotNull()
    java.lang.String endpoint, @org.jetbrains.annotations.NotNull()
    retrofit2.Response<com.senarios.customer.models.ModelBaseResponse> t, @org.jetbrains.annotations.NotNull()
    java.lang.String response, @org.jetbrains.annotations.NotNull()
    java.lang.String message) {
    }
    
    @java.lang.Override()
    public void OnError(@org.jetbrains.annotations.NotNull()
    java.lang.String endpoint, int code, @org.jetbrains.annotations.NotNull()
    java.lang.String message) {
    }
    
    @java.lang.Override()
    public void OnException(@org.jetbrains.annotations.NotNull()
    java.lang.String endpoint, @org.jetbrains.annotations.NotNull()
    java.lang.Throwable exception) {
    }
    
    @java.lang.Override()
    public void OnNetworkError(@org.jetbrains.annotations.NotNull()
    java.lang.String endpoint, @org.jetbrains.annotations.NotNull()
    java.lang.String message) {
    }
    
    @java.lang.Override()
    public boolean onEditorAction(@org.jetbrains.annotations.Nullable()
    android.widget.TextView v, int actionId, @org.jetbrains.annotations.Nullable()
    android.view.KeyEvent event) {
        return false;
    }
    
    @java.lang.Override()
    public void onCancel() {
    }
    
    @java.lang.Override()
    public void onSuccess(@org.jetbrains.annotations.Nullable()
    com.facebook.login.LoginResult result) {
    }
    
    @java.lang.Override()
    public void onResume() {
    }
    
    @java.lang.Override()
    public void onError(@org.jetbrains.annotations.Nullable()
    com.facebook.FacebookException error) {
    }
    
    @java.lang.Override()
    public void onActivityResult(int requestCode, int resultCode, @org.jetbrains.annotations.Nullable()
    android.content.Intent data) {
    }
    
    @java.lang.Override()
    public void onCompleted(@org.jetbrains.annotations.Nullable()
    org.json.JSONObject json, @org.jetbrains.annotations.Nullable()
    com.facebook.GraphResponse response) {
    }
    
    public LoginSignupDialog() {
        super();
    }
}