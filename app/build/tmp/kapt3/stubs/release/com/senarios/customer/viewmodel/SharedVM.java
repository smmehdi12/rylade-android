package com.senarios.customer.viewmodel;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000V\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\u0002\n\u0002\b\u0007\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\f\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u00070\u0017J\u001c\u0010\u0018\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\n0\tj\b\u0012\u0004\u0012\u00020\n`\u000b0\u0017J\f\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\r0\u0017J\f\u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\u000f0\u0017J\f\u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\r0\u0017J\u0006\u0010\u001c\u001a\u00020\u0012J\f\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\u000f0\u0017J\f\u0010\u001e\u001a\b\u0012\u0004\u0012\u00020\u00150\u0017J\u000e\u0010\u001f\u001a\u00020 2\u0006\u0010\u0005\u001a\u00020\u0007J\u001e\u0010!\u001a\u00020 2\u0016\u0010\"\u001a\u0012\u0012\u0004\u0012\u00020\n0\tj\b\u0012\u0004\u0012\u00020\n`\u000bJ\u000e\u0010#\u001a\u00020 2\u0006\u0010\f\u001a\u00020\rJ\u000e\u0010$\u001a\u00020 2\u0006\u0010\u0013\u001a\u00020\u000fJ\u000e\u0010%\u001a\u00020 2\u0006\u0010\u0013\u001a\u00020\u000fJ\u000e\u0010&\u001a\u00020 2\u0006\u0010\u0014\u001a\u00020\u0015R\u0014\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R$\u0010\b\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\n0\tj\b\u0012\u0004\u0012\u00020\n`\u000b0\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\f\u001a\b\u0012\u0004\u0012\u00020\r0\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u000f0\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\r0\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u000f0\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00150\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\'"}, d2 = {"Lcom/senarios/customer/viewmodel/SharedVM;", "Landroidx/lifecycle/AndroidViewModel;", "application", "Landroid/app/Application;", "(Landroid/app/Application;)V", "address", "Landroidx/lifecycle/MutableLiveData;", "Lcom/senarios/customer/models/Address;", "comments", "Ljava/util/ArrayList;", "Lcom/senarios/customer/models/CommentsItem;", "Lkotlin/collections/ArrayList;", "email", "", "newUser", "Lcom/senarios/customer/models/User;", "otp", "preference", "Landroid/content/SharedPreferences;", "user", "vendor", "Lcom/senarios/customer/models/Vendor;", "getAddress", "Landroidx/lifecycle/LiveData;", "getComments", "getEmail", "getLoggedUser", "getOTP", "getSharedPreference", "getUser", "getVendor", "setAddress", "", "setComments", "list", "setEmail", "setLoggedUser", "setUser", "setVendor", "app_release"})
public final class SharedVM extends androidx.lifecycle.AndroidViewModel {
    private android.content.SharedPreferences preference;
    private androidx.lifecycle.MutableLiveData<com.senarios.customer.models.User> user;
    private androidx.lifecycle.MutableLiveData<com.senarios.customer.models.User> newUser;
    private androidx.lifecycle.MutableLiveData<com.senarios.customer.models.Address> address;
    private androidx.lifecycle.MutableLiveData<java.lang.String> otp;
    private androidx.lifecycle.MutableLiveData<com.senarios.customer.models.Vendor> vendor;
    private androidx.lifecycle.MutableLiveData<java.util.ArrayList<com.senarios.customer.models.CommentsItem>> comments;
    private androidx.lifecycle.MutableLiveData<java.lang.String> email;
    
    @org.jetbrains.annotations.NotNull()
    public final android.content.SharedPreferences getSharedPreference() {
        return null;
    }
    
    public final void setLoggedUser(@org.jetbrains.annotations.NotNull()
    com.senarios.customer.models.User user) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<com.senarios.customer.models.User> getLoggedUser() {
        return null;
    }
    
    public final void setUser(@org.jetbrains.annotations.NotNull()
    com.senarios.customer.models.User user) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<com.senarios.customer.models.User> getUser() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<com.senarios.customer.models.Address> getAddress() {
        return null;
    }
    
    public final void setAddress(@org.jetbrains.annotations.NotNull()
    com.senarios.customer.models.Address address) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<java.lang.String> getOTP() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<java.lang.String> getEmail() {
        return null;
    }
    
    public final void setEmail(@org.jetbrains.annotations.NotNull()
    java.lang.String email) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<java.util.ArrayList<com.senarios.customer.models.CommentsItem>> getComments() {
        return null;
    }
    
    public final void setComments(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.senarios.customer.models.CommentsItem> list) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<com.senarios.customer.models.Vendor> getVendor() {
        return null;
    }
    
    public final void setVendor(@org.jetbrains.annotations.NotNull()
    com.senarios.customer.models.Vendor vendor) {
    }
    
    public SharedVM(@org.jetbrains.annotations.NotNull()
    android.app.Application application) {
        super(null);
    }
}