package com.senarios.customer.models;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\u0018\u00002\u00020\u0001B7\u0012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u0012\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u0012\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00070\u0003\u0012\u0006\u0010\b\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\tR\u001c\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u00038\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u001c\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00040\u00038\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\f\u0010\u000bR\u001c\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00070\u00038\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000bR\u0016\u0010\b\u001a\u00020\u00048\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000f\u00a8\u0006\u0010"}, d2 = {"Lcom/senarios/customer/models/GoogleDistanceApiResponse;", "", "destination_addresses", "", "", "origin_addresses", "rows", "Lcom/senarios/customer/models/Rows;", "status", "(Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/String;)V", "getDestination_addresses", "()Ljava/util/List;", "getOrigin_addresses", "getRows", "getStatus", "()Ljava/lang/String;", "app_release"})
public final class GoogleDistanceApiResponse {
    @org.jetbrains.annotations.NotNull()
    @com.google.gson.annotations.SerializedName(value = "destination_addresses")
    private final java.util.List<java.lang.String> destination_addresses = null;
    @org.jetbrains.annotations.NotNull()
    @com.google.gson.annotations.SerializedName(value = "origin_addresses")
    private final java.util.List<java.lang.String> origin_addresses = null;
    @org.jetbrains.annotations.NotNull()
    @com.google.gson.annotations.SerializedName(value = "rows")
    private final java.util.List<com.senarios.customer.models.Rows> rows = null;
    @org.jetbrains.annotations.NotNull()
    @com.google.gson.annotations.SerializedName(value = "status")
    private final java.lang.String status = null;
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<java.lang.String> getDestination_addresses() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<java.lang.String> getOrigin_addresses() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.senarios.customer.models.Rows> getRows() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getStatus() {
        return null;
    }
    
    public GoogleDistanceApiResponse(@org.jetbrains.annotations.NotNull()
    java.util.List<java.lang.String> destination_addresses, @org.jetbrains.annotations.NotNull()
    java.util.List<java.lang.String> origin_addresses, @org.jetbrains.annotations.NotNull()
    java.util.List<com.senarios.customer.models.Rows> rows, @org.jetbrains.annotations.NotNull()
    java.lang.String status) {
        super();
    }
}