package com.senarios.customer.adaptors;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\f\u0012\b\u0012\u00060\u0002R\u00020\u00000\u0001:\u0001\u001bB#\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006\u0012\u0006\u0010\b\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\b\u0010\u0011\u001a\u00020\u0012H\u0016J\u001c\u0010\u0013\u001a\u00020\u00142\n\u0010\u0015\u001a\u00060\u0002R\u00020\u00002\u0006\u0010\u0016\u001a\u00020\u0012H\u0016J\u001c\u0010\u0017\u001a\u00060\u0002R\u00020\u00002\u0006\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u0012H\u0016R\u0011\u0010\b\u001a\u00020\t\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u0017\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010\u00a8\u0006\u001c"}, d2 = {"Lcom/senarios/customer/adaptors/VendorProductAdapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lcom/senarios/customer/adaptors/VendorProductAdapter$VendorProductViewHolder;", "context", "Landroid/content/Context;", "productslist", "", "Lcom/senarios/customer/models/ProductCategoryModel;", "callback", "Lcom/senarios/customer/adaptors/RecyclerViewCallback;", "(Landroid/content/Context;Ljava/util/List;Lcom/senarios/customer/adaptors/RecyclerViewCallback;)V", "getCallback", "()Lcom/senarios/customer/adaptors/RecyclerViewCallback;", "getContext", "()Landroid/content/Context;", "getProductslist", "()Ljava/util/List;", "getItemCount", "", "onBindViewHolder", "", "holder", "position", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "VendorProductViewHolder", "app_release"})
public final class VendorProductAdapter extends androidx.recyclerview.widget.RecyclerView.Adapter<com.senarios.customer.adaptors.VendorProductAdapter.VendorProductViewHolder> {
    @org.jetbrains.annotations.NotNull()
    private final android.content.Context context = null;
    @org.jetbrains.annotations.NotNull()
    private final java.util.List<com.senarios.customer.models.ProductCategoryModel> productslist = null;
    @org.jetbrains.annotations.NotNull()
    private final com.senarios.customer.adaptors.RecyclerViewCallback callback = null;
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.senarios.customer.adaptors.VendorProductAdapter.VendorProductViewHolder onCreateViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup parent, int viewType) {
        return null;
    }
    
    @java.lang.Override()
    public int getItemCount() {
        return 0;
    }
    
    @java.lang.Override()
    public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
    com.senarios.customer.adaptors.VendorProductAdapter.VendorProductViewHolder holder, int position) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.content.Context getContext() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.senarios.customer.models.ProductCategoryModel> getProductslist() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.senarios.customer.adaptors.RecyclerViewCallback getCallback() {
        return null;
    }
    
    public VendorProductAdapter(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    java.util.List<com.senarios.customer.models.ProductCategoryModel> productslist, @org.jetbrains.annotations.NotNull()
    com.senarios.customer.adaptors.RecyclerViewCallback callback) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\b\u0086\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\u0007"}, d2 = {"Lcom/senarios/customer/adaptors/VendorProductAdapter$VendorProductViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "bindings", "Lcom/senarios/customer/databinding/ProductcategoriesbyidBinding;", "(Lcom/senarios/customer/adaptors/VendorProductAdapter;Lcom/senarios/customer/databinding/ProductcategoriesbyidBinding;)V", "getBindings", "()Lcom/senarios/customer/databinding/ProductcategoriesbyidBinding;", "app_release"})
    public final class VendorProductViewHolder extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
        @org.jetbrains.annotations.NotNull()
        private final com.senarios.customer.databinding.ProductcategoriesbyidBinding bindings = null;
        
        @org.jetbrains.annotations.NotNull()
        public final com.senarios.customer.databinding.ProductcategoriesbyidBinding getBindings() {
            return null;
        }
        
        public VendorProductViewHolder(@org.jetbrains.annotations.NotNull()
        com.senarios.customer.databinding.ProductcategoriesbyidBinding bindings) {
            super(null);
        }
    }
}