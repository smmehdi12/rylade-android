package com.senarios.customer.adaptors;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010 \n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\f\u0012\b\u0012\u00060\u0002R\u00020\u00000\u0001:\u0001\u0016B\u0015\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\u0006\u0010\u000f\u001a\u00020\u0010J\b\u0010\u0011\u001a\u00020\u0012H\u0016J\f\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\f0\u0014J\u001c\u0010\u0015\u001a\u00020\u00102\n\u0010\u0016\u001a\u00060\u0002R\u00020\u00002\u0006\u0010\u0017\u001a\u00020\u0012H\u0016J\u001c\u0010\u0018\u001a\u00060\u0002R\u00020\u00002\u0006\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u0012H\u0016J\u000e\u0010\u001c\u001a\u00020\u00102\u0006\u0010\u0017\u001a\u00020\u0012J\u000e\u0010\u001d\u001a\u00020\u00102\u0006\u0010\u001e\u001a\u00020\u001fJ\u000e\u0010 \u001a\u00020\u00102\u0006\u0010\u001e\u001a\u00020\u001fR\u0011\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0014\u0010\n\u001a\b\u0012\u0004\u0012\u00020\f0\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000e\u00a8\u0006!"}, d2 = {"Lcom/senarios/customer/adaptors/AdaptorMedicine;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lcom/senarios/customer/adaptors/AdaptorMedicine$holder;", "context", "Landroid/content/Context;", "recyclerViewCallback", "Lcom/senarios/customer/adaptors/RecyclerViewCallback;", "(Landroid/content/Context;Lcom/senarios/customer/adaptors/RecyclerViewCallback;)V", "getContext", "()Landroid/content/Context;", "list", "", "Lcom/senarios/customer/models/MedicineModel;", "getRecyclerViewCallback", "()Lcom/senarios/customer/adaptors/RecyclerViewCallback;", "addItem", "", "getItemCount", "", "getItems", "", "onBindViewHolder", "holder", "position", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "removeItem", "setGrayDrawable", "editText", "Landroid/widget/EditText;", "setGreenDrawable", "app_release"})
public final class AdaptorMedicine extends androidx.recyclerview.widget.RecyclerView.Adapter<com.senarios.customer.adaptors.AdaptorMedicine.holder> {
    private final java.util.List<com.senarios.customer.models.MedicineModel> list = null;
    @org.jetbrains.annotations.NotNull()
    private final android.content.Context context = null;
    @org.jetbrains.annotations.NotNull()
    private final com.senarios.customer.adaptors.RecyclerViewCallback recyclerViewCallback = null;
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.senarios.customer.adaptors.AdaptorMedicine.holder onCreateViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup parent, int viewType) {
        return null;
    }
    
    @java.lang.Override()
    public int getItemCount() {
        return 0;
    }
    
    @java.lang.Override()
    public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
    com.senarios.customer.adaptors.AdaptorMedicine.holder holder, int position) {
    }
    
    public final void removeItem(int position) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.senarios.customer.models.MedicineModel> getItems() {
        return null;
    }
    
    public final void addItem() {
    }
    
    public final void setGrayDrawable(@org.jetbrains.annotations.NotNull()
    android.widget.EditText editText) {
    }
    
    public final void setGreenDrawable(@org.jetbrains.annotations.NotNull()
    android.widget.EditText editText) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.content.Context getContext() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.senarios.customer.adaptors.RecyclerViewCallback getRecyclerViewCallback() {
        return null;
    }
    
    public AdaptorMedicine(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    com.senarios.customer.adaptors.RecyclerViewCallback recyclerViewCallback) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\\\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0000\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\b\u0004\b\u0086\u0004\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u00032\u00020\u00042\u00020\u0005B\r\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\bJ\u0012\u0010\u000b\u001a\u00020\f2\b\u0010\r\u001a\u0004\u0018\u00010\u000eH\u0016J*\u0010\u000f\u001a\u00020\f2\b\u0010\r\u001a\u0004\u0018\u00010\u00102\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u00122\u0006\u0010\u0014\u001a\u00020\u0012H\u0016J\u0012\u0010\u0015\u001a\u00020\f2\b\u0010\u0016\u001a\u0004\u0018\u00010\u0017H\u0016J\u001a\u0010\u0018\u001a\u00020\f2\b\u0010\u0016\u001a\u0004\u0018\u00010\u00172\u0006\u0010\u0019\u001a\u00020\u001aH\u0016J0\u0010\u001b\u001a\u00020\f2\f\u0010\u001c\u001a\b\u0012\u0002\b\u0003\u0018\u00010\u001d2\b\u0010\u001e\u001a\u0004\u0018\u00010\u00172\u0006\u0010\u001f\u001a\u00020\u00122\u0006\u0010 \u001a\u00020!H\u0016J\u0016\u0010\"\u001a\u00020\f2\f\u0010\u001c\u001a\b\u0012\u0002\b\u0003\u0018\u00010\u001dH\u0016J*\u0010#\u001a\u00020\f2\b\u0010\r\u001a\u0004\u0018\u00010\u00102\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010$\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0012H\u0016R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n\u00a8\u0006%"}, d2 = {"Lcom/senarios/customer/adaptors/AdaptorMedicine$holder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "Landroid/text/TextWatcher;", "Landroid/view/View$OnFocusChangeListener;", "Landroid/widget/AdapterView$OnItemSelectedListener;", "Landroid/view/View$OnClickListener;", "binding", "Lcom/senarios/customer/databinding/ItemMedicineViewBinding;", "(Lcom/senarios/customer/adaptors/AdaptorMedicine;Lcom/senarios/customer/databinding/ItemMedicineViewBinding;)V", "getBinding", "()Lcom/senarios/customer/databinding/ItemMedicineViewBinding;", "afterTextChanged", "", "s", "Landroid/text/Editable;", "beforeTextChanged", "", "start", "", "count", "after", "onClick", "v", "Landroid/view/View;", "onFocusChange", "hasFocus", "", "onItemSelected", "parent", "Landroid/widget/AdapterView;", "view", "position", "id", "", "onNothingSelected", "onTextChanged", "before", "app_release"})
    public final class holder extends androidx.recyclerview.widget.RecyclerView.ViewHolder implements android.text.TextWatcher, android.view.View.OnFocusChangeListener, android.widget.AdapterView.OnItemSelectedListener, android.view.View.OnClickListener {
        @org.jetbrains.annotations.NotNull()
        private final com.senarios.customer.databinding.ItemMedicineViewBinding binding = null;
        
        @java.lang.Override()
        public void onClick(@org.jetbrains.annotations.Nullable()
        android.view.View v) {
        }
        
        @java.lang.Override()
        public void onNothingSelected(@org.jetbrains.annotations.Nullable()
        android.widget.AdapterView<?> parent) {
        }
        
        @java.lang.Override()
        public void onItemSelected(@org.jetbrains.annotations.Nullable()
        android.widget.AdapterView<?> parent, @org.jetbrains.annotations.Nullable()
        android.view.View view, int position, long id) {
        }
        
        @java.lang.Override()
        public void onFocusChange(@org.jetbrains.annotations.Nullable()
        android.view.View v, boolean hasFocus) {
        }
        
        @java.lang.Override()
        public void afterTextChanged(@org.jetbrains.annotations.Nullable()
        android.text.Editable s) {
        }
        
        @java.lang.Override()
        public void beforeTextChanged(@org.jetbrains.annotations.Nullable()
        java.lang.CharSequence s, int start, int count, int after) {
        }
        
        @java.lang.Override()
        public void onTextChanged(@org.jetbrains.annotations.Nullable()
        java.lang.CharSequence s, int start, int before, int count) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.senarios.customer.databinding.ItemMedicineViewBinding getBinding() {
            return null;
        }
        
        public holder(@org.jetbrains.annotations.NotNull()
        com.senarios.customer.databinding.ItemMedicineViewBinding binding) {
            super(null);
        }
    }
}