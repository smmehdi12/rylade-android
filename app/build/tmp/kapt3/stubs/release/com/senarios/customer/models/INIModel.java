package com.senarios.customer.models;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u0019\u0018\u0000 \u001d2\u00020\u0001:\u0001\u001dB_\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0005\u0012\u0006\u0010\b\u001a\u00020\u0005\u0012\u0006\u0010\t\u001a\u00020\u0005\u0012\u0006\u0010\n\u001a\u00020\u0003\u0012\b\u0010\u000b\u001a\u0004\u0018\u00010\u0003\u0012\u0006\u0010\f\u001a\u00020\u0005\u0012\u0006\u0010\r\u001a\u00020\u0005\u0012\u0006\u0010\u000e\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u000fR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011R\u0013\u0010\u000b\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0011R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0014R\u0011\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0014R\u0011\u0010\r\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0014R\u0011\u0010\u0007\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0017\u0010\u0014R\u0011\u0010\b\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\u0014R\u0011\u0010\u000e\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\u0014R\u0011\u0010\t\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001a\u0010\u0014R\u0011\u0010\f\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001b\u0010\u0014R\u0011\u0010\n\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001c\u0010\u0011\u00a8\u0006\u001e"}, d2 = {"Lcom/senarios/customer/models/INIModel;", "", "created_at", "", "id", "", "medicine_instance", "medicine_non_instance", "parcel_instance", "parcel_non_instance", "updated_at", "delivery_fee", "rider_module", "medicine_module", "parcel_module", "(Ljava/lang/String;IIIIILjava/lang/String;Ljava/lang/String;III)V", "getCreated_at", "()Ljava/lang/String;", "getDelivery_fee", "getId", "()I", "getMedicine_instance", "getMedicine_module", "getMedicine_non_instance", "getParcel_instance", "getParcel_module", "getParcel_non_instance", "getRider_module", "getUpdated_at", "Companion", "app_release"})
public final class INIModel {
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String created_at = null;
    private final int id = 0;
    private final int medicine_instance = 0;
    private final int medicine_non_instance = 0;
    private final int parcel_instance = 0;
    private final int parcel_non_instance = 0;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String updated_at = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String delivery_fee = null;
    private final int rider_module = 0;
    private final int medicine_module = 0;
    private final int parcel_module = 0;
    public static final com.senarios.customer.models.INIModel.Companion Companion = null;
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getCreated_at() {
        return null;
    }
    
    public final int getId() {
        return 0;
    }
    
    public final int getMedicine_instance() {
        return 0;
    }
    
    public final int getMedicine_non_instance() {
        return 0;
    }
    
    public final int getParcel_instance() {
        return 0;
    }
    
    public final int getParcel_non_instance() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getUpdated_at() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getDelivery_fee() {
        return null;
    }
    
    public final int getRider_module() {
        return 0;
    }
    
    public final int getMedicine_module() {
        return 0;
    }
    
    public final int getParcel_module() {
        return 0;
    }
    
    public INIModel(@org.jetbrains.annotations.NotNull()
    java.lang.String created_at, int id, int medicine_instance, int medicine_non_instance, int parcel_instance, int parcel_non_instance, @org.jetbrains.annotations.NotNull()
    java.lang.String updated_at, @org.jetbrains.annotations.Nullable()
    java.lang.String delivery_fee, int rider_module, int medicine_module, int parcel_module) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0005\u001a\u00020\u0006\u00a8\u0006\u0007"}, d2 = {"Lcom/senarios/customer/models/INIModel$Companion;", "", "()V", "getModelFromSP", "Lcom/senarios/customer/models/INIModel;", "context", "Landroid/content/Context;", "app_release"})
    public static final class Companion {
        
        @org.jetbrains.annotations.Nullable()
        public final com.senarios.customer.models.INIModel getModelFromSP(@org.jetbrains.annotations.NotNull()
        android.content.Context context) {
            return null;
        }
        
        private Companion() {
            super();
        }
    }
}