package com.senarios.customer.models;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\b\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\bR\u0016\u0010\u0002\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0016\u0010\u0004\u001a\u00020\u00058\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0016\u0010\u0006\u001a\u00020\u00078\u0006X\u0087\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000e\u00a8\u0006\u000f"}, d2 = {"Lcom/senarios/customer/models/Elements;", "", "distance", "Lcom/senarios/customer/models/Distance;", "duration", "Lcom/senarios/customer/models/Duration;", "status", "", "(Lcom/senarios/customer/models/Distance;Lcom/senarios/customer/models/Duration;Ljava/lang/String;)V", "getDistance", "()Lcom/senarios/customer/models/Distance;", "getDuration", "()Lcom/senarios/customer/models/Duration;", "getStatus", "()Ljava/lang/String;", "app_release"})
public final class Elements {
    @org.jetbrains.annotations.NotNull()
    @com.google.gson.annotations.SerializedName(value = "distance")
    private final com.senarios.customer.models.Distance distance = null;
    @org.jetbrains.annotations.NotNull()
    @com.google.gson.annotations.SerializedName(value = "duration")
    private final com.senarios.customer.models.Duration duration = null;
    @org.jetbrains.annotations.NotNull()
    @com.google.gson.annotations.SerializedName(value = "status")
    private final java.lang.String status = null;
    
    @org.jetbrains.annotations.NotNull()
    public final com.senarios.customer.models.Distance getDistance() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.senarios.customer.models.Duration getDuration() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getStatus() {
        return null;
    }
    
    public Elements(@org.jetbrains.annotations.NotNull()
    com.senarios.customer.models.Distance distance, @org.jetbrains.annotations.NotNull()
    com.senarios.customer.models.Duration duration, @org.jetbrains.annotations.NotNull()
    java.lang.String status) {
        super();
    }
}