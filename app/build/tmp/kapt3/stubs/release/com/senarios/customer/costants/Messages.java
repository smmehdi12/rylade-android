package com.senarios.customer.costants;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\bf\u0018\u0000 \u00022\u00020\u0001:\u0001\u0002\u00a8\u0006\u0003"}, d2 = {"Lcom/senarios/customer/costants/Messages;", "", "Companion", "app_release"})
public abstract interface Messages {
    public static final com.senarios.customer.costants.Messages.Companion Companion = null;
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String NETWORK_ERROR = "Please enable Wifi | Mobile data";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String EMPTY_EMAIL = "Enter email";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String EMPTY_FIELD = "This Field Is Required";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String INVALID_EMAIL = "Enter valid email";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String NETWORK_ERROR_TITLE = "Network Error";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String PASSWORD_ERROR = "Password Must be 8 Characters";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String NULL_BODY_ERROR = "No Response from Server";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String ERROR_TITLE = "Error";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String INPUT_MISMATCH = "Both fields should be same";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String UNVERIFIED_TITLE = "Unverified User";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String UNVERIFIED_MESSAGE = "Your account is not verified, Do you want to verify it now?";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String VERIFIED_TITLE = "Verification Successful";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String VERIFIED_MESSAGE = "Your account has been verified! Please Login and enjoy Our Services";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String EMAIL_NO_EXIST = "Email Not Exists";
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u000e\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0012"}, d2 = {"Lcom/senarios/customer/costants/Messages$Companion;", "", "()V", "EMAIL_NO_EXIST", "", "EMPTY_EMAIL", "EMPTY_FIELD", "ERROR_TITLE", "INPUT_MISMATCH", "INVALID_EMAIL", "NETWORK_ERROR", "NETWORK_ERROR_TITLE", "NULL_BODY_ERROR", "PASSWORD_ERROR", "UNVERIFIED_MESSAGE", "UNVERIFIED_TITLE", "VERIFIED_MESSAGE", "VERIFIED_TITLE", "app_release"})
    public static final class Companion {
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String NETWORK_ERROR = "Please enable Wifi | Mobile data";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String EMPTY_EMAIL = "Enter email";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String EMPTY_FIELD = "This Field Is Required";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String INVALID_EMAIL = "Enter valid email";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String NETWORK_ERROR_TITLE = "Network Error";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String PASSWORD_ERROR = "Password Must be 8 Characters";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String NULL_BODY_ERROR = "No Response from Server";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String ERROR_TITLE = "Error";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String INPUT_MISMATCH = "Both fields should be same";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String UNVERIFIED_TITLE = "Unverified User";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String UNVERIFIED_MESSAGE = "Your account is not verified, Do you want to verify it now?";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String VERIFIED_TITLE = "Verification Successful";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String VERIFIED_MESSAGE = "Your account has been verified! Please Login and enjoy Our Services";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String EMAIL_NO_EXIST = "Email Not Exists";
        
        private Companion() {
            super();
        }
    }
}