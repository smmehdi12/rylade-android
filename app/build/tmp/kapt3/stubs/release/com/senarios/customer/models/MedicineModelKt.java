package com.senarios.customer.models;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 2, d1 = {"\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\u001a\u001a\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005H\u0007\u00a8\u0006\u0006"}, d2 = {"loadImageMedicine", "", "imageview", "Landroid/view/View;", "photo", "", "app_release"})
public final class MedicineModelKt {
    
    @androidx.databinding.BindingAdapter(value = {"loadImageMedicine"})
    public static final void loadImageMedicine(@org.jetbrains.annotations.NotNull()
    android.view.View imageview, @org.jetbrains.annotations.Nullable()
    java.lang.String photo) {
    }
}