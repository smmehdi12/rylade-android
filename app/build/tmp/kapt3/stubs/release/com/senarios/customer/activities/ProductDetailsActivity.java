package com.senarios.customer.activities;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000v\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u0003\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\n\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u00032\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00060\u00050\u0004B\u0005\u00a2\u0006\u0002\u0010\u0007J\b\u0010\u000f\u001a\u00020\u0010H\u0016J \u0010\u0011\u001a\u00020\u00102\u0006\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u0013H\u0016J\u0018\u0010\u0017\u001a\u00020\u00102\u0006\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0018\u001a\u00020\u0019H\u0016J\u0018\u0010\u001a\u001a\u00020\u00102\u0006\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0016\u001a\u00020\u0013H\u0016J\b\u0010\u001b\u001a\u00020\u0010H\u0016J.\u0010\u001c\u001a\u00020\u00102\u0006\u0010\u0012\u001a\u00020\u00132\f\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\u001f0\u001e2\u0006\u0010 \u001a\u00020\u00132\u0006\u0010\u0016\u001a\u00020\u0013H\u0016J&\u0010!\u001a\u00020\u00102\u0006\u0010\u0012\u001a\u00020\u00132\f\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\u001f0\u001e2\u0006\u0010 \u001a\u00020\u0013H\u0016J\b\u0010\"\u001a\u00020\u0010H\u0016J\b\u0010#\u001a\u00020\u0010H\u0002J\b\u0010$\u001a\u00020\u0010H\u0014J\b\u0010%\u001a\u00020&H\u0002J\u0018\u0010\'\u001a\u00020\u00102\u000e\u0010\u001d\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0005H\u0016J\u0012\u0010(\u001a\u00020\u00102\b\u0010)\u001a\u0004\u0018\u00010*H\u0016J\u0012\u0010+\u001a\u00020&2\b\u0010,\u001a\u0004\u0018\u00010-H\u0016J\u0010\u0010.\u001a\u00020&2\u0006\u0010/\u001a\u00020\fH\u0016J\b\u00100\u001a\u00020\u0010H\u0002J\b\u00101\u001a\u00020\u0010H\u0016J\b\u00102\u001a\u00020\u0010H\u0002J\u0010\u00103\u001a\u00020\u00102\u0006\u00104\u001a\u00020\u0015H\u0002J\u0010\u00105\u001a\u00020\u00102\u0006\u00106\u001a\u00020&H\u0002R\u000e\u0010\b\u001a\u00020\tX\u0082.\u00a2\u0006\u0002\n\u0000R\u0010\u0010\n\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u00067"}, d2 = {"Lcom/senarios/customer/activities/ProductDetailsActivity;", "Lcom/senarios/customer/activities/BaseAcivity;", "Landroid/view/View$OnClickListener;", "Lcom/senarios/customer/retrofit/ApiResponse;", "Landroidx/lifecycle/Observer;", "", "Lcom/senarios/customer/models/CartModel;", "()V", "binding", "Lcom/senarios/customer/databinding/ActivityProductDetailsBinding;", "cartModel", "menuItem", "Landroid/view/MenuItem;", "productModel", "Lcom/senarios/customer/models/ProductModel;", "OnCancelPermissioDialog", "", "OnError", "endpoint", "", "code", "", "message", "OnException", "exception", "", "OnNetworkError", "OnPermissionApproved", "OnStatusfalse", "t", "Lretrofit2/Response;", "Lcom/senarios/customer/models/ModelBaseResponse;", "response", "OnSuccess", "OnTrigger", "addToCart", "init", "isMenu", "", "onChanged", "onClick", "v", "Landroid/view/View;", "onCreateOptionsMenu", "menu", "Landroid/view/Menu;", "onOptionsItemSelected", "item", "popError", "setBinding", "setToolbar", "updateMenuItem", "count", "updateQuantity", "flag", "app_release"})
public final class ProductDetailsActivity extends com.senarios.customer.activities.BaseAcivity implements android.view.View.OnClickListener, com.senarios.customer.retrofit.ApiResponse, androidx.lifecycle.Observer<java.util.List<com.senarios.customer.models.CartModel>> {
    private com.senarios.customer.models.ProductModel productModel;
    private com.senarios.customer.databinding.ActivityProductDetailsBinding binding;
    private com.senarios.customer.models.CartModel cartModel;
    private android.view.MenuItem menuItem;
    private java.util.HashMap _$_findViewCache;
    
    @java.lang.Override()
    public void setBinding() {
    }
    
    @java.lang.Override()
    protected void init() {
    }
    
    private final void setToolbar() {
    }
    
    @java.lang.Override()
    public void OnCancelPermissioDialog() {
    }
    
    @java.lang.Override()
    public void OnPermissionApproved() {
    }
    
    @java.lang.Override()
    public void OnTrigger() {
    }
    
    @java.lang.Override()
    public void onClick(@org.jetbrains.annotations.Nullable()
    android.view.View v) {
    }
    
    private final void popError() {
    }
    
    private final void updateQuantity(boolean flag) {
    }
    
    private final void addToCart() {
    }
    
    @java.lang.Override()
    public void onChanged(@org.jetbrains.annotations.Nullable()
    java.util.List<com.senarios.customer.models.CartModel> t) {
    }
    
    @java.lang.Override()
    public boolean onCreateOptionsMenu(@org.jetbrains.annotations.Nullable()
    android.view.Menu menu) {
        return false;
    }
    
    private final void updateMenuItem(int count) {
    }
    
    @java.lang.Override()
    public boolean onOptionsItemSelected(@org.jetbrains.annotations.NotNull()
    android.view.MenuItem item) {
        return false;
    }
    
    private final boolean isMenu() {
        return false;
    }
    
    @java.lang.Override()
    public void OnSuccess(@org.jetbrains.annotations.NotNull()
    java.lang.String endpoint, @org.jetbrains.annotations.NotNull()
    retrofit2.Response<com.senarios.customer.models.ModelBaseResponse> t, @org.jetbrains.annotations.NotNull()
    java.lang.String response) {
    }
    
    @java.lang.Override()
    public void OnStatusfalse(@org.jetbrains.annotations.NotNull()
    java.lang.String endpoint, @org.jetbrains.annotations.NotNull()
    retrofit2.Response<com.senarios.customer.models.ModelBaseResponse> t, @org.jetbrains.annotations.NotNull()
    java.lang.String response, @org.jetbrains.annotations.NotNull()
    java.lang.String message) {
    }
    
    @java.lang.Override()
    public void OnError(@org.jetbrains.annotations.NotNull()
    java.lang.String endpoint, int code, @org.jetbrains.annotations.NotNull()
    java.lang.String message) {
    }
    
    @java.lang.Override()
    public void OnException(@org.jetbrains.annotations.NotNull()
    java.lang.String endpoint, @org.jetbrains.annotations.NotNull()
    java.lang.Throwable exception) {
    }
    
    @java.lang.Override()
    public void OnNetworkError(@org.jetbrains.annotations.NotNull()
    java.lang.String endpoint, @org.jetbrains.annotations.NotNull()
    java.lang.String message) {
    }
    
    public ProductDetailsActivity() {
        super();
    }
}