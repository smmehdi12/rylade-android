package com.senarios.customer.models;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0011\u0018\u00002\u00020\u0001B\u0017\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0005B\u001f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\b\u00a2\u0006\u0002\u0010\tB\u0005\u00a2\u0006\u0002\u0010\nR\u001a\u0010\u000b\u001a\u00020\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\f\u0010\r\"\u0004\b\u000e\u0010\u000fR\u001a\u0010\u0007\u001a\u00020\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0007\u0010\u0010\"\u0004\b\u0011\u0010\u0012R\u001a\u0010\u0002\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0013\u0010\u0014\"\u0004\b\u0015\u0010\u0016R\u001a\u0010\u0004\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0017\u0010\u0014\"\u0004\b\u0018\u0010\u0016\u00a8\u0006\u0019"}, d2 = {"Lcom/senarios/customer/models/ModelCommunication;", "", "key", "", "value", "(Ljava/lang/String;Ljava/lang/String;)V", "Lcom/google/gson/JsonArray;", "isJsonArray", "", "(Ljava/lang/String;Lcom/google/gson/JsonArray;Z)V", "()V", "array", "getArray", "()Lcom/google/gson/JsonArray;", "setArray", "(Lcom/google/gson/JsonArray;)V", "()Z", "setJsonArray", "(Z)V", "getKey", "()Ljava/lang/String;", "setKey", "(Ljava/lang/String;)V", "getValue", "setValue", "app_release"})
public final class ModelCommunication {
    @org.jetbrains.annotations.NotNull()
    private com.google.gson.JsonArray array;
    @org.jetbrains.annotations.NotNull()
    private java.lang.String key = "";
    @org.jetbrains.annotations.NotNull()
    private java.lang.String value = "";
    private boolean isJsonArray = false;
    
    @org.jetbrains.annotations.NotNull()
    public final com.google.gson.JsonArray getArray() {
        return null;
    }
    
    public final void setArray(@org.jetbrains.annotations.NotNull()
    com.google.gson.JsonArray p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getKey() {
        return null;
    }
    
    public final void setKey(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getValue() {
        return null;
    }
    
    public final void setValue(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    public final boolean isJsonArray() {
        return false;
    }
    
    public final void setJsonArray(boolean p0) {
    }
    
    public ModelCommunication() {
        super();
    }
    
    public ModelCommunication(@org.jetbrains.annotations.NotNull()
    java.lang.String key, @org.jetbrains.annotations.NotNull()
    java.lang.String value) {
        super();
    }
    
    public ModelCommunication(@org.jetbrains.annotations.NotNull()
    java.lang.String key, @org.jetbrains.annotations.NotNull()
    com.google.gson.JsonArray value, boolean isJsonArray) {
        super();
    }
}