package com.senarios.customer.adaptors;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\f\u0012\b\u0012\u00060\u0002R\u00020\u00000\u0001:\u0001\u0015B#\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006\u0012\u0006\u0010\b\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\b\u0010\u0011\u001a\u00020\u0012H\u0016J\u001c\u0010\u0013\u001a\u00020\u00142\n\u0010\u0015\u001a\u00060\u0002R\u00020\u00002\u0006\u0010\u0016\u001a\u00020\u0012H\u0016J\u001c\u0010\u0017\u001a\u00060\u0002R\u00020\u00002\u0006\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u0012H\u0016J\u000e\u0010\u001b\u001a\u00020\u00142\u0006\u0010\u0016\u001a\u00020\u0012R\u0017\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\b\u001a\u00020\t\u00a2\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u0011\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010\u00a8\u0006\u001c"}, d2 = {"Lcom/senarios/customer/adaptors/AdaptorAddress;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lcom/senarios/customer/adaptors/AdaptorAddress$holder;", "context", "Landroid/content/Context;", "addresses", "", "Lcom/senarios/customer/models/Address;", "callback", "Lcom/senarios/customer/adaptors/RecyclerViewCallback;", "(Landroid/content/Context;Ljava/util/List;Lcom/senarios/customer/adaptors/RecyclerViewCallback;)V", "getAddresses", "()Ljava/util/List;", "getCallback", "()Lcom/senarios/customer/adaptors/RecyclerViewCallback;", "getContext", "()Landroid/content/Context;", "getItemCount", "", "onBindViewHolder", "", "holder", "position", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "removeItem", "app_release"})
public final class AdaptorAddress extends androidx.recyclerview.widget.RecyclerView.Adapter<com.senarios.customer.adaptors.AdaptorAddress.holder> {
    @org.jetbrains.annotations.NotNull()
    private final android.content.Context context = null;
    @org.jetbrains.annotations.NotNull()
    private final java.util.List<com.senarios.customer.models.Address> addresses = null;
    @org.jetbrains.annotations.NotNull()
    private final com.senarios.customer.adaptors.RecyclerViewCallback callback = null;
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.senarios.customer.adaptors.AdaptorAddress.holder onCreateViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup parent, int viewType) {
        return null;
    }
    
    @java.lang.Override()
    public int getItemCount() {
        return 0;
    }
    
    @java.lang.Override()
    public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
    com.senarios.customer.adaptors.AdaptorAddress.holder holder, int position) {
    }
    
    public final void removeItem(int position) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.content.Context getContext() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.senarios.customer.models.Address> getAddresses() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.senarios.customer.adaptors.RecyclerViewCallback getCallback() {
        return null;
    }
    
    public AdaptorAddress(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    java.util.List<com.senarios.customer.models.Address> addresses, @org.jetbrains.annotations.NotNull()
    com.senarios.customer.adaptors.RecyclerViewCallback callback) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0004\u0018\u00002\u00020\u00012\u00020\u0002B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\u0012\u0010\b\u001a\u00020\t2\b\u0010\n\u001a\u0004\u0018\u00010\u000bH\u0016R\u0011\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007\u00a8\u0006\f"}, d2 = {"Lcom/senarios/customer/adaptors/AdaptorAddress$holder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "Landroid/view/View$OnClickListener;", "binding", "Lcom/senarios/customer/databinding/ItemAddressesBinding;", "(Lcom/senarios/customer/adaptors/AdaptorAddress;Lcom/senarios/customer/databinding/ItemAddressesBinding;)V", "getBinding", "()Lcom/senarios/customer/databinding/ItemAddressesBinding;", "onClick", "", "v", "Landroid/view/View;", "app_release"})
    public final class holder extends androidx.recyclerview.widget.RecyclerView.ViewHolder implements android.view.View.OnClickListener {
        @org.jetbrains.annotations.NotNull()
        private final com.senarios.customer.databinding.ItemAddressesBinding binding = null;
        
        @java.lang.Override()
        public void onClick(@org.jetbrains.annotations.Nullable()
        android.view.View v) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.senarios.customer.databinding.ItemAddressesBinding getBinding() {
            return null;
        }
        
        public holder(@org.jetbrains.annotations.NotNull()
        com.senarios.customer.databinding.ItemAddressesBinding binding) {
            super(null);
        }
    }
}