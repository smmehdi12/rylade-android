package com.senarios.rylad;

import java.lang.System;

/**
 * A simple [Fragment] subclass.
 */
@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u008e\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u0011\n\u0002\u0010\u000e\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0015\n\u0002\b\f\b&\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u0003B\u0005\u00a2\u0006\u0002\u0010\u0004J\u0006\u0010\u000f\u001a\u00020\u0010J\u0006\u0010\u0011\u001a\u00020\u0010J\u0006\u0010\u0012\u001a\u00020\u0010J\u0006\u0010\u0013\u001a\u00020\u0010J\u0006\u0010\u0014\u001a\u00020\u0010J\u0006\u0010\u0015\u001a\u00020\u0010J\u0006\u0010\u0016\u001a\u00020\u0010J\u0006\u0010\u0017\u001a\u00020\u0010J\n\u0010\u0018\u001a\u0006\u0012\u0002\b\u00030\bJ\u0006\u0010\u0019\u001a\u00020\nJ\u0006\u0010\u001a\u001a\u00020\nJ&\u0010\u001b\u001a\u0004\u0018\u00010\u001c2\u0006\u0010\u001d\u001a\u00020\u001e2\b\u0010\u001f\u001a\u0004\u0018\u00010 2\b\u0010!\u001a\u0004\u0018\u00010\"H&J\u0006\u0010#\u001a\u00020\fJ\u0006\u0010$\u001a\u00020%J\b\u0010&\u001a\u00020\'H\u0016J\u0006\u0010(\u001a\u00020\u000eJ%\u0010)\u001a\u00020*2\b\u0010+\u001a\u0004\u0018\u00010\f2\f\u0010,\u001a\b\u0012\u0004\u0012\u00020.0-H\u0016\u00a2\u0006\u0002\u0010/J\u0006\u00100\u001a\u00020\u0010J\b\u00101\u001a\u00020\u0010H$J\u0010\u00102\u001a\u00020*2\u0006\u0010+\u001a\u00020\fH\u0017J\u0010\u00103\u001a\u00020\u00102\u0006\u0010+\u001a\u00020\fH\u0016J\u0010\u00104\u001a\u00020\u00102\u0006\u00105\u001a\u000206H\u0016J\u0012\u00107\u001a\u00020\u00102\b\u0010!\u001a\u0004\u0018\u00010\"H\u0016J\u0012\u00108\u001a\u0002092\b\u0010!\u001a\u0004\u0018\u00010\"H\u0016J&\u0010:\u001a\u0004\u0018\u00010\u001c2\u0006\u0010\u001d\u001a\u00020\u001e2\b\u0010\u001f\u001a\u0004\u0018\u00010 2\b\u0010!\u001a\u0004\u0018\u00010\"H\u0016J\u0010\u0010;\u001a\u00020\u00102\u0006\u00105\u001a\u000206H\u0016J-\u0010<\u001a\u00020\u00102\u0006\u0010=\u001a\u00020\'2\u000e\u0010,\u001a\n\u0012\u0006\b\u0001\u0012\u00020.0-2\u0006\u0010>\u001a\u00020?H\u0016\u00a2\u0006\u0002\u0010@J\b\u0010A\u001a\u00020\u0010H\u0016J\u0006\u0010B\u001a\u00020\u0010J1\u0010C\u001a\u00020\u00102\u0006\u0010D\u001a\u00020.2\u0006\u0010E\u001a\u00020.2\f\u0010F\u001a\b\u0012\u0004\u0012\u00020.0-2\u0006\u0010G\u001a\u00020*\u00a2\u0006\u0002\u0010HJ\u0006\u0010I\u001a\u00020\u0010J\u0018\u0010J\u001a\u00020\u00102\b\u0010+\u001a\u0004\u0018\u00010\f2\u0006\u0010E\u001a\u00020.R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082.\u00a2\u0006\u0002\n\u0000R\u0012\u0010\u0007\u001a\u0006\u0012\u0002\b\u00030\bX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000b\u001a\u0004\u0018\u00010\fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006K"}, d2 = {"Lcom/senarios/rylad/BaseDialogeFragment;", "Lcom/senarios/customer/costants/Messages;", "Lcom/google/android/material/bottomsheet/BottomSheetDialogFragment;", "Lcom/senarios/customer/callbacks/ActivityStates;", "()V", "activityFragment", "Lcom/senarios/customer/callbacks/ActivityFragment;", "behaviour", "Lcom/google/android/material/bottomsheet/BottomSheetBehavior;", "dialgoCallback", "Lcom/senarios/customer/dialogfragments/DialogeCallback;", "reference", "Landroid/content/Context;", "sharedVM", "Lcom/senarios/customer/viewmodel/SharedVM;", "checkCameraPermission", "", "checkLocationPermission", "checkMYPermission", "checkMediaPermission", "checkSMSPermission", "checkStoragePermission", "collapse", "expand", "getBehaviour", "getDialogCB", "getFragmentChanger", "getFragmentView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "getReference", "getService", "Lcom/senarios/customer/retrofit/DataService;", "getTheme", "", "getViewModel", "hasPermissions", "", "context", "permissions", "", "", "(Landroid/content/Context;[Ljava/lang/String;)Z", "hideSoftKeyboard", "init", "isConnected", "onAttach", "onCancel", "dialog", "Landroid/content/DialogInterface;", "onCreate", "onCreateDialog", "Landroid/app/Dialog;", "onCreateView", "onDismiss", "onRequestPermissionsResult", "requestCode", "grantResults", "", "(I[Ljava/lang/String;[I)V", "onResume", "showHideSoftKeyboard", "showPermissionDialog", "title", "message", "permission", "isSetting", "(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Z)V", "showSoftKeyboard", "showToast", "app_release"})
public abstract class BaseDialogeFragment extends com.google.android.material.bottomsheet.BottomSheetDialogFragment implements com.senarios.customer.costants.Messages, com.senarios.customer.callbacks.ActivityStates {
    private com.senarios.customer.callbacks.ActivityFragment activityFragment;
    private com.senarios.customer.viewmodel.SharedVM sharedVM;
    private com.senarios.customer.dialogfragments.DialogeCallback dialgoCallback;
    private com.google.android.material.bottomsheet.BottomSheetBehavior<?> behaviour;
    private android.content.Context reference;
    private java.util.HashMap _$_findViewCache;
    
    @java.lang.Override()
    public void onAttach(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.google.android.material.bottomsheet.BottomSheetBehavior<?> getBehaviour() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.content.Context getReference() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public android.app.Dialog onCreateDialog(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    @java.lang.Override()
    public void onResume() {
    }
    
    protected abstract void init();
    
    public final void expand() {
    }
    
    public final void collapse() {
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public abstract android.view.View getFragmentView(@org.jetbrains.annotations.NotNull()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState);
    
    public final void showToast(@org.jetbrains.annotations.Nullable()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    java.lang.String message) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.senarios.customer.dialogfragments.DialogeCallback getDialogCB() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.senarios.customer.viewmodel.SharedVM getViewModel() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.senarios.customer.retrofit.DataService getService() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.senarios.customer.dialogfragments.DialogeCallback getFragmentChanger() {
        return null;
    }
    
    @java.lang.Deprecated()
    public boolean isConnected(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
        return false;
    }
    
    public final void hideSoftKeyboard() {
    }
    
    public final void showSoftKeyboard() {
    }
    
    public final void showHideSoftKeyboard() {
    }
    
    @java.lang.Override()
    public void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @java.lang.Override()
    public int getTheme() {
        return 0;
    }
    
    @java.lang.Override()
    public void onDismiss(@org.jetbrains.annotations.NotNull()
    android.content.DialogInterface dialog) {
    }
    
    @java.lang.Override()
    public void onCancel(@org.jetbrains.annotations.NotNull()
    android.content.DialogInterface dialog) {
    }
    
    @java.lang.Override()
    public void onRequestPermissionsResult(int requestCode, @org.jetbrains.annotations.NotNull()
    java.lang.String[] permissions, @org.jetbrains.annotations.NotNull()
    int[] grantResults) {
    }
    
    public final void showPermissionDialog(@org.jetbrains.annotations.NotNull()
    java.lang.String title, @org.jetbrains.annotations.NotNull()
    java.lang.String message, @org.jetbrains.annotations.NotNull()
    java.lang.String[] permission, boolean isSetting) {
    }
    
    public boolean hasPermissions(@org.jetbrains.annotations.Nullable()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    java.lang.String[] permissions) {
        return false;
    }
    
    public final void checkMYPermission() {
    }
    
    public final void checkSMSPermission() {
    }
    
    public final void checkLocationPermission() {
    }
    
    public final void checkStoragePermission() {
    }
    
    public final void checkCameraPermission() {
    }
    
    public final void checkMediaPermission() {
    }
    
    public BaseDialogeFragment() {
        super();
    }
}