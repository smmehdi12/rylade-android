package com.senarios.customer.costants;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\bf\u0018\u0000 \u00022\u00020\u0001:\u0001\u0002\u00a8\u0006\u0003"}, d2 = {"Lcom/senarios/customer/costants/Codes;", "", "Companion", "app_release"})
public abstract interface Codes {
    public static final com.senarios.customer.costants.Codes.Companion Companion = null;
    public static final int ALL_PERMISSION_REQUEST = 0;
    public static final int SMS_PERMISSION_REQUEST = 10;
    public static final int LOCATION_PERMISSION_REQUEST = 20;
    public static final int STORAGE_PERMISSION_REQUEST = 30;
    public static final int CAMERA_PERMISSION_REQUEST = 40;
    public static final int MEDIA_PERMISSION_REQUEST = 50;
    public static final int ADDRESS_REQUEST = 60;
    public static final int IMAGE_REQUEST = 40;
    public static final int LATLNG_REQUEST_CODE = 70;
    public static final int CONTACTS_PERMISSION_REQUEST = 80;
    public static final int SENDER_CONTACTS_REQUEST = 90;
    public static final int RECEIVER_CONTACTS_REQUEST = 100;
    public static final int SENDER_ADDRESS_REQUEST = 110;
    public static final int RECEIVER_ADDRESS_REQUEST = 120;
    public static final int RIDE_PICKUP_REQUEST = 130;
    public static final int RIDE_DROPOFF_REQUEST = 140;
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0010\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0014"}, d2 = {"Lcom/senarios/customer/costants/Codes$Companion;", "", "()V", "ADDRESS_REQUEST", "", "ALL_PERMISSION_REQUEST", "CAMERA_PERMISSION_REQUEST", "CONTACTS_PERMISSION_REQUEST", "IMAGE_REQUEST", "LATLNG_REQUEST_CODE", "LOCATION_PERMISSION_REQUEST", "MEDIA_PERMISSION_REQUEST", "RECEIVER_ADDRESS_REQUEST", "RECEIVER_CONTACTS_REQUEST", "RIDE_DROPOFF_REQUEST", "RIDE_PICKUP_REQUEST", "SENDER_ADDRESS_REQUEST", "SENDER_CONTACTS_REQUEST", "SMS_PERMISSION_REQUEST", "STORAGE_PERMISSION_REQUEST", "app_release"})
    public static final class Companion {
        public static final int ALL_PERMISSION_REQUEST = 0;
        public static final int SMS_PERMISSION_REQUEST = 10;
        public static final int LOCATION_PERMISSION_REQUEST = 20;
        public static final int STORAGE_PERMISSION_REQUEST = 30;
        public static final int CAMERA_PERMISSION_REQUEST = 40;
        public static final int MEDIA_PERMISSION_REQUEST = 50;
        public static final int ADDRESS_REQUEST = 60;
        public static final int IMAGE_REQUEST = 40;
        public static final int LATLNG_REQUEST_CODE = 70;
        public static final int CONTACTS_PERMISSION_REQUEST = 80;
        public static final int SENDER_CONTACTS_REQUEST = 90;
        public static final int RECEIVER_CONTACTS_REQUEST = 100;
        public static final int SENDER_ADDRESS_REQUEST = 110;
        public static final int RECEIVER_ADDRESS_REQUEST = 120;
        public static final int RIDE_PICKUP_REQUEST = 130;
        public static final int RIDE_DROPOFF_REQUEST = 140;
        
        private Companion() {
            super();
        }
    }
}