package com.senarios.customer;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\u0018\u0000 \u00122\u00020\u00012\b\u0012\u0004\u0012\u00020\u00030\u0002:\u0001\u0012B\u0005\u00a2\u0006\u0002\u0010\u0004J\u0006\u0010\u000b\u001a\u00020\fJ\b\u0010\r\u001a\u00020\u000eH\u0016J\u0012\u0010\u000f\u001a\u00020\u000e2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0003H\u0016J\b\u0010\u0011\u001a\u00020\u000eH\u0002R\u001a\u0010\u0005\u001a\u00020\u0006X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0007\u0010\b\"\u0004\b\t\u0010\n\u00a8\u0006\u0013"}, d2 = {"Lcom/senarios/customer/Rylade;", "Landroid/app/Application;", "Lcom/google/android/gms/tasks/OnSuccessListener;", "Lcom/google/firebase/iid/InstanceIdResult;", "()V", "component", "Lcom/senarios/customer/di/Component;", "getComponent", "()Lcom/senarios/customer/di/Component;", "setComponent", "(Lcom/senarios/customer/di/Component;)V", "getDB", "Lcom/senarios/customer/db/DAO;", "onCreate", "", "onSuccess", "p0", "setUpComponent", "Companion", "app_release"})
public final class Rylade extends android.app.Application implements com.google.android.gms.tasks.OnSuccessListener<com.google.firebase.iid.InstanceIdResult> {
    @org.jetbrains.annotations.NotNull()
    public com.senarios.customer.di.Component component;
    private static com.senarios.customer.Rylade mInstance;
    public static final com.senarios.customer.Rylade.Companion Companion = null;
    
    @org.jetbrains.annotations.NotNull()
    public final com.senarios.customer.di.Component getComponent() {
        return null;
    }
    
    public final void setComponent(@org.jetbrains.annotations.NotNull()
    com.senarios.customer.di.Component p0) {
    }
    
    @java.lang.Override()
    public void onCreate() {
    }
    
    private final void setUpComponent() {
    }
    
    @java.lang.Override()
    public void onSuccess(@org.jetbrains.annotations.Nullable()
    com.google.firebase.iid.InstanceIdResult p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.senarios.customer.db.DAO getDB() {
        return null;
    }
    
    public Rylade() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u0005\u001a\u00020\u0004R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0006"}, d2 = {"Lcom/senarios/customer/Rylade$Companion;", "", "()V", "mInstance", "Lcom/senarios/customer/Rylade;", "getInstance", "app_release"})
    public static final class Companion {
        
        @org.jetbrains.annotations.NotNull()
        public final com.senarios.customer.Rylade getInstance() {
            return null;
        }
        
        private Companion() {
            super();
        }
    }
}