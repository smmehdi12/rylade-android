package com.senarios.customer.adaptors;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010!\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0000\u0018\u00002\f\u0012\b\u0012\u00060\u0002R\u00020\u00000\u0001:\u0001\u0016B\u0015\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\f\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u000e0\u0011J\b\u0010\u0012\u001a\u00020\u0013H\u0016J\u001c\u0010\u0014\u001a\u00020\u00152\n\u0010\u0016\u001a\u00060\u0002R\u00020\u00002\u0006\u0010\u0017\u001a\u00020\u0013H\u0016J\u001c\u0010\u0018\u001a\u00060\u0002R\u00020\u00002\u0006\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u0013H\u0016J\u0014\u0010\u001c\u001a\u00020\u00152\f\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u000e0\u001dR\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0011\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u001e\u0010\f\u001a\u0012\u0012\u0004\u0012\u00020\u000e0\rj\b\u0012\u0004\u0012\u00020\u000e`\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001e"}, d2 = {"Lcom/senarios/customer/adaptors/AdaptorCartItems;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lcom/senarios/customer/adaptors/AdaptorCartItems$holder;", "context", "Landroid/content/Context;", "callback", "Lcom/senarios/customer/adaptors/RecyclerViewCallback;", "(Landroid/content/Context;Lcom/senarios/customer/adaptors/RecyclerViewCallback;)V", "getCallback", "()Lcom/senarios/customer/adaptors/RecyclerViewCallback;", "getContext", "()Landroid/content/Context;", "list", "Ljava/util/ArrayList;", "Lcom/senarios/customer/models/CartModel;", "Lkotlin/collections/ArrayList;", "getData", "", "getItemCount", "", "onBindViewHolder", "", "holder", "position", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "setData", "", "app_release"})
public final class AdaptorCartItems extends androidx.recyclerview.widget.RecyclerView.Adapter<com.senarios.customer.adaptors.AdaptorCartItems.holder> {
    private final java.util.ArrayList<com.senarios.customer.models.CartModel> list = null;
    @org.jetbrains.annotations.NotNull()
    private final android.content.Context context = null;
    @org.jetbrains.annotations.NotNull()
    private final com.senarios.customer.adaptors.RecyclerViewCallback callback = null;
    
    public final void setData(@org.jetbrains.annotations.NotNull()
    java.util.List<com.senarios.customer.models.CartModel> list) {
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.senarios.customer.adaptors.AdaptorCartItems.holder onCreateViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup parent, int viewType) {
        return null;
    }
    
    @java.lang.Override()
    public int getItemCount() {
        return 0;
    }
    
    @java.lang.Override()
    public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
    com.senarios.customer.adaptors.AdaptorCartItems.holder holder, int position) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.senarios.customer.models.CartModel> getData() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.content.Context getContext() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.senarios.customer.adaptors.RecyclerViewCallback getCallback() {
        return null;
    }
    
    public AdaptorCartItems(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    com.senarios.customer.adaptors.RecyclerViewCallback callback) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0004\u0018\u00002\u00020\u00012\u00020\u0002B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\u0012\u0010\b\u001a\u00020\t2\b\u0010\n\u001a\u0004\u0018\u00010\u000bH\u0016R\u0011\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007\u00a8\u0006\f"}, d2 = {"Lcom/senarios/customer/adaptors/AdaptorCartItems$holder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "Landroid/view/View$OnClickListener;", "binding", "Lcom/senarios/customer/databinding/ItemCartItemBinding;", "(Lcom/senarios/customer/adaptors/AdaptorCartItems;Lcom/senarios/customer/databinding/ItemCartItemBinding;)V", "getBinding", "()Lcom/senarios/customer/databinding/ItemCartItemBinding;", "onClick", "", "v", "Landroid/view/View;", "app_release"})
    public final class holder extends androidx.recyclerview.widget.RecyclerView.ViewHolder implements android.view.View.OnClickListener {
        @org.jetbrains.annotations.NotNull()
        private final com.senarios.customer.databinding.ItemCartItemBinding binding = null;
        
        @java.lang.Override()
        public void onClick(@org.jetbrains.annotations.Nullable()
        android.view.View v) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.senarios.customer.databinding.ItemCartItemBinding getBinding() {
            return null;
        }
        
        public holder(@org.jetbrains.annotations.NotNull()
        com.senarios.customer.databinding.ItemCartItemBinding binding) {
            super(null);
        }
    }
}