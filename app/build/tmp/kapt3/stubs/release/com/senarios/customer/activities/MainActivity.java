package com.senarios.customer.activities;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u00bc\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0011\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\u000b\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0015\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u00032\u00020\u00042\u00020\u00052\u00020\u0006B\u0005\u00a2\u0006\u0002\u0010\u0007J\b\u0010#\u001a\u00020$H\u0016J\u0018\u0010%\u001a\u00020$2\u0006\u0010&\u001a\u00020\'2\u0006\u0010(\u001a\u00020\u000bH\u0016J\u0010\u0010%\u001a\u00020$2\u0006\u0010&\u001a\u00020)H\u0016J\b\u0010*\u001a\u00020$H\u0016J\b\u0010+\u001a\u00020$H\u0016J\b\u0010,\u001a\u00020$H\u0002J\u0006\u0010-\u001a\u00020\rJ\b\u0010.\u001a\u00020$H\u0002J\u0006\u0010/\u001a\u00020$J\b\u00100\u001a\u00020$H\u0014J\b\u00101\u001a\u000202H\u0002J\b\u00103\u001a\u000202H\u0002J\b\u00104\u001a\u00020$H\u0002J\b\u00105\u001a\u00020$H\u0002J\"\u00106\u001a\u00020$2\u0006\u00107\u001a\u00020\t2\u0006\u00108\u001a\u00020\t2\b\u00109\u001a\u0004\u0018\u00010:H\u0014J\b\u0010;\u001a\u00020$H\u0016J\u0012\u0010<\u001a\u00020$2\b\u0010=\u001a\u0004\u0018\u00010>H\u0017J0\u0010?\u001a\u00020$2\f\u0010@\u001a\b\u0012\u0002\b\u0003\u0018\u00010A2\b\u0010B\u001a\u0004\u0018\u00010>2\u0006\u0010C\u001a\u00020\t2\u0006\u0010D\u001a\u00020EH\u0016J\u0010\u0010F\u001a\u0002022\u0006\u0010G\u001a\u00020HH\u0016J\u0016\u0010I\u001a\u00020$2\f\u0010@\u001a\b\u0012\u0002\b\u0003\u0018\u00010AH\u0016J-\u0010J\u001a\u00020$2\u0006\u00107\u001a\u00020\t2\u000e\u0010K\u001a\n\u0012\u0006\b\u0001\u0012\u00020\u000b0\u001f2\u0006\u0010L\u001a\u00020MH\u0016\u00a2\u0006\u0002\u0010NJ\b\u0010O\u001a\u00020$H\u0014J\b\u0010P\u001a\u00020$H\u0014J\b\u0010Q\u001a\u00020$H\u0002J\b\u0010R\u001a\u00020$H\u0002J\b\u0010S\u001a\u00020$H\u0016J\u0010\u0010T\u001a\u00020$2\u0006\u0010U\u001a\u00020VH\u0016J\u0010\u0010W\u001a\u00020$2\u0006\u0010X\u001a\u00020\tH\u0002J\b\u0010Y\u001a\u00020$H\u0002J\u0010\u0010Z\u001a\u00020$2\u0006\u0010X\u001a\u00020\tH\u0002R\u000e\u0010\b\u001a\u00020\tX\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\rX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082.\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0010\u001a\u0004\u0018\u00010\u0011X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001e\u0010\u0012\u001a\u00020\u00138\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0014\u0010\u0015\"\u0004\b\u0016\u0010\u0017R\u000e\u0010\u0018\u001a\u00020\u0019X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u001bX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001c\u001a\u00020\u001dX\u0082.\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u001e\u001a\b\u0012\u0004\u0012\u00020\u000b0\u001fX\u0082\u0004\u00a2\u0006\u0004\n\u0002\u0010 R\u000e\u0010!\u001a\u00020\"X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006["}, d2 = {"Lcom/senarios/customer/activities/MainActivity;", "Lcom/senarios/customer/activities/BaseAcivity;", "Lcom/senarios/customer/callbacks/FragmentChanger;", "Lcom/senarios/customer/dialogfragments/DialogeCallback;", "Landroid/widget/AdapterView$OnItemSelectedListener;", "Landroid/view/View$OnClickListener;", "Lcom/google/android/material/navigation/NavigationView$OnNavigationItemSelectedListener;", "()V", "REQUEST_CHECK_SETTINGS", "", "TAG", "", "binding", "Lcom/senarios/customer/databinding/ActivityMainBinding;", "builder", "Lcom/google/android/gms/location/LocationSettingsRequest$Builder;", "dialog", "Lcom/senarios/customer/dialogfragments/RatingFragmentDialog;", "directory", "Lcom/senarios/customer/models/Directory;", "getDirectory", "()Lcom/senarios/customer/models/Directory;", "setDirectory", "(Lcom/senarios/customer/models/Directory;)V", "location", "Landroid/location/Location;", "locationCallback", "Lcom/google/android/gms/location/LocationCallback;", "locationRequest", "Lcom/google/android/gms/location/LocationRequest;", "selectionList", "", "[Ljava/lang/String;", "sharedVM", "Lcom/senarios/customer/viewmodel/SharedVM;", "OnCancelPermissioDialog", "", "OnChange", "fragment", "Landroidx/fragment/app/Fragment;", "tag", "Lcom/senarios/rylad/BaseDialogeFragment;", "OnPermissionApproved", "OnTrigger", "createLocationRequest", "getbinding", "handleReviewBox", "inflateMenu", "init", "isLocation", "", "isLocationCallback", "locationRequestTask", "logout", "onActivityResult", "requestCode", "resultCode", "data", "Landroid/content/Intent;", "onBackPressed", "onClick", "v", "Landroid/view/View;", "onItemSelected", "parent", "Landroid/widget/AdapterView;", "view", "position", "id", "", "onNavigationItemSelected", "item", "Landroid/view/MenuItem;", "onNothingSelected", "onRequestPermissionsResult", "permissions", "grantResults", "", "(I[Ljava/lang/String;[I)V", "onResume", "onStop", "removeRequestLocation", "requestLocation", "setBinding", "setComponent", "component", "Lcom/senarios/customer/di/Component;", "setSelection", "selection", "showSelectionDialog", "sortSelection", "app_release"})
public final class MainActivity extends com.senarios.customer.activities.BaseAcivity implements com.senarios.customer.callbacks.FragmentChanger, com.senarios.customer.dialogfragments.DialogeCallback, android.widget.AdapterView.OnItemSelectedListener, android.view.View.OnClickListener, com.google.android.material.navigation.NavigationView.OnNavigationItemSelectedListener {
    private final int REQUEST_CHECK_SETTINGS = 1000;
    private com.senarios.customer.databinding.ActivityMainBinding binding;
    private final java.lang.String TAG = null;
    private com.google.android.gms.location.LocationRequest locationRequest;
    private com.google.android.gms.location.LocationCallback locationCallback;
    private android.location.Location location;
    private com.google.android.gms.location.LocationSettingsRequest.Builder builder;
    private com.senarios.customer.viewmodel.SharedVM sharedVM;
    @org.jetbrains.annotations.NotNull()
    @javax.inject.Inject()
    public com.senarios.customer.models.Directory directory;
    private com.senarios.customer.dialogfragments.RatingFragmentDialog dialog;
    private final java.lang.String[] selectionList = {"Current Location", "Other Location"};
    private java.util.HashMap _$_findViewCache;
    
    @org.jetbrains.annotations.NotNull()
    public final com.senarios.customer.models.Directory getDirectory() {
        return null;
    }
    
    public final void setDirectory(@org.jetbrains.annotations.NotNull()
    com.senarios.customer.models.Directory p0) {
    }
    
    @java.lang.Override()
    public void setComponent(@org.jetbrains.annotations.NotNull()
    com.senarios.customer.di.Component component) {
    }
    
    @java.lang.Override()
    public void setBinding() {
    }
    
    @java.lang.Override()
    protected void onResume() {
    }
    
    @java.lang.Override()
    public void onBackPressed() {
    }
    
    @java.lang.Override()
    public void OnChange(@org.jetbrains.annotations.NotNull()
    androidx.fragment.app.Fragment fragment, @org.jetbrains.annotations.NotNull()
    java.lang.String tag) {
    }
    
    @java.lang.Override()
    public void OnChange(@org.jetbrains.annotations.NotNull()
    com.senarios.rylad.BaseDialogeFragment fragment) {
    }
    
    @java.lang.Override()
    public void OnCancelPermissioDialog() {
    }
    
    @java.lang.Override()
    public void OnPermissionApproved() {
    }
    
    @java.lang.Override()
    public void OnTrigger() {
    }
    
    @java.lang.Override()
    protected void init() {
    }
    
    private final void handleReviewBox() {
    }
    
    @java.lang.Override()
    public boolean onNavigationItemSelected(@org.jetbrains.annotations.NotNull()
    android.view.MenuItem item) {
        return false;
    }
    
    private final void logout() {
    }
    
    @android.annotation.SuppressLint(value = {"WrongConstant"})
    @java.lang.Override()
    public void onClick(@org.jetbrains.annotations.Nullable()
    android.view.View v) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.senarios.customer.databinding.ActivityMainBinding getbinding() {
        return null;
    }
    
    public final void inflateMenu() {
    }
    
    @java.lang.Override()
    protected void onActivityResult(int requestCode, int resultCode, @org.jetbrains.annotations.Nullable()
    android.content.Intent data) {
    }
    
    private final void createLocationRequest() {
    }
    
    private final void locationRequestTask() {
    }
    
    @java.lang.Override()
    public void onRequestPermissionsResult(int requestCode, @org.jetbrains.annotations.NotNull()
    java.lang.String[] permissions, @org.jetbrains.annotations.NotNull()
    int[] grantResults) {
    }
    
    private final boolean isLocation() {
        return false;
    }
    
    @java.lang.Override()
    protected void onStop() {
    }
    
    private final void requestLocation() {
    }
    
    private final void removeRequestLocation() {
    }
    
    private final boolean isLocationCallback() {
        return false;
    }
    
    @java.lang.Override()
    public void onNothingSelected(@org.jetbrains.annotations.Nullable()
    android.widget.AdapterView<?> parent) {
    }
    
    @java.lang.Override()
    public void onItemSelected(@org.jetbrains.annotations.Nullable()
    android.widget.AdapterView<?> parent, @org.jetbrains.annotations.Nullable()
    android.view.View view, int position, long id) {
    }
    
    private final void setSelection(int selection) {
    }
    
    private final void showSelectionDialog() {
    }
    
    private final void sortSelection(int selection) {
    }
    
    public MainActivity() {
        super();
    }
}