package com.senarios.customer.activities;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u00bc\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\u0003\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u00032\u00020\u00042\u00020\u00052\u00020\u0006B\u0005\u00a2\u0006\u0002\u0010\u0007J\b\u0010 \u001a\u00020!H\u0016J\u0018\u0010\"\u001a\u00020!2\u0006\u0010#\u001a\u00020\u00172\u0006\u0010$\u001a\u00020%H\u0016J \u0010&\u001a\u00020!2\u0006\u0010\'\u001a\u00020(2\u0006\u0010)\u001a\u00020\u00172\u0006\u0010*\u001a\u00020(H\u0016J\u0018\u0010+\u001a\u00020!2\u0006\u0010\'\u001a\u00020(2\u0006\u0010,\u001a\u00020-H\u0016J\u0018\u0010.\u001a\u00020!2\u0006\u0010\'\u001a\u00020(2\u0006\u0010*\u001a\u00020(H\u0016J\b\u0010/\u001a\u00020!H\u0016J.\u00100\u001a\u00020!2\u0006\u0010\'\u001a\u00020(2\f\u00101\u001a\b\u0012\u0004\u0012\u000203022\u0006\u00104\u001a\u00020(2\u0006\u0010*\u001a\u00020(H\u0016J&\u00105\u001a\u00020!2\u0006\u0010\'\u001a\u00020(2\f\u00101\u001a\b\u0012\u0004\u0012\u000203022\u0006\u00104\u001a\u00020(H\u0016J\b\u00106\u001a\u00020!H\u0016J\b\u00107\u001a\u00020!H\u0002J\b\u00108\u001a\u00020!H\u0002J\b\u00109\u001a\u00020:H\u0002J\b\u0010;\u001a\u00020!H\u0014J\b\u0010<\u001a\u00020!H\u0002J\b\u0010=\u001a\u00020!H\u0002J\b\u0010>\u001a\u00020!H\u0002J\b\u0010?\u001a\u00020!H\u0002J\b\u0010@\u001a\u00020AH\u0002J\b\u0010B\u001a\u00020AH\u0002J\b\u0010C\u001a\u00020AH\u0002J\b\u0010D\u001a\u00020AH\u0002J\u0018\u0010E\u001a\u00020!2\u0006\u0010F\u001a\u00020:2\u0006\u0010G\u001a\u00020HH\u0002J\u0012\u0010I\u001a\u00020!2\b\u0010J\u001a\u0004\u0018\u00010KH\u0016J\b\u0010L\u001a\u00020!H\u0014J\b\u0010M\u001a\u00020!H\u0016J\u0012\u0010N\u001a\u00020!2\b\u0010O\u001a\u0004\u0018\u00010HH\u0016J\u001a\u0010P\u001a\u00020!2\b\u0010Q\u001a\u0004\u0018\u00010R2\u0006\u0010S\u001a\u00020\u0017H\u0016J\b\u0010T\u001a\u00020!H\u0014J\b\u0010U\u001a\u00020!H\u0014J\b\u0010V\u001a\u00020!H\u0014J\b\u0010W\u001a\u00020!H\u0016J\b\u0010X\u001a\u00020!H\u0002R\u0010\u0010\b\u001a\u0004\u0018\u00010\tX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u000e0\rX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0017X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0019X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u001bX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001c\u001a\u00020\u001dX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001e\u001a\u00020\u001fX\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006Y"}, d2 = {"Lcom/senarios/customer/activities/OrderDetailsActivity;", "Lcom/senarios/customer/activities/BaseAcivity;", "Lcom/google/android/material/appbar/AppBarLayout$OnOffsetChangedListener;", "Lcom/senarios/customer/retrofit/ApiResponse;", "Lcom/google/android/gms/maps/OnMapReadyCallback;", "Landroid/view/View$OnClickListener;", "Lcom/senarios/customer/adaptors/RecyclerViewCallback;", "()V", "bundle", "Landroid/os/Bundle;", "ecommerceOrderResponse", "Lcom/senarios/customer/models/BaseEcommerceOrderResponse;", "medicineList", "", "Lcom/senarios/customer/models/MedicineModel;", "medicineModel", "order", "Lcom/senarios/customer/models/OrderModel;", "parcelOrderModel", "Lcom/senarios/customer/models/ParcelModel;", "rideModel", "Lcom/senarios/customer/models/RideModel;", "scrollRange", "", "vEcommerceBinding", "Lcom/senarios/customer/databinding/FragmentOrderEcommerceDetailsBinding;", "vMedicineBinding", "Lcom/senarios/customer/databinding/FragmentOrderMedicineDetailsBinding;", "vParcelbinding", "Lcom/senarios/customer/databinding/FragmentOrderParcelDetailsBinding;", "vRideBinding", "Lcom/senarios/customer/databinding/FragmentOrderRideDetailsBinding;", "OnCancelPermissioDialog", "", "OnClick", "position", "any", "", "OnError", "endpoint", "", "code", "message", "OnException", "exception", "", "OnNetworkError", "OnPermissionApproved", "OnStatusfalse", "t", "Lretrofit2/Response;", "Lcom/senarios/customer/models/ModelBaseResponse;", "response", "OnSuccess", "OnTrigger", "callCancelAPI", "callOrderDetailAPI", "getBounds", "Lcom/google/android/gms/maps/model/LatLngBounds;", "init", "initEcommerceDetails", "initMedicineDetails", "initParcelDetails", "initRideDetails", "isEcom", "", "isMedicine", "isParcel", "isRide", "moveCamera", "bounds", "map", "Lcom/google/android/gms/maps/GoogleMap;", "onClick", "v", "Landroid/view/View;", "onDestroy", "onLowMemory", "onMapReady", "p0", "onOffsetChanged", "appBarLayout", "Lcom/google/android/material/appbar/AppBarLayout;", "verticalOffset", "onResume", "onStart", "onStop", "setBinding", "setRideData", "app_release"})
public final class OrderDetailsActivity extends com.senarios.customer.activities.BaseAcivity implements com.google.android.material.appbar.AppBarLayout.OnOffsetChangedListener, com.senarios.customer.retrofit.ApiResponse, com.google.android.gms.maps.OnMapReadyCallback, android.view.View.OnClickListener, com.senarios.customer.adaptors.RecyclerViewCallback {
    private com.senarios.customer.databinding.FragmentOrderParcelDetailsBinding vParcelbinding;
    private com.senarios.customer.databinding.FragmentOrderMedicineDetailsBinding vMedicineBinding;
    private com.senarios.customer.databinding.FragmentOrderRideDetailsBinding vRideBinding;
    private com.senarios.customer.databinding.FragmentOrderEcommerceDetailsBinding vEcommerceBinding;
    private com.senarios.customer.models.BaseEcommerceOrderResponse ecommerceOrderResponse;
    private com.senarios.customer.models.OrderModel order;
    private com.senarios.customer.models.ParcelModel parcelOrderModel;
    private com.senarios.customer.models.RideModel rideModel;
    private com.senarios.customer.models.MedicineModel medicineModel;
    private java.util.List<com.senarios.customer.models.MedicineModel> medicineList;
    private int scrollRange = -1;
    private android.os.Bundle bundle;
    private java.util.HashMap _$_findViewCache;
    
    @java.lang.Override()
    public void setBinding() {
    }
    
    private final void initMedicineDetails() {
    }
    
    private final void callOrderDetailAPI() {
    }
    
    private final void initParcelDetails() {
    }
    
    private final void initRideDetails() {
    }
    
    @java.lang.Override()
    protected void init() {
    }
    
    @java.lang.Override()
    public void OnCancelPermissioDialog() {
    }
    
    @java.lang.Override()
    public void OnPermissionApproved() {
    }
    
    @java.lang.Override()
    public void OnTrigger() {
    }
    
    @java.lang.Override()
    public void onOffsetChanged(@org.jetbrains.annotations.Nullable()
    com.google.android.material.appbar.AppBarLayout appBarLayout, int verticalOffset) {
    }
    
    @java.lang.Override()
    public void OnSuccess(@org.jetbrains.annotations.NotNull()
    java.lang.String endpoint, @org.jetbrains.annotations.NotNull()
    retrofit2.Response<com.senarios.customer.models.ModelBaseResponse> t, @org.jetbrains.annotations.NotNull()
    java.lang.String response) {
    }
    
    private final void initEcommerceDetails() {
    }
    
    @java.lang.Override()
    public void OnStatusfalse(@org.jetbrains.annotations.NotNull()
    java.lang.String endpoint, @org.jetbrains.annotations.NotNull()
    retrofit2.Response<com.senarios.customer.models.ModelBaseResponse> t, @org.jetbrains.annotations.NotNull()
    java.lang.String response, @org.jetbrains.annotations.NotNull()
    java.lang.String message) {
    }
    
    @java.lang.Override()
    public void OnError(@org.jetbrains.annotations.NotNull()
    java.lang.String endpoint, int code, @org.jetbrains.annotations.NotNull()
    java.lang.String message) {
    }
    
    @java.lang.Override()
    public void OnException(@org.jetbrains.annotations.NotNull()
    java.lang.String endpoint, @org.jetbrains.annotations.NotNull()
    java.lang.Throwable exception) {
    }
    
    @java.lang.Override()
    public void OnNetworkError(@org.jetbrains.annotations.NotNull()
    java.lang.String endpoint, @org.jetbrains.annotations.NotNull()
    java.lang.String message) {
    }
    
    @java.lang.Override()
    public void OnClick(int position, @org.jetbrains.annotations.NotNull()
    java.lang.Object any) {
    }
    
    @java.lang.Override()
    public void onClick(@org.jetbrains.annotations.Nullable()
    android.view.View v) {
    }
    
    private final void callCancelAPI() {
    }
    
    private final boolean isParcel() {
        return false;
    }
    
    private final boolean isMedicine() {
        return false;
    }
    
    private final boolean isRide() {
        return false;
    }
    
    private final boolean isEcom() {
        return false;
    }
    
    @java.lang.Override()
    public void onMapReady(@org.jetbrains.annotations.Nullable()
    com.google.android.gms.maps.GoogleMap p0) {
    }
    
    private final void setRideData() {
    }
    
    private final void moveCamera(com.google.android.gms.maps.model.LatLngBounds bounds, com.google.android.gms.maps.GoogleMap map) {
    }
    
    private final com.google.android.gms.maps.model.LatLngBounds getBounds() {
        return null;
    }
    
    @java.lang.Override()
    protected void onStart() {
    }
    
    @java.lang.Override()
    protected void onResume() {
    }
    
    @java.lang.Override()
    protected void onStop() {
    }
    
    @java.lang.Override()
    protected void onDestroy() {
    }
    
    @java.lang.Override()
    public void onLowMemory() {
    }
    
    public OrderDetailsActivity() {
        super();
    }
    
    public void OnEdit(int position, @org.jetbrains.annotations.NotNull()
    java.lang.Object any) {
    }
    
    public void OnDelete(int position, @org.jetbrains.annotations.NotNull()
    java.lang.Object any) {
    }
    
    public void OnScroll() {
    }
    
    public void openGallery(int position) {
    }
    
    public void getPosition(int position) {
    }
    
    public void updateCartItem(int position, @org.jetbrains.annotations.NotNull()
    com.senarios.customer.models.CartModel cartModel, int quantity, boolean flag) {
    }
    
    public void setDefaultAddress(int position, @org.jetbrains.annotations.NotNull()
    com.senarios.customer.models.Address mAddress) {
    }
}