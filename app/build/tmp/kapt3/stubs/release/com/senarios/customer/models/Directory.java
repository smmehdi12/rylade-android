package com.senarios.customer.models;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\f\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0006R\u001a\u0010\u0002\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0007\u0010\b\"\u0004\b\t\u0010\nR\u001a\u0010\u0005\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\b\"\u0004\b\f\u0010\nR\u001a\u0010\u0004\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\b\"\u0004\b\u000e\u0010\n\u00a8\u0006\u000f"}, d2 = {"Lcom/senarios/customer/models/Directory;", "", "MEDIA_PICKER", "", "MEDIA_PICKER_VIDEOS", "MEDIA_PICKER_IMAGES", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "getMEDIA_PICKER", "()Ljava/lang/String;", "setMEDIA_PICKER", "(Ljava/lang/String;)V", "getMEDIA_PICKER_IMAGES", "setMEDIA_PICKER_IMAGES", "getMEDIA_PICKER_VIDEOS", "setMEDIA_PICKER_VIDEOS", "app_release"})
public final class Directory {
    @org.jetbrains.annotations.NotNull()
    private java.lang.String MEDIA_PICKER;
    @org.jetbrains.annotations.NotNull()
    private java.lang.String MEDIA_PICKER_VIDEOS;
    @org.jetbrains.annotations.NotNull()
    private java.lang.String MEDIA_PICKER_IMAGES;
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getMEDIA_PICKER() {
        return null;
    }
    
    public final void setMEDIA_PICKER(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getMEDIA_PICKER_VIDEOS() {
        return null;
    }
    
    public final void setMEDIA_PICKER_VIDEOS(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getMEDIA_PICKER_IMAGES() {
        return null;
    }
    
    public final void setMEDIA_PICKER_IMAGES(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    public Directory(@org.jetbrains.annotations.NotNull()
    java.lang.String MEDIA_PICKER, @org.jetbrains.annotations.NotNull()
    java.lang.String MEDIA_PICKER_VIDEOS, @org.jetbrains.annotations.NotNull()
    java.lang.String MEDIA_PICKER_IMAGES) {
        super();
    }
}