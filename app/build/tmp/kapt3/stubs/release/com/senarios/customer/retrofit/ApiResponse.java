package com.senarios.customer.retrofit;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 1, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u0003\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bf\u0018\u00002\u00020\u0001J \u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\u0005H\u0016J\u0018\u0010\t\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\n\u001a\u00020\u000bH\u0016J\u0018\u0010\f\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\b\u001a\u00020\u0005H\u0016J.\u0010\r\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\f\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u00100\u000f2\u0006\u0010\u0011\u001a\u00020\u00052\u0006\u0010\b\u001a\u00020\u0005H\u0016J&\u0010\u0012\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\f\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u00100\u000f2\u0006\u0010\u0011\u001a\u00020\u0005H\u0016\u00a8\u0006\u0013"}, d2 = {"Lcom/senarios/customer/retrofit/ApiResponse;", "", "OnError", "", "endpoint", "", "code", "", "message", "OnException", "exception", "", "OnNetworkError", "OnStatusfalse", "t", "Lretrofit2/Response;", "Lcom/senarios/customer/models/ModelBaseResponse;", "response", "OnSuccess", "app_release"})
public abstract interface ApiResponse {
    
    public abstract void OnSuccess(@org.jetbrains.annotations.NotNull()
    java.lang.String endpoint, @org.jetbrains.annotations.NotNull()
    retrofit2.Response<com.senarios.customer.models.ModelBaseResponse> t, @org.jetbrains.annotations.NotNull()
    java.lang.String response);
    
    public abstract void OnStatusfalse(@org.jetbrains.annotations.NotNull()
    java.lang.String endpoint, @org.jetbrains.annotations.NotNull()
    retrofit2.Response<com.senarios.customer.models.ModelBaseResponse> t, @org.jetbrains.annotations.NotNull()
    java.lang.String response, @org.jetbrains.annotations.NotNull()
    java.lang.String message);
    
    public abstract void OnError(@org.jetbrains.annotations.NotNull()
    java.lang.String endpoint, int code, @org.jetbrains.annotations.NotNull()
    java.lang.String message);
    
    public abstract void OnException(@org.jetbrains.annotations.NotNull()
    java.lang.String endpoint, @org.jetbrains.annotations.NotNull()
    java.lang.Throwable exception);
    
    public abstract void OnNetworkError(@org.jetbrains.annotations.NotNull()
    java.lang.String endpoint, @org.jetbrains.annotations.NotNull()
    java.lang.String message);
    
    @kotlin.Metadata(mv = {1, 1, 16}, bv = {1, 0, 3}, k = 3)
    public final class DefaultImpls {
        
        public static void OnSuccess(com.senarios.customer.retrofit.ApiResponse $this, @org.jetbrains.annotations.NotNull()
        java.lang.String endpoint, @org.jetbrains.annotations.NotNull()
        retrofit2.Response<com.senarios.customer.models.ModelBaseResponse> t, @org.jetbrains.annotations.NotNull()
        java.lang.String response) {
        }
        
        public static void OnStatusfalse(com.senarios.customer.retrofit.ApiResponse $this, @org.jetbrains.annotations.NotNull()
        java.lang.String endpoint, @org.jetbrains.annotations.NotNull()
        retrofit2.Response<com.senarios.customer.models.ModelBaseResponse> t, @org.jetbrains.annotations.NotNull()
        java.lang.String response, @org.jetbrains.annotations.NotNull()
        java.lang.String message) {
        }
        
        public static void OnError(com.senarios.customer.retrofit.ApiResponse $this, @org.jetbrains.annotations.NotNull()
        java.lang.String endpoint, int code, @org.jetbrains.annotations.NotNull()
        java.lang.String message) {
        }
        
        public static void OnException(com.senarios.customer.retrofit.ApiResponse $this, @org.jetbrains.annotations.NotNull()
        java.lang.String endpoint, @org.jetbrains.annotations.NotNull()
        java.lang.Throwable exception) {
        }
        
        public static void OnNetworkError(com.senarios.customer.retrofit.ApiResponse $this, @org.jetbrains.annotations.NotNull()
        java.lang.String endpoint, @org.jetbrains.annotations.NotNull()
        java.lang.String message) {
        }
    }
}