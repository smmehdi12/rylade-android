package com.senarios.customer.databinding;
import com.senarios.customer.R;
import com.senarios.customer.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentRequestRideBindingImpl extends FragmentRequestRideBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.actionbar, 4);
        sViewsWithIds.put(R.id.views, 5);
        sViewsWithIds.put(R.id.locations_cv, 6);
        sViewsWithIds.put(R.id.title_text, 7);
        sViewsWithIds.put(R.id.dropoff_LL, 8);
        sViewsWithIds.put(R.id.dropoff_edt, 9);
        sViewsWithIds.put(R.id.view, 10);
        sViewsWithIds.put(R.id.pickup_LL, 11);
        sViewsWithIds.put(R.id.pickup_edt, 12);
        sViewsWithIds.put(R.id.view_pickup, 13);
        sViewsWithIds.put(R.id.confirmbtnlocation, 14);
        sViewsWithIds.put(R.id.confirmation_cv, 15);
        sViewsWithIds.put(R.id.estimate_fare, 16);
        sViewsWithIds.put(R.id.confirmbtnride, 17);
        sViewsWithIds.put(R.id.cancelbtnride, 18);
        sViewsWithIds.put(R.id.ride_View, 19);
        sViewsWithIds.put(R.id.rider_img, 20);
        sViewsWithIds.put(R.id.ride_img, 21);
        sViewsWithIds.put(R.id.rating, 22);
        sViewsWithIds.put(R.id.contact_view, 23);
        sViewsWithIds.put(R.id.cancel_view, 24);
        sViewsWithIds.put(R.id.guideline_inside, 25);
        sViewsWithIds.put(R.id.waitingCV, 26);
        sViewsWithIds.put(R.id.text, 27);
        sViewsWithIds.put(R.id.guideline, 28);
    }
    // views
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView0;
    @NonNull
    private final android.widget.TextView mboundView1;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FragmentRequestRideBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 29, sIncludes, sViewsWithIds));
    }
    private FragmentRequestRideBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (androidx.appcompat.widget.Toolbar) bindings[4]
            , (android.widget.TextView) bindings[3]
            , (android.widget.TextView) bindings[2]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[24]
            , (android.widget.Button) bindings[18]
            , (androidx.cardview.widget.CardView) bindings[15]
            , (android.widget.Button) bindings[14]
            , (android.widget.Button) bindings[17]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[23]
            , (android.widget.EditText) bindings[9]
            , (android.widget.LinearLayout) bindings[8]
            , (android.widget.TextView) bindings[16]
            , (androidx.constraintlayout.widget.Guideline) bindings[28]
            , (androidx.constraintlayout.widget.Guideline) bindings[25]
            , (androidx.cardview.widget.CardView) bindings[6]
            , (android.widget.EditText) bindings[12]
            , (android.widget.LinearLayout) bindings[11]
            , (android.widget.TextView) bindings[22]
            , (de.hdodenhof.circleimageview.CircleImageView) bindings[21]
            , (androidx.cardview.widget.CardView) bindings[19]
            , (de.hdodenhof.circleimageview.CircleImageView) bindings[20]
            , (android.widget.TextView) bindings[27]
            , (android.widget.TextView) bindings[7]
            , (android.view.View) bindings[10]
            , (android.view.View) bindings[13]
            , (android.widget.RelativeLayout) bindings[5]
            , (androidx.cardview.widget.CardView) bindings[26]
            );
        this.bikeModel.setTag(null);
        this.bikeNumber.setTag(null);
        this.mboundView0 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView1 = (android.widget.TextView) bindings[1];
        this.mboundView1.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x8L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.orderModel == variableId) {
            setOrderModel((com.senarios.customer.models.OrderModel) variable);
        }
        else if (BR.rideModel == variableId) {
            setRideModel((com.senarios.customer.models.RideModel) variable);
        }
        else if (BR.rider == variableId) {
            setRider((com.senarios.customer.models.User) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setOrderModel(@Nullable com.senarios.customer.models.OrderModel OrderModel) {
        this.mOrderModel = OrderModel;
    }
    public void setRideModel(@Nullable com.senarios.customer.models.RideModel RideModel) {
        this.mRideModel = RideModel;
    }
    public void setRider(@Nullable com.senarios.customer.models.User Rider) {
        this.mRider = Rider;
        synchronized(this) {
            mDirtyFlags |= 0x4L;
        }
        notifyPropertyChanged(BR.rider);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String riderNoPlate = null;
        java.lang.String riderModel = null;
        com.senarios.customer.models.User rider = mRider;
        java.lang.String riderFNameRiderLName = null;
        java.lang.String riderLName = null;
        java.lang.String riderFName = null;

        if ((dirtyFlags & 0xcL) != 0) {



                if (rider != null) {
                    // read rider.no_plate
                    riderNoPlate = rider.getNo_plate();
                    // read rider.model
                    riderModel = rider.getModel();
                    // read rider.l_name
                    riderLName = rider.getL_name();
                    // read rider.f_name
                    riderFName = rider.getF_name();
                }


                // read (rider.f_name) + (rider.l_name)
                riderFNameRiderLName = (riderFName) + (riderLName);
        }
        // batch finished
        if ((dirtyFlags & 0xcL) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.bikeModel, riderModel);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.bikeNumber, riderNoPlate);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.mboundView1, riderFNameRiderLName);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): orderModel
        flag 1 (0x2L): rideModel
        flag 2 (0x3L): rider
        flag 3 (0x4L): null
    flag mapping end*/
    //end
}