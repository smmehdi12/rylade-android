package com.senarios.customer.databinding;
import com.senarios.customer.R;
import com.senarios.customer.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentAddNewAddressBindingImpl extends FragmentAddNewAddressBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.back_imageView, 4);
        sViewsWithIds.put(R.id.signup_textView, 5);
        sViewsWithIds.put(R.id.divider_horizontal_imageView2, 6);
        sViewsWithIds.put(R.id.tv_add, 7);
        sViewsWithIds.put(R.id.home, 8);
        sViewsWithIds.put(R.id.home_img, 9);
        sViewsWithIds.put(R.id.home_txt, 10);
        sViewsWithIds.put(R.id.guideline22, 11);
        sViewsWithIds.put(R.id.guideline23, 12);
        sViewsWithIds.put(R.id.work, 13);
        sViewsWithIds.put(R.id.work_img, 14);
        sViewsWithIds.put(R.id.work_txt, 15);
        sViewsWithIds.put(R.id.other, 16);
        sViewsWithIds.put(R.id.other_img, 17);
        sViewsWithIds.put(R.id.other_txt, 18);
    }
    // views
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FragmentAddNewAddressBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 19, sIncludes, sViewsWithIds));
    }
    private FragmentAddNewAddressBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (android.widget.EditText) bindings[2]
            , (android.widget.ImageView) bindings[4]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[0]
            , (android.widget.ImageView) bindings[6]
            , (androidx.constraintlayout.widget.Guideline) bindings[11]
            , (androidx.constraintlayout.widget.Guideline) bindings[12]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[8]
            , (android.widget.ImageView) bindings[9]
            , (android.widget.TextView) bindings[10]
            , (android.widget.EditText) bindings[3]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[16]
            , (android.widget.ImageView) bindings[17]
            , (android.widget.EditText) bindings[1]
            , (android.widget.TextView) bindings[18]
            , (android.widget.TextView) bindings[5]
            , (android.widget.TextView) bindings[7]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[13]
            , (android.widget.ImageView) bindings[14]
            , (android.widget.TextView) bindings[15]
            );
        this.address.setTag(null);
        this.constraintLayout.setTag(null);
        this.manualAddress.setTag(null);
        this.otherTitle.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.data == variableId) {
            setData((com.senarios.customer.models.Address) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setData(@Nullable com.senarios.customer.models.Address Data) {
        this.mData = Data;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.data);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String dataManualAddress = null;
        java.lang.String dataAddress = null;
        com.senarios.customer.models.Address data = mData;
        java.lang.String dataOtherTitle = null;

        if ((dirtyFlags & 0x3L) != 0) {



                if (data != null) {
                    // read data.manual_address
                    dataManualAddress = data.getManual_address();
                    // read data.address
                    dataAddress = data.getAddress();
                    // read data.other_title
                    dataOtherTitle = data.getOther_title();
                }
        }
        // batch finished
        if ((dirtyFlags & 0x3L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.address, dataAddress);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.manualAddress, dataManualAddress);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.otherTitle, dataOtherTitle);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): data
        flag 1 (0x2L): null
    flag mapping end*/
    //end
}