package com.senarios.customer.db;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import androidx.room.EntityInsertionAdapter;
import androidx.room.RoomDatabase;
import androidx.room.RoomSQLiteQuery;
import androidx.room.SharedSQLiteStatement;
import androidx.room.util.CursorUtil;
import androidx.room.util.DBUtil;
import androidx.sqlite.db.SupportSQLiteStatement;
import com.senarios.customer.models.CartModel;
import com.senarios.customer.models.MainCategoryModel;
import com.senarios.customer.models.ProductModel;
import com.senarios.customer.models.Vendor;
import com.senarios.customer.models.VendorCategoryModel;
import java.lang.Boolean;
import java.lang.Exception;
import java.lang.Integer;
import java.lang.Long;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

@SuppressWarnings({"unchecked", "deprecation"})
public final class DAO_Impl extends DAO {
  private final RoomDatabase __db;

  private final EntityInsertionAdapter<MainCategoryModel> __insertionAdapterOfMainCategoryModel;

  private final EntityInsertionAdapter<Vendor> __insertionAdapterOfVendor;

  private final EntityInsertionAdapter<VendorCategoryModel> __insertionAdapterOfVendorCategoryModel;

  private final EntityInsertionAdapter<ProductModel> __insertionAdapterOfProductModel;

  private final EntityInsertionAdapter<CartModel> __insertionAdapterOfCartModel;

  private final SharedSQLiteStatement __preparedStmtOfClearMainCategoryTable;

  private final SharedSQLiteStatement __preparedStmtOfClearVendorTable;

  private final SharedSQLiteStatement __preparedStmtOfClearVendorCategoryTable;

  private final SharedSQLiteStatement __preparedStmtOfClearProductsTable;

  private final SharedSQLiteStatement __preparedStmtOfClearCartTable;

  private final SharedSQLiteStatement __preparedStmtOfUpdateQuantity;

  private final SharedSQLiteStatement __preparedStmtOfDeleteCartItem;

  private final SharedSQLiteStatement __preparedStmtOfDeleteVendorCategory;

  private final SharedSQLiteStatement __preparedStmtOfDeleteProducts;

  private final SharedSQLiteStatement __preparedStmtOfDeleteVendors;

  private final SharedSQLiteStatement __preparedStmtOfUpdateOutOfOrder;

  public DAO_Impl(RoomDatabase __db) {
    this.__db = __db;
    this.__insertionAdapterOfMainCategoryModel = new EntityInsertionAdapter<MainCategoryModel>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR REPLACE INTO `MainCategory` (`created_at`,`id`,`name`,`updated_at`,`image_url`) VALUES (?,?,?,?,?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, MainCategoryModel value) {
        if (value.getCreatedAt() == null) {
          stmt.bindNull(1);
        } else {
          stmt.bindString(1, value.getCreatedAt());
        }
        if (value.getId() == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindLong(2, value.getId());
        }
        if (value.getName() == null) {
          stmt.bindNull(3);
        } else {
          stmt.bindString(3, value.getName());
        }
        if (value.getUpdatedAt() == null) {
          stmt.bindNull(4);
        } else {
          stmt.bindString(4, value.getUpdatedAt());
        }
        if (value.getImage_url() == null) {
          stmt.bindNull(5);
        } else {
          stmt.bindString(5, value.getImage_url());
        }
      }
    };
    this.__insertionAdapterOfVendor = new EntityInsertionAdapter<Vendor>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR REPLACE INTO `Vendor` (`category_id`,`created_at`,`email`,`email_verified_at`,`f_name`,`fcm`,`id`,`image`,`is_verified`,`l_name`,`login_facebook`,`model`,`no_plate`,`otp`,`phone_number`,`rider_availability`,`rider_work_status`,`service_area_id`,`token`,`updated_at`,`user_status`,`user_type`,`is_delivery`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, Vendor value) {
        if (value.getCategoryId() == null) {
          stmt.bindNull(1);
        } else {
          stmt.bindLong(1, value.getCategoryId());
        }
        if (value.getCreatedAt() == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.getCreatedAt());
        }
        if (value.getEmail() == null) {
          stmt.bindNull(3);
        } else {
          stmt.bindString(3, value.getEmail());
        }
        if (value.getEmailVerifiedAt() == null) {
          stmt.bindNull(4);
        } else {
          stmt.bindString(4, value.getEmailVerifiedAt());
        }
        if (value.getFName() == null) {
          stmt.bindNull(5);
        } else {
          stmt.bindString(5, value.getFName());
        }
        if (value.getFcm() == null) {
          stmt.bindNull(6);
        } else {
          stmt.bindString(6, value.getFcm());
        }
        if (value.getId() == null) {
          stmt.bindNull(7);
        } else {
          stmt.bindLong(7, value.getId());
        }
        if (value.getImage() == null) {
          stmt.bindNull(8);
        } else {
          stmt.bindString(8, value.getImage());
        }
        final Integer _tmp;
        _tmp = value.isVerified() == null ? null : (value.isVerified() ? 1 : 0);
        if (_tmp == null) {
          stmt.bindNull(9);
        } else {
          stmt.bindLong(9, _tmp);
        }
        if (value.getLName() == null) {
          stmt.bindNull(10);
        } else {
          stmt.bindString(10, value.getLName());
        }
        final Integer _tmp_1;
        _tmp_1 = value.getLoginFacebook() == null ? null : (value.getLoginFacebook() ? 1 : 0);
        if (_tmp_1 == null) {
          stmt.bindNull(11);
        } else {
          stmt.bindLong(11, _tmp_1);
        }
        if (value.getModel() == null) {
          stmt.bindNull(12);
        } else {
          stmt.bindString(12, value.getModel());
        }
        if (value.getNoPlate() == null) {
          stmt.bindNull(13);
        } else {
          stmt.bindString(13, value.getNoPlate());
        }
        if (value.getOtp() == null) {
          stmt.bindNull(14);
        } else {
          stmt.bindString(14, value.getOtp());
        }
        if (value.getPhoneNumber() == null) {
          stmt.bindNull(15);
        } else {
          stmt.bindString(15, value.getPhoneNumber());
        }
        if (value.getRiderAvailability() == null) {
          stmt.bindNull(16);
        } else {
          stmt.bindString(16, value.getRiderAvailability());
        }
        if (value.getRiderWorkStatus() == null) {
          stmt.bindNull(17);
        } else {
          stmt.bindString(17, value.getRiderWorkStatus());
        }
        if (value.getServiceAreaId() == null) {
          stmt.bindNull(18);
        } else {
          stmt.bindString(18, value.getServiceAreaId());
        }
        if (value.getToken() == null) {
          stmt.bindNull(19);
        } else {
          stmt.bindString(19, value.getToken());
        }
        if (value.getUpdatedAt() == null) {
          stmt.bindNull(20);
        } else {
          stmt.bindString(20, value.getUpdatedAt());
        }
        if (value.getUserStatus() == null) {
          stmt.bindNull(21);
        } else {
          stmt.bindLong(21, value.getUserStatus());
        }
        if (value.getUserType() == null) {
          stmt.bindNull(22);
        } else {
          stmt.bindString(22, value.getUserType());
        }
        stmt.bindLong(23, value.is_delivery());
      }
    };
    this.__insertionAdapterOfVendorCategoryModel = new EntityInsertionAdapter<VendorCategoryModel>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR REPLACE INTO `VendorCategory` (`category_name`,`created_at`,`id`,`updated_at`,`vendor_id`) VALUES (?,?,?,?,?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, VendorCategoryModel value) {
        if (value.getCategoryName() == null) {
          stmt.bindNull(1);
        } else {
          stmt.bindString(1, value.getCategoryName());
        }
        if (value.getCreatedAt() == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.getCreatedAt());
        }
        if (value.getId() == null) {
          stmt.bindNull(3);
        } else {
          stmt.bindLong(3, value.getId());
        }
        if (value.getUpdatedAt() == null) {
          stmt.bindNull(4);
        } else {
          stmt.bindString(4, value.getUpdatedAt());
        }
        if (value.getVendorId() == null) {
          stmt.bindNull(5);
        } else {
          stmt.bindLong(5, value.getVendorId());
        }
      }
    };
    this.__insertionAdapterOfProductModel = new EntityInsertionAdapter<ProductModel>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR REPLACE INTO `Products` (`category_id`,`category_name`,`created_at`,`description`,`id`,`image`,`price`,`product_name`,`promo_code_id`,`quantity_available`,`status`,`sub_category_name`,`updated_at`,`weight`,`vendor_id`,`sales_tax`,`fixed_discount`,`iz_discount`,`comment`,`rating`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, ProductModel value) {
        if (value.getCategoryId() == null) {
          stmt.bindNull(1);
        } else {
          stmt.bindLong(1, value.getCategoryId());
        }
        if (value.getCategoryName() == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, value.getCategoryName());
        }
        if (value.getCreatedAt() == null) {
          stmt.bindNull(3);
        } else {
          stmt.bindString(3, value.getCreatedAt());
        }
        if (value.getDescription() == null) {
          stmt.bindNull(4);
        } else {
          stmt.bindString(4, value.getDescription());
        }
        if (value.getId() == null) {
          stmt.bindNull(5);
        } else {
          stmt.bindLong(5, value.getId());
        }
        if (value.getImage() == null) {
          stmt.bindNull(6);
        } else {
          stmt.bindString(6, value.getImage());
        }
        if (value.getPrice() == null) {
          stmt.bindNull(7);
        } else {
          stmt.bindLong(7, value.getPrice());
        }
        if (value.getProductName() == null) {
          stmt.bindNull(8);
        } else {
          stmt.bindString(8, value.getProductName());
        }
        if (value.getPromoCodeId() == null) {
          stmt.bindNull(9);
        } else {
          stmt.bindLong(9, value.getPromoCodeId());
        }
        if (value.getQuantityAvailable() == null) {
          stmt.bindNull(10);
        } else {
          stmt.bindLong(10, value.getQuantityAvailable());
        }
        if (value.getStatus() == null) {
          stmt.bindNull(11);
        } else {
          stmt.bindLong(11, value.getStatus());
        }
        if (value.getSubCategoryName() == null) {
          stmt.bindNull(12);
        } else {
          stmt.bindString(12, value.getSubCategoryName());
        }
        if (value.getUpdatedAt() == null) {
          stmt.bindNull(13);
        } else {
          stmt.bindString(13, value.getUpdatedAt());
        }
        if (value.getWeight() == null) {
          stmt.bindNull(14);
        } else {
          stmt.bindLong(14, value.getWeight());
        }
        if (value.getVendorId() == null) {
          stmt.bindNull(15);
        } else {
          stmt.bindLong(15, value.getVendorId());
        }
        stmt.bindDouble(16, value.getSales_tax());
        if (value.getFixed_discount() == null) {
          stmt.bindNull(17);
        } else {
          stmt.bindLong(17, value.getFixed_discount());
        }
        if (value.getIz_discount() == null) {
          stmt.bindNull(18);
        } else {
          stmt.bindLong(18, value.getIz_discount());
        }
        if (value.getComment() == null) {
          stmt.bindNull(19);
        } else {
          stmt.bindString(19, value.getComment());
        }
        if (value.getRating() == null) {
          stmt.bindNull(20);
        } else {
          stmt.bindString(20, value.getRating());
        }
      }
    };
    this.__insertionAdapterOfCartModel = new EntityInsertionAdapter<CartModel>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR REPLACE INTO `Cart` (`id`,`category_id`,`category_name`,`created_at`,`description`,`product_id`,`image`,`price`,`product_name`,`promo_code_id`,`quantity_available`,`status`,`sub_category_name`,`updated_at`,`weight`,`vendor_id`,`vendor_name`,`quantity`,`order_id`,`sales_tax`,`fixed_discount`,`iss_discount`,`originalPrice`,`discounted_price`,`isOutStock`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, CartModel value) {
        if (value.getId() == null) {
          stmt.bindNull(1);
        } else {
          stmt.bindLong(1, value.getId());
        }
        if (value.getCategoryId() == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindLong(2, value.getCategoryId());
        }
        if (value.getCategoryName() == null) {
          stmt.bindNull(3);
        } else {
          stmt.bindString(3, value.getCategoryName());
        }
        if (value.getCreatedAt() == null) {
          stmt.bindNull(4);
        } else {
          stmt.bindString(4, value.getCreatedAt());
        }
        if (value.getDescription() == null) {
          stmt.bindNull(5);
        } else {
          stmt.bindString(5, value.getDescription());
        }
        if (value.getProduct_id() == null) {
          stmt.bindNull(6);
        } else {
          stmt.bindLong(6, value.getProduct_id());
        }
        if (value.getImage() == null) {
          stmt.bindNull(7);
        } else {
          stmt.bindString(7, value.getImage());
        }
        if (value.getPrice() == null) {
          stmt.bindNull(8);
        } else {
          stmt.bindLong(8, value.getPrice());
        }
        if (value.getProductName() == null) {
          stmt.bindNull(9);
        } else {
          stmt.bindString(9, value.getProductName());
        }
        if (value.getPromoCodeId() == null) {
          stmt.bindNull(10);
        } else {
          stmt.bindLong(10, value.getPromoCodeId());
        }
        if (value.getQuantityAvailable() == null) {
          stmt.bindNull(11);
        } else {
          stmt.bindLong(11, value.getQuantityAvailable());
        }
        if (value.getStatus() == null) {
          stmt.bindNull(12);
        } else {
          stmt.bindLong(12, value.getStatus());
        }
        if (value.getSubCategoryName() == null) {
          stmt.bindNull(13);
        } else {
          stmt.bindString(13, value.getSubCategoryName());
        }
        if (value.getUpdatedAt() == null) {
          stmt.bindNull(14);
        } else {
          stmt.bindString(14, value.getUpdatedAt());
        }
        if (value.getWeight() == null) {
          stmt.bindNull(15);
        } else {
          stmt.bindLong(15, value.getWeight());
        }
        if (value.getVendorId() == null) {
          stmt.bindNull(16);
        } else {
          stmt.bindLong(16, value.getVendorId());
        }
        if (value.getVendor_name() == null) {
          stmt.bindNull(17);
        } else {
          stmt.bindString(17, value.getVendor_name());
        }
        if (value.getQuantity() == null) {
          stmt.bindNull(18);
        } else {
          stmt.bindLong(18, value.getQuantity());
        }
        if (value.getOrder_id() == null) {
          stmt.bindNull(19);
        } else {
          stmt.bindLong(19, value.getOrder_id());
        }
        stmt.bindDouble(20, value.getSales_tax());
        if (value.getFixed_discount() == null) {
          stmt.bindNull(21);
        } else {
          stmt.bindLong(21, value.getFixed_discount());
        }
        if (value.getIss_discount() == null) {
          stmt.bindNull(22);
        } else {
          stmt.bindLong(22, value.getIss_discount());
        }
        if (value.getOriginalPrice() == null) {
          stmt.bindNull(23);
        } else {
          stmt.bindLong(23, value.getOriginalPrice());
        }
        if (value.getDiscounted_price() == null) {
          stmt.bindNull(24);
        } else {
          stmt.bindLong(24, value.getDiscounted_price());
        }
        final int _tmp;
        _tmp = value.isOutStock() ? 1 : 0;
        stmt.bindLong(25, _tmp);
      }
    };
    this.__preparedStmtOfClearMainCategoryTable = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "delete from MainCategory";
        return _query;
      }
    };
    this.__preparedStmtOfClearVendorTable = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "delete from Vendor";
        return _query;
      }
    };
    this.__preparedStmtOfClearVendorCategoryTable = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "delete from VendorCategory";
        return _query;
      }
    };
    this.__preparedStmtOfClearProductsTable = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "delete from Products";
        return _query;
      }
    };
    this.__preparedStmtOfClearCartTable = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "delete from Cart";
        return _query;
      }
    };
    this.__preparedStmtOfUpdateQuantity = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "update cart set quantity=? where id=?";
        return _query;
      }
    };
    this.__preparedStmtOfDeleteCartItem = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "delete from Cart where id=?";
        return _query;
      }
    };
    this.__preparedStmtOfDeleteVendorCategory = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "delete from VendorCategory where vendor_id=?";
        return _query;
      }
    };
    this.__preparedStmtOfDeleteProducts = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "delete from Products where category_id=? AND vendor_id=?";
        return _query;
      }
    };
    this.__preparedStmtOfDeleteVendors = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "delete from Vendor where category_id=?";
        return _query;
      }
    };
    this.__preparedStmtOfUpdateOutOfOrder = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "Update Cart set isOutStock=? where id=?";
        return _query;
      }
    };
  }

  @Override
  public void insertMainCategories(final List<MainCategoryModel> list) {
    __db.assertNotSuspendingTransaction();
    __db.beginTransaction();
    try {
      __insertionAdapterOfMainCategoryModel.insert(list);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void insertVendors(final List<Vendor> list) {
    __db.assertNotSuspendingTransaction();
    __db.beginTransaction();
    try {
      __insertionAdapterOfVendor.insert(list);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void insertVendor(final Vendor model) {
    __db.assertNotSuspendingTransaction();
    __db.beginTransaction();
    try {
      __insertionAdapterOfVendor.insert(model);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void insertVendorCategories(final List<VendorCategoryModel> list) {
    __db.assertNotSuspendingTransaction();
    __db.beginTransaction();
    try {
      __insertionAdapterOfVendorCategoryModel.insert(list);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void insertVendorProducts(final List<ProductModel> list) {
    __db.assertNotSuspendingTransaction();
    __db.beginTransaction();
    try {
      __insertionAdapterOfProductModel.insert(list);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void inserProduct(final ProductModel product) {
    __db.assertNotSuspendingTransaction();
    __db.beginTransaction();
    try {
      __insertionAdapterOfProductModel.insert(product);
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public long insertCart(final CartModel model) {
    __db.assertNotSuspendingTransaction();
    __db.beginTransaction();
    try {
      long _result = __insertionAdapterOfCartModel.insertAndReturnId(model);
      __db.setTransactionSuccessful();
      return _result;
    } finally {
      __db.endTransaction();
    }
  }

  @Override
  public void clearMainCategoryTable() {
    __db.assertNotSuspendingTransaction();
    final SupportSQLiteStatement _stmt = __preparedStmtOfClearMainCategoryTable.acquire();
    __db.beginTransaction();
    try {
      _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
      __preparedStmtOfClearMainCategoryTable.release(_stmt);
    }
  }

  @Override
  public void clearVendorTable() {
    __db.assertNotSuspendingTransaction();
    final SupportSQLiteStatement _stmt = __preparedStmtOfClearVendorTable.acquire();
    __db.beginTransaction();
    try {
      _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
      __preparedStmtOfClearVendorTable.release(_stmt);
    }
  }

  @Override
  public void clearVendorCategoryTable() {
    __db.assertNotSuspendingTransaction();
    final SupportSQLiteStatement _stmt = __preparedStmtOfClearVendorCategoryTable.acquire();
    __db.beginTransaction();
    try {
      _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
      __preparedStmtOfClearVendorCategoryTable.release(_stmt);
    }
  }

  @Override
  public void clearProductsTable() {
    __db.assertNotSuspendingTransaction();
    final SupportSQLiteStatement _stmt = __preparedStmtOfClearProductsTable.acquire();
    __db.beginTransaction();
    try {
      _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
      __preparedStmtOfClearProductsTable.release(_stmt);
    }
  }

  @Override
  public void clearCartTable() {
    __db.assertNotSuspendingTransaction();
    final SupportSQLiteStatement _stmt = __preparedStmtOfClearCartTable.acquire();
    __db.beginTransaction();
    try {
      _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
      __preparedStmtOfClearCartTable.release(_stmt);
    }
  }

  @Override
  public void updateQuantity(final long id, final int quantity) {
    __db.assertNotSuspendingTransaction();
    final SupportSQLiteStatement _stmt = __preparedStmtOfUpdateQuantity.acquire();
    int _argIndex = 1;
    _stmt.bindLong(_argIndex, quantity);
    _argIndex = 2;
    _stmt.bindLong(_argIndex, id);
    __db.beginTransaction();
    try {
      _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
      __preparedStmtOfUpdateQuantity.release(_stmt);
    }
  }

  @Override
  public void deleteCartItem(final long id) {
    __db.assertNotSuspendingTransaction();
    final SupportSQLiteStatement _stmt = __preparedStmtOfDeleteCartItem.acquire();
    int _argIndex = 1;
    _stmt.bindLong(_argIndex, id);
    __db.beginTransaction();
    try {
      _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
      __preparedStmtOfDeleteCartItem.release(_stmt);
    }
  }

  @Override
  public void deleteVendorCategory(final int vendor_id) {
    __db.assertNotSuspendingTransaction();
    final SupportSQLiteStatement _stmt = __preparedStmtOfDeleteVendorCategory.acquire();
    int _argIndex = 1;
    _stmt.bindLong(_argIndex, vendor_id);
    __db.beginTransaction();
    try {
      _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
      __preparedStmtOfDeleteVendorCategory.release(_stmt);
    }
  }

  @Override
  public void deleteProducts(final int category_id, final int vendor_id) {
    __db.assertNotSuspendingTransaction();
    final SupportSQLiteStatement _stmt = __preparedStmtOfDeleteProducts.acquire();
    int _argIndex = 1;
    _stmt.bindLong(_argIndex, category_id);
    _argIndex = 2;
    _stmt.bindLong(_argIndex, vendor_id);
    __db.beginTransaction();
    try {
      _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
      __preparedStmtOfDeleteProducts.release(_stmt);
    }
  }

  @Override
  public void deleteVendors(final int category_id) {
    __db.assertNotSuspendingTransaction();
    final SupportSQLiteStatement _stmt = __preparedStmtOfDeleteVendors.acquire();
    int _argIndex = 1;
    _stmt.bindLong(_argIndex, category_id);
    __db.beginTransaction();
    try {
      _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
      __preparedStmtOfDeleteVendors.release(_stmt);
    }
  }

  @Override
  public void updateOutOfOrder(final long id, final boolean isOutStock) {
    __db.assertNotSuspendingTransaction();
    final SupportSQLiteStatement _stmt = __preparedStmtOfUpdateOutOfOrder.acquire();
    int _argIndex = 1;
    final int _tmp;
    _tmp = isOutStock ? 1 : 0;
    _stmt.bindLong(_argIndex, _tmp);
    _argIndex = 2;
    _stmt.bindLong(_argIndex, id);
    __db.beginTransaction();
    try {
      _stmt.executeUpdateDelete();
      __db.setTransactionSuccessful();
    } finally {
      __db.endTransaction();
      __preparedStmtOfUpdateOutOfOrder.release(_stmt);
    }
  }

  @Override
  public LiveData<List<MainCategoryModel>> getMainCategories() {
    final String _sql = "select * from MainCategory";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    return __db.getInvalidationTracker().createLiveData(new String[]{"MainCategory"}, false, new Callable<List<MainCategoryModel>>() {
      @Override
      public List<MainCategoryModel> call() throws Exception {
        final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
        try {
          final int _cursorIndexOfCreatedAt = CursorUtil.getColumnIndexOrThrow(_cursor, "created_at");
          final int _cursorIndexOfId = CursorUtil.getColumnIndexOrThrow(_cursor, "id");
          final int _cursorIndexOfName = CursorUtil.getColumnIndexOrThrow(_cursor, "name");
          final int _cursorIndexOfUpdatedAt = CursorUtil.getColumnIndexOrThrow(_cursor, "updated_at");
          final int _cursorIndexOfImageUrl = CursorUtil.getColumnIndexOrThrow(_cursor, "image_url");
          final List<MainCategoryModel> _result = new ArrayList<MainCategoryModel>(_cursor.getCount());
          while(_cursor.moveToNext()) {
            final MainCategoryModel _item;
            final String _tmpCreatedAt;
            _tmpCreatedAt = _cursor.getString(_cursorIndexOfCreatedAt);
            final Integer _tmpId;
            if (_cursor.isNull(_cursorIndexOfId)) {
              _tmpId = null;
            } else {
              _tmpId = _cursor.getInt(_cursorIndexOfId);
            }
            final String _tmpName;
            _tmpName = _cursor.getString(_cursorIndexOfName);
            final String _tmpUpdatedAt;
            _tmpUpdatedAt = _cursor.getString(_cursorIndexOfUpdatedAt);
            final String _tmpImage_url;
            _tmpImage_url = _cursor.getString(_cursorIndexOfImageUrl);
            _item = new MainCategoryModel(_tmpCreatedAt,_tmpId,_tmpName,_tmpUpdatedAt,_tmpImage_url);
            _result.add(_item);
          }
          return _result;
        } finally {
          _cursor.close();
        }
      }

      @Override
      protected void finalize() {
        _statement.release();
      }
    });
  }

  @Override
  public LiveData<List<Vendor>> getVendor(final int categoryid) {
    final String _sql = "select * from Vendor where category_id=? AND user_status==1";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 1);
    int _argIndex = 1;
    _statement.bindLong(_argIndex, categoryid);
    return __db.getInvalidationTracker().createLiveData(new String[]{"Vendor"}, false, new Callable<List<Vendor>>() {
      @Override
      public List<Vendor> call() throws Exception {
        final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
        try {
          final int _cursorIndexOfCategoryId = CursorUtil.getColumnIndexOrThrow(_cursor, "category_id");
          final int _cursorIndexOfCreatedAt = CursorUtil.getColumnIndexOrThrow(_cursor, "created_at");
          final int _cursorIndexOfEmail = CursorUtil.getColumnIndexOrThrow(_cursor, "email");
          final int _cursorIndexOfEmailVerifiedAt = CursorUtil.getColumnIndexOrThrow(_cursor, "email_verified_at");
          final int _cursorIndexOfFName = CursorUtil.getColumnIndexOrThrow(_cursor, "f_name");
          final int _cursorIndexOfFcm = CursorUtil.getColumnIndexOrThrow(_cursor, "fcm");
          final int _cursorIndexOfId = CursorUtil.getColumnIndexOrThrow(_cursor, "id");
          final int _cursorIndexOfImage = CursorUtil.getColumnIndexOrThrow(_cursor, "image");
          final int _cursorIndexOfIsVerified = CursorUtil.getColumnIndexOrThrow(_cursor, "is_verified");
          final int _cursorIndexOfLName = CursorUtil.getColumnIndexOrThrow(_cursor, "l_name");
          final int _cursorIndexOfLoginFacebook = CursorUtil.getColumnIndexOrThrow(_cursor, "login_facebook");
          final int _cursorIndexOfModel = CursorUtil.getColumnIndexOrThrow(_cursor, "model");
          final int _cursorIndexOfNoPlate = CursorUtil.getColumnIndexOrThrow(_cursor, "no_plate");
          final int _cursorIndexOfOtp = CursorUtil.getColumnIndexOrThrow(_cursor, "otp");
          final int _cursorIndexOfPhoneNumber = CursorUtil.getColumnIndexOrThrow(_cursor, "phone_number");
          final int _cursorIndexOfRiderAvailability = CursorUtil.getColumnIndexOrThrow(_cursor, "rider_availability");
          final int _cursorIndexOfRiderWorkStatus = CursorUtil.getColumnIndexOrThrow(_cursor, "rider_work_status");
          final int _cursorIndexOfServiceAreaId = CursorUtil.getColumnIndexOrThrow(_cursor, "service_area_id");
          final int _cursorIndexOfToken = CursorUtil.getColumnIndexOrThrow(_cursor, "token");
          final int _cursorIndexOfUpdatedAt = CursorUtil.getColumnIndexOrThrow(_cursor, "updated_at");
          final int _cursorIndexOfUserStatus = CursorUtil.getColumnIndexOrThrow(_cursor, "user_status");
          final int _cursorIndexOfUserType = CursorUtil.getColumnIndexOrThrow(_cursor, "user_type");
          final int _cursorIndexOfIsDelivery = CursorUtil.getColumnIndexOrThrow(_cursor, "is_delivery");
          final List<Vendor> _result = new ArrayList<Vendor>(_cursor.getCount());
          while(_cursor.moveToNext()) {
            final Vendor _item;
            final Integer _tmpCategoryId;
            if (_cursor.isNull(_cursorIndexOfCategoryId)) {
              _tmpCategoryId = null;
            } else {
              _tmpCategoryId = _cursor.getInt(_cursorIndexOfCategoryId);
            }
            final String _tmpCreatedAt;
            _tmpCreatedAt = _cursor.getString(_cursorIndexOfCreatedAt);
            final String _tmpEmail;
            _tmpEmail = _cursor.getString(_cursorIndexOfEmail);
            final String _tmpEmailVerifiedAt;
            _tmpEmailVerifiedAt = _cursor.getString(_cursorIndexOfEmailVerifiedAt);
            final String _tmpFName;
            _tmpFName = _cursor.getString(_cursorIndexOfFName);
            final String _tmpFcm;
            _tmpFcm = _cursor.getString(_cursorIndexOfFcm);
            final Integer _tmpId;
            if (_cursor.isNull(_cursorIndexOfId)) {
              _tmpId = null;
            } else {
              _tmpId = _cursor.getInt(_cursorIndexOfId);
            }
            final String _tmpImage;
            _tmpImage = _cursor.getString(_cursorIndexOfImage);
            final Boolean _tmpIsVerified;
            final Integer _tmp;
            if (_cursor.isNull(_cursorIndexOfIsVerified)) {
              _tmp = null;
            } else {
              _tmp = _cursor.getInt(_cursorIndexOfIsVerified);
            }
            _tmpIsVerified = _tmp == null ? null : _tmp != 0;
            final String _tmpLName;
            _tmpLName = _cursor.getString(_cursorIndexOfLName);
            final Boolean _tmpLoginFacebook;
            final Integer _tmp_1;
            if (_cursor.isNull(_cursorIndexOfLoginFacebook)) {
              _tmp_1 = null;
            } else {
              _tmp_1 = _cursor.getInt(_cursorIndexOfLoginFacebook);
            }
            _tmpLoginFacebook = _tmp_1 == null ? null : _tmp_1 != 0;
            final String _tmpModel;
            _tmpModel = _cursor.getString(_cursorIndexOfModel);
            final String _tmpNoPlate;
            _tmpNoPlate = _cursor.getString(_cursorIndexOfNoPlate);
            final String _tmpOtp;
            _tmpOtp = _cursor.getString(_cursorIndexOfOtp);
            final String _tmpPhoneNumber;
            _tmpPhoneNumber = _cursor.getString(_cursorIndexOfPhoneNumber);
            final String _tmpRiderAvailability;
            _tmpRiderAvailability = _cursor.getString(_cursorIndexOfRiderAvailability);
            final String _tmpRiderWorkStatus;
            _tmpRiderWorkStatus = _cursor.getString(_cursorIndexOfRiderWorkStatus);
            final String _tmpServiceAreaId;
            _tmpServiceAreaId = _cursor.getString(_cursorIndexOfServiceAreaId);
            final String _tmpToken;
            _tmpToken = _cursor.getString(_cursorIndexOfToken);
            final String _tmpUpdatedAt;
            _tmpUpdatedAt = _cursor.getString(_cursorIndexOfUpdatedAt);
            final Integer _tmpUserStatus;
            if (_cursor.isNull(_cursorIndexOfUserStatus)) {
              _tmpUserStatus = null;
            } else {
              _tmpUserStatus = _cursor.getInt(_cursorIndexOfUserStatus);
            }
            final String _tmpUserType;
            _tmpUserType = _cursor.getString(_cursorIndexOfUserType);
            final int _tmpIs_delivery;
            _tmpIs_delivery = _cursor.getInt(_cursorIndexOfIsDelivery);
            _item = new Vendor(_tmpCategoryId,_tmpCreatedAt,_tmpEmail,_tmpEmailVerifiedAt,_tmpFName,_tmpFcm,_tmpId,_tmpImage,_tmpIsVerified,_tmpLName,_tmpLoginFacebook,_tmpModel,_tmpNoPlate,_tmpOtp,_tmpPhoneNumber,_tmpRiderAvailability,_tmpRiderWorkStatus,_tmpServiceAreaId,_tmpToken,_tmpUpdatedAt,_tmpUserStatus,_tmpUserType,_tmpIs_delivery);
            _result.add(_item);
          }
          return _result;
        } finally {
          _cursor.close();
        }
      }

      @Override
      protected void finalize() {
        _statement.release();
      }
    });
  }

  @Override
  public LiveData<Integer> getVendorDeliveryStatus(final int vendor_id) {
    final String _sql = "select is_delivery from Vendor where id=?";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 1);
    int _argIndex = 1;
    _statement.bindLong(_argIndex, vendor_id);
    return __db.getInvalidationTracker().createLiveData(new String[]{"Vendor"}, false, new Callable<Integer>() {
      @Override
      public Integer call() throws Exception {
        final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
        try {
          final Integer _result;
          if(_cursor.moveToFirst()) {
            if (_cursor.isNull(0)) {
              _result = null;
            } else {
              _result = _cursor.getInt(0);
            }
          } else {
            _result = null;
          }
          return _result;
        } finally {
          _cursor.close();
        }
      }

      @Override
      protected void finalize() {
        _statement.release();
      }
    });
  }

  @Override
  public LiveData<List<VendorCategoryModel>> getVendorCategory(final int vendor_id) {
    final String _sql = "select * from VendorCategory where vendor_id=?";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 1);
    int _argIndex = 1;
    _statement.bindLong(_argIndex, vendor_id);
    return __db.getInvalidationTracker().createLiveData(new String[]{"VendorCategory"}, false, new Callable<List<VendorCategoryModel>>() {
      @Override
      public List<VendorCategoryModel> call() throws Exception {
        final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
        try {
          final int _cursorIndexOfCategoryName = CursorUtil.getColumnIndexOrThrow(_cursor, "category_name");
          final int _cursorIndexOfCreatedAt = CursorUtil.getColumnIndexOrThrow(_cursor, "created_at");
          final int _cursorIndexOfId = CursorUtil.getColumnIndexOrThrow(_cursor, "id");
          final int _cursorIndexOfUpdatedAt = CursorUtil.getColumnIndexOrThrow(_cursor, "updated_at");
          final int _cursorIndexOfVendorId = CursorUtil.getColumnIndexOrThrow(_cursor, "vendor_id");
          final List<VendorCategoryModel> _result = new ArrayList<VendorCategoryModel>(_cursor.getCount());
          while(_cursor.moveToNext()) {
            final VendorCategoryModel _item;
            final String _tmpCategoryName;
            _tmpCategoryName = _cursor.getString(_cursorIndexOfCategoryName);
            final String _tmpCreatedAt;
            _tmpCreatedAt = _cursor.getString(_cursorIndexOfCreatedAt);
            final Integer _tmpId;
            if (_cursor.isNull(_cursorIndexOfId)) {
              _tmpId = null;
            } else {
              _tmpId = _cursor.getInt(_cursorIndexOfId);
            }
            final String _tmpUpdatedAt;
            _tmpUpdatedAt = _cursor.getString(_cursorIndexOfUpdatedAt);
            final Integer _tmpVendorId;
            if (_cursor.isNull(_cursorIndexOfVendorId)) {
              _tmpVendorId = null;
            } else {
              _tmpVendorId = _cursor.getInt(_cursorIndexOfVendorId);
            }
            _item = new VendorCategoryModel(_tmpCategoryName,_tmpCreatedAt,_tmpId,_tmpUpdatedAt,_tmpVendorId);
            _result.add(_item);
          }
          return _result;
        } finally {
          _cursor.close();
        }
      }

      @Override
      protected void finalize() {
        _statement.release();
      }
    });
  }

  @Override
  public LiveData<List<ProductModel>> getProducts(final int category_id, final int vendor_id) {
    final String _sql = "select * from Products where category_id=? AND vendor_id=? AND status==1";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 2);
    int _argIndex = 1;
    _statement.bindLong(_argIndex, category_id);
    _argIndex = 2;
    _statement.bindLong(_argIndex, vendor_id);
    return __db.getInvalidationTracker().createLiveData(new String[]{"Products"}, false, new Callable<List<ProductModel>>() {
      @Override
      public List<ProductModel> call() throws Exception {
        final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
        try {
          final int _cursorIndexOfCategoryId = CursorUtil.getColumnIndexOrThrow(_cursor, "category_id");
          final int _cursorIndexOfCategoryName = CursorUtil.getColumnIndexOrThrow(_cursor, "category_name");
          final int _cursorIndexOfCreatedAt = CursorUtil.getColumnIndexOrThrow(_cursor, "created_at");
          final int _cursorIndexOfDescription = CursorUtil.getColumnIndexOrThrow(_cursor, "description");
          final int _cursorIndexOfId = CursorUtil.getColumnIndexOrThrow(_cursor, "id");
          final int _cursorIndexOfImage = CursorUtil.getColumnIndexOrThrow(_cursor, "image");
          final int _cursorIndexOfPrice = CursorUtil.getColumnIndexOrThrow(_cursor, "price");
          final int _cursorIndexOfProductName = CursorUtil.getColumnIndexOrThrow(_cursor, "product_name");
          final int _cursorIndexOfPromoCodeId = CursorUtil.getColumnIndexOrThrow(_cursor, "promo_code_id");
          final int _cursorIndexOfQuantityAvailable = CursorUtil.getColumnIndexOrThrow(_cursor, "quantity_available");
          final int _cursorIndexOfStatus = CursorUtil.getColumnIndexOrThrow(_cursor, "status");
          final int _cursorIndexOfSubCategoryName = CursorUtil.getColumnIndexOrThrow(_cursor, "sub_category_name");
          final int _cursorIndexOfUpdatedAt = CursorUtil.getColumnIndexOrThrow(_cursor, "updated_at");
          final int _cursorIndexOfWeight = CursorUtil.getColumnIndexOrThrow(_cursor, "weight");
          final int _cursorIndexOfVendorId = CursorUtil.getColumnIndexOrThrow(_cursor, "vendor_id");
          final int _cursorIndexOfSalesTax = CursorUtil.getColumnIndexOrThrow(_cursor, "sales_tax");
          final int _cursorIndexOfFixedDiscount = CursorUtil.getColumnIndexOrThrow(_cursor, "fixed_discount");
          final int _cursorIndexOfIzDiscount = CursorUtil.getColumnIndexOrThrow(_cursor, "iz_discount");
          final int _cursorIndexOfComment = CursorUtil.getColumnIndexOrThrow(_cursor, "comment");
          final int _cursorIndexOfRating = CursorUtil.getColumnIndexOrThrow(_cursor, "rating");
          final List<ProductModel> _result = new ArrayList<ProductModel>(_cursor.getCount());
          while(_cursor.moveToNext()) {
            final ProductModel _item;
            final Integer _tmpCategoryId;
            if (_cursor.isNull(_cursorIndexOfCategoryId)) {
              _tmpCategoryId = null;
            } else {
              _tmpCategoryId = _cursor.getInt(_cursorIndexOfCategoryId);
            }
            final String _tmpCategoryName;
            _tmpCategoryName = _cursor.getString(_cursorIndexOfCategoryName);
            final String _tmpCreatedAt;
            _tmpCreatedAt = _cursor.getString(_cursorIndexOfCreatedAt);
            final String _tmpDescription;
            _tmpDescription = _cursor.getString(_cursorIndexOfDescription);
            final Integer _tmpId;
            if (_cursor.isNull(_cursorIndexOfId)) {
              _tmpId = null;
            } else {
              _tmpId = _cursor.getInt(_cursorIndexOfId);
            }
            final String _tmpImage;
            _tmpImage = _cursor.getString(_cursorIndexOfImage);
            final Integer _tmpPrice;
            if (_cursor.isNull(_cursorIndexOfPrice)) {
              _tmpPrice = null;
            } else {
              _tmpPrice = _cursor.getInt(_cursorIndexOfPrice);
            }
            final String _tmpProductName;
            _tmpProductName = _cursor.getString(_cursorIndexOfProductName);
            final Integer _tmpPromoCodeId;
            if (_cursor.isNull(_cursorIndexOfPromoCodeId)) {
              _tmpPromoCodeId = null;
            } else {
              _tmpPromoCodeId = _cursor.getInt(_cursorIndexOfPromoCodeId);
            }
            final Integer _tmpQuantityAvailable;
            if (_cursor.isNull(_cursorIndexOfQuantityAvailable)) {
              _tmpQuantityAvailable = null;
            } else {
              _tmpQuantityAvailable = _cursor.getInt(_cursorIndexOfQuantityAvailable);
            }
            final Integer _tmpStatus;
            if (_cursor.isNull(_cursorIndexOfStatus)) {
              _tmpStatus = null;
            } else {
              _tmpStatus = _cursor.getInt(_cursorIndexOfStatus);
            }
            final String _tmpSubCategoryName;
            _tmpSubCategoryName = _cursor.getString(_cursorIndexOfSubCategoryName);
            final String _tmpUpdatedAt;
            _tmpUpdatedAt = _cursor.getString(_cursorIndexOfUpdatedAt);
            final Integer _tmpWeight;
            if (_cursor.isNull(_cursorIndexOfWeight)) {
              _tmpWeight = null;
            } else {
              _tmpWeight = _cursor.getInt(_cursorIndexOfWeight);
            }
            final Integer _tmpVendorId;
            if (_cursor.isNull(_cursorIndexOfVendorId)) {
              _tmpVendorId = null;
            } else {
              _tmpVendorId = _cursor.getInt(_cursorIndexOfVendorId);
            }
            final double _tmpSales_tax;
            _tmpSales_tax = _cursor.getDouble(_cursorIndexOfSalesTax);
            final Integer _tmpFixed_discount;
            if (_cursor.isNull(_cursorIndexOfFixedDiscount)) {
              _tmpFixed_discount = null;
            } else {
              _tmpFixed_discount = _cursor.getInt(_cursorIndexOfFixedDiscount);
            }
            final Integer _tmpIz_discount;
            if (_cursor.isNull(_cursorIndexOfIzDiscount)) {
              _tmpIz_discount = null;
            } else {
              _tmpIz_discount = _cursor.getInt(_cursorIndexOfIzDiscount);
            }
            final String _tmpComment;
            _tmpComment = _cursor.getString(_cursorIndexOfComment);
            final String _tmpRating;
            _tmpRating = _cursor.getString(_cursorIndexOfRating);
            _item = new ProductModel(_tmpCategoryId,_tmpCategoryName,_tmpCreatedAt,_tmpDescription,_tmpId,_tmpImage,_tmpPrice,_tmpProductName,_tmpPromoCodeId,_tmpQuantityAvailable,_tmpStatus,_tmpSubCategoryName,_tmpUpdatedAt,_tmpWeight,_tmpVendorId,_tmpSales_tax,_tmpFixed_discount,_tmpIz_discount,_tmpComment,_tmpRating);
            _result.add(_item);
          }
          return _result;
        } finally {
          _cursor.close();
        }
      }

      @Override
      protected void finalize() {
        _statement.release();
      }
    });
  }

  @Override
  public LiveData<List<CartModel>> getCart() {
    final String _sql = "select * from Cart";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    return __db.getInvalidationTracker().createLiveData(new String[]{"Cart"}, false, new Callable<List<CartModel>>() {
      @Override
      public List<CartModel> call() throws Exception {
        final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
        try {
          final int _cursorIndexOfId = CursorUtil.getColumnIndexOrThrow(_cursor, "id");
          final int _cursorIndexOfCategoryId = CursorUtil.getColumnIndexOrThrow(_cursor, "category_id");
          final int _cursorIndexOfCategoryName = CursorUtil.getColumnIndexOrThrow(_cursor, "category_name");
          final int _cursorIndexOfCreatedAt = CursorUtil.getColumnIndexOrThrow(_cursor, "created_at");
          final int _cursorIndexOfDescription = CursorUtil.getColumnIndexOrThrow(_cursor, "description");
          final int _cursorIndexOfProductId = CursorUtil.getColumnIndexOrThrow(_cursor, "product_id");
          final int _cursorIndexOfImage = CursorUtil.getColumnIndexOrThrow(_cursor, "image");
          final int _cursorIndexOfPrice = CursorUtil.getColumnIndexOrThrow(_cursor, "price");
          final int _cursorIndexOfProductName = CursorUtil.getColumnIndexOrThrow(_cursor, "product_name");
          final int _cursorIndexOfPromoCodeId = CursorUtil.getColumnIndexOrThrow(_cursor, "promo_code_id");
          final int _cursorIndexOfQuantityAvailable = CursorUtil.getColumnIndexOrThrow(_cursor, "quantity_available");
          final int _cursorIndexOfStatus = CursorUtil.getColumnIndexOrThrow(_cursor, "status");
          final int _cursorIndexOfSubCategoryName = CursorUtil.getColumnIndexOrThrow(_cursor, "sub_category_name");
          final int _cursorIndexOfUpdatedAt = CursorUtil.getColumnIndexOrThrow(_cursor, "updated_at");
          final int _cursorIndexOfWeight = CursorUtil.getColumnIndexOrThrow(_cursor, "weight");
          final int _cursorIndexOfVendorId = CursorUtil.getColumnIndexOrThrow(_cursor, "vendor_id");
          final int _cursorIndexOfVendorName = CursorUtil.getColumnIndexOrThrow(_cursor, "vendor_name");
          final int _cursorIndexOfQuantity = CursorUtil.getColumnIndexOrThrow(_cursor, "quantity");
          final int _cursorIndexOfOrderId = CursorUtil.getColumnIndexOrThrow(_cursor, "order_id");
          final int _cursorIndexOfSalesTax = CursorUtil.getColumnIndexOrThrow(_cursor, "sales_tax");
          final int _cursorIndexOfFixedDiscount = CursorUtil.getColumnIndexOrThrow(_cursor, "fixed_discount");
          final int _cursorIndexOfIssDiscount = CursorUtil.getColumnIndexOrThrow(_cursor, "iss_discount");
          final int _cursorIndexOfOriginalPrice = CursorUtil.getColumnIndexOrThrow(_cursor, "originalPrice");
          final int _cursorIndexOfDiscountedPrice = CursorUtil.getColumnIndexOrThrow(_cursor, "discounted_price");
          final int _cursorIndexOfIsOutStock = CursorUtil.getColumnIndexOrThrow(_cursor, "isOutStock");
          final List<CartModel> _result = new ArrayList<CartModel>(_cursor.getCount());
          while(_cursor.moveToNext()) {
            final CartModel _item;
            final Long _tmpId;
            if (_cursor.isNull(_cursorIndexOfId)) {
              _tmpId = null;
            } else {
              _tmpId = _cursor.getLong(_cursorIndexOfId);
            }
            final Integer _tmpCategoryId;
            if (_cursor.isNull(_cursorIndexOfCategoryId)) {
              _tmpCategoryId = null;
            } else {
              _tmpCategoryId = _cursor.getInt(_cursorIndexOfCategoryId);
            }
            final String _tmpCategoryName;
            _tmpCategoryName = _cursor.getString(_cursorIndexOfCategoryName);
            final String _tmpCreatedAt;
            _tmpCreatedAt = _cursor.getString(_cursorIndexOfCreatedAt);
            final String _tmpDescription;
            _tmpDescription = _cursor.getString(_cursorIndexOfDescription);
            final Integer _tmpProduct_id;
            if (_cursor.isNull(_cursorIndexOfProductId)) {
              _tmpProduct_id = null;
            } else {
              _tmpProduct_id = _cursor.getInt(_cursorIndexOfProductId);
            }
            final String _tmpImage;
            _tmpImage = _cursor.getString(_cursorIndexOfImage);
            final Integer _tmpPrice;
            if (_cursor.isNull(_cursorIndexOfPrice)) {
              _tmpPrice = null;
            } else {
              _tmpPrice = _cursor.getInt(_cursorIndexOfPrice);
            }
            final String _tmpProductName;
            _tmpProductName = _cursor.getString(_cursorIndexOfProductName);
            final Integer _tmpPromoCodeId;
            if (_cursor.isNull(_cursorIndexOfPromoCodeId)) {
              _tmpPromoCodeId = null;
            } else {
              _tmpPromoCodeId = _cursor.getInt(_cursorIndexOfPromoCodeId);
            }
            final Integer _tmpQuantityAvailable;
            if (_cursor.isNull(_cursorIndexOfQuantityAvailable)) {
              _tmpQuantityAvailable = null;
            } else {
              _tmpQuantityAvailable = _cursor.getInt(_cursorIndexOfQuantityAvailable);
            }
            final Integer _tmpStatus;
            if (_cursor.isNull(_cursorIndexOfStatus)) {
              _tmpStatus = null;
            } else {
              _tmpStatus = _cursor.getInt(_cursorIndexOfStatus);
            }
            final String _tmpSubCategoryName;
            _tmpSubCategoryName = _cursor.getString(_cursorIndexOfSubCategoryName);
            final String _tmpUpdatedAt;
            _tmpUpdatedAt = _cursor.getString(_cursorIndexOfUpdatedAt);
            final Integer _tmpWeight;
            if (_cursor.isNull(_cursorIndexOfWeight)) {
              _tmpWeight = null;
            } else {
              _tmpWeight = _cursor.getInt(_cursorIndexOfWeight);
            }
            final Integer _tmpVendorId;
            if (_cursor.isNull(_cursorIndexOfVendorId)) {
              _tmpVendorId = null;
            } else {
              _tmpVendorId = _cursor.getInt(_cursorIndexOfVendorId);
            }
            final String _tmpVendor_name;
            _tmpVendor_name = _cursor.getString(_cursorIndexOfVendorName);
            final Integer _tmpQuantity;
            if (_cursor.isNull(_cursorIndexOfQuantity)) {
              _tmpQuantity = null;
            } else {
              _tmpQuantity = _cursor.getInt(_cursorIndexOfQuantity);
            }
            final Integer _tmpOrder_id;
            if (_cursor.isNull(_cursorIndexOfOrderId)) {
              _tmpOrder_id = null;
            } else {
              _tmpOrder_id = _cursor.getInt(_cursorIndexOfOrderId);
            }
            final double _tmpSales_tax;
            _tmpSales_tax = _cursor.getDouble(_cursorIndexOfSalesTax);
            final Integer _tmpFixed_discount;
            if (_cursor.isNull(_cursorIndexOfFixedDiscount)) {
              _tmpFixed_discount = null;
            } else {
              _tmpFixed_discount = _cursor.getInt(_cursorIndexOfFixedDiscount);
            }
            final Integer _tmpIss_discount;
            if (_cursor.isNull(_cursorIndexOfIssDiscount)) {
              _tmpIss_discount = null;
            } else {
              _tmpIss_discount = _cursor.getInt(_cursorIndexOfIssDiscount);
            }
            final Integer _tmpOriginalPrice;
            if (_cursor.isNull(_cursorIndexOfOriginalPrice)) {
              _tmpOriginalPrice = null;
            } else {
              _tmpOriginalPrice = _cursor.getInt(_cursorIndexOfOriginalPrice);
            }
            final Integer _tmpDiscounted_price;
            if (_cursor.isNull(_cursorIndexOfDiscountedPrice)) {
              _tmpDiscounted_price = null;
            } else {
              _tmpDiscounted_price = _cursor.getInt(_cursorIndexOfDiscountedPrice);
            }
            final boolean _tmpIsOutStock;
            final int _tmp;
            _tmp = _cursor.getInt(_cursorIndexOfIsOutStock);
            _tmpIsOutStock = _tmp != 0;
            _item = new CartModel(_tmpId,_tmpCategoryId,_tmpCategoryName,_tmpCreatedAt,_tmpDescription,_tmpProduct_id,_tmpImage,_tmpPrice,_tmpProductName,_tmpPromoCodeId,_tmpQuantityAvailable,_tmpStatus,_tmpSubCategoryName,_tmpUpdatedAt,_tmpWeight,_tmpVendorId,_tmpVendor_name,_tmpQuantity,_tmpOrder_id,_tmpSales_tax,_tmpFixed_discount,_tmpIss_discount,_tmpOriginalPrice,_tmpDiscounted_price,_tmpIsOutStock);
            _result.add(_item);
          }
          return _result;
        } finally {
          _cursor.close();
        }
      }

      @Override
      protected void finalize() {
        _statement.release();
      }
    });
  }

  @Override
  public List<CartModel> getCartItems() {
    final String _sql = "select * from Cart";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    __db.assertNotSuspendingTransaction();
    final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
    try {
      final int _cursorIndexOfId = CursorUtil.getColumnIndexOrThrow(_cursor, "id");
      final int _cursorIndexOfCategoryId = CursorUtil.getColumnIndexOrThrow(_cursor, "category_id");
      final int _cursorIndexOfCategoryName = CursorUtil.getColumnIndexOrThrow(_cursor, "category_name");
      final int _cursorIndexOfCreatedAt = CursorUtil.getColumnIndexOrThrow(_cursor, "created_at");
      final int _cursorIndexOfDescription = CursorUtil.getColumnIndexOrThrow(_cursor, "description");
      final int _cursorIndexOfProductId = CursorUtil.getColumnIndexOrThrow(_cursor, "product_id");
      final int _cursorIndexOfImage = CursorUtil.getColumnIndexOrThrow(_cursor, "image");
      final int _cursorIndexOfPrice = CursorUtil.getColumnIndexOrThrow(_cursor, "price");
      final int _cursorIndexOfProductName = CursorUtil.getColumnIndexOrThrow(_cursor, "product_name");
      final int _cursorIndexOfPromoCodeId = CursorUtil.getColumnIndexOrThrow(_cursor, "promo_code_id");
      final int _cursorIndexOfQuantityAvailable = CursorUtil.getColumnIndexOrThrow(_cursor, "quantity_available");
      final int _cursorIndexOfStatus = CursorUtil.getColumnIndexOrThrow(_cursor, "status");
      final int _cursorIndexOfSubCategoryName = CursorUtil.getColumnIndexOrThrow(_cursor, "sub_category_name");
      final int _cursorIndexOfUpdatedAt = CursorUtil.getColumnIndexOrThrow(_cursor, "updated_at");
      final int _cursorIndexOfWeight = CursorUtil.getColumnIndexOrThrow(_cursor, "weight");
      final int _cursorIndexOfVendorId = CursorUtil.getColumnIndexOrThrow(_cursor, "vendor_id");
      final int _cursorIndexOfVendorName = CursorUtil.getColumnIndexOrThrow(_cursor, "vendor_name");
      final int _cursorIndexOfQuantity = CursorUtil.getColumnIndexOrThrow(_cursor, "quantity");
      final int _cursorIndexOfOrderId = CursorUtil.getColumnIndexOrThrow(_cursor, "order_id");
      final int _cursorIndexOfSalesTax = CursorUtil.getColumnIndexOrThrow(_cursor, "sales_tax");
      final int _cursorIndexOfFixedDiscount = CursorUtil.getColumnIndexOrThrow(_cursor, "fixed_discount");
      final int _cursorIndexOfIssDiscount = CursorUtil.getColumnIndexOrThrow(_cursor, "iss_discount");
      final int _cursorIndexOfOriginalPrice = CursorUtil.getColumnIndexOrThrow(_cursor, "originalPrice");
      final int _cursorIndexOfDiscountedPrice = CursorUtil.getColumnIndexOrThrow(_cursor, "discounted_price");
      final int _cursorIndexOfIsOutStock = CursorUtil.getColumnIndexOrThrow(_cursor, "isOutStock");
      final List<CartModel> _result = new ArrayList<CartModel>(_cursor.getCount());
      while(_cursor.moveToNext()) {
        final CartModel _item;
        final Long _tmpId;
        if (_cursor.isNull(_cursorIndexOfId)) {
          _tmpId = null;
        } else {
          _tmpId = _cursor.getLong(_cursorIndexOfId);
        }
        final Integer _tmpCategoryId;
        if (_cursor.isNull(_cursorIndexOfCategoryId)) {
          _tmpCategoryId = null;
        } else {
          _tmpCategoryId = _cursor.getInt(_cursorIndexOfCategoryId);
        }
        final String _tmpCategoryName;
        _tmpCategoryName = _cursor.getString(_cursorIndexOfCategoryName);
        final String _tmpCreatedAt;
        _tmpCreatedAt = _cursor.getString(_cursorIndexOfCreatedAt);
        final String _tmpDescription;
        _tmpDescription = _cursor.getString(_cursorIndexOfDescription);
        final Integer _tmpProduct_id;
        if (_cursor.isNull(_cursorIndexOfProductId)) {
          _tmpProduct_id = null;
        } else {
          _tmpProduct_id = _cursor.getInt(_cursorIndexOfProductId);
        }
        final String _tmpImage;
        _tmpImage = _cursor.getString(_cursorIndexOfImage);
        final Integer _tmpPrice;
        if (_cursor.isNull(_cursorIndexOfPrice)) {
          _tmpPrice = null;
        } else {
          _tmpPrice = _cursor.getInt(_cursorIndexOfPrice);
        }
        final String _tmpProductName;
        _tmpProductName = _cursor.getString(_cursorIndexOfProductName);
        final Integer _tmpPromoCodeId;
        if (_cursor.isNull(_cursorIndexOfPromoCodeId)) {
          _tmpPromoCodeId = null;
        } else {
          _tmpPromoCodeId = _cursor.getInt(_cursorIndexOfPromoCodeId);
        }
        final Integer _tmpQuantityAvailable;
        if (_cursor.isNull(_cursorIndexOfQuantityAvailable)) {
          _tmpQuantityAvailable = null;
        } else {
          _tmpQuantityAvailable = _cursor.getInt(_cursorIndexOfQuantityAvailable);
        }
        final Integer _tmpStatus;
        if (_cursor.isNull(_cursorIndexOfStatus)) {
          _tmpStatus = null;
        } else {
          _tmpStatus = _cursor.getInt(_cursorIndexOfStatus);
        }
        final String _tmpSubCategoryName;
        _tmpSubCategoryName = _cursor.getString(_cursorIndexOfSubCategoryName);
        final String _tmpUpdatedAt;
        _tmpUpdatedAt = _cursor.getString(_cursorIndexOfUpdatedAt);
        final Integer _tmpWeight;
        if (_cursor.isNull(_cursorIndexOfWeight)) {
          _tmpWeight = null;
        } else {
          _tmpWeight = _cursor.getInt(_cursorIndexOfWeight);
        }
        final Integer _tmpVendorId;
        if (_cursor.isNull(_cursorIndexOfVendorId)) {
          _tmpVendorId = null;
        } else {
          _tmpVendorId = _cursor.getInt(_cursorIndexOfVendorId);
        }
        final String _tmpVendor_name;
        _tmpVendor_name = _cursor.getString(_cursorIndexOfVendorName);
        final Integer _tmpQuantity;
        if (_cursor.isNull(_cursorIndexOfQuantity)) {
          _tmpQuantity = null;
        } else {
          _tmpQuantity = _cursor.getInt(_cursorIndexOfQuantity);
        }
        final Integer _tmpOrder_id;
        if (_cursor.isNull(_cursorIndexOfOrderId)) {
          _tmpOrder_id = null;
        } else {
          _tmpOrder_id = _cursor.getInt(_cursorIndexOfOrderId);
        }
        final double _tmpSales_tax;
        _tmpSales_tax = _cursor.getDouble(_cursorIndexOfSalesTax);
        final Integer _tmpFixed_discount;
        if (_cursor.isNull(_cursorIndexOfFixedDiscount)) {
          _tmpFixed_discount = null;
        } else {
          _tmpFixed_discount = _cursor.getInt(_cursorIndexOfFixedDiscount);
        }
        final Integer _tmpIss_discount;
        if (_cursor.isNull(_cursorIndexOfIssDiscount)) {
          _tmpIss_discount = null;
        } else {
          _tmpIss_discount = _cursor.getInt(_cursorIndexOfIssDiscount);
        }
        final Integer _tmpOriginalPrice;
        if (_cursor.isNull(_cursorIndexOfOriginalPrice)) {
          _tmpOriginalPrice = null;
        } else {
          _tmpOriginalPrice = _cursor.getInt(_cursorIndexOfOriginalPrice);
        }
        final Integer _tmpDiscounted_price;
        if (_cursor.isNull(_cursorIndexOfDiscountedPrice)) {
          _tmpDiscounted_price = null;
        } else {
          _tmpDiscounted_price = _cursor.getInt(_cursorIndexOfDiscountedPrice);
        }
        final boolean _tmpIsOutStock;
        final int _tmp;
        _tmp = _cursor.getInt(_cursorIndexOfIsOutStock);
        _tmpIsOutStock = _tmp != 0;
        _item = new CartModel(_tmpId,_tmpCategoryId,_tmpCategoryName,_tmpCreatedAt,_tmpDescription,_tmpProduct_id,_tmpImage,_tmpPrice,_tmpProductName,_tmpPromoCodeId,_tmpQuantityAvailable,_tmpStatus,_tmpSubCategoryName,_tmpUpdatedAt,_tmpWeight,_tmpVendorId,_tmpVendor_name,_tmpQuantity,_tmpOrder_id,_tmpSales_tax,_tmpFixed_discount,_tmpIss_discount,_tmpOriginalPrice,_tmpDiscounted_price,_tmpIsOutStock);
        _result.add(_item);
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  @Override
  public List<ProductModel> searchProducts(final String product_name) {
    final String _sql = "select * from products where lower(product_name) LIKE lower(?)";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 1);
    int _argIndex = 1;
    if (product_name == null) {
      _statement.bindNull(_argIndex);
    } else {
      _statement.bindString(_argIndex, product_name);
    }
    __db.assertNotSuspendingTransaction();
    final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
    try {
      final int _cursorIndexOfCategoryId = CursorUtil.getColumnIndexOrThrow(_cursor, "category_id");
      final int _cursorIndexOfCategoryName = CursorUtil.getColumnIndexOrThrow(_cursor, "category_name");
      final int _cursorIndexOfCreatedAt = CursorUtil.getColumnIndexOrThrow(_cursor, "created_at");
      final int _cursorIndexOfDescription = CursorUtil.getColumnIndexOrThrow(_cursor, "description");
      final int _cursorIndexOfId = CursorUtil.getColumnIndexOrThrow(_cursor, "id");
      final int _cursorIndexOfImage = CursorUtil.getColumnIndexOrThrow(_cursor, "image");
      final int _cursorIndexOfPrice = CursorUtil.getColumnIndexOrThrow(_cursor, "price");
      final int _cursorIndexOfProductName = CursorUtil.getColumnIndexOrThrow(_cursor, "product_name");
      final int _cursorIndexOfPromoCodeId = CursorUtil.getColumnIndexOrThrow(_cursor, "promo_code_id");
      final int _cursorIndexOfQuantityAvailable = CursorUtil.getColumnIndexOrThrow(_cursor, "quantity_available");
      final int _cursorIndexOfStatus = CursorUtil.getColumnIndexOrThrow(_cursor, "status");
      final int _cursorIndexOfSubCategoryName = CursorUtil.getColumnIndexOrThrow(_cursor, "sub_category_name");
      final int _cursorIndexOfUpdatedAt = CursorUtil.getColumnIndexOrThrow(_cursor, "updated_at");
      final int _cursorIndexOfWeight = CursorUtil.getColumnIndexOrThrow(_cursor, "weight");
      final int _cursorIndexOfVendorId = CursorUtil.getColumnIndexOrThrow(_cursor, "vendor_id");
      final int _cursorIndexOfSalesTax = CursorUtil.getColumnIndexOrThrow(_cursor, "sales_tax");
      final int _cursorIndexOfFixedDiscount = CursorUtil.getColumnIndexOrThrow(_cursor, "fixed_discount");
      final int _cursorIndexOfIzDiscount = CursorUtil.getColumnIndexOrThrow(_cursor, "iz_discount");
      final int _cursorIndexOfComment = CursorUtil.getColumnIndexOrThrow(_cursor, "comment");
      final int _cursorIndexOfRating = CursorUtil.getColumnIndexOrThrow(_cursor, "rating");
      final List<ProductModel> _result = new ArrayList<ProductModel>(_cursor.getCount());
      while(_cursor.moveToNext()) {
        final ProductModel _item;
        final Integer _tmpCategoryId;
        if (_cursor.isNull(_cursorIndexOfCategoryId)) {
          _tmpCategoryId = null;
        } else {
          _tmpCategoryId = _cursor.getInt(_cursorIndexOfCategoryId);
        }
        final String _tmpCategoryName;
        _tmpCategoryName = _cursor.getString(_cursorIndexOfCategoryName);
        final String _tmpCreatedAt;
        _tmpCreatedAt = _cursor.getString(_cursorIndexOfCreatedAt);
        final String _tmpDescription;
        _tmpDescription = _cursor.getString(_cursorIndexOfDescription);
        final Integer _tmpId;
        if (_cursor.isNull(_cursorIndexOfId)) {
          _tmpId = null;
        } else {
          _tmpId = _cursor.getInt(_cursorIndexOfId);
        }
        final String _tmpImage;
        _tmpImage = _cursor.getString(_cursorIndexOfImage);
        final Integer _tmpPrice;
        if (_cursor.isNull(_cursorIndexOfPrice)) {
          _tmpPrice = null;
        } else {
          _tmpPrice = _cursor.getInt(_cursorIndexOfPrice);
        }
        final String _tmpProductName;
        _tmpProductName = _cursor.getString(_cursorIndexOfProductName);
        final Integer _tmpPromoCodeId;
        if (_cursor.isNull(_cursorIndexOfPromoCodeId)) {
          _tmpPromoCodeId = null;
        } else {
          _tmpPromoCodeId = _cursor.getInt(_cursorIndexOfPromoCodeId);
        }
        final Integer _tmpQuantityAvailable;
        if (_cursor.isNull(_cursorIndexOfQuantityAvailable)) {
          _tmpQuantityAvailable = null;
        } else {
          _tmpQuantityAvailable = _cursor.getInt(_cursorIndexOfQuantityAvailable);
        }
        final Integer _tmpStatus;
        if (_cursor.isNull(_cursorIndexOfStatus)) {
          _tmpStatus = null;
        } else {
          _tmpStatus = _cursor.getInt(_cursorIndexOfStatus);
        }
        final String _tmpSubCategoryName;
        _tmpSubCategoryName = _cursor.getString(_cursorIndexOfSubCategoryName);
        final String _tmpUpdatedAt;
        _tmpUpdatedAt = _cursor.getString(_cursorIndexOfUpdatedAt);
        final Integer _tmpWeight;
        if (_cursor.isNull(_cursorIndexOfWeight)) {
          _tmpWeight = null;
        } else {
          _tmpWeight = _cursor.getInt(_cursorIndexOfWeight);
        }
        final Integer _tmpVendorId;
        if (_cursor.isNull(_cursorIndexOfVendorId)) {
          _tmpVendorId = null;
        } else {
          _tmpVendorId = _cursor.getInt(_cursorIndexOfVendorId);
        }
        final double _tmpSales_tax;
        _tmpSales_tax = _cursor.getDouble(_cursorIndexOfSalesTax);
        final Integer _tmpFixed_discount;
        if (_cursor.isNull(_cursorIndexOfFixedDiscount)) {
          _tmpFixed_discount = null;
        } else {
          _tmpFixed_discount = _cursor.getInt(_cursorIndexOfFixedDiscount);
        }
        final Integer _tmpIz_discount;
        if (_cursor.isNull(_cursorIndexOfIzDiscount)) {
          _tmpIz_discount = null;
        } else {
          _tmpIz_discount = _cursor.getInt(_cursorIndexOfIzDiscount);
        }
        final String _tmpComment;
        _tmpComment = _cursor.getString(_cursorIndexOfComment);
        final String _tmpRating;
        _tmpRating = _cursor.getString(_cursorIndexOfRating);
        _item = new ProductModel(_tmpCategoryId,_tmpCategoryName,_tmpCreatedAt,_tmpDescription,_tmpId,_tmpImage,_tmpPrice,_tmpProductName,_tmpPromoCodeId,_tmpQuantityAvailable,_tmpStatus,_tmpSubCategoryName,_tmpUpdatedAt,_tmpWeight,_tmpVendorId,_tmpSales_tax,_tmpFixed_discount,_tmpIz_discount,_tmpComment,_tmpRating);
        _result.add(_item);
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  @Override
  public List<ProductModel> getAllProducts() {
    final String _sql = "select * from products";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    __db.assertNotSuspendingTransaction();
    final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
    try {
      final int _cursorIndexOfCategoryId = CursorUtil.getColumnIndexOrThrow(_cursor, "category_id");
      final int _cursorIndexOfCategoryName = CursorUtil.getColumnIndexOrThrow(_cursor, "category_name");
      final int _cursorIndexOfCreatedAt = CursorUtil.getColumnIndexOrThrow(_cursor, "created_at");
      final int _cursorIndexOfDescription = CursorUtil.getColumnIndexOrThrow(_cursor, "description");
      final int _cursorIndexOfId = CursorUtil.getColumnIndexOrThrow(_cursor, "id");
      final int _cursorIndexOfImage = CursorUtil.getColumnIndexOrThrow(_cursor, "image");
      final int _cursorIndexOfPrice = CursorUtil.getColumnIndexOrThrow(_cursor, "price");
      final int _cursorIndexOfProductName = CursorUtil.getColumnIndexOrThrow(_cursor, "product_name");
      final int _cursorIndexOfPromoCodeId = CursorUtil.getColumnIndexOrThrow(_cursor, "promo_code_id");
      final int _cursorIndexOfQuantityAvailable = CursorUtil.getColumnIndexOrThrow(_cursor, "quantity_available");
      final int _cursorIndexOfStatus = CursorUtil.getColumnIndexOrThrow(_cursor, "status");
      final int _cursorIndexOfSubCategoryName = CursorUtil.getColumnIndexOrThrow(_cursor, "sub_category_name");
      final int _cursorIndexOfUpdatedAt = CursorUtil.getColumnIndexOrThrow(_cursor, "updated_at");
      final int _cursorIndexOfWeight = CursorUtil.getColumnIndexOrThrow(_cursor, "weight");
      final int _cursorIndexOfVendorId = CursorUtil.getColumnIndexOrThrow(_cursor, "vendor_id");
      final int _cursorIndexOfSalesTax = CursorUtil.getColumnIndexOrThrow(_cursor, "sales_tax");
      final int _cursorIndexOfFixedDiscount = CursorUtil.getColumnIndexOrThrow(_cursor, "fixed_discount");
      final int _cursorIndexOfIzDiscount = CursorUtil.getColumnIndexOrThrow(_cursor, "iz_discount");
      final int _cursorIndexOfComment = CursorUtil.getColumnIndexOrThrow(_cursor, "comment");
      final int _cursorIndexOfRating = CursorUtil.getColumnIndexOrThrow(_cursor, "rating");
      final List<ProductModel> _result = new ArrayList<ProductModel>(_cursor.getCount());
      while(_cursor.moveToNext()) {
        final ProductModel _item;
        final Integer _tmpCategoryId;
        if (_cursor.isNull(_cursorIndexOfCategoryId)) {
          _tmpCategoryId = null;
        } else {
          _tmpCategoryId = _cursor.getInt(_cursorIndexOfCategoryId);
        }
        final String _tmpCategoryName;
        _tmpCategoryName = _cursor.getString(_cursorIndexOfCategoryName);
        final String _tmpCreatedAt;
        _tmpCreatedAt = _cursor.getString(_cursorIndexOfCreatedAt);
        final String _tmpDescription;
        _tmpDescription = _cursor.getString(_cursorIndexOfDescription);
        final Integer _tmpId;
        if (_cursor.isNull(_cursorIndexOfId)) {
          _tmpId = null;
        } else {
          _tmpId = _cursor.getInt(_cursorIndexOfId);
        }
        final String _tmpImage;
        _tmpImage = _cursor.getString(_cursorIndexOfImage);
        final Integer _tmpPrice;
        if (_cursor.isNull(_cursorIndexOfPrice)) {
          _tmpPrice = null;
        } else {
          _tmpPrice = _cursor.getInt(_cursorIndexOfPrice);
        }
        final String _tmpProductName;
        _tmpProductName = _cursor.getString(_cursorIndexOfProductName);
        final Integer _tmpPromoCodeId;
        if (_cursor.isNull(_cursorIndexOfPromoCodeId)) {
          _tmpPromoCodeId = null;
        } else {
          _tmpPromoCodeId = _cursor.getInt(_cursorIndexOfPromoCodeId);
        }
        final Integer _tmpQuantityAvailable;
        if (_cursor.isNull(_cursorIndexOfQuantityAvailable)) {
          _tmpQuantityAvailable = null;
        } else {
          _tmpQuantityAvailable = _cursor.getInt(_cursorIndexOfQuantityAvailable);
        }
        final Integer _tmpStatus;
        if (_cursor.isNull(_cursorIndexOfStatus)) {
          _tmpStatus = null;
        } else {
          _tmpStatus = _cursor.getInt(_cursorIndexOfStatus);
        }
        final String _tmpSubCategoryName;
        _tmpSubCategoryName = _cursor.getString(_cursorIndexOfSubCategoryName);
        final String _tmpUpdatedAt;
        _tmpUpdatedAt = _cursor.getString(_cursorIndexOfUpdatedAt);
        final Integer _tmpWeight;
        if (_cursor.isNull(_cursorIndexOfWeight)) {
          _tmpWeight = null;
        } else {
          _tmpWeight = _cursor.getInt(_cursorIndexOfWeight);
        }
        final Integer _tmpVendorId;
        if (_cursor.isNull(_cursorIndexOfVendorId)) {
          _tmpVendorId = null;
        } else {
          _tmpVendorId = _cursor.getInt(_cursorIndexOfVendorId);
        }
        final double _tmpSales_tax;
        _tmpSales_tax = _cursor.getDouble(_cursorIndexOfSalesTax);
        final Integer _tmpFixed_discount;
        if (_cursor.isNull(_cursorIndexOfFixedDiscount)) {
          _tmpFixed_discount = null;
        } else {
          _tmpFixed_discount = _cursor.getInt(_cursorIndexOfFixedDiscount);
        }
        final Integer _tmpIz_discount;
        if (_cursor.isNull(_cursorIndexOfIzDiscount)) {
          _tmpIz_discount = null;
        } else {
          _tmpIz_discount = _cursor.getInt(_cursorIndexOfIzDiscount);
        }
        final String _tmpComment;
        _tmpComment = _cursor.getString(_cursorIndexOfComment);
        final String _tmpRating;
        _tmpRating = _cursor.getString(_cursorIndexOfRating);
        _item = new ProductModel(_tmpCategoryId,_tmpCategoryName,_tmpCreatedAt,_tmpDescription,_tmpId,_tmpImage,_tmpPrice,_tmpProductName,_tmpPromoCodeId,_tmpQuantityAvailable,_tmpStatus,_tmpSubCategoryName,_tmpUpdatedAt,_tmpWeight,_tmpVendorId,_tmpSales_tax,_tmpFixed_discount,_tmpIz_discount,_tmpComment,_tmpRating);
        _result.add(_item);
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  @Override
  public List<ProductModel> checkProductExist(final int id) {
    final String _sql = "select * from products where id =?";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 1);
    int _argIndex = 1;
    _statement.bindLong(_argIndex, id);
    __db.assertNotSuspendingTransaction();
    final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
    try {
      final int _cursorIndexOfCategoryId = CursorUtil.getColumnIndexOrThrow(_cursor, "category_id");
      final int _cursorIndexOfCategoryName = CursorUtil.getColumnIndexOrThrow(_cursor, "category_name");
      final int _cursorIndexOfCreatedAt = CursorUtil.getColumnIndexOrThrow(_cursor, "created_at");
      final int _cursorIndexOfDescription = CursorUtil.getColumnIndexOrThrow(_cursor, "description");
      final int _cursorIndexOfId = CursorUtil.getColumnIndexOrThrow(_cursor, "id");
      final int _cursorIndexOfImage = CursorUtil.getColumnIndexOrThrow(_cursor, "image");
      final int _cursorIndexOfPrice = CursorUtil.getColumnIndexOrThrow(_cursor, "price");
      final int _cursorIndexOfProductName = CursorUtil.getColumnIndexOrThrow(_cursor, "product_name");
      final int _cursorIndexOfPromoCodeId = CursorUtil.getColumnIndexOrThrow(_cursor, "promo_code_id");
      final int _cursorIndexOfQuantityAvailable = CursorUtil.getColumnIndexOrThrow(_cursor, "quantity_available");
      final int _cursorIndexOfStatus = CursorUtil.getColumnIndexOrThrow(_cursor, "status");
      final int _cursorIndexOfSubCategoryName = CursorUtil.getColumnIndexOrThrow(_cursor, "sub_category_name");
      final int _cursorIndexOfUpdatedAt = CursorUtil.getColumnIndexOrThrow(_cursor, "updated_at");
      final int _cursorIndexOfWeight = CursorUtil.getColumnIndexOrThrow(_cursor, "weight");
      final int _cursorIndexOfVendorId = CursorUtil.getColumnIndexOrThrow(_cursor, "vendor_id");
      final int _cursorIndexOfSalesTax = CursorUtil.getColumnIndexOrThrow(_cursor, "sales_tax");
      final int _cursorIndexOfFixedDiscount = CursorUtil.getColumnIndexOrThrow(_cursor, "fixed_discount");
      final int _cursorIndexOfIzDiscount = CursorUtil.getColumnIndexOrThrow(_cursor, "iz_discount");
      final int _cursorIndexOfComment = CursorUtil.getColumnIndexOrThrow(_cursor, "comment");
      final int _cursorIndexOfRating = CursorUtil.getColumnIndexOrThrow(_cursor, "rating");
      final List<ProductModel> _result = new ArrayList<ProductModel>(_cursor.getCount());
      while(_cursor.moveToNext()) {
        final ProductModel _item;
        final Integer _tmpCategoryId;
        if (_cursor.isNull(_cursorIndexOfCategoryId)) {
          _tmpCategoryId = null;
        } else {
          _tmpCategoryId = _cursor.getInt(_cursorIndexOfCategoryId);
        }
        final String _tmpCategoryName;
        _tmpCategoryName = _cursor.getString(_cursorIndexOfCategoryName);
        final String _tmpCreatedAt;
        _tmpCreatedAt = _cursor.getString(_cursorIndexOfCreatedAt);
        final String _tmpDescription;
        _tmpDescription = _cursor.getString(_cursorIndexOfDescription);
        final Integer _tmpId;
        if (_cursor.isNull(_cursorIndexOfId)) {
          _tmpId = null;
        } else {
          _tmpId = _cursor.getInt(_cursorIndexOfId);
        }
        final String _tmpImage;
        _tmpImage = _cursor.getString(_cursorIndexOfImage);
        final Integer _tmpPrice;
        if (_cursor.isNull(_cursorIndexOfPrice)) {
          _tmpPrice = null;
        } else {
          _tmpPrice = _cursor.getInt(_cursorIndexOfPrice);
        }
        final String _tmpProductName;
        _tmpProductName = _cursor.getString(_cursorIndexOfProductName);
        final Integer _tmpPromoCodeId;
        if (_cursor.isNull(_cursorIndexOfPromoCodeId)) {
          _tmpPromoCodeId = null;
        } else {
          _tmpPromoCodeId = _cursor.getInt(_cursorIndexOfPromoCodeId);
        }
        final Integer _tmpQuantityAvailable;
        if (_cursor.isNull(_cursorIndexOfQuantityAvailable)) {
          _tmpQuantityAvailable = null;
        } else {
          _tmpQuantityAvailable = _cursor.getInt(_cursorIndexOfQuantityAvailable);
        }
        final Integer _tmpStatus;
        if (_cursor.isNull(_cursorIndexOfStatus)) {
          _tmpStatus = null;
        } else {
          _tmpStatus = _cursor.getInt(_cursorIndexOfStatus);
        }
        final String _tmpSubCategoryName;
        _tmpSubCategoryName = _cursor.getString(_cursorIndexOfSubCategoryName);
        final String _tmpUpdatedAt;
        _tmpUpdatedAt = _cursor.getString(_cursorIndexOfUpdatedAt);
        final Integer _tmpWeight;
        if (_cursor.isNull(_cursorIndexOfWeight)) {
          _tmpWeight = null;
        } else {
          _tmpWeight = _cursor.getInt(_cursorIndexOfWeight);
        }
        final Integer _tmpVendorId;
        if (_cursor.isNull(_cursorIndexOfVendorId)) {
          _tmpVendorId = null;
        } else {
          _tmpVendorId = _cursor.getInt(_cursorIndexOfVendorId);
        }
        final double _tmpSales_tax;
        _tmpSales_tax = _cursor.getDouble(_cursorIndexOfSalesTax);
        final Integer _tmpFixed_discount;
        if (_cursor.isNull(_cursorIndexOfFixedDiscount)) {
          _tmpFixed_discount = null;
        } else {
          _tmpFixed_discount = _cursor.getInt(_cursorIndexOfFixedDiscount);
        }
        final Integer _tmpIz_discount;
        if (_cursor.isNull(_cursorIndexOfIzDiscount)) {
          _tmpIz_discount = null;
        } else {
          _tmpIz_discount = _cursor.getInt(_cursorIndexOfIzDiscount);
        }
        final String _tmpComment;
        _tmpComment = _cursor.getString(_cursorIndexOfComment);
        final String _tmpRating;
        _tmpRating = _cursor.getString(_cursorIndexOfRating);
        _item = new ProductModel(_tmpCategoryId,_tmpCategoryName,_tmpCreatedAt,_tmpDescription,_tmpId,_tmpImage,_tmpPrice,_tmpProductName,_tmpPromoCodeId,_tmpQuantityAvailable,_tmpStatus,_tmpSubCategoryName,_tmpUpdatedAt,_tmpWeight,_tmpVendorId,_tmpSales_tax,_tmpFixed_discount,_tmpIz_discount,_tmpComment,_tmpRating);
        _result.add(_item);
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  @Override
  public long getVendorIDFromCart() {
    final String _sql = "SELECT vendor_id FROM Cart ORDER BY id ASC LIMIT 1";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    __db.assertNotSuspendingTransaction();
    final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
    try {
      final long _result;
      if(_cursor.moveToFirst()) {
        _result = _cursor.getLong(0);
      } else {
        _result = 0;
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }

  @Override
  public List<Vendor> getVendorByID(final int vendor_id) {
    final String _sql = "select * from vendor where id=?";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 1);
    int _argIndex = 1;
    _statement.bindLong(_argIndex, vendor_id);
    __db.assertNotSuspendingTransaction();
    final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
    try {
      final int _cursorIndexOfCategoryId = CursorUtil.getColumnIndexOrThrow(_cursor, "category_id");
      final int _cursorIndexOfCreatedAt = CursorUtil.getColumnIndexOrThrow(_cursor, "created_at");
      final int _cursorIndexOfEmail = CursorUtil.getColumnIndexOrThrow(_cursor, "email");
      final int _cursorIndexOfEmailVerifiedAt = CursorUtil.getColumnIndexOrThrow(_cursor, "email_verified_at");
      final int _cursorIndexOfFName = CursorUtil.getColumnIndexOrThrow(_cursor, "f_name");
      final int _cursorIndexOfFcm = CursorUtil.getColumnIndexOrThrow(_cursor, "fcm");
      final int _cursorIndexOfId = CursorUtil.getColumnIndexOrThrow(_cursor, "id");
      final int _cursorIndexOfImage = CursorUtil.getColumnIndexOrThrow(_cursor, "image");
      final int _cursorIndexOfIsVerified = CursorUtil.getColumnIndexOrThrow(_cursor, "is_verified");
      final int _cursorIndexOfLName = CursorUtil.getColumnIndexOrThrow(_cursor, "l_name");
      final int _cursorIndexOfLoginFacebook = CursorUtil.getColumnIndexOrThrow(_cursor, "login_facebook");
      final int _cursorIndexOfModel = CursorUtil.getColumnIndexOrThrow(_cursor, "model");
      final int _cursorIndexOfNoPlate = CursorUtil.getColumnIndexOrThrow(_cursor, "no_plate");
      final int _cursorIndexOfOtp = CursorUtil.getColumnIndexOrThrow(_cursor, "otp");
      final int _cursorIndexOfPhoneNumber = CursorUtil.getColumnIndexOrThrow(_cursor, "phone_number");
      final int _cursorIndexOfRiderAvailability = CursorUtil.getColumnIndexOrThrow(_cursor, "rider_availability");
      final int _cursorIndexOfRiderWorkStatus = CursorUtil.getColumnIndexOrThrow(_cursor, "rider_work_status");
      final int _cursorIndexOfServiceAreaId = CursorUtil.getColumnIndexOrThrow(_cursor, "service_area_id");
      final int _cursorIndexOfToken = CursorUtil.getColumnIndexOrThrow(_cursor, "token");
      final int _cursorIndexOfUpdatedAt = CursorUtil.getColumnIndexOrThrow(_cursor, "updated_at");
      final int _cursorIndexOfUserStatus = CursorUtil.getColumnIndexOrThrow(_cursor, "user_status");
      final int _cursorIndexOfUserType = CursorUtil.getColumnIndexOrThrow(_cursor, "user_type");
      final int _cursorIndexOfIsDelivery = CursorUtil.getColumnIndexOrThrow(_cursor, "is_delivery");
      final List<Vendor> _result = new ArrayList<Vendor>(_cursor.getCount());
      while(_cursor.moveToNext()) {
        final Vendor _item;
        final Integer _tmpCategoryId;
        if (_cursor.isNull(_cursorIndexOfCategoryId)) {
          _tmpCategoryId = null;
        } else {
          _tmpCategoryId = _cursor.getInt(_cursorIndexOfCategoryId);
        }
        final String _tmpCreatedAt;
        _tmpCreatedAt = _cursor.getString(_cursorIndexOfCreatedAt);
        final String _tmpEmail;
        _tmpEmail = _cursor.getString(_cursorIndexOfEmail);
        final String _tmpEmailVerifiedAt;
        _tmpEmailVerifiedAt = _cursor.getString(_cursorIndexOfEmailVerifiedAt);
        final String _tmpFName;
        _tmpFName = _cursor.getString(_cursorIndexOfFName);
        final String _tmpFcm;
        _tmpFcm = _cursor.getString(_cursorIndexOfFcm);
        final Integer _tmpId;
        if (_cursor.isNull(_cursorIndexOfId)) {
          _tmpId = null;
        } else {
          _tmpId = _cursor.getInt(_cursorIndexOfId);
        }
        final String _tmpImage;
        _tmpImage = _cursor.getString(_cursorIndexOfImage);
        final Boolean _tmpIsVerified;
        final Integer _tmp;
        if (_cursor.isNull(_cursorIndexOfIsVerified)) {
          _tmp = null;
        } else {
          _tmp = _cursor.getInt(_cursorIndexOfIsVerified);
        }
        _tmpIsVerified = _tmp == null ? null : _tmp != 0;
        final String _tmpLName;
        _tmpLName = _cursor.getString(_cursorIndexOfLName);
        final Boolean _tmpLoginFacebook;
        final Integer _tmp_1;
        if (_cursor.isNull(_cursorIndexOfLoginFacebook)) {
          _tmp_1 = null;
        } else {
          _tmp_1 = _cursor.getInt(_cursorIndexOfLoginFacebook);
        }
        _tmpLoginFacebook = _tmp_1 == null ? null : _tmp_1 != 0;
        final String _tmpModel;
        _tmpModel = _cursor.getString(_cursorIndexOfModel);
        final String _tmpNoPlate;
        _tmpNoPlate = _cursor.getString(_cursorIndexOfNoPlate);
        final String _tmpOtp;
        _tmpOtp = _cursor.getString(_cursorIndexOfOtp);
        final String _tmpPhoneNumber;
        _tmpPhoneNumber = _cursor.getString(_cursorIndexOfPhoneNumber);
        final String _tmpRiderAvailability;
        _tmpRiderAvailability = _cursor.getString(_cursorIndexOfRiderAvailability);
        final String _tmpRiderWorkStatus;
        _tmpRiderWorkStatus = _cursor.getString(_cursorIndexOfRiderWorkStatus);
        final String _tmpServiceAreaId;
        _tmpServiceAreaId = _cursor.getString(_cursorIndexOfServiceAreaId);
        final String _tmpToken;
        _tmpToken = _cursor.getString(_cursorIndexOfToken);
        final String _tmpUpdatedAt;
        _tmpUpdatedAt = _cursor.getString(_cursorIndexOfUpdatedAt);
        final Integer _tmpUserStatus;
        if (_cursor.isNull(_cursorIndexOfUserStatus)) {
          _tmpUserStatus = null;
        } else {
          _tmpUserStatus = _cursor.getInt(_cursorIndexOfUserStatus);
        }
        final String _tmpUserType;
        _tmpUserType = _cursor.getString(_cursorIndexOfUserType);
        final int _tmpIs_delivery;
        _tmpIs_delivery = _cursor.getInt(_cursorIndexOfIsDelivery);
        _item = new Vendor(_tmpCategoryId,_tmpCreatedAt,_tmpEmail,_tmpEmailVerifiedAt,_tmpFName,_tmpFcm,_tmpId,_tmpImage,_tmpIsVerified,_tmpLName,_tmpLoginFacebook,_tmpModel,_tmpNoPlate,_tmpOtp,_tmpPhoneNumber,_tmpRiderAvailability,_tmpRiderWorkStatus,_tmpServiceAreaId,_tmpToken,_tmpUpdatedAt,_tmpUserStatus,_tmpUserType,_tmpIs_delivery);
        _result.add(_item);
      }
      return _result;
    } finally {
      _cursor.close();
      _statement.release();
    }
  }
}
