package com.senarios.customer.databinding;
import com.senarios.customer.R;
import com.senarios.customer.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentOrderMedicineDetailsBindingImpl extends FragmentOrderMedicineDetailsBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.appbar, 4);
        sViewsWithIds.put(R.id.toolbar, 5);
        sViewsWithIds.put(R.id.cardview, 6);
        sViewsWithIds.put(R.id.view1, 7);
        sViewsWithIds.put(R.id.view2, 8);
        sViewsWithIds.put(R.id.address, 9);
        sViewsWithIds.put(R.id.recyclerView, 10);
        sViewsWithIds.put(R.id.cancel, 11);
    }
    // views
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FragmentOrderMedicineDetailsBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 12, sIncludes, sViewsWithIds));
    }
    private FragmentOrderMedicineDetailsBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (android.widget.TextView) bindings[9]
            , (com.google.android.material.appbar.AppBarLayout) bindings[4]
            , (android.widget.Button) bindings[11]
            , (androidx.cardview.widget.CardView) bindings[6]
            , (androidx.coordinatorlayout.widget.CoordinatorLayout) bindings[0]
            , (de.hdodenhof.circleimageview.CircleImageView) bindings[1]
            , (android.widget.TextView) bindings[3]
            , (androidx.recyclerview.widget.RecyclerView) bindings[10]
            , (android.widget.TextView) bindings[2]
            , (androidx.appcompat.widget.Toolbar) bindings[5]
            , (android.view.View) bindings[7]
            , (android.view.View) bindings[8]
            );
        this.constraintLayout.setTag(null);
        this.imageviewMedicineOrder.setTag(null);
        this.orderID.setTag(null);
        this.status.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x4L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.order == variableId) {
            setOrder((com.senarios.customer.models.OrderModel) variable);
        }
        else if (BR.data == variableId) {
            setData((com.senarios.customer.models.MedicineModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setOrder(@Nullable com.senarios.customer.models.OrderModel Order) {
        this.mOrder = Order;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.order);
        super.requestRebind();
    }
    public void setData(@Nullable com.senarios.customer.models.MedicineModel Data) {
        this.mData = Data;
        synchronized(this) {
            mDirtyFlags |= 0x2L;
        }
        notifyPropertyChanged(BR.data);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String javaLangStringOrderIDOrderId = null;
        java.lang.String orderStatus = null;
        java.lang.String dataPhoto = null;
        int orderId = 0;
        com.senarios.customer.models.OrderModel order = mOrder;
        com.senarios.customer.models.MedicineModel data = mData;

        if ((dirtyFlags & 0x5L) != 0) {



                if (order != null) {
                    // read order.status
                    orderStatus = order.getStatus();
                    // read order.id
                    orderId = order.getId();
                }


                // read ("Order ID : ") + (order.id)
                javaLangStringOrderIDOrderId = ("Order ID : ") + (orderId);
        }
        if ((dirtyFlags & 0x6L) != 0) {



                if (data != null) {
                    // read data.photo
                    dataPhoto = data.getPhoto();
                }
        }
        // batch finished
        if ((dirtyFlags & 0x6L) != 0) {
            // api target 1

            com.senarios.customer.models.MedicineModelKt.loadImageMedicine(this.imageviewMedicineOrder, dataPhoto);
        }
        if ((dirtyFlags & 0x5L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.orderID, javaLangStringOrderIDOrderId);
            com.senarios.customer.models.OrderModelKt.setOrderStatus(this.status, orderStatus);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): order
        flag 1 (0x2L): data
        flag 2 (0x3L): null
    flag mapping end*/
    //end
}