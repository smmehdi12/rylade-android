package com.senarios.customer.databinding;
import com.senarios.customer.R;
import com.senarios.customer.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ItemCartItemBindingImpl extends ItemCartItemBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.card_view, 6);
        sViewsWithIds.put(R.id.nested_constraint, 7);
        sViewsWithIds.put(R.id.minus_icon, 8);
        sViewsWithIds.put(R.id.plus_icon, 9);
    }
    // views
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ItemCartItemBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 10, sIncludes, sViewsWithIds));
    }
    private ItemCartItemBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (androidx.cardview.widget.CardView) bindings[6]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[0]
            , (android.widget.ImageView) bindings[8]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[7]
            , (android.widget.ImageView) bindings[9]
            , (android.widget.TextView) bindings[1]
            , (android.widget.TextView) bindings[4]
            , (android.widget.TextView) bindings[5]
            , (android.widget.TextView) bindings[2]
            , (android.widget.TextView) bindings[3]
            );
        this.mainConstraint.setTag(null);
        this.tvCounter.setTag(null);
        this.tvDProductPrice.setTag(null);
        this.tvOutOfStock.setTag(null);
        this.tvProductName.setTag(null);
        this.tvProductPrice.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.data == variableId) {
            setData((com.senarios.customer.models.CartModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setData(@Nullable com.senarios.customer.models.CartModel Data) {
        this.mData = Data;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.data);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.Integer dataFixedDiscount = null;
        boolean dataOutStock = false;
        java.lang.Integer dataIssDiscount = null;
        java.lang.Integer dataQuantity = null;
        java.lang.String dataProductName = null;
        java.lang.Integer dataOriginalPrice = null;
        com.senarios.customer.models.CartModel data = mData;
        java.lang.String dataQuantityJavaLangString = null;

        if ((dirtyFlags & 0x3L) != 0) {



                if (data != null) {
                    // read data.fixed_discount
                    dataFixedDiscount = data.getFixed_discount();
                    // read data.outStock
                    dataOutStock = data.isOutStock();
                    // read data.iss_discount
                    dataIssDiscount = data.getIss_discount();
                    // read data.quantity
                    dataQuantity = data.getQuantity();
                    // read data.productName
                    dataProductName = data.getProductName();
                    // read data.originalPrice
                    dataOriginalPrice = data.getOriginalPrice();
                }


                // read (data.quantity) + ("")
                dataQuantityJavaLangString = (dataQuantity) + ("");
        }
        // batch finished
        if ((dirtyFlags & 0x3L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvCounter, dataQuantityJavaLangString);
            com.senarios.customer.models.ProductModelKt.setPrice(this.tvDProductPrice, dataFixedDiscount, java.lang.String.valueOf(1), dataIssDiscount);
            com.senarios.customer.models.CartModelKt.setoutstock(this.tvOutOfStock, dataOutStock);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvProductName, dataProductName);
            com.senarios.customer.models.ProductModelKt.setPrice(this.tvProductPrice, dataOriginalPrice, java.lang.String.valueOf(0), dataIssDiscount);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): data
        flag 1 (0x2L): null
    flag mapping end*/
    //end
}