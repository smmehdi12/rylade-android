package com.senarios.customer.databinding;
import com.senarios.customer.R;
import com.senarios.customer.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentProfileBindingImpl extends FragmentProfileBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.toolbar, 5);
        sViewsWithIds.put(R.id.profile_textView, 6);
        sViewsWithIds.put(R.id.cross_img, 7);
        sViewsWithIds.put(R.id.constraintLayout2, 8);
        sViewsWithIds.put(R.id.textView3, 9);
        sViewsWithIds.put(R.id.first_name_txt, 10);
        sViewsWithIds.put(R.id.view, 11);
        sViewsWithIds.put(R.id.last_name_txt, 12);
        sViewsWithIds.put(R.id.view2, 13);
        sViewsWithIds.put(R.id.email_txt, 14);
        sViewsWithIds.put(R.id.view3, 15);
        sViewsWithIds.put(R.id.number_txt, 16);
        sViewsWithIds.put(R.id.view4, 17);
        sViewsWithIds.put(R.id.constraintLayout3, 18);
        sViewsWithIds.put(R.id.textView4, 19);
        sViewsWithIds.put(R.id.password_txt, 20);
        sViewsWithIds.put(R.id.change_password_edt, 21);
        sViewsWithIds.put(R.id.view5, 22);
        sViewsWithIds.put(R.id.save_button, 23);
        sViewsWithIds.put(R.id.guideline25, 24);
        sViewsWithIds.put(R.id.group, 25);
    }
    // views
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FragmentProfileBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 26, sIncludes, sViewsWithIds));
    }
    private FragmentProfileBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (android.widget.EditText) bindings[21]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[0]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[8]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[18]
            , (android.widget.ImageView) bindings[7]
            , (android.widget.EditText) bindings[3]
            , (android.widget.TextView) bindings[14]
            , (android.widget.EditText) bindings[1]
            , (android.widget.TextView) bindings[10]
            , (androidx.constraintlayout.widget.Group) bindings[25]
            , (androidx.constraintlayout.widget.Guideline) bindings[24]
            , (android.widget.EditText) bindings[2]
            , (android.widget.TextView) bindings[12]
            , (android.widget.EditText) bindings[4]
            , (android.widget.TextView) bindings[16]
            , (android.widget.TextView) bindings[20]
            , (android.widget.TextView) bindings[6]
            , (android.widget.Button) bindings[23]
            , (android.widget.TextView) bindings[9]
            , (android.widget.TextView) bindings[19]
            , (androidx.appcompat.widget.Toolbar) bindings[5]
            , (android.view.View) bindings[11]
            , (android.view.View) bindings[13]
            , (android.view.View) bindings[15]
            , (android.view.View) bindings[17]
            , (android.view.View) bindings[22]
            );
        this.constraintLayout.setTag(null);
        this.emailEdt.setTag(null);
        this.firstNameEdt.setTag(null);
        this.lastNameEdt.setTag(null);
        this.numberEdt.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.data == variableId) {
            setData((com.senarios.customer.models.User) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setData(@Nullable com.senarios.customer.models.User Data) {
        this.mData = Data;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.data);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String dataFName = null;
        java.lang.String dataEmail = null;
        com.senarios.customer.models.User data = mData;
        java.lang.String dataLName = null;
        java.lang.String dataPhoneNumber = null;

        if ((dirtyFlags & 0x3L) != 0) {



                if (data != null) {
                    // read data.f_name
                    dataFName = data.getF_name();
                    // read data.email
                    dataEmail = data.getEmail();
                    // read data.l_name
                    dataLName = data.getL_name();
                    // read data.phone_number
                    dataPhoneNumber = data.getPhone_number();
                }
        }
        // batch finished
        if ((dirtyFlags & 0x3L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.emailEdt, dataEmail);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.firstNameEdt, dataFName);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.lastNameEdt, dataLName);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.numberEdt, dataPhoneNumber);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): data
        flag 1 (0x2L): null
    flag mapping end*/
    //end
}