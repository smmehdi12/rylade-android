package com.senarios.customer;

public class BR {
  public static final int _all = 0;

  public static final int adaptor = 1;

  public static final int address = 2;

  public static final int addressID = 3;

  public static final int data = 4;

  public static final int deliveryFee = 5;

  public static final int model = 6;

  public static final int order = 7;

  public static final int orderModel = 8;

  public static final int product = 9;

  public static final int promo = 10;

  public static final int promoAmount = 11;

  public static final int promoID = 12;

  public static final int promoPercent = 13;

  public static final int promoTitle = 14;

  public static final int rideModel = 15;

  public static final int rider = 16;

  public static final int subTotal = 17;

  public static final int taxAmount = 18;

  public static final int totalAmount = 19;

  public static final int vendor = 20;

  public static final int vendorID = 21;
}
