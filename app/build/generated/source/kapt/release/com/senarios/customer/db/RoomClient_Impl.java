package com.senarios.customer.db;

import androidx.room.DatabaseConfiguration;
import androidx.room.InvalidationTracker;
import androidx.room.RoomOpenHelper;
import androidx.room.RoomOpenHelper.Delegate;
import androidx.room.RoomOpenHelper.ValidationResult;
import androidx.room.util.DBUtil;
import androidx.room.util.TableInfo;
import androidx.room.util.TableInfo.Column;
import androidx.room.util.TableInfo.ForeignKey;
import androidx.room.util.TableInfo.Index;
import androidx.sqlite.db.SupportSQLiteDatabase;
import androidx.sqlite.db.SupportSQLiteOpenHelper;
import androidx.sqlite.db.SupportSQLiteOpenHelper.Callback;
import androidx.sqlite.db.SupportSQLiteOpenHelper.Configuration;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

@SuppressWarnings({"unchecked", "deprecation"})
public final class RoomClient_Impl extends RoomClient {
  private volatile DAO _dAO;

  @Override
  protected SupportSQLiteOpenHelper createOpenHelper(DatabaseConfiguration configuration) {
    final SupportSQLiteOpenHelper.Callback _openCallback = new RoomOpenHelper(configuration, new RoomOpenHelper.Delegate(22) {
      @Override
      public void createAllTables(SupportSQLiteDatabase _db) {
        _db.execSQL("CREATE TABLE IF NOT EXISTS `MainCategory` (`created_at` TEXT DEFAULT '', `id` INTEGER, `name` TEXT, `updated_at` TEXT, `image_url` TEXT, PRIMARY KEY(`id`))");
        _db.execSQL("CREATE TABLE IF NOT EXISTS `Vendor` (`category_id` INTEGER, `created_at` TEXT, `email` TEXT, `email_verified_at` TEXT, `f_name` TEXT, `fcm` TEXT, `id` INTEGER, `image` TEXT, `is_verified` INTEGER, `l_name` TEXT, `login_facebook` INTEGER, `model` TEXT, `no_plate` TEXT, `otp` TEXT, `phone_number` TEXT, `rider_availability` TEXT, `rider_work_status` TEXT, `service_area_id` TEXT, `token` TEXT, `updated_at` TEXT, `user_status` INTEGER, `user_type` TEXT, `is_delivery` INTEGER NOT NULL, PRIMARY KEY(`id`))");
        _db.execSQL("CREATE TABLE IF NOT EXISTS `VendorCategory` (`category_name` TEXT, `created_at` TEXT, `id` INTEGER, `updated_at` TEXT, `vendor_id` INTEGER, PRIMARY KEY(`id`))");
        _db.execSQL("CREATE TABLE IF NOT EXISTS `Products` (`category_id` INTEGER, `category_name` TEXT, `created_at` TEXT, `description` TEXT, `id` INTEGER, `image` TEXT, `price` INTEGER, `product_name` TEXT COLLATE NOCASE, `promo_code_id` INTEGER, `quantity_available` INTEGER, `status` INTEGER, `sub_category_name` TEXT, `updated_at` TEXT, `weight` INTEGER, `vendor_id` INTEGER, `sales_tax` REAL NOT NULL, `fixed_discount` INTEGER, `iz_discount` INTEGER, `comment` TEXT DEFAULT '', `rating` TEXT DEFAULT '', PRIMARY KEY(`id`))");
        _db.execSQL("CREATE TABLE IF NOT EXISTS `Cart` (`id` INTEGER PRIMARY KEY AUTOINCREMENT, `category_id` INTEGER, `category_name` TEXT, `created_at` TEXT, `description` TEXT, `product_id` INTEGER, `image` TEXT, `price` INTEGER, `product_name` TEXT, `promo_code_id` INTEGER, `quantity_available` INTEGER, `status` INTEGER, `sub_category_name` TEXT, `updated_at` TEXT, `weight` INTEGER, `vendor_id` INTEGER, `vendor_name` TEXT, `quantity` INTEGER, `order_id` INTEGER, `sales_tax` REAL NOT NULL, `fixed_discount` INTEGER, `iss_discount` INTEGER, `originalPrice` INTEGER, `discounted_price` INTEGER, `isOutStock` INTEGER NOT NULL)");
        _db.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
        _db.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, '6deef35dce7aa306dcd72b445061ce15')");
      }

      @Override
      public void dropAllTables(SupportSQLiteDatabase _db) {
        _db.execSQL("DROP TABLE IF EXISTS `MainCategory`");
        _db.execSQL("DROP TABLE IF EXISTS `Vendor`");
        _db.execSQL("DROP TABLE IF EXISTS `VendorCategory`");
        _db.execSQL("DROP TABLE IF EXISTS `Products`");
        _db.execSQL("DROP TABLE IF EXISTS `Cart`");
        if (mCallbacks != null) {
          for (int _i = 0, _size = mCallbacks.size(); _i < _size; _i++) {
            mCallbacks.get(_i).onDestructiveMigration(_db);
          }
        }
      }

      @Override
      protected void onCreate(SupportSQLiteDatabase _db) {
        if (mCallbacks != null) {
          for (int _i = 0, _size = mCallbacks.size(); _i < _size; _i++) {
            mCallbacks.get(_i).onCreate(_db);
          }
        }
      }

      @Override
      public void onOpen(SupportSQLiteDatabase _db) {
        mDatabase = _db;
        internalInitInvalidationTracker(_db);
        if (mCallbacks != null) {
          for (int _i = 0, _size = mCallbacks.size(); _i < _size; _i++) {
            mCallbacks.get(_i).onOpen(_db);
          }
        }
      }

      @Override
      public void onPreMigrate(SupportSQLiteDatabase _db) {
        DBUtil.dropFtsSyncTriggers(_db);
      }

      @Override
      public void onPostMigrate(SupportSQLiteDatabase _db) {
      }

      @Override
      protected RoomOpenHelper.ValidationResult onValidateSchema(SupportSQLiteDatabase _db) {
        final HashMap<String, TableInfo.Column> _columnsMainCategory = new HashMap<String, TableInfo.Column>(5);
        _columnsMainCategory.put("created_at", new TableInfo.Column("created_at", "TEXT", false, 0, "''", TableInfo.CREATED_FROM_ENTITY));
        _columnsMainCategory.put("id", new TableInfo.Column("id", "INTEGER", false, 1, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsMainCategory.put("name", new TableInfo.Column("name", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsMainCategory.put("updated_at", new TableInfo.Column("updated_at", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsMainCategory.put("image_url", new TableInfo.Column("image_url", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        final HashSet<TableInfo.ForeignKey> _foreignKeysMainCategory = new HashSet<TableInfo.ForeignKey>(0);
        final HashSet<TableInfo.Index> _indicesMainCategory = new HashSet<TableInfo.Index>(0);
        final TableInfo _infoMainCategory = new TableInfo("MainCategory", _columnsMainCategory, _foreignKeysMainCategory, _indicesMainCategory);
        final TableInfo _existingMainCategory = TableInfo.read(_db, "MainCategory");
        if (! _infoMainCategory.equals(_existingMainCategory)) {
          return new RoomOpenHelper.ValidationResult(false, "MainCategory(com.senarios.customer.models.MainCategoryModel).\n"
                  + " Expected:\n" + _infoMainCategory + "\n"
                  + " Found:\n" + _existingMainCategory);
        }
        final HashMap<String, TableInfo.Column> _columnsVendor = new HashMap<String, TableInfo.Column>(23);
        _columnsVendor.put("category_id", new TableInfo.Column("category_id", "INTEGER", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsVendor.put("created_at", new TableInfo.Column("created_at", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsVendor.put("email", new TableInfo.Column("email", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsVendor.put("email_verified_at", new TableInfo.Column("email_verified_at", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsVendor.put("f_name", new TableInfo.Column("f_name", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsVendor.put("fcm", new TableInfo.Column("fcm", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsVendor.put("id", new TableInfo.Column("id", "INTEGER", false, 1, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsVendor.put("image", new TableInfo.Column("image", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsVendor.put("is_verified", new TableInfo.Column("is_verified", "INTEGER", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsVendor.put("l_name", new TableInfo.Column("l_name", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsVendor.put("login_facebook", new TableInfo.Column("login_facebook", "INTEGER", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsVendor.put("model", new TableInfo.Column("model", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsVendor.put("no_plate", new TableInfo.Column("no_plate", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsVendor.put("otp", new TableInfo.Column("otp", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsVendor.put("phone_number", new TableInfo.Column("phone_number", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsVendor.put("rider_availability", new TableInfo.Column("rider_availability", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsVendor.put("rider_work_status", new TableInfo.Column("rider_work_status", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsVendor.put("service_area_id", new TableInfo.Column("service_area_id", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsVendor.put("token", new TableInfo.Column("token", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsVendor.put("updated_at", new TableInfo.Column("updated_at", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsVendor.put("user_status", new TableInfo.Column("user_status", "INTEGER", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsVendor.put("user_type", new TableInfo.Column("user_type", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsVendor.put("is_delivery", new TableInfo.Column("is_delivery", "INTEGER", true, 0, null, TableInfo.CREATED_FROM_ENTITY));
        final HashSet<TableInfo.ForeignKey> _foreignKeysVendor = new HashSet<TableInfo.ForeignKey>(0);
        final HashSet<TableInfo.Index> _indicesVendor = new HashSet<TableInfo.Index>(0);
        final TableInfo _infoVendor = new TableInfo("Vendor", _columnsVendor, _foreignKeysVendor, _indicesVendor);
        final TableInfo _existingVendor = TableInfo.read(_db, "Vendor");
        if (! _infoVendor.equals(_existingVendor)) {
          return new RoomOpenHelper.ValidationResult(false, "Vendor(com.senarios.customer.models.Vendor).\n"
                  + " Expected:\n" + _infoVendor + "\n"
                  + " Found:\n" + _existingVendor);
        }
        final HashMap<String, TableInfo.Column> _columnsVendorCategory = new HashMap<String, TableInfo.Column>(5);
        _columnsVendorCategory.put("category_name", new TableInfo.Column("category_name", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsVendorCategory.put("created_at", new TableInfo.Column("created_at", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsVendorCategory.put("id", new TableInfo.Column("id", "INTEGER", false, 1, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsVendorCategory.put("updated_at", new TableInfo.Column("updated_at", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsVendorCategory.put("vendor_id", new TableInfo.Column("vendor_id", "INTEGER", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        final HashSet<TableInfo.ForeignKey> _foreignKeysVendorCategory = new HashSet<TableInfo.ForeignKey>(0);
        final HashSet<TableInfo.Index> _indicesVendorCategory = new HashSet<TableInfo.Index>(0);
        final TableInfo _infoVendorCategory = new TableInfo("VendorCategory", _columnsVendorCategory, _foreignKeysVendorCategory, _indicesVendorCategory);
        final TableInfo _existingVendorCategory = TableInfo.read(_db, "VendorCategory");
        if (! _infoVendorCategory.equals(_existingVendorCategory)) {
          return new RoomOpenHelper.ValidationResult(false, "VendorCategory(com.senarios.customer.models.VendorCategoryModel).\n"
                  + " Expected:\n" + _infoVendorCategory + "\n"
                  + " Found:\n" + _existingVendorCategory);
        }
        final HashMap<String, TableInfo.Column> _columnsProducts = new HashMap<String, TableInfo.Column>(20);
        _columnsProducts.put("category_id", new TableInfo.Column("category_id", "INTEGER", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsProducts.put("category_name", new TableInfo.Column("category_name", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsProducts.put("created_at", new TableInfo.Column("created_at", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsProducts.put("description", new TableInfo.Column("description", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsProducts.put("id", new TableInfo.Column("id", "INTEGER", false, 1, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsProducts.put("image", new TableInfo.Column("image", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsProducts.put("price", new TableInfo.Column("price", "INTEGER", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsProducts.put("product_name", new TableInfo.Column("product_name", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsProducts.put("promo_code_id", new TableInfo.Column("promo_code_id", "INTEGER", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsProducts.put("quantity_available", new TableInfo.Column("quantity_available", "INTEGER", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsProducts.put("status", new TableInfo.Column("status", "INTEGER", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsProducts.put("sub_category_name", new TableInfo.Column("sub_category_name", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsProducts.put("updated_at", new TableInfo.Column("updated_at", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsProducts.put("weight", new TableInfo.Column("weight", "INTEGER", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsProducts.put("vendor_id", new TableInfo.Column("vendor_id", "INTEGER", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsProducts.put("sales_tax", new TableInfo.Column("sales_tax", "REAL", true, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsProducts.put("fixed_discount", new TableInfo.Column("fixed_discount", "INTEGER", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsProducts.put("iz_discount", new TableInfo.Column("iz_discount", "INTEGER", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsProducts.put("comment", new TableInfo.Column("comment", "TEXT", false, 0, "''", TableInfo.CREATED_FROM_ENTITY));
        _columnsProducts.put("rating", new TableInfo.Column("rating", "TEXT", false, 0, "''", TableInfo.CREATED_FROM_ENTITY));
        final HashSet<TableInfo.ForeignKey> _foreignKeysProducts = new HashSet<TableInfo.ForeignKey>(0);
        final HashSet<TableInfo.Index> _indicesProducts = new HashSet<TableInfo.Index>(0);
        final TableInfo _infoProducts = new TableInfo("Products", _columnsProducts, _foreignKeysProducts, _indicesProducts);
        final TableInfo _existingProducts = TableInfo.read(_db, "Products");
        if (! _infoProducts.equals(_existingProducts)) {
          return new RoomOpenHelper.ValidationResult(false, "Products(com.senarios.customer.models.ProductModel).\n"
                  + " Expected:\n" + _infoProducts + "\n"
                  + " Found:\n" + _existingProducts);
        }
        final HashMap<String, TableInfo.Column> _columnsCart = new HashMap<String, TableInfo.Column>(25);
        _columnsCart.put("id", new TableInfo.Column("id", "INTEGER", false, 1, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsCart.put("category_id", new TableInfo.Column("category_id", "INTEGER", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsCart.put("category_name", new TableInfo.Column("category_name", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsCart.put("created_at", new TableInfo.Column("created_at", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsCart.put("description", new TableInfo.Column("description", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsCart.put("product_id", new TableInfo.Column("product_id", "INTEGER", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsCart.put("image", new TableInfo.Column("image", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsCart.put("price", new TableInfo.Column("price", "INTEGER", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsCart.put("product_name", new TableInfo.Column("product_name", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsCart.put("promo_code_id", new TableInfo.Column("promo_code_id", "INTEGER", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsCart.put("quantity_available", new TableInfo.Column("quantity_available", "INTEGER", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsCart.put("status", new TableInfo.Column("status", "INTEGER", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsCart.put("sub_category_name", new TableInfo.Column("sub_category_name", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsCart.put("updated_at", new TableInfo.Column("updated_at", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsCart.put("weight", new TableInfo.Column("weight", "INTEGER", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsCart.put("vendor_id", new TableInfo.Column("vendor_id", "INTEGER", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsCart.put("vendor_name", new TableInfo.Column("vendor_name", "TEXT", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsCart.put("quantity", new TableInfo.Column("quantity", "INTEGER", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsCart.put("order_id", new TableInfo.Column("order_id", "INTEGER", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsCart.put("sales_tax", new TableInfo.Column("sales_tax", "REAL", true, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsCart.put("fixed_discount", new TableInfo.Column("fixed_discount", "INTEGER", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsCart.put("iss_discount", new TableInfo.Column("iss_discount", "INTEGER", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsCart.put("originalPrice", new TableInfo.Column("originalPrice", "INTEGER", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsCart.put("discounted_price", new TableInfo.Column("discounted_price", "INTEGER", false, 0, null, TableInfo.CREATED_FROM_ENTITY));
        _columnsCart.put("isOutStock", new TableInfo.Column("isOutStock", "INTEGER", true, 0, null, TableInfo.CREATED_FROM_ENTITY));
        final HashSet<TableInfo.ForeignKey> _foreignKeysCart = new HashSet<TableInfo.ForeignKey>(0);
        final HashSet<TableInfo.Index> _indicesCart = new HashSet<TableInfo.Index>(0);
        final TableInfo _infoCart = new TableInfo("Cart", _columnsCart, _foreignKeysCart, _indicesCart);
        final TableInfo _existingCart = TableInfo.read(_db, "Cart");
        if (! _infoCart.equals(_existingCart)) {
          return new RoomOpenHelper.ValidationResult(false, "Cart(com.senarios.customer.models.CartModel).\n"
                  + " Expected:\n" + _infoCart + "\n"
                  + " Found:\n" + _existingCart);
        }
        return new RoomOpenHelper.ValidationResult(true, null);
      }
    }, "6deef35dce7aa306dcd72b445061ce15", "352cd31f4b83ce1d7afad8c7823cd25a");
    final SupportSQLiteOpenHelper.Configuration _sqliteConfig = SupportSQLiteOpenHelper.Configuration.builder(configuration.context)
        .name(configuration.name)
        .callback(_openCallback)
        .build();
    final SupportSQLiteOpenHelper _helper = configuration.sqliteOpenHelperFactory.create(_sqliteConfig);
    return _helper;
  }

  @Override
  protected InvalidationTracker createInvalidationTracker() {
    final HashMap<String, String> _shadowTablesMap = new HashMap<String, String>(0);
    HashMap<String, Set<String>> _viewTables = new HashMap<String, Set<String>>(0);
    return new InvalidationTracker(this, _shadowTablesMap, _viewTables, "MainCategory","Vendor","VendorCategory","Products","Cart");
  }

  @Override
  public void clearAllTables() {
    super.assertNotMainThread();
    final SupportSQLiteDatabase _db = super.getOpenHelper().getWritableDatabase();
    try {
      super.beginTransaction();
      _db.execSQL("DELETE FROM `MainCategory`");
      _db.execSQL("DELETE FROM `Vendor`");
      _db.execSQL("DELETE FROM `VendorCategory`");
      _db.execSQL("DELETE FROM `Products`");
      _db.execSQL("DELETE FROM `Cart`");
      super.setTransactionSuccessful();
    } finally {
      super.endTransaction();
      _db.query("PRAGMA wal_checkpoint(FULL)").close();
      if (!_db.inTransaction()) {
        _db.execSQL("VACUUM");
      }
    }
  }

  @Override
  public DAO dao() {
    if (_dAO != null) {
      return _dAO;
    } else {
      synchronized(this) {
        if(_dAO == null) {
          _dAO = new DAO_Impl(this);
        }
        return _dAO;
      }
    }
  }
}
