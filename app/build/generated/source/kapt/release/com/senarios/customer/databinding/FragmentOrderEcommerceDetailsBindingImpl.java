package com.senarios.customer.databinding;
import com.senarios.customer.R;
import com.senarios.customer.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentOrderEcommerceDetailsBindingImpl extends FragmentOrderEcommerceDetailsBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.toolbar, 11);
        sViewsWithIds.put(R.id.cvOrderInfo, 12);
        sViewsWithIds.put(R.id.recyclerView, 13);
        sViewsWithIds.put(R.id.cvAmount, 14);
        sViewsWithIds.put(R.id.cancel, 15);
    }
    // views
    @NonNull
    private final androidx.core.widget.NestedScrollView mboundView0;
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView1;
    @NonNull
    private final android.view.View mboundView7;
    @NonNull
    private final android.view.View mboundView9;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FragmentOrderEcommerceDetailsBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 16, sIncludes, sViewsWithIds));
    }
    private FragmentOrderEcommerceDetailsBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (android.widget.TextView) bindings[3]
            , (android.widget.Button) bindings[15]
            , (androidx.cardview.widget.CardView) bindings[14]
            , (androidx.cardview.widget.CardView) bindings[12]
            , (android.widget.TextView) bindings[2]
            , (androidx.recyclerview.widget.RecyclerView) bindings[13]
            , (android.view.View) bindings[11]
            , (android.widget.TextView) bindings[6]
            , (android.widget.TextView) bindings[8]
            , (android.widget.TextView) bindings[5]
            , (android.widget.TextView) bindings[10]
            , (android.widget.TextView) bindings[4]
            );
        this.DeliveryAddress.setTag(null);
        this.mboundView0 = (androidx.core.widget.NestedScrollView) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView1 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[1];
        this.mboundView1.setTag(null);
        this.mboundView7 = (android.view.View) bindings[7];
        this.mboundView7.setTag(null);
        this.mboundView9 = (android.view.View) bindings[9];
        this.mboundView9.setTag(null);
        this.orderID.setTag(null);
        this.tvDeliveryPrice.setTag(null);
        this.tvPromo.setTag(null);
        this.tvSubtotal.setTag(null);
        this.tvTotalPrice.setTag(null);
        this.vendorName.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x8L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.product == variableId) {
            setProduct((com.senarios.customer.models.CartModel) variable);
        }
        else if (BR.address == variableId) {
            setAddress((com.senarios.customer.models.Address) variable);
        }
        else if (BR.order == variableId) {
            setOrder((com.senarios.customer.models.OrderModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setProduct(@Nullable com.senarios.customer.models.CartModel Product) {
        this.mProduct = Product;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.product);
        super.requestRebind();
    }
    public void setAddress(@Nullable com.senarios.customer.models.Address Address) {
        this.mAddress = Address;
        synchronized(this) {
            mDirtyFlags |= 0x2L;
        }
        notifyPropertyChanged(BR.address);
        super.requestRebind();
    }
    public void setOrder(@Nullable com.senarios.customer.models.OrderModel Order) {
        this.mOrder = Order;
        synchronized(this) {
            mDirtyFlags |= 0x4L;
        }
        notifyPropertyChanged(BR.order);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String addressAddress = null;
        com.senarios.customer.models.CartModel product = mProduct;
        int orderId = 0;
        com.senarios.customer.models.Address address = mAddress;
        java.lang.String orderDeliveryFees = null;
        java.lang.String javaLangStringTotalAmountRsOrderAmount = null;
        java.lang.String orderPromoAmount = null;
        java.lang.String javaLangStringVendorProductVendorName = null;
        java.lang.String orderAmount = null;
        java.lang.String javaLangStringAddressAddressAddress = null;
        java.lang.String productVendorName = null;
        java.lang.String orderSubTotal = null;
        com.senarios.customer.models.OrderModel order = mOrder;
        java.lang.String javaLangStringOrderIDOrderId = null;
        java.lang.String javaLangStringSubtotalRsOrderSubTotal = null;

        if ((dirtyFlags & 0x9L) != 0) {



                if (product != null) {
                    // read product.vendor_name
                    productVendorName = product.getVendor_name();
                }


                // read ("Vendor : ") + (product.vendor_name)
                javaLangStringVendorProductVendorName = ("Vendor : ") + (productVendorName);
        }
        if ((dirtyFlags & 0xaL) != 0) {



                if (address != null) {
                    // read address.address
                    addressAddress = address.getAddress();
                }


                // read ("Address : ") + (address.address)
                javaLangStringAddressAddressAddress = ("Address : ") + (addressAddress);
        }
        if ((dirtyFlags & 0xcL) != 0) {



                if (order != null) {
                    // read order.id
                    orderId = order.getId();
                    // read order.deliveryFees
                    orderDeliveryFees = order.getDeliveryFees();
                    // read order.promoAmount
                    orderPromoAmount = order.getPromoAmount();
                    // read order.amount
                    orderAmount = order.getAmount();
                    // read order.subTotal
                    orderSubTotal = order.getSubTotal();
                }


                // read ("OrderID : ") + (order.id)
                javaLangStringOrderIDOrderId = ("OrderID : ") + (orderId);
                // read ("Total Amount : Rs. ") + (order.amount)
                javaLangStringTotalAmountRsOrderAmount = ("Total Amount : Rs. ") + (orderAmount);
                // read ("Subtotal : Rs. ") + (order.subTotal)
                javaLangStringSubtotalRsOrderSubTotal = ("Subtotal : Rs. ") + (orderSubTotal);
        }
        // batch finished
        if ((dirtyFlags & 0xaL) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.DeliveryAddress, javaLangStringAddressAddressAddress);
        }
        if ((dirtyFlags & 0xcL) != 0) {
            // api target 1

            com.senarios.customer.models.OrderModelKt.setView(this.mboundView7, orderDeliveryFees);
            com.senarios.customer.models.OrderModelKt.setView(this.mboundView9, orderPromoAmount);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.orderID, javaLangStringOrderIDOrderId);
            com.senarios.customer.models.OrderModelKt.setDeliveryFee(this.tvDeliveryPrice, orderDeliveryFees);
            com.senarios.customer.models.OrderModelKt.setPromoAmount(this.tvPromo, orderPromoAmount);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvSubtotal, javaLangStringSubtotalRsOrderSubTotal);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvTotalPrice, javaLangStringTotalAmountRsOrderAmount);
        }
        if ((dirtyFlags & 0x9L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.vendorName, javaLangStringVendorProductVendorName);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): product
        flag 1 (0x2L): address
        flag 2 (0x3L): order
        flag 3 (0x4L): null
    flag mapping end*/
    //end
}