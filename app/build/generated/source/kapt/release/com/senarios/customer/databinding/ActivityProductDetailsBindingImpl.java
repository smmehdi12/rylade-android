package com.senarios.customer.databinding;
import com.senarios.customer.R;
import com.senarios.customer.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ActivityProductDetailsBindingImpl extends ActivityProductDetailsBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.toolbar, 6);
        sViewsWithIds.put(R.id.guideline, 7);
        sViewsWithIds.put(R.id.cart_group, 8);
        sViewsWithIds.put(R.id.tv_rating, 9);
        sViewsWithIds.put(R.id.rating, 10);
        sViewsWithIds.put(R.id.btn_minus, 11);
        sViewsWithIds.put(R.id.tv_counter, 12);
        sViewsWithIds.put(R.id.btn_add, 13);
        sViewsWithIds.put(R.id.btn_addToCart, 14);
    }
    // views
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView0;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ActivityProductDetailsBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 15, sIncludes, sViewsWithIds));
    }
    private ActivityProductDetailsBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (android.widget.ImageView) bindings[13]
            , (android.widget.Button) bindings[14]
            , (android.widget.ImageView) bindings[11]
            , (androidx.constraintlayout.widget.Group) bindings[8]
            , (androidx.constraintlayout.widget.Guideline) bindings[7]
            , (android.widget.ImageView) bindings[1]
            , (android.widget.TextView) bindings[10]
            , (android.view.View) bindings[6]
            , (android.widget.TextView) bindings[12]
            , (android.widget.TextView) bindings[5]
            , (android.widget.TextView) bindings[2]
            , (android.widget.TextView) bindings[4]
            , (android.widget.TextView) bindings[9]
            , (android.widget.TextView) bindings[3]
            );
        this.headerImage.setTag(null);
        this.mboundView0 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.tvDPrice.setTag(null);
        this.tvDescription.setTag(null);
        this.tvPrice.setTag(null);
        this.tvTitle.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.data == variableId) {
            setData((com.senarios.customer.models.ProductModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setData(@Nullable com.senarios.customer.models.ProductModel Data) {
        this.mData = Data;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.data);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.Integer dataIzDiscount = null;
        java.lang.Integer dataFixedDiscount = null;
        java.lang.String dataProductName = null;
        com.senarios.customer.models.ProductModel data = mData;
        java.lang.Integer dataPrice = null;
        java.lang.String dataImage = null;
        java.lang.String dataDescription = null;

        if ((dirtyFlags & 0x3L) != 0) {



                if (data != null) {
                    // read data.iz_discount
                    dataIzDiscount = data.getIz_discount();
                    // read data.fixed_discount
                    dataFixedDiscount = data.getFixed_discount();
                    // read data.productName
                    dataProductName = data.getProductName();
                    // read data.price
                    dataPrice = data.getPrice();
                    // read data.image
                    dataImage = data.getImage();
                    // read data.description
                    dataDescription = data.getDescription();
                }
        }
        // batch finished
        if ((dirtyFlags & 0x3L) != 0) {
            // api target 1

            com.senarios.customer.models.ProductModelKt.loadProductImageView(this.headerImage, dataImage);
            com.senarios.customer.models.ProductModelKt.setPrice(this.tvDPrice, dataFixedDiscount, java.lang.String.valueOf(1), dataIzDiscount);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvDescription, dataDescription);
            com.senarios.customer.models.ProductModelKt.setPrice(this.tvPrice, dataPrice, java.lang.String.valueOf(0), dataIzDiscount);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvTitle, dataProductName);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): data
        flag 1 (0x2L): null
    flag mapping end*/
    //end
}