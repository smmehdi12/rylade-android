package com.senarios.customer.databinding;
import com.senarios.customer.R;
import com.senarios.customer.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentRatingDialogBindingImpl extends FragmentRatingDialogBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.root, 1);
        sViewsWithIds.put(R.id.progress_bar, 2);
        sViewsWithIds.put(R.id.back_imageView, 3);
        sViewsWithIds.put(R.id.signup_textView, 4);
        sViewsWithIds.put(R.id.divider_horizontal_imageView2, 5);
        sViewsWithIds.put(R.id.rider_text, 6);
        sViewsWithIds.put(R.id.listitemrating_rider, 7);
        sViewsWithIds.put(R.id.listitemrating_products, 8);
        sViewsWithIds.put(R.id.rating_comment, 9);
        sViewsWithIds.put(R.id.text_products, 10);
        sViewsWithIds.put(R.id.recyclerview, 11);
        sViewsWithIds.put(R.id.rating_done_button, 12);
        sViewsWithIds.put(R.id.space, 13);
        sViewsWithIds.put(R.id.group, 14);
    }
    // views
    @NonNull
    private final androidx.core.widget.NestedScrollView mboundView0;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FragmentRatingDialogBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 15, sIncludes, sViewsWithIds));
    }
    private FragmentRatingDialogBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (android.widget.ImageView) bindings[3]
            , (android.widget.ImageView) bindings[5]
            , (androidx.constraintlayout.widget.Group) bindings[14]
            , (android.widget.RatingBar) bindings[8]
            , (android.widget.RatingBar) bindings[7]
            , (android.widget.ProgressBar) bindings[2]
            , (android.widget.EditText) bindings[9]
            , (android.widget.Button) bindings[12]
            , (androidx.recyclerview.widget.RecyclerView) bindings[11]
            , (android.widget.TextView) bindings[6]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[1]
            , (android.widget.TextView) bindings[4]
            , (android.view.View) bindings[13]
            , (android.widget.TextView) bindings[10]
            );
        this.mboundView0 = (androidx.core.widget.NestedScrollView) bindings[0];
        this.mboundView0.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x1L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
            return variableSet;
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        // batch finished
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): null
    flag mapping end*/
    //end
}