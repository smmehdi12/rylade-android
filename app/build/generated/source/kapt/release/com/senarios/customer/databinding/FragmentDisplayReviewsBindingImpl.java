package com.senarios.customer.databinding;
import com.senarios.customer.R;
import com.senarios.customer.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentDisplayReviewsBindingImpl extends FragmentDisplayReviewsBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.toolbar, 3);
        sViewsWithIds.put(R.id.review_address, 4);
        sViewsWithIds.put(R.id.review_average, 5);
        sViewsWithIds.put(R.id.recyclerView, 6);
        sViewsWithIds.put(R.id.guideline21, 7);
    }
    // views
    @NonNull
    private final androidx.constraintlayout.widget.ConstraintLayout mboundView0;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FragmentDisplayReviewsBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 8, sIncludes, sViewsWithIds));
    }
    private FragmentDisplayReviewsBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (androidx.constraintlayout.widget.Guideline) bindings[7]
            , (de.hdodenhof.circleimageview.CircleImageView) bindings[1]
            , (androidx.recyclerview.widget.RecyclerView) bindings[6]
            , (android.widget.TextView) bindings[4]
            , (android.widget.TextView) bindings[5]
            , (android.widget.TextView) bindings[2]
            , (androidx.appcompat.widget.Toolbar) bindings[3]
            );
        this.imageViewReview.setTag(null);
        this.mboundView0 = (androidx.constraintlayout.widget.ConstraintLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.reviewUserName.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.vendor == variableId) {
            setVendor((com.senarios.customer.models.Vendor) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setVendor(@Nullable com.senarios.customer.models.Vendor Vendor) {
        this.mVendor = Vendor;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.vendor);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String vendorLName = null;
        java.lang.String javaLangStringVendorNameVendorFNameVendorLName = null;
        java.lang.String vendorImage = null;
        com.senarios.customer.models.Vendor vendor = mVendor;
        java.lang.String javaLangStringVendorNameVendorFName = null;
        java.lang.String vendorFName = null;

        if ((dirtyFlags & 0x3L) != 0) {



                if (vendor != null) {
                    // read vendor.LName
                    vendorLName = vendor.getLName();
                    // read vendor.image
                    vendorImage = vendor.getImage();
                    // read vendor.FName
                    vendorFName = vendor.getFName();
                }


                // read ("Vendor Name : ") + (vendor.FName)
                javaLangStringVendorNameVendorFName = ("Vendor Name : ") + (vendorFName);


                // read (("Vendor Name : ") + (vendor.FName)) + (vendor.LName)
                javaLangStringVendorNameVendorFNameVendorLName = (javaLangStringVendorNameVendorFName) + (vendorLName);
        }
        // batch finished
        if ((dirtyFlags & 0x3L) != 0) {
            // api target 1

            com.senarios.customer.models.VendorKt.loadImage(this.imageViewReview, vendorImage);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.reviewUserName, javaLangStringVendorNameVendorFNameVendorLName);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): vendor
        flag 1 (0x2L): null
    flag mapping end*/
    //end
}