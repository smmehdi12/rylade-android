package com.senarios.customer.databinding;
import com.senarios.customer.R;
import com.senarios.customer.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ItemOrderEcommerceBindingImpl extends ItemOrderEcommerceBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = null;
    }
    // views
    @NonNull
    private final androidx.cardview.widget.CardView mboundView0;
    @NonNull
    private final android.widget.TextView mboundView4;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ItemOrderEcommerceBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 5, sIncludes, sViewsWithIds));
    }
    private ItemOrderEcommerceBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (de.hdodenhof.circleimageview.CircleImageView) bindings[1]
            , (android.widget.TextView) bindings[3]
            , (android.widget.TextView) bindings[2]
            );
        this.imageview.setTag(null);
        this.mboundView0 = (androidx.cardview.widget.CardView) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView4 = (android.widget.TextView) bindings[4];
        this.mboundView4.setTag(null);
        this.price.setTag(null);
        this.tvProductNameQuantity.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.data == variableId) {
            setData((com.senarios.customer.models.CartModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setData(@Nullable com.senarios.customer.models.CartModel Data) {
        this.mData = Data;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.data);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String dataImage = null;
        java.lang.String dataQuantityJavaLangStringXDataProductName = null;
        java.lang.String dataQuantityJavaLangStringX = null;
        java.lang.Integer dataIssDiscount = null;
        java.lang.Integer dataQuantity = null;
        java.lang.String dataProductName = null;
        java.lang.Integer dataDiscountedPrice = null;
        com.senarios.customer.models.CartModel data = mData;
        java.lang.Integer dataPrice = null;

        if ((dirtyFlags & 0x3L) != 0) {



                if (data != null) {
                    // read data.image
                    dataImage = data.getImage();
                    // read data.iss_discount
                    dataIssDiscount = data.getIss_discount();
                    // read data.quantity
                    dataQuantity = data.getQuantity();
                    // read data.productName
                    dataProductName = data.getProductName();
                    // read data.discounted_price
                    dataDiscountedPrice = data.getDiscounted_price();
                    // read data.price
                    dataPrice = data.getPrice();
                }


                // read (data.quantity) + (" x ")
                dataQuantityJavaLangStringX = (dataQuantity) + (" x ");


                // read ((data.quantity) + (" x ")) + (data.productName)
                dataQuantityJavaLangStringXDataProductName = (dataQuantityJavaLangStringX) + (dataProductName);
        }
        // batch finished
        if ((dirtyFlags & 0x3L) != 0) {
            // api target 1

            com.senarios.customer.models.ProductModelKt.loadProductImage(this.imageview, dataImage);
            com.senarios.customer.models.ProductModelKt.setPrice(this.mboundView4, dataDiscountedPrice, java.lang.String.valueOf(1), dataIssDiscount);
            com.senarios.customer.models.ProductModelKt.setPrice(this.price, dataPrice, java.lang.String.valueOf(0), dataIssDiscount);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvProductNameQuantity, dataQuantityJavaLangStringXDataProductName);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): data
        flag 1 (0x2L): null
    flag mapping end*/
    //end
}