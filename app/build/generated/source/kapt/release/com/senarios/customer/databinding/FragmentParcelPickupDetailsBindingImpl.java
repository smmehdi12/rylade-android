package com.senarios.customer.databinding;
import com.senarios.customer.R;
import com.senarios.customer.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentParcelPickupDetailsBindingImpl extends FragmentParcelPickupDetailsBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.constraintLayout, 1);
        sViewsWithIds.put(R.id.toolbar, 2);
        sViewsWithIds.put(R.id.picture_img, 3);
        sViewsWithIds.put(R.id.take_snap_prescription_btn, 4);
        sViewsWithIds.put(R.id.imageView5, 5);
        sViewsWithIds.put(R.id.snap_prescription_txt, 6);
        sViewsWithIds.put(R.id.name_edt, 7);
        sViewsWithIds.put(R.id.weight_edt, 8);
        sViewsWithIds.put(R.id.senders_contact, 9);
        sViewsWithIds.put(R.id.senders_name, 10);
        sViewsWithIds.put(R.id.receivers_contact, 11);
        sViewsWithIds.put(R.id.receivers_name, 12);
        sViewsWithIds.put(R.id.confirm_btn, 13);
        sViewsWithIds.put(R.id.senders_phonebook, 14);
        sViewsWithIds.put(R.id.receivers_phonebook, 15);
        sViewsWithIds.put(R.id.sender_address, 16);
        sViewsWithIds.put(R.id.map_sender_address, 17);
        sViewsWithIds.put(R.id.receivers_address, 18);
        sViewsWithIds.put(R.id.map_receivers_address, 19);
        sViewsWithIds.put(R.id.isInstant, 20);
    }
    // views
    @NonNull
    private final androidx.core.widget.NestedScrollView mboundView0;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FragmentParcelPickupDetailsBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 21, sIncludes, sViewsWithIds));
    }
    private FragmentParcelPickupDetailsBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (android.widget.Button) bindings[13]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[1]
            , (android.widget.ImageView) bindings[5]
            , (android.widget.Switch) bindings[20]
            , (android.widget.EditText) bindings[19]
            , (android.widget.EditText) bindings[17]
            , (android.widget.EditText) bindings[7]
            , (de.hdodenhof.circleimageview.CircleImageView) bindings[3]
            , (android.widget.EditText) bindings[18]
            , (android.widget.EditText) bindings[11]
            , (android.widget.EditText) bindings[12]
            , (android.widget.ImageView) bindings[15]
            , (android.widget.EditText) bindings[16]
            , (android.widget.EditText) bindings[9]
            , (android.widget.EditText) bindings[10]
            , (android.widget.ImageView) bindings[14]
            , (android.widget.TextView) bindings[6]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[4]
            , (android.widget.Toolbar) bindings[2]
            , (android.widget.EditText) bindings[8]
            );
        this.mboundView0 = (androidx.core.widget.NestedScrollView) bindings[0];
        this.mboundView0.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x1L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
            return variableSet;
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        // batch finished
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): null
    flag mapping end*/
    //end
}