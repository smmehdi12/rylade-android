package com.senarios.customer;

import android.util.SparseArray;
import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.DataBinderMapper;
import androidx.databinding.DataBindingComponent;
import androidx.databinding.ViewDataBinding;
import com.senarios.customer.databinding.ActivityCartBindingImpl;
import com.senarios.customer.databinding.ActivityEcommerceBindingImpl;
import com.senarios.customer.databinding.ActivityFaqBindingImpl;
import com.senarios.customer.databinding.ActivityMainBindingImpl;
import com.senarios.customer.databinding.ActivityProductDetailsBindingImpl;
import com.senarios.customer.databinding.ActivitySearchBindingImpl;
import com.senarios.customer.databinding.ActivitySplashBindingImpl;
import com.senarios.customer.databinding.ActivityTermsConditionsBindingImpl;
import com.senarios.customer.databinding.ActivityVendorProductsBindingImpl;
import com.senarios.customer.databinding.FragmentAddNewAddressBindingImpl;
import com.senarios.customer.databinding.FragmentAddressesBindingImpl;
import com.senarios.customer.databinding.FragmentDisplayReviewsBindingImpl;
import com.senarios.customer.databinding.FragmentEditPasswordBindingImpl;
import com.senarios.customer.databinding.FragmentForgetPasswordBindingImpl;
import com.senarios.customer.databinding.FragmentHomeBindingImpl;
import com.senarios.customer.databinding.FragmentLoginBindingImpl;
import com.senarios.customer.databinding.FragmentLoginSignupBindingImpl;
import com.senarios.customer.databinding.FragmentMapBindingImpl;
import com.senarios.customer.databinding.FragmentMedicineBindingImpl;
import com.senarios.customer.databinding.FragmentOrderEcommerceDetailsBindingImpl;
import com.senarios.customer.databinding.FragmentOrderMedicineDetailsBindingImpl;
import com.senarios.customer.databinding.FragmentOrderParcelDetailsBindingImpl;
import com.senarios.customer.databinding.FragmentOrderRideDetailsBindingImpl;
import com.senarios.customer.databinding.FragmentOrdersBindingImpl;
import com.senarios.customer.databinding.FragmentOtpBindingImpl;
import com.senarios.customer.databinding.FragmentParcelPickupDetailsBindingImpl;
import com.senarios.customer.databinding.FragmentPasswordChangeBindingImpl;
import com.senarios.customer.databinding.FragmentProfileBindingImpl;
import com.senarios.customer.databinding.FragmentPromoCodeBindingImpl;
import com.senarios.customer.databinding.FragmentRatingDialogBindingImpl;
import com.senarios.customer.databinding.FragmentRequestRideBindingImpl;
import com.senarios.customer.databinding.FragmentSignupBindingImpl;
import com.senarios.customer.databinding.FragmentVendorProductsBindingImpl;
import com.senarios.customer.databinding.FragmentVendorsBindingImpl;
import com.senarios.customer.databinding.FragmentVerifyEmailPhoneBindingImpl;
import com.senarios.customer.databinding.ItemAddressesBindingImpl;
import com.senarios.customer.databinding.ItemCartItemBindingImpl;
import com.senarios.customer.databinding.ItemFaqBindingImpl;
import com.senarios.customer.databinding.ItemMainActivityBindingImpl;
import com.senarios.customer.databinding.ItemMedicineViewBindingImpl;
import com.senarios.customer.databinding.ItemOrderEcommerceBindingImpl;
import com.senarios.customer.databinding.ItemOrderMedicineBindingImpl;
import com.senarios.customer.databinding.ItemOrdersBindingImpl;
import com.senarios.customer.databinding.ItemPlacesBindingImpl;
import com.senarios.customer.databinding.ItemSearchProductBindingImpl;
import com.senarios.customer.databinding.ItemVendorBindingImpl;
import com.senarios.customer.databinding.ItemVendorProductsBindingImpl;
import com.senarios.customer.databinding.ItemsProductReviewBindingImpl;
import com.senarios.customer.databinding.ItemsReviewBindingImpl;
import com.senarios.customer.databinding.LayoutCartIconBindingImpl;
import com.senarios.customer.databinding.ProductcategoriesbyidBindingImpl;
import com.senarios.customer.databinding.RyladeLoadingDialogeBindingImpl;
import com.senarios.customer.databinding.VendorproductcategorieslistBindingImpl;
import com.senarios.customer.databinding.VendorproductslistBindingImpl;
import java.lang.IllegalArgumentException;
import java.lang.Integer;
import java.lang.Object;
import java.lang.Override;
import java.lang.RuntimeException;
import java.lang.String;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DataBinderMapperImpl extends DataBinderMapper {
  private static final int LAYOUT_ACTIVITYCART = 1;

  private static final int LAYOUT_ACTIVITYECOMMERCE = 2;

  private static final int LAYOUT_ACTIVITYFAQ = 3;

  private static final int LAYOUT_ACTIVITYMAIN = 4;

  private static final int LAYOUT_ACTIVITYPRODUCTDETAILS = 5;

  private static final int LAYOUT_ACTIVITYSEARCH = 6;

  private static final int LAYOUT_ACTIVITYSPLASH = 7;

  private static final int LAYOUT_ACTIVITYTERMSCONDITIONS = 8;

  private static final int LAYOUT_ACTIVITYVENDORPRODUCTS = 9;

  private static final int LAYOUT_FRAGMENTADDNEWADDRESS = 10;

  private static final int LAYOUT_FRAGMENTADDRESSES = 11;

  private static final int LAYOUT_FRAGMENTDISPLAYREVIEWS = 12;

  private static final int LAYOUT_FRAGMENTEDITPASSWORD = 13;

  private static final int LAYOUT_FRAGMENTFORGETPASSWORD = 14;

  private static final int LAYOUT_FRAGMENTHOME = 15;

  private static final int LAYOUT_FRAGMENTLOGIN = 16;

  private static final int LAYOUT_FRAGMENTLOGINSIGNUP = 17;

  private static final int LAYOUT_FRAGMENTMAP = 18;

  private static final int LAYOUT_FRAGMENTMEDICINE = 19;

  private static final int LAYOUT_FRAGMENTORDERECOMMERCEDETAILS = 20;

  private static final int LAYOUT_FRAGMENTORDERMEDICINEDETAILS = 21;

  private static final int LAYOUT_FRAGMENTORDERPARCELDETAILS = 22;

  private static final int LAYOUT_FRAGMENTORDERRIDEDETAILS = 23;

  private static final int LAYOUT_FRAGMENTORDERS = 24;

  private static final int LAYOUT_FRAGMENTOTP = 25;

  private static final int LAYOUT_FRAGMENTPARCELPICKUPDETAILS = 26;

  private static final int LAYOUT_FRAGMENTPASSWORDCHANGE = 27;

  private static final int LAYOUT_FRAGMENTPROFILE = 28;

  private static final int LAYOUT_FRAGMENTPROMOCODE = 29;

  private static final int LAYOUT_FRAGMENTRATINGDIALOG = 30;

  private static final int LAYOUT_FRAGMENTREQUESTRIDE = 31;

  private static final int LAYOUT_FRAGMENTSIGNUP = 32;

  private static final int LAYOUT_FRAGMENTVENDORPRODUCTS = 33;

  private static final int LAYOUT_FRAGMENTVENDORS = 34;

  private static final int LAYOUT_FRAGMENTVERIFYEMAILPHONE = 35;

  private static final int LAYOUT_ITEMADDRESSES = 36;

  private static final int LAYOUT_ITEMCARTITEM = 37;

  private static final int LAYOUT_ITEMFAQ = 38;

  private static final int LAYOUT_ITEMMAINACTIVITY = 39;

  private static final int LAYOUT_ITEMMEDICINEVIEW = 40;

  private static final int LAYOUT_ITEMORDERECOMMERCE = 41;

  private static final int LAYOUT_ITEMORDERMEDICINE = 42;

  private static final int LAYOUT_ITEMORDERS = 43;

  private static final int LAYOUT_ITEMPLACES = 44;

  private static final int LAYOUT_ITEMSEARCHPRODUCT = 45;

  private static final int LAYOUT_ITEMVENDOR = 46;

  private static final int LAYOUT_ITEMVENDORPRODUCTS = 47;

  private static final int LAYOUT_ITEMSPRODUCTREVIEW = 48;

  private static final int LAYOUT_ITEMSREVIEW = 49;

  private static final int LAYOUT_LAYOUTCARTICON = 50;

  private static final int LAYOUT_PRODUCTCATEGORIESBYID = 51;

  private static final int LAYOUT_RYLADELOADINGDIALOGE = 52;

  private static final int LAYOUT_VENDORPRODUCTCATEGORIESLIST = 53;

  private static final int LAYOUT_VENDORPRODUCTSLIST = 54;

  private static final SparseIntArray INTERNAL_LAYOUT_ID_LOOKUP = new SparseIntArray(54);

  static {
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.senarios.customer.R.layout.activity_cart, LAYOUT_ACTIVITYCART);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.senarios.customer.R.layout.activity_ecommerce, LAYOUT_ACTIVITYECOMMERCE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.senarios.customer.R.layout.activity_faq, LAYOUT_ACTIVITYFAQ);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.senarios.customer.R.layout.activity_main, LAYOUT_ACTIVITYMAIN);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.senarios.customer.R.layout.activity_product_details, LAYOUT_ACTIVITYPRODUCTDETAILS);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.senarios.customer.R.layout.activity_search, LAYOUT_ACTIVITYSEARCH);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.senarios.customer.R.layout.activity_splash, LAYOUT_ACTIVITYSPLASH);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.senarios.customer.R.layout.activity_terms_conditions, LAYOUT_ACTIVITYTERMSCONDITIONS);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.senarios.customer.R.layout.activity_vendor_products, LAYOUT_ACTIVITYVENDORPRODUCTS);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.senarios.customer.R.layout.fragment_add_new_address, LAYOUT_FRAGMENTADDNEWADDRESS);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.senarios.customer.R.layout.fragment_addresses, LAYOUT_FRAGMENTADDRESSES);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.senarios.customer.R.layout.fragment_display_reviews, LAYOUT_FRAGMENTDISPLAYREVIEWS);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.senarios.customer.R.layout.fragment_edit_password, LAYOUT_FRAGMENTEDITPASSWORD);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.senarios.customer.R.layout.fragment_forget_password, LAYOUT_FRAGMENTFORGETPASSWORD);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.senarios.customer.R.layout.fragment_home, LAYOUT_FRAGMENTHOME);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.senarios.customer.R.layout.fragment_login, LAYOUT_FRAGMENTLOGIN);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.senarios.customer.R.layout.fragment_login_signup, LAYOUT_FRAGMENTLOGINSIGNUP);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.senarios.customer.R.layout.fragment_map, LAYOUT_FRAGMENTMAP);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.senarios.customer.R.layout.fragment_medicine, LAYOUT_FRAGMENTMEDICINE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.senarios.customer.R.layout.fragment_order_ecommerce_details, LAYOUT_FRAGMENTORDERECOMMERCEDETAILS);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.senarios.customer.R.layout.fragment_order_medicine_details, LAYOUT_FRAGMENTORDERMEDICINEDETAILS);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.senarios.customer.R.layout.fragment_order_parcel_details, LAYOUT_FRAGMENTORDERPARCELDETAILS);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.senarios.customer.R.layout.fragment_order_ride_details, LAYOUT_FRAGMENTORDERRIDEDETAILS);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.senarios.customer.R.layout.fragment_orders, LAYOUT_FRAGMENTORDERS);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.senarios.customer.R.layout.fragment_otp, LAYOUT_FRAGMENTOTP);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.senarios.customer.R.layout.fragment_parcel_pickup_details, LAYOUT_FRAGMENTPARCELPICKUPDETAILS);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.senarios.customer.R.layout.fragment_password_change, LAYOUT_FRAGMENTPASSWORDCHANGE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.senarios.customer.R.layout.fragment_profile, LAYOUT_FRAGMENTPROFILE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.senarios.customer.R.layout.fragment_promo_code, LAYOUT_FRAGMENTPROMOCODE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.senarios.customer.R.layout.fragment_rating_dialog, LAYOUT_FRAGMENTRATINGDIALOG);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.senarios.customer.R.layout.fragment_request_ride, LAYOUT_FRAGMENTREQUESTRIDE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.senarios.customer.R.layout.fragment_signup, LAYOUT_FRAGMENTSIGNUP);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.senarios.customer.R.layout.fragment_vendor_products, LAYOUT_FRAGMENTVENDORPRODUCTS);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.senarios.customer.R.layout.fragment_vendors, LAYOUT_FRAGMENTVENDORS);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.senarios.customer.R.layout.fragment_verify_email_phone, LAYOUT_FRAGMENTVERIFYEMAILPHONE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.senarios.customer.R.layout.item_addresses, LAYOUT_ITEMADDRESSES);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.senarios.customer.R.layout.item_cart_item, LAYOUT_ITEMCARTITEM);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.senarios.customer.R.layout.item_faq, LAYOUT_ITEMFAQ);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.senarios.customer.R.layout.item_main_activity, LAYOUT_ITEMMAINACTIVITY);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.senarios.customer.R.layout.item_medicine_view, LAYOUT_ITEMMEDICINEVIEW);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.senarios.customer.R.layout.item_order_ecommerce, LAYOUT_ITEMORDERECOMMERCE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.senarios.customer.R.layout.item_order_medicine, LAYOUT_ITEMORDERMEDICINE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.senarios.customer.R.layout.item_orders, LAYOUT_ITEMORDERS);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.senarios.customer.R.layout.item_places, LAYOUT_ITEMPLACES);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.senarios.customer.R.layout.item_search_product, LAYOUT_ITEMSEARCHPRODUCT);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.senarios.customer.R.layout.item_vendor, LAYOUT_ITEMVENDOR);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.senarios.customer.R.layout.item_vendor_products, LAYOUT_ITEMVENDORPRODUCTS);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.senarios.customer.R.layout.items_product_review, LAYOUT_ITEMSPRODUCTREVIEW);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.senarios.customer.R.layout.items_review, LAYOUT_ITEMSREVIEW);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.senarios.customer.R.layout.layout_cart_icon, LAYOUT_LAYOUTCARTICON);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.senarios.customer.R.layout.productcategoriesbyid, LAYOUT_PRODUCTCATEGORIESBYID);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.senarios.customer.R.layout.rylade_loading_dialoge, LAYOUT_RYLADELOADINGDIALOGE);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.senarios.customer.R.layout.vendorproductcategorieslist, LAYOUT_VENDORPRODUCTCATEGORIESLIST);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.senarios.customer.R.layout.vendorproductslist, LAYOUT_VENDORPRODUCTSLIST);
  }

  private final ViewDataBinding internalGetViewDataBinding0(DataBindingComponent component,
      View view, int internalId, Object tag) {
    switch(internalId) {
      case  LAYOUT_ACTIVITYCART: {
        if ("layout/activity_cart_0".equals(tag)) {
          return new ActivityCartBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for activity_cart is invalid. Received: " + tag);
      }
      case  LAYOUT_ACTIVITYECOMMERCE: {
        if ("layout/activity_ecommerce_0".equals(tag)) {
          return new ActivityEcommerceBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for activity_ecommerce is invalid. Received: " + tag);
      }
      case  LAYOUT_ACTIVITYFAQ: {
        if ("layout/activity_faq_0".equals(tag)) {
          return new ActivityFaqBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for activity_faq is invalid. Received: " + tag);
      }
      case  LAYOUT_ACTIVITYMAIN: {
        if ("layout/activity_main_0".equals(tag)) {
          return new ActivityMainBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for activity_main is invalid. Received: " + tag);
      }
      case  LAYOUT_ACTIVITYPRODUCTDETAILS: {
        if ("layout/activity_product_details_0".equals(tag)) {
          return new ActivityProductDetailsBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for activity_product_details is invalid. Received: " + tag);
      }
      case  LAYOUT_ACTIVITYSEARCH: {
        if ("layout/activity_search_0".equals(tag)) {
          return new ActivitySearchBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for activity_search is invalid. Received: " + tag);
      }
      case  LAYOUT_ACTIVITYSPLASH: {
        if ("layout/activity_splash_0".equals(tag)) {
          return new ActivitySplashBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for activity_splash is invalid. Received: " + tag);
      }
      case  LAYOUT_ACTIVITYTERMSCONDITIONS: {
        if ("layout/activity_terms_conditions_0".equals(tag)) {
          return new ActivityTermsConditionsBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for activity_terms_conditions is invalid. Received: " + tag);
      }
      case  LAYOUT_ACTIVITYVENDORPRODUCTS: {
        if ("layout/activity_vendor_products_0".equals(tag)) {
          return new ActivityVendorProductsBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for activity_vendor_products is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTADDNEWADDRESS: {
        if ("layout/fragment_add_new_address_0".equals(tag)) {
          return new FragmentAddNewAddressBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_add_new_address is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTADDRESSES: {
        if ("layout/fragment_addresses_0".equals(tag)) {
          return new FragmentAddressesBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_addresses is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTDISPLAYREVIEWS: {
        if ("layout/fragment_display_reviews_0".equals(tag)) {
          return new FragmentDisplayReviewsBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_display_reviews is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTEDITPASSWORD: {
        if ("layout/fragment_edit_password_0".equals(tag)) {
          return new FragmentEditPasswordBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_edit_password is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTFORGETPASSWORD: {
        if ("layout/fragment_forget_password_0".equals(tag)) {
          return new FragmentForgetPasswordBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_forget_password is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTHOME: {
        if ("layout/fragment_home_0".equals(tag)) {
          return new FragmentHomeBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_home is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTLOGIN: {
        if ("layout/fragment_login_0".equals(tag)) {
          return new FragmentLoginBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_login is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTLOGINSIGNUP: {
        if ("layout/fragment_login_signup_0".equals(tag)) {
          return new FragmentLoginSignupBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_login_signup is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTMAP: {
        if ("layout/fragment_map_0".equals(tag)) {
          return new FragmentMapBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_map is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTMEDICINE: {
        if ("layout/fragment_medicine_0".equals(tag)) {
          return new FragmentMedicineBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_medicine is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTORDERECOMMERCEDETAILS: {
        if ("layout/fragment_order_ecommerce_details_0".equals(tag)) {
          return new FragmentOrderEcommerceDetailsBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_order_ecommerce_details is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTORDERMEDICINEDETAILS: {
        if ("layout/fragment_order_medicine_details_0".equals(tag)) {
          return new FragmentOrderMedicineDetailsBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_order_medicine_details is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTORDERPARCELDETAILS: {
        if ("layout/fragment_order_parcel_details_0".equals(tag)) {
          return new FragmentOrderParcelDetailsBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_order_parcel_details is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTORDERRIDEDETAILS: {
        if ("layout/fragment_order_ride_details_0".equals(tag)) {
          return new FragmentOrderRideDetailsBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_order_ride_details is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTORDERS: {
        if ("layout/fragment_orders_0".equals(tag)) {
          return new FragmentOrdersBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_orders is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTOTP: {
        if ("layout/fragment_otp_0".equals(tag)) {
          return new FragmentOtpBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_otp is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTPARCELPICKUPDETAILS: {
        if ("layout/fragment_parcel_pickup_details_0".equals(tag)) {
          return new FragmentParcelPickupDetailsBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_parcel_pickup_details is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTPASSWORDCHANGE: {
        if ("layout/fragment_password_change_0".equals(tag)) {
          return new FragmentPasswordChangeBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_password_change is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTPROFILE: {
        if ("layout/fragment_profile_0".equals(tag)) {
          return new FragmentProfileBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_profile is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTPROMOCODE: {
        if ("layout/fragment_promo_code_0".equals(tag)) {
          return new FragmentPromoCodeBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_promo_code is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTRATINGDIALOG: {
        if ("layout/fragment_rating_dialog_0".equals(tag)) {
          return new FragmentRatingDialogBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_rating_dialog is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTREQUESTRIDE: {
        if ("layout/fragment_request_ride_0".equals(tag)) {
          return new FragmentRequestRideBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_request_ride is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTSIGNUP: {
        if ("layout/fragment_signup_0".equals(tag)) {
          return new FragmentSignupBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_signup is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTVENDORPRODUCTS: {
        if ("layout/fragment_vendor_products_0".equals(tag)) {
          return new FragmentVendorProductsBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_vendor_products is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTVENDORS: {
        if ("layout/fragment_vendors_0".equals(tag)) {
          return new FragmentVendorsBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_vendors is invalid. Received: " + tag);
      }
      case  LAYOUT_FRAGMENTVERIFYEMAILPHONE: {
        if ("layout/fragment_verify_email_phone_0".equals(tag)) {
          return new FragmentVerifyEmailPhoneBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for fragment_verify_email_phone is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMADDRESSES: {
        if ("layout/item_addresses_0".equals(tag)) {
          return new ItemAddressesBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_addresses is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMCARTITEM: {
        if ("layout/item_cart_item_0".equals(tag)) {
          return new ItemCartItemBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_cart_item is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMFAQ: {
        if ("layout/item_faq_0".equals(tag)) {
          return new ItemFaqBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_faq is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMMAINACTIVITY: {
        if ("layout/item_main_activity_0".equals(tag)) {
          return new ItemMainActivityBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_main_activity is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMMEDICINEVIEW: {
        if ("layout/item_medicine_view_0".equals(tag)) {
          return new ItemMedicineViewBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_medicine_view is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMORDERECOMMERCE: {
        if ("layout/item_order_ecommerce_0".equals(tag)) {
          return new ItemOrderEcommerceBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_order_ecommerce is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMORDERMEDICINE: {
        if ("layout/item_order_medicine_0".equals(tag)) {
          return new ItemOrderMedicineBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_order_medicine is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMORDERS: {
        if ("layout/item_orders_0".equals(tag)) {
          return new ItemOrdersBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_orders is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMPLACES: {
        if ("layout/item_places_0".equals(tag)) {
          return new ItemPlacesBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_places is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMSEARCHPRODUCT: {
        if ("layout/item_search_product_0".equals(tag)) {
          return new ItemSearchProductBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_search_product is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMVENDOR: {
        if ("layout/item_vendor_0".equals(tag)) {
          return new ItemVendorBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_vendor is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMVENDORPRODUCTS: {
        if ("layout/item_vendor_products_0".equals(tag)) {
          return new ItemVendorProductsBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for item_vendor_products is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMSPRODUCTREVIEW: {
        if ("layout/items_product_review_0".equals(tag)) {
          return new ItemsProductReviewBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for items_product_review is invalid. Received: " + tag);
      }
      case  LAYOUT_ITEMSREVIEW: {
        if ("layout/items_review_0".equals(tag)) {
          return new ItemsReviewBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for items_review is invalid. Received: " + tag);
      }
      case  LAYOUT_LAYOUTCARTICON: {
        if ("layout/layout_cart_icon_0".equals(tag)) {
          return new LayoutCartIconBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for layout_cart_icon is invalid. Received: " + tag);
      }
    }
    return null;
  }

  private final ViewDataBinding internalGetViewDataBinding1(DataBindingComponent component,
      View view, int internalId, Object tag) {
    switch(internalId) {
      case  LAYOUT_PRODUCTCATEGORIESBYID: {
        if ("layout/productcategoriesbyid_0".equals(tag)) {
          return new ProductcategoriesbyidBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for productcategoriesbyid is invalid. Received: " + tag);
      }
      case  LAYOUT_RYLADELOADINGDIALOGE: {
        if ("layout/rylade_loading_dialoge_0".equals(tag)) {
          return new RyladeLoadingDialogeBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for rylade_loading_dialoge is invalid. Received: " + tag);
      }
      case  LAYOUT_VENDORPRODUCTCATEGORIESLIST: {
        if ("layout/vendorproductcategorieslist_0".equals(tag)) {
          return new VendorproductcategorieslistBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for vendorproductcategorieslist is invalid. Received: " + tag);
      }
      case  LAYOUT_VENDORPRODUCTSLIST: {
        if ("layout/vendorproductslist_0".equals(tag)) {
          return new VendorproductslistBindingImpl(component, view);
        }
        throw new IllegalArgumentException("The tag for vendorproductslist is invalid. Received: " + tag);
      }
    }
    return null;
  }

  @Override
  public ViewDataBinding getDataBinder(DataBindingComponent component, View view, int layoutId) {
    int localizedLayoutId = INTERNAL_LAYOUT_ID_LOOKUP.get(layoutId);
    if(localizedLayoutId > 0) {
      final Object tag = view.getTag();
      if(tag == null) {
        throw new RuntimeException("view must have a tag");
      }
      // find which method will have it. -1 is necessary becausefirst id starts with 1;
      int methodIndex = (localizedLayoutId - 1) / 50;
      switch(methodIndex) {
        case 0: {
          return internalGetViewDataBinding0(component, view, localizedLayoutId, tag);
        }
        case 1: {
          return internalGetViewDataBinding1(component, view, localizedLayoutId, tag);
        }
      }
    }
    return null;
  }

  @Override
  public ViewDataBinding getDataBinder(DataBindingComponent component, View[] views, int layoutId) {
    if(views == null || views.length == 0) {
      return null;
    }
    int localizedLayoutId = INTERNAL_LAYOUT_ID_LOOKUP.get(layoutId);
    if(localizedLayoutId > 0) {
      final Object tag = views[0].getTag();
      if(tag == null) {
        throw new RuntimeException("view must have a tag");
      }
      switch(localizedLayoutId) {
      }
    }
    return null;
  }

  @Override
  public int getLayoutId(String tag) {
    if (tag == null) {
      return 0;
    }
    Integer tmpVal = InnerLayoutIdLookup.sKeys.get(tag);
    return tmpVal == null ? 0 : tmpVal;
  }

  @Override
  public String convertBrIdToString(int localId) {
    String tmpVal = InnerBrLookup.sKeys.get(localId);
    return tmpVal;
  }

  @Override
  public List<DataBinderMapper> collectDependencies() {
    ArrayList<DataBinderMapper> result = new ArrayList<DataBinderMapper>(1);
    result.add(new androidx.databinding.library.baseAdapters.DataBinderMapperImpl());
    return result;
  }

  private static class InnerBrLookup {
    static final SparseArray<String> sKeys = new SparseArray<String>(22);

    static {
      sKeys.put(0, "_all");
      sKeys.put(1, "adaptor");
      sKeys.put(2, "address");
      sKeys.put(3, "addressID");
      sKeys.put(4, "data");
      sKeys.put(5, "deliveryFee");
      sKeys.put(6, "model");
      sKeys.put(7, "order");
      sKeys.put(8, "orderModel");
      sKeys.put(9, "product");
      sKeys.put(10, "promo");
      sKeys.put(11, "promoAmount");
      sKeys.put(12, "promoID");
      sKeys.put(13, "promoPercent");
      sKeys.put(14, "promoTitle");
      sKeys.put(15, "rideModel");
      sKeys.put(16, "rider");
      sKeys.put(17, "subTotal");
      sKeys.put(18, "taxAmount");
      sKeys.put(19, "totalAmount");
      sKeys.put(20, "vendor");
      sKeys.put(21, "vendorID");
    }
  }

  private static class InnerLayoutIdLookup {
    static final HashMap<String, Integer> sKeys = new HashMap<String, Integer>(54);

    static {
      sKeys.put("layout/activity_cart_0", com.senarios.customer.R.layout.activity_cart);
      sKeys.put("layout/activity_ecommerce_0", com.senarios.customer.R.layout.activity_ecommerce);
      sKeys.put("layout/activity_faq_0", com.senarios.customer.R.layout.activity_faq);
      sKeys.put("layout/activity_main_0", com.senarios.customer.R.layout.activity_main);
      sKeys.put("layout/activity_product_details_0", com.senarios.customer.R.layout.activity_product_details);
      sKeys.put("layout/activity_search_0", com.senarios.customer.R.layout.activity_search);
      sKeys.put("layout/activity_splash_0", com.senarios.customer.R.layout.activity_splash);
      sKeys.put("layout/activity_terms_conditions_0", com.senarios.customer.R.layout.activity_terms_conditions);
      sKeys.put("layout/activity_vendor_products_0", com.senarios.customer.R.layout.activity_vendor_products);
      sKeys.put("layout/fragment_add_new_address_0", com.senarios.customer.R.layout.fragment_add_new_address);
      sKeys.put("layout/fragment_addresses_0", com.senarios.customer.R.layout.fragment_addresses);
      sKeys.put("layout/fragment_display_reviews_0", com.senarios.customer.R.layout.fragment_display_reviews);
      sKeys.put("layout/fragment_edit_password_0", com.senarios.customer.R.layout.fragment_edit_password);
      sKeys.put("layout/fragment_forget_password_0", com.senarios.customer.R.layout.fragment_forget_password);
      sKeys.put("layout/fragment_home_0", com.senarios.customer.R.layout.fragment_home);
      sKeys.put("layout/fragment_login_0", com.senarios.customer.R.layout.fragment_login);
      sKeys.put("layout/fragment_login_signup_0", com.senarios.customer.R.layout.fragment_login_signup);
      sKeys.put("layout/fragment_map_0", com.senarios.customer.R.layout.fragment_map);
      sKeys.put("layout/fragment_medicine_0", com.senarios.customer.R.layout.fragment_medicine);
      sKeys.put("layout/fragment_order_ecommerce_details_0", com.senarios.customer.R.layout.fragment_order_ecommerce_details);
      sKeys.put("layout/fragment_order_medicine_details_0", com.senarios.customer.R.layout.fragment_order_medicine_details);
      sKeys.put("layout/fragment_order_parcel_details_0", com.senarios.customer.R.layout.fragment_order_parcel_details);
      sKeys.put("layout/fragment_order_ride_details_0", com.senarios.customer.R.layout.fragment_order_ride_details);
      sKeys.put("layout/fragment_orders_0", com.senarios.customer.R.layout.fragment_orders);
      sKeys.put("layout/fragment_otp_0", com.senarios.customer.R.layout.fragment_otp);
      sKeys.put("layout/fragment_parcel_pickup_details_0", com.senarios.customer.R.layout.fragment_parcel_pickup_details);
      sKeys.put("layout/fragment_password_change_0", com.senarios.customer.R.layout.fragment_password_change);
      sKeys.put("layout/fragment_profile_0", com.senarios.customer.R.layout.fragment_profile);
      sKeys.put("layout/fragment_promo_code_0", com.senarios.customer.R.layout.fragment_promo_code);
      sKeys.put("layout/fragment_rating_dialog_0", com.senarios.customer.R.layout.fragment_rating_dialog);
      sKeys.put("layout/fragment_request_ride_0", com.senarios.customer.R.layout.fragment_request_ride);
      sKeys.put("layout/fragment_signup_0", com.senarios.customer.R.layout.fragment_signup);
      sKeys.put("layout/fragment_vendor_products_0", com.senarios.customer.R.layout.fragment_vendor_products);
      sKeys.put("layout/fragment_vendors_0", com.senarios.customer.R.layout.fragment_vendors);
      sKeys.put("layout/fragment_verify_email_phone_0", com.senarios.customer.R.layout.fragment_verify_email_phone);
      sKeys.put("layout/item_addresses_0", com.senarios.customer.R.layout.item_addresses);
      sKeys.put("layout/item_cart_item_0", com.senarios.customer.R.layout.item_cart_item);
      sKeys.put("layout/item_faq_0", com.senarios.customer.R.layout.item_faq);
      sKeys.put("layout/item_main_activity_0", com.senarios.customer.R.layout.item_main_activity);
      sKeys.put("layout/item_medicine_view_0", com.senarios.customer.R.layout.item_medicine_view);
      sKeys.put("layout/item_order_ecommerce_0", com.senarios.customer.R.layout.item_order_ecommerce);
      sKeys.put("layout/item_order_medicine_0", com.senarios.customer.R.layout.item_order_medicine);
      sKeys.put("layout/item_orders_0", com.senarios.customer.R.layout.item_orders);
      sKeys.put("layout/item_places_0", com.senarios.customer.R.layout.item_places);
      sKeys.put("layout/item_search_product_0", com.senarios.customer.R.layout.item_search_product);
      sKeys.put("layout/item_vendor_0", com.senarios.customer.R.layout.item_vendor);
      sKeys.put("layout/item_vendor_products_0", com.senarios.customer.R.layout.item_vendor_products);
      sKeys.put("layout/items_product_review_0", com.senarios.customer.R.layout.items_product_review);
      sKeys.put("layout/items_review_0", com.senarios.customer.R.layout.items_review);
      sKeys.put("layout/layout_cart_icon_0", com.senarios.customer.R.layout.layout_cart_icon);
      sKeys.put("layout/productcategoriesbyid_0", com.senarios.customer.R.layout.productcategoriesbyid);
      sKeys.put("layout/rylade_loading_dialoge_0", com.senarios.customer.R.layout.rylade_loading_dialoge);
      sKeys.put("layout/vendorproductcategorieslist_0", com.senarios.customer.R.layout.vendorproductcategorieslist);
      sKeys.put("layout/vendorproductslist_0", com.senarios.customer.R.layout.vendorproductslist);
    }
  }
}
