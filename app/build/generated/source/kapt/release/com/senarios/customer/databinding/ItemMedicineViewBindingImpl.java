package com.senarios.customer.databinding;
import com.senarios.customer.R;
import com.senarios.customer.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ItemMedicineViewBindingImpl extends ItemMedicineViewBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.constraintLayout, 5);
        sViewsWithIds.put(R.id.iv_remove_view, 6);
        sViewsWithIds.put(R.id.picture_img, 7);
        sViewsWithIds.put(R.id.take_snap_prescription_btn, 8);
        sViewsWithIds.put(R.id.imageView5, 9);
        sViewsWithIds.put(R.id.snap_prescription_txt, 10);
        sViewsWithIds.put(R.id.guideline8, 11);
        sViewsWithIds.put(R.id.guideline9, 12);
        sViewsWithIds.put(R.id.guideline10, 13);
        sViewsWithIds.put(R.id.guideline12, 14);
        sViewsWithIds.put(R.id.guideline14, 15);
        sViewsWithIds.put(R.id.guideline15, 16);
        sViewsWithIds.put(R.id.guideline16, 17);
        sViewsWithIds.put(R.id.guideline17, 18);
        sViewsWithIds.put(R.id.guideline18, 19);
        sViewsWithIds.put(R.id.guideline19, 20);
        sViewsWithIds.put(R.id.guideline20, 21);
        sViewsWithIds.put(R.id.type, 22);
    }
    // views
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ItemMedicineViewBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 23, sIncludes, sViewsWithIds));
    }
    private ItemMedicineViewBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (androidx.cardview.widget.CardView) bindings[0]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[5]
            , (android.widget.EditText) bindings[3]
            , (androidx.constraintlayout.widget.Guideline) bindings[13]
            , (androidx.constraintlayout.widget.Guideline) bindings[14]
            , (androidx.constraintlayout.widget.Guideline) bindings[15]
            , (androidx.constraintlayout.widget.Guideline) bindings[16]
            , (androidx.constraintlayout.widget.Guideline) bindings[17]
            , (androidx.constraintlayout.widget.Guideline) bindings[18]
            , (androidx.constraintlayout.widget.Guideline) bindings[19]
            , (androidx.constraintlayout.widget.Guideline) bindings[20]
            , (androidx.constraintlayout.widget.Guideline) bindings[21]
            , (androidx.constraintlayout.widget.Guideline) bindings[11]
            , (androidx.constraintlayout.widget.Guideline) bindings[12]
            , (android.widget.ImageView) bindings[9]
            , (android.widget.EditText) bindings[4]
            , (android.widget.ImageView) bindings[6]
            , (android.widget.EditText) bindings[1]
            , (de.hdodenhof.circleimageview.CircleImageView) bindings[7]
            , (android.widget.EditText) bindings[2]
            , (android.widget.TextView) bindings[10]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[8]
            , (android.widget.Spinner) bindings[22]
            );
        this.cardview.setTag(null);
        this.doseEdt.setTag(null);
        this.instructionsTxt.setTag(null);
        this.nameEdt.setTag(null);
        this.quantityEdt.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.data == variableId) {
            setData((com.senarios.customer.models.MedicineModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setData(@Nullable com.senarios.customer.models.MedicineModel Data) {
        this.mData = Data;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.data);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String dataDose = null;
        java.lang.String dataInstruction = null;
        java.lang.String dataQuantity = null;
        com.senarios.customer.models.MedicineModel data = mData;
        java.lang.String dataName = null;

        if ((dirtyFlags & 0x3L) != 0) {



                if (data != null) {
                    // read data.dose
                    dataDose = data.getDose();
                    // read data.instruction
                    dataInstruction = data.getInstruction();
                    // read data.quantity
                    dataQuantity = data.getQuantity();
                    // read data.name
                    dataName = data.getName();
                }
        }
        // batch finished
        if ((dirtyFlags & 0x3L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.doseEdt, dataDose);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.instructionsTxt, dataInstruction);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.nameEdt, dataName);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.quantityEdt, dataQuantity);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): data
        flag 1 (0x2L): null
    flag mapping end*/
    //end
}