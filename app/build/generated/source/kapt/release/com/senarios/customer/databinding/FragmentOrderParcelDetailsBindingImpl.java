package com.senarios.customer.databinding;
import com.senarios.customer.R;
import com.senarios.customer.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentOrderParcelDetailsBindingImpl extends FragmentOrderParcelDetailsBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.appbar, 12);
        sViewsWithIds.put(R.id.collapseActionView, 13);
        sViewsWithIds.put(R.id.toolbar, 14);
        sViewsWithIds.put(R.id.scrollView, 15);
        sViewsWithIds.put(R.id.cardview, 16);
        sViewsWithIds.put(R.id.view1, 17);
        sViewsWithIds.put(R.id.view2, 18);
        sViewsWithIds.put(R.id.view3, 19);
        sViewsWithIds.put(R.id.view4, 20);
        sViewsWithIds.put(R.id.view5, 21);
        sViewsWithIds.put(R.id.view6, 22);
        sViewsWithIds.put(R.id.view7, 23);
        sViewsWithIds.put(R.id.view8, 24);
        sViewsWithIds.put(R.id.view9, 25);
        sViewsWithIds.put(R.id.cancel, 26);
    }
    // views
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FragmentOrderParcelDetailsBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 27, sIncludes, sViewsWithIds));
    }
    private FragmentOrderParcelDetailsBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (com.google.android.material.appbar.AppBarLayout) bindings[12]
            , (android.widget.Button) bindings[26]
            , (androidx.cardview.widget.CardView) bindings[16]
            , (com.google.android.material.appbar.CollapsingToolbarLayout) bindings[13]
            , (androidx.coordinatorlayout.widget.CoordinatorLayout) bindings[0]
            , (de.hdodenhof.circleimageview.CircleImageView) bindings[1]
            , (android.widget.TextView) bindings[4]
            , (android.widget.TextView) bindings[3]
            , (android.widget.TextView) bindings[11]
            , (android.widget.TextView) bindings[10]
            , (android.widget.TextView) bindings[9]
            , (androidx.core.widget.NestedScrollView) bindings[15]
            , (android.widget.TextView) bindings[8]
            , (android.widget.TextView) bindings[7]
            , (android.widget.TextView) bindings[6]
            , (android.widget.TextView) bindings[2]
            , (androidx.appcompat.widget.Toolbar) bindings[14]
            , (android.view.View) bindings[17]
            , (android.view.View) bindings[18]
            , (android.view.View) bindings[19]
            , (android.view.View) bindings[20]
            , (android.view.View) bindings[21]
            , (android.view.View) bindings[22]
            , (android.view.View) bindings[23]
            , (android.view.View) bindings[24]
            , (android.view.View) bindings[25]
            , (android.widget.TextView) bindings[5]
            );
        this.constraintLayout.setTag(null);
        this.imageview.setTag(null);
        this.name.setTag(null);
        this.orderID.setTag(null);
        this.receiversAddress.setTag(null);
        this.receiversContact.setTag(null);
        this.receiversName.setTag(null);
        this.sendersAddress.setTag(null);
        this.sendersContact.setTag(null);
        this.sendersName.setTag(null);
        this.status.setTag(null);
        this.weight.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x4L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.data == variableId) {
            setData((com.senarios.customer.models.ParcelModel) variable);
        }
        else if (BR.order == variableId) {
            setOrder((com.senarios.customer.models.OrderModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setData(@Nullable com.senarios.customer.models.ParcelModel Data) {
        this.mData = Data;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.data);
        super.requestRebind();
    }
    public void setOrder(@Nullable com.senarios.customer.models.OrderModel Order) {
        this.mOrder = Order;
        synchronized(this) {
            mDirtyFlags |= 0x2L;
        }
        notifyPropertyChanged(BR.order);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String orderStatus = null;
        java.lang.String javaLangStringReceiverAddressDataReceiverAddress = null;
        java.lang.String dataReceiverPhone = null;
        com.senarios.customer.models.ParcelModel data = mData;
        java.lang.String javaLangStringOrderIDOrderId = null;
        java.lang.String javaLangStringReceiverContactDataReceiverPhone = null;
        java.lang.String dataReceiverAddress = null;
        java.lang.String dataItemWeight = null;
        java.lang.String javaLangStringReceiverNameDataReceiverName = null;
        com.senarios.customer.models.OrderModel order = mOrder;
        java.lang.String javaLangStringWeightDataItemWeight = null;
        java.lang.String javaLangStringSenderPhoneDataSenderPhone = null;
        java.lang.String dataItemName = null;
        java.lang.String dataSenderName = null;
        int orderId = 0;
        java.lang.String javaLangStringSenderNameDataSenderName = null;
        java.lang.String dataSenderPhone = null;
        java.lang.String javaLangStringSenderAddressDataSenderAddress = null;
        java.lang.String dataSenderAddress = null;
        java.lang.String javaLangStringNameDataItemName = null;
        java.lang.String dataReceiverName = null;
        java.lang.String dataPhoto = null;

        if ((dirtyFlags & 0x5L) != 0) {



                if (data != null) {
                    // read data.receiver_phone
                    dataReceiverPhone = data.getReceiver_phone();
                    // read data.receiver_address
                    dataReceiverAddress = data.getReceiver_address();
                    // read data.item_weight
                    dataItemWeight = data.getItem_weight();
                    // read data.item_name
                    dataItemName = data.getItem_name();
                    // read data.sender_name
                    dataSenderName = data.getSender_name();
                    // read data.sender_phone
                    dataSenderPhone = data.getSender_phone();
                    // read data.sender_address
                    dataSenderAddress = data.getSender_address();
                    // read data.receiver_name
                    dataReceiverName = data.getReceiver_name();
                    // read data.photo
                    dataPhoto = data.getPhoto();
                }


                // read ("Receiver Contact : ") + (data.receiver_phone)
                javaLangStringReceiverContactDataReceiverPhone = ("Receiver Contact : ") + (dataReceiverPhone);
                // read ("Receiver Address : ") + (data.receiver_address)
                javaLangStringReceiverAddressDataReceiverAddress = ("Receiver Address : ") + (dataReceiverAddress);
                // read ("Weight : ") + (data.item_weight)
                javaLangStringWeightDataItemWeight = ("Weight : ") + (dataItemWeight);
                // read ("Name : ") + (data.item_name)
                javaLangStringNameDataItemName = ("Name : ") + (dataItemName);
                // read ("Sender Name : ") + (data.sender_name)
                javaLangStringSenderNameDataSenderName = ("Sender Name : ") + (dataSenderName);
                // read ("Sender Phone : ") + (data.sender_phone)
                javaLangStringSenderPhoneDataSenderPhone = ("Sender Phone : ") + (dataSenderPhone);
                // read ("Sender Address : ") + (data.sender_address)
                javaLangStringSenderAddressDataSenderAddress = ("Sender Address : ") + (dataSenderAddress);
                // read ("Receiver Name : ") + (data.receiver_name)
                javaLangStringReceiverNameDataReceiverName = ("Receiver Name : ") + (dataReceiverName);
        }
        if ((dirtyFlags & 0x6L) != 0) {



                if (order != null) {
                    // read order.status
                    orderStatus = order.getStatus();
                    // read order.id
                    orderId = order.getId();
                }


                // read ("Order ID : ") + (order.id)
                javaLangStringOrderIDOrderId = ("Order ID : ") + (orderId);
        }
        // batch finished
        if ((dirtyFlags & 0x5L) != 0) {
            // api target 1

            com.senarios.customer.models.ParcelModelKt.photo(this.imageview, dataPhoto);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.name, javaLangStringNameDataItemName);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.receiversAddress, javaLangStringReceiverAddressDataReceiverAddress);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.receiversContact, javaLangStringReceiverContactDataReceiverPhone);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.receiversName, javaLangStringReceiverNameDataReceiverName);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.sendersAddress, javaLangStringSenderAddressDataSenderAddress);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.sendersContact, javaLangStringSenderPhoneDataSenderPhone);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.sendersName, javaLangStringSenderNameDataSenderName);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.weight, javaLangStringWeightDataItemWeight);
        }
        if ((dirtyFlags & 0x6L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.orderID, javaLangStringOrderIDOrderId);
            com.senarios.customer.models.OrderModelKt.setOrderStatus(this.status, orderStatus);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): data
        flag 1 (0x2L): order
        flag 2 (0x3L): null
    flag mapping end*/
    //end
}