package com.senarios.customer.databinding;
import com.senarios.customer.R;
import com.senarios.customer.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class FragmentOrderRideDetailsBindingImpl extends FragmentOrderRideDetailsBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.toolbar, 18);
        sViewsWithIds.put(R.id.mapView, 19);
        sViewsWithIds.put(R.id.guideline5, 20);
        sViewsWithIds.put(R.id.cardview, 21);
        sViewsWithIds.put(R.id.view1, 22);
        sViewsWithIds.put(R.id.view2, 23);
        sViewsWithIds.put(R.id.cancel, 24);
    }
    // views
    @NonNull
    private final androidx.core.widget.NestedScrollView mboundView0;
    @NonNull
    private final android.view.View mboundView10;
    @NonNull
    private final android.view.View mboundView12;
    @NonNull
    private final android.view.View mboundView14;
    @NonNull
    private final android.view.View mboundView16;
    @NonNull
    private final android.view.View mboundView3;
    @NonNull
    private final android.view.View mboundView8;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public FragmentOrderRideDetailsBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 25, sIncludes, sViewsWithIds));
    }
    private FragmentOrderRideDetailsBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (android.widget.TextView) bindings[7]
            , (android.widget.Button) bindings[24]
            , (androidx.cardview.widget.CardView) bindings[21]
            , (android.widget.TextView) bindings[5]
            , (android.widget.TextView) bindings[1]
            , (android.widget.TextView) bindings[4]
            , (androidx.constraintlayout.widget.Guideline) bindings[20]
            , (com.google.android.gms.maps.MapView) bindings[19]
            , (android.widget.TextView) bindings[2]
            , (androidx.appcompat.widget.Toolbar) bindings[18]
            , (android.widget.TextView) bindings[13]
            , (android.widget.TextView) bindings[9]
            , (android.widget.TextView) bindings[11]
            , (android.view.View) bindings[22]
            , (android.view.View) bindings[23]
            , (android.view.View) bindings[6]
            , (android.widget.TextView) bindings[17]
            , (android.widget.TextView) bindings[15]
            );
        this.amount.setTag(null);
        this.distance.setTag(null);
        this.dropoffAddress.setTag(null);
        this.estimateTime.setTag(null);
        this.mboundView0 = (androidx.core.widget.NestedScrollView) bindings[0];
        this.mboundView0.setTag(null);
        this.mboundView10 = (android.view.View) bindings[10];
        this.mboundView10.setTag(null);
        this.mboundView12 = (android.view.View) bindings[12];
        this.mboundView12.setTag(null);
        this.mboundView14 = (android.view.View) bindings[14];
        this.mboundView14.setTag(null);
        this.mboundView16 = (android.view.View) bindings[16];
        this.mboundView16.setTag(null);
        this.mboundView3 = (android.view.View) bindings[3];
        this.mboundView3.setTag(null);
        this.mboundView8 = (android.view.View) bindings[8];
        this.mboundView8.setTag(null);
        this.pickupAddress.setTag(null);
        this.totalFare.setTag(null);
        this.totalTime.setTag(null);
        this.totaldistance.setTag(null);
        this.view3.setTag(null);
        this.waitingCost.setTag(null);
        this.waitingTime.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.data == variableId) {
            setData((com.senarios.customer.models.RideModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setData(@Nullable com.senarios.customer.models.RideModel Data) {
        this.mData = Data;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.data);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String dataTotalFare = null;
        java.lang.String javaLangStringTotalDistanceDataDistanceJavaLangStringKM = null;
        com.senarios.customer.models.RideModel data = mData;
        java.lang.String dataPickupAddress = null;
        java.lang.String dataDropoffAddress = null;
        java.lang.String dataEstimatedDistance = null;
        java.lang.String javaLangStringEstimateDistanceDataEstimatedDistanceJavaLangStringKM = null;
        java.lang.String dataWaitingCost = null;
        java.lang.String dataEstimateTime = null;
        java.lang.String dataTotalTime = null;
        java.lang.String javaLangStringTotalDistanceDataDistance = null;
        java.lang.String dataDistance = null;
        java.lang.String javaLangStringEstimateDistanceDataEstimatedDistance = null;
        java.lang.String dataWaitingTime = null;
        java.lang.String dataEstimateFare = null;

        if ((dirtyFlags & 0x3L) != 0) {



                if (data != null) {
                    // read data.total_Fare
                    dataTotalFare = data.getTotal_Fare();
                    // read data.pickup_address
                    dataPickupAddress = data.getPickup_address();
                    // read data.dropoff_address
                    dataDropoffAddress = data.getDropoff_address();
                    // read data.estimated_distance
                    dataEstimatedDistance = data.getEstimated_distance();
                    // read data.waiting_cost
                    dataWaitingCost = data.getWaiting_cost();
                    // read data.estimate_time
                    dataEstimateTime = data.getEstimate_time();
                    // read data.total_time
                    dataTotalTime = data.getTotal_time();
                    // read data.distance
                    dataDistance = data.getDistance();
                    // read data.waiting_time
                    dataWaitingTime = data.getWaiting_time();
                    // read data.estimate_fare
                    dataEstimateFare = data.getEstimate_fare();
                }


                // read ("Estimate Distance : ") + (data.estimated_distance)
                javaLangStringEstimateDistanceDataEstimatedDistance = ("Estimate Distance : ") + (dataEstimatedDistance);
                // read ("Total Distance : ") + (data.distance)
                javaLangStringTotalDistanceDataDistance = ("Total Distance : ") + (dataDistance);


                // read (("Estimate Distance : ") + (data.estimated_distance)) + (" KM")
                javaLangStringEstimateDistanceDataEstimatedDistanceJavaLangStringKM = (javaLangStringEstimateDistanceDataEstimatedDistance) + (" KM");
                // read (("Total Distance : ") + (data.distance)) + (" KM")
                javaLangStringTotalDistanceDataDistanceJavaLangStringKM = (javaLangStringTotalDistanceDataDistance) + (" KM");
        }
        // batch finished
        if ((dirtyFlags & 0x3L) != 0) {
            // api target 1

            com.senarios.customer.models.BindingAdaptorsKt.setTitleText(this.amount, "Estimate Fare : ", dataEstimateFare, " PKR");
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.distance, javaLangStringEstimateDistanceDataEstimatedDistanceJavaLangStringKM);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.dropoffAddress, dataDropoffAddress);
            com.senarios.customer.models.BindingAdaptorsKt.setTitleText(this.estimateTime, "Estimate Time : ", dataEstimateTime, " Min");
            com.senarios.customer.models.OrderModelKt.setView(this.mboundView10, dataDistance);
            com.senarios.customer.models.OrderModelKt.setView(this.mboundView12, dataTotalFare);
            com.senarios.customer.models.OrderModelKt.setView(this.mboundView14, dataWaitingTime);
            com.senarios.customer.models.OrderModelKt.setView(this.mboundView16, dataWaitingCost);
            com.senarios.customer.models.OrderModelKt.setView(this.mboundView3, dataEstimateTime);
            com.senarios.customer.models.OrderModelKt.setView(this.mboundView8, dataTotalTime);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.pickupAddress, dataPickupAddress);
            com.senarios.customer.models.BindingAdaptorsKt.setTitleText(this.totalFare, "Total Fare : ", dataTotalFare, " PKR");
            com.senarios.customer.models.BindingAdaptorsKt.setTitleText(this.totalTime, "Total Time : ", dataTotalTime, " Min");
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.totaldistance, javaLangStringTotalDistanceDataDistanceJavaLangStringKM);
            com.senarios.customer.models.OrderModelKt.setView(this.view3, dataEstimateFare);
            com.senarios.customer.models.BindingAdaptorsKt.setTitleText(this.waitingCost, "Waiting Cost : ", dataWaitingCost, " PKR");
            com.senarios.customer.models.BindingAdaptorsKt.setTitleText(this.waitingTime, "Waiting Time : ", dataWaitingTime, " Min");
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): data
        flag 1 (0x2L): null
    flag mapping end*/
    //end
}