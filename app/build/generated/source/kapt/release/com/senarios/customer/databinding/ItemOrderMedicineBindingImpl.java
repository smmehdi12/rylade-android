package com.senarios.customer.databinding;
import com.senarios.customer.R;
import com.senarios.customer.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ItemOrderMedicineBindingImpl extends ItemOrderMedicineBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.title_main, 6);
        sViewsWithIds.put(R.id.view_first, 7);
        sViewsWithIds.put(R.id.view_second, 8);
        sViewsWithIds.put(R.id.view_third, 9);
    }
    // views
    @NonNull
    private final androidx.cardview.widget.CardView mboundView0;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ItemOrderMedicineBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 10, sIncludes, sViewsWithIds));
    }
    private ItemOrderMedicineBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (android.widget.TextView) bindings[3]
            , (de.hdodenhof.circleimageview.CircleImageView) bindings[1]
            , (android.widget.TextView) bindings[2]
            , (android.widget.TextView) bindings[5]
            , (android.widget.TextView) bindings[6]
            , (android.widget.TextView) bindings[4]
            , (android.view.View) bindings[7]
            , (android.view.View) bindings[8]
            , (android.view.View) bindings[9]
            );
        this.dose.setTag(null);
        this.imageView.setTag(null);
        this.mboundView0 = (androidx.cardview.widget.CardView) bindings[0];
        this.mboundView0.setTag(null);
        this.name.setTag(null);
        this.quantity.setTag(null);
        this.type.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.data == variableId) {
            setData((com.senarios.customer.models.MedicineModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setData(@Nullable com.senarios.customer.models.MedicineModel Data) {
        this.mData = Data;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.data);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String dataDose = null;
        java.lang.String dataPhoto = null;
        java.lang.String javaLangStringNameDataName = null;
        java.lang.String javaLangStringDoseTypeDataType = null;
        java.lang.String javaLangStringQuantityDataQuantity = null;
        java.lang.String javaLangStringDoseDataDose = null;
        java.lang.String dataQuantity = null;
        java.lang.String dataName = null;
        java.lang.String dataType = null;
        com.senarios.customer.models.MedicineModel data = mData;

        if ((dirtyFlags & 0x3L) != 0) {



                if (data != null) {
                    // read data.dose
                    dataDose = data.getDose();
                    // read data.photo
                    dataPhoto = data.getPhoto();
                    // read data.quantity
                    dataQuantity = data.getQuantity();
                    // read data.name
                    dataName = data.getName();
                    // read data.type
                    dataType = data.getType();
                }


                // read ("Dose : ") + (data.dose)
                javaLangStringDoseDataDose = ("Dose : ") + (dataDose);
                // read ("Quantity : ") + (data.quantity)
                javaLangStringQuantityDataQuantity = ("Quantity : ") + (dataQuantity);
                // read ("Name : ") + (data.name)
                javaLangStringNameDataName = ("Name : ") + (dataName);
                // read ("Dose Type : ") + (data.type)
                javaLangStringDoseTypeDataType = ("Dose Type : ") + (dataType);
        }
        // batch finished
        if ((dirtyFlags & 0x3L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.dose, javaLangStringDoseDataDose);
            com.senarios.customer.models.MedicineModelKt.loadImageMedicine(this.imageView, dataPhoto);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.name, javaLangStringNameDataName);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.quantity, javaLangStringQuantityDataQuantity);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.type, javaLangStringDoseTypeDataType);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): data
        flag 1 (0x2L): null
    flag mapping end*/
    //end
}