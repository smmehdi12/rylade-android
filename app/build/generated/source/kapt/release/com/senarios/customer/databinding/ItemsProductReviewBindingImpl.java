package com.senarios.customer.databinding;
import com.senarios.customer.R;
import com.senarios.customer.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ItemsProductReviewBindingImpl extends ItemsProductReviewBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.rating, 5);
        sViewsWithIds.put(R.id.rating_comment, 6);
    }
    // views
    @NonNull
    private final androidx.cardview.widget.CardView mboundView0;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ItemsProductReviewBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 7, sIncludes, sViewsWithIds));
    }
    private ItemsProductReviewBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (android.widget.TextView) bindings[4]
            , (de.hdodenhof.circleimageview.CircleImageView) bindings[1]
            , (android.widget.TextView) bindings[3]
            , (android.widget.RatingBar) bindings[5]
            , (android.widget.EditText) bindings[6]
            , (android.widget.TextView) bindings[2]
            );
        this.dPrice.setTag(null);
        this.imageview.setTag(null);
        this.mboundView0 = (androidx.cardview.widget.CardView) bindings[0];
        this.mboundView0.setTag(null);
        this.price.setTag(null);
        this.tvProductNameQuantity.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x4L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.data == variableId) {
            setData((com.senarios.customer.models.ProductModel) variable);
        }
        else if (BR.adaptor == variableId) {
            setAdaptor((com.senarios.customer.adaptors.ProductReviewAdapter) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setData(@Nullable com.senarios.customer.models.ProductModel Data) {
        this.mData = Data;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.data);
        super.requestRebind();
    }
    public void setAdaptor(@Nullable com.senarios.customer.adaptors.ProductReviewAdapter Adaptor) {
        this.mAdaptor = Adaptor;
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        java.lang.String dataImage = null;
        java.lang.Integer dataIzDiscount = null;
        java.lang.Integer dataFixedDiscount = null;
        java.lang.String dataProductName = null;
        com.senarios.customer.models.ProductModel data = mData;
        java.lang.Integer dataPrice = null;

        if ((dirtyFlags & 0x5L) != 0) {



                if (data != null) {
                    // read data.image
                    dataImage = data.getImage();
                    // read data.iz_discount
                    dataIzDiscount = data.getIz_discount();
                    // read data.fixed_discount
                    dataFixedDiscount = data.getFixed_discount();
                    // read data.productName
                    dataProductName = data.getProductName();
                    // read data.price
                    dataPrice = data.getPrice();
                }
        }
        // batch finished
        if ((dirtyFlags & 0x5L) != 0) {
            // api target 1

            com.senarios.customer.models.ProductModelKt.setPrice(this.dPrice, dataFixedDiscount, java.lang.String.valueOf(1), dataIzDiscount);
            com.senarios.customer.models.ProductModelKt.loadProductImage(this.imageview, dataImage);
            com.senarios.customer.models.ProductModelKt.setPrice(this.price, dataPrice, java.lang.String.valueOf(0), dataIzDiscount);
            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvProductNameQuantity, dataProductName);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): data
        flag 1 (0x2L): adaptor
        flag 2 (0x3L): null
    flag mapping end*/
    //end
}