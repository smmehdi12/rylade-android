package com.senarios.customer.databinding;
import com.senarios.customer.R;
import com.senarios.customer.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ActivityCartBindingImpl extends ActivityCartBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.toolbar, 7);
        sViewsWithIds.put(R.id.nested_ScrollView, 8);
        sViewsWithIds.put(R.id.main_constraint, 9);
        sViewsWithIds.put(R.id.recyclerview, 10);
        sViewsWithIds.put(R.id.cardViewTwo, 11);
        sViewsWithIds.put(R.id.constraintViewTwo, 12);
        sViewsWithIds.put(R.id.tv_Tax, 13);
        sViewsWithIds.put(R.id.tv_deliveryFee, 14);
        sViewsWithIds.put(R.id.tv_Subtotal, 15);
        sViewsWithIds.put(R.id.tv_promo, 16);
        sViewsWithIds.put(R.id.tv_remove_promo, 17);
        sViewsWithIds.put(R.id.view2, 18);
        sViewsWithIds.put(R.id.tv_total, 19);
        sViewsWithIds.put(R.id.gl_v_5, 20);
        sViewsWithIds.put(R.id.group_coupon, 21);
        sViewsWithIds.put(R.id.cvAddress, 22);
        sViewsWithIds.put(R.id.tv_deliveryDetails, 23);
        sViewsWithIds.put(R.id.tv_address, 24);
        sViewsWithIds.put(R.id.img_rightArrow, 25);
        sViewsWithIds.put(R.id.tv_terms, 26);
        sViewsWithIds.put(R.id.tv_agreement, 27);
        sViewsWithIds.put(R.id.bottom_cv, 28);
        sViewsWithIds.put(R.id.btn_const, 29);
        sViewsWithIds.put(R.id.btn_placeOrder, 30);
        sViewsWithIds.put(R.id.tv_counter, 31);
    }
    // views
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ActivityCartBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 32, sIncludes, sViewsWithIds));
    }
    private ActivityCartBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 1
            , (androidx.cardview.widget.CardView) bindings[28]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[29]
            , (android.widget.Button) bindings[30]
            , (androidx.cardview.widget.CardView) bindings[11]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[12]
            , (androidx.cardview.widget.CardView) bindings[22]
            , (androidx.constraintlayout.widget.Guideline) bindings[20]
            , (androidx.constraintlayout.widget.Group) bindings[21]
            , (android.widget.ImageView) bindings[25]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[9]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[0]
            , (androidx.core.widget.NestedScrollView) bindings[8]
            , (androidx.recyclerview.widget.RecyclerView) bindings[10]
            , (android.widget.TextView) bindings[5]
            , (android.view.View) bindings[7]
            , (android.widget.TextView) bindings[24]
            , (android.widget.TextView) bindings[27]
            , (android.widget.TextView) bindings[31]
            , (android.widget.TextView) bindings[23]
            , (android.widget.TextView) bindings[14]
            , (android.widget.TextView) bindings[2]
            , (android.widget.TextView) bindings[1]
            , (android.widget.TextView) bindings[16]
            , (android.widget.TextView) bindings[6]
            , (android.widget.TextView) bindings[17]
            , (android.widget.TextView) bindings[15]
            , (android.widget.TextView) bindings[13]
            , (android.widget.TextView) bindings[3]
            , (android.widget.TextView) bindings[26]
            , (android.widget.TextView) bindings[19]
            , (android.widget.TextView) bindings[4]
            , (android.view.View) bindings[18]
            );
        this.mainConstraintTop.setTag(null);
        this.textView2.setTag(null);
        this.tvDeliveryFeePrice.setTag(null);
        this.tvPriceSubtotal.setTag(null);
        this.tvPromoPrice.setTag(null);
        this.tvTaxPrice.setTag(null);
        this.tvTotalPrice.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x80L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.model == variableId) {
            setModel((com.senarios.customer.models.EcommerceOrderModel) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setModel(@Nullable com.senarios.customer.models.EcommerceOrderModel Model) {
        updateRegistration(0, Model);
        this.mModel = Model;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.model);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
            case 0 :
                return onChangeModel((com.senarios.customer.models.EcommerceOrderModel) object, fieldId);
        }
        return false;
    }
    private boolean onChangeModel(com.senarios.customer.models.EcommerceOrderModel Model, int fieldId) {
        if (fieldId == BR._all) {
            synchronized(this) {
                    mDirtyFlags |= 0x1L;
            }
            return true;
        }
        else if (fieldId == BR.subTotal) {
            synchronized(this) {
                    mDirtyFlags |= 0x2L;
            }
            return true;
        }
        else if (fieldId == BR.deliveryFee) {
            synchronized(this) {
                    mDirtyFlags |= 0x4L;
            }
            return true;
        }
        else if (fieldId == BR.taxAmount) {
            synchronized(this) {
                    mDirtyFlags |= 0x8L;
            }
            return true;
        }
        else if (fieldId == BR.totalAmount) {
            synchronized(this) {
                    mDirtyFlags |= 0x10L;
            }
            return true;
        }
        else if (fieldId == BR.promoTitle) {
            synchronized(this) {
                    mDirtyFlags |= 0x20L;
            }
            return true;
        }
        else if (fieldId == BR.promoAmount) {
            synchronized(this) {
                    mDirtyFlags |= 0x40L;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        com.senarios.customer.models.EcommerceOrderModel model = mModel;
        java.lang.String javaLangStringRsMathRoundModelPromoAmountJavaLangString0 = null;
        double modelSubTotal = 0.0;
        java.lang.String javaLangStringPromoCodeJavaLangStringModelPromoTitleJavaLangString = null;
        long mathRoundModelTotalAmount = 0;
        long mathRoundModelTaxAmount = 0;
        java.lang.String javaLangStringRsMathRoundModelTotalAmount = null;
        double modelPromoAmount = 0.0;
        long mathRoundModelSubTotal = 0;
        long mathRoundModelPromoAmount = 0;
        double modelTaxAmount = 0.0;
        java.lang.String javaLangStringRsMathRoundModelSubTotalJavaLangString0 = null;
        java.lang.String javaLangStringPromoCodeJavaLangStringModelPromoTitle = null;
        java.lang.String javaLangStringRsMathRoundModelPromoAmount = null;
        java.lang.String javaLangStringRsMathRoundModelTotalAmountJavaLangString0 = null;
        java.lang.String modelPromoTitle = null;
        java.lang.String javaLangStringRsMathRoundModelTaxAmountJavaLangString0 = null;
        java.lang.String javaLangStringRsMathRoundModelDeliveryFeeJavaLangString0 = null;
        java.lang.String javaLangStringRsMathRoundModelTaxAmount = null;
        java.lang.String javaLangStringRsMathRoundModelDeliveryFee = null;
        long mathRoundModelDeliveryFee = 0;
        java.lang.String javaLangStringRsMathRoundModelSubTotal = null;
        double modelDeliveryFee = 0.0;
        double modelTotalAmount = 0.0;

        if ((dirtyFlags & 0xffL) != 0) {


            if ((dirtyFlags & 0x83L) != 0) {

                    if (model != null) {
                        // read model.subTotal
                        modelSubTotal = model.getSubTotal();
                    }


                    // read Math.round(model.subTotal)
                    mathRoundModelSubTotal = java.lang.Math.round(modelSubTotal);


                    // read ("Rs. ") + (Math.round(model.subTotal))
                    javaLangStringRsMathRoundModelSubTotal = ("Rs. ") + (mathRoundModelSubTotal);


                    // read (("Rs. ") + (Math.round(model.subTotal))) + (".0")
                    javaLangStringRsMathRoundModelSubTotalJavaLangString0 = (javaLangStringRsMathRoundModelSubTotal) + (".0");
            }
            if ((dirtyFlags & 0xc1L) != 0) {

                    if (model != null) {
                        // read model.promoAmount
                        modelPromoAmount = model.getPromoAmount();
                    }


                    // read Math.round(model.promoAmount)
                    mathRoundModelPromoAmount = java.lang.Math.round(modelPromoAmount);


                    // read ("Rs. ") + (Math.round(model.promoAmount))
                    javaLangStringRsMathRoundModelPromoAmount = ("Rs. ") + (mathRoundModelPromoAmount);


                    // read (("Rs. ") + (Math.round(model.promoAmount))) + (".0")
                    javaLangStringRsMathRoundModelPromoAmountJavaLangString0 = (javaLangStringRsMathRoundModelPromoAmount) + (".0");
            }
            if ((dirtyFlags & 0x89L) != 0) {

                    if (model != null) {
                        // read model.taxAmount
                        modelTaxAmount = model.getTaxAmount();
                    }


                    // read Math.round(model.taxAmount)
                    mathRoundModelTaxAmount = java.lang.Math.round(modelTaxAmount);


                    // read ("Rs. ") + (Math.round(model.taxAmount))
                    javaLangStringRsMathRoundModelTaxAmount = ("Rs. ") + (mathRoundModelTaxAmount);


                    // read (("Rs. ") + (Math.round(model.taxAmount))) + (".0")
                    javaLangStringRsMathRoundModelTaxAmountJavaLangString0 = (javaLangStringRsMathRoundModelTaxAmount) + (".0");
            }
            if ((dirtyFlags & 0xa1L) != 0) {

                    if (model != null) {
                        // read model.promoTitle
                        modelPromoTitle = model.getPromoTitle();
                    }


                    // read (("Promo Code ") + ("( ")) + (model.promoTitle)
                    javaLangStringPromoCodeJavaLangStringModelPromoTitle = (("Promo Code ") + ("( ")) + (modelPromoTitle);


                    // read ((("Promo Code ") + ("( ")) + (model.promoTitle)) + (" )")
                    javaLangStringPromoCodeJavaLangStringModelPromoTitleJavaLangString = (javaLangStringPromoCodeJavaLangStringModelPromoTitle) + (" )");
            }
            if ((dirtyFlags & 0x85L) != 0) {

                    if (model != null) {
                        // read model.deliveryFee
                        modelDeliveryFee = model.getDeliveryFee();
                    }


                    // read Math.round(model.deliveryFee)
                    mathRoundModelDeliveryFee = java.lang.Math.round(modelDeliveryFee);


                    // read ("Rs. ") + (Math.round(model.deliveryFee))
                    javaLangStringRsMathRoundModelDeliveryFee = ("Rs. ") + (mathRoundModelDeliveryFee);


                    // read (("Rs. ") + (Math.round(model.deliveryFee))) + (".0")
                    javaLangStringRsMathRoundModelDeliveryFeeJavaLangString0 = (javaLangStringRsMathRoundModelDeliveryFee) + (".0");
            }
            if ((dirtyFlags & 0x91L) != 0) {

                    if (model != null) {
                        // read model.totalAmount
                        modelTotalAmount = model.getTotalAmount();
                    }


                    // read Math.round(model.totalAmount)
                    mathRoundModelTotalAmount = java.lang.Math.round(modelTotalAmount);


                    // read ("Rs. ") + (Math.round(model.totalAmount))
                    javaLangStringRsMathRoundModelTotalAmount = ("Rs. ") + (mathRoundModelTotalAmount);


                    // read (("Rs. ") + (Math.round(model.totalAmount))) + (".0")
                    javaLangStringRsMathRoundModelTotalAmountJavaLangString0 = (javaLangStringRsMathRoundModelTotalAmount) + (".0");
            }
        }
        // batch finished
        if ((dirtyFlags & 0xa1L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.textView2, javaLangStringPromoCodeJavaLangStringModelPromoTitleJavaLangString);
        }
        if ((dirtyFlags & 0x85L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvDeliveryFeePrice, javaLangStringRsMathRoundModelDeliveryFeeJavaLangString0);
        }
        if ((dirtyFlags & 0x83L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvPriceSubtotal, javaLangStringRsMathRoundModelSubTotalJavaLangString0);
        }
        if ((dirtyFlags & 0xc1L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvPromoPrice, javaLangStringRsMathRoundModelPromoAmountJavaLangString0);
        }
        if ((dirtyFlags & 0x89L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvTaxPrice, javaLangStringRsMathRoundModelTaxAmountJavaLangString0);
        }
        if ((dirtyFlags & 0x91L) != 0) {
            // api target 1

            androidx.databinding.adapters.TextViewBindingAdapter.setText(this.tvTotalPrice, javaLangStringRsMathRoundModelTotalAmountJavaLangString0);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): model
        flag 1 (0x2L): model.subTotal
        flag 2 (0x3L): model.deliveryFee
        flag 3 (0x4L): model.taxAmount
        flag 4 (0x5L): model.totalAmount
        flag 5 (0x6L): model.promoTitle
        flag 6 (0x7L): model.promoAmount
        flag 7 (0x8L): null
    flag mapping end*/
    //end
}