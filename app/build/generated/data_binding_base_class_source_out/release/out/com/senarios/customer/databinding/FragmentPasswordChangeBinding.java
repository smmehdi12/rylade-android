// Generated by data binding compiler. Do not edit!
package com.senarios.customer.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.Guideline;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.senarios.customer.R;
import java.lang.Deprecated;
import java.lang.Object;

public abstract class FragmentPasswordChangeBinding extends ViewDataBinding {
  @NonNull
  public final ImageView backImageView;

  @NonNull
  public final Button buttonChangePassword;

  @NonNull
  public final TextInputEditText confirmPasswordEdittext;

  @NonNull
  public final TextInputLayout confirmPasswordEdittext1;

  @NonNull
  public final ConstraintLayout constraintLayout;

  @NonNull
  public final ImageView dividerHorizontalImageView2;

  @NonNull
  public final Guideline guideline3;

  @NonNull
  public final Guideline guideline4;

  @NonNull
  public final Guideline guideline5;

  @NonNull
  public final Guideline guideline6;

  @NonNull
  public final TextInputEditText newPasswordEditText;

  @NonNull
  public final TextInputLayout newPasswordEditText1;

  @NonNull
  public final TextView signupTextView;

  @NonNull
  public final View space;

  @NonNull
  public final TextView textView;

  protected FragmentPasswordChangeBinding(Object _bindingComponent, View _root,
      int _localFieldCount, ImageView backImageView, Button buttonChangePassword,
      TextInputEditText confirmPasswordEdittext, TextInputLayout confirmPasswordEdittext1,
      ConstraintLayout constraintLayout, ImageView dividerHorizontalImageView2,
      Guideline guideline3, Guideline guideline4, Guideline guideline5, Guideline guideline6,
      TextInputEditText newPasswordEditText, TextInputLayout newPasswordEditText1,
      TextView signupTextView, View space, TextView textView) {
    super(_bindingComponent, _root, _localFieldCount);
    this.backImageView = backImageView;
    this.buttonChangePassword = buttonChangePassword;
    this.confirmPasswordEdittext = confirmPasswordEdittext;
    this.confirmPasswordEdittext1 = confirmPasswordEdittext1;
    this.constraintLayout = constraintLayout;
    this.dividerHorizontalImageView2 = dividerHorizontalImageView2;
    this.guideline3 = guideline3;
    this.guideline4 = guideline4;
    this.guideline5 = guideline5;
    this.guideline6 = guideline6;
    this.newPasswordEditText = newPasswordEditText;
    this.newPasswordEditText1 = newPasswordEditText1;
    this.signupTextView = signupTextView;
    this.space = space;
    this.textView = textView;
  }

  @NonNull
  public static FragmentPasswordChangeBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.fragment_password_change, root, attachToRoot, component)
   */
  @NonNull
  @Deprecated
  public static FragmentPasswordChangeBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable Object component) {
    return ViewDataBinding.<FragmentPasswordChangeBinding>inflateInternal(inflater, R.layout.fragment_password_change, root, attachToRoot, component);
  }

  @NonNull
  public static FragmentPasswordChangeBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.fragment_password_change, null, false, component)
   */
  @NonNull
  @Deprecated
  public static FragmentPasswordChangeBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable Object component) {
    return ViewDataBinding.<FragmentPasswordChangeBinding>inflateInternal(inflater, R.layout.fragment_password_change, null, false, component);
  }

  public static FragmentPasswordChangeBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.bind(view, component)
   */
  @Deprecated
  public static FragmentPasswordChangeBinding bind(@NonNull View view, @Nullable Object component) {
    return (FragmentPasswordChangeBinding)bind(component, view, R.layout.fragment_password_change);
  }
}
