// Generated by data binding compiler. Do not edit!
package com.senarios.customer.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.Group;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.viewpager.widget.ViewPager;
import com.ogaclejapan.smarttablayout.SmartTabLayout;
import com.senarios.customer.R;
import java.lang.Deprecated;
import java.lang.Object;

public abstract class ActivityEcommerceBinding extends ViewDataBinding {
  @NonNull
  public final FrameLayout container;

  @NonNull
  public final Group group;

  @NonNull
  public final ConstraintLayout root;

  @NonNull
  public final View toolbar;

  @NonNull
  public final ViewPager viewpager;

  @NonNull
  public final SmartTabLayout viewpagertab;

  protected ActivityEcommerceBinding(Object _bindingComponent, View _root, int _localFieldCount,
      FrameLayout container, Group group, ConstraintLayout root, View toolbar, ViewPager viewpager,
      SmartTabLayout viewpagertab) {
    super(_bindingComponent, _root, _localFieldCount);
    this.container = container;
    this.group = group;
    this.root = root;
    this.toolbar = toolbar;
    this.viewpager = viewpager;
    this.viewpagertab = viewpagertab;
  }

  @NonNull
  public static ActivityEcommerceBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.activity_ecommerce, root, attachToRoot, component)
   */
  @NonNull
  @Deprecated
  public static ActivityEcommerceBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable Object component) {
    return ViewDataBinding.<ActivityEcommerceBinding>inflateInternal(inflater, R.layout.activity_ecommerce, root, attachToRoot, component);
  }

  @NonNull
  public static ActivityEcommerceBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.inflate(inflater, R.layout.activity_ecommerce, null, false, component)
   */
  @NonNull
  @Deprecated
  public static ActivityEcommerceBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable Object component) {
    return ViewDataBinding.<ActivityEcommerceBinding>inflateInternal(inflater, R.layout.activity_ecommerce, null, false, component);
  }

  public static ActivityEcommerceBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  /**
   * This method receives DataBindingComponent instance as type Object instead of
   * type DataBindingComponent to avoid causing too many compilation errors if
   * compilation fails for another reason.
   * https://issuetracker.google.com/issues/116541301
   * @Deprecated Use DataBindingUtil.bind(view, component)
   */
  @Deprecated
  public static ActivityEcommerceBinding bind(@NonNull View view, @Nullable Object component) {
    return (ActivityEcommerceBinding)bind(component, view, R.layout.activity_ecommerce);
  }
}
